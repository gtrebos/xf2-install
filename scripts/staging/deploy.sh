#!/usr/bin/env bash

CURRENT_DIR=`dirname "$0"`
source $CURRENT_DIR/../config/global.conf

echo ""
echo "*******************************"
echo "=> Installing Composer"
echo "*******************************"
yarn add getcomposer

echo ""
echo "*******************************"
echo "=> Running Composer Install"
echo "*******************************"
npx composer install

echo ""
echo "*******************************"
echo "=> Preparing ${SITE} add-ons..."
echo "*******************************"
bash ./scripts/prepare_addons.sh ${SITE}

echo ""
echo "*******************************"
echo "=> Get ${SITE} deployment file"
echo "*******************************"
cp ./scripts/config/${SITE}/.mage.yml ./

echo ""
echo "*******************************"
echo "=> Creating logs directory"
echo "*******************************"
mkdir -p logs

echo ""
echo "*******************************"
echo "=> Deploying code to server"
echo "*******************************"
bin/mage deploy staging
