#!/usr/bin/env bash

CURRENT_DIR=`dirname "$0"`
source $CURRENT_DIR/config/global.conf

echo ""
echo "******************************************************************"
echo "=> Moving ${SITE} Addon Files"
echo "******************************************************************"
# looping on the site config add-ons per sites (the order is important)
cat ./scripts/config/${SITE}/addons.conf | while read addon; do
    echo "- ${addon}"
    mkdir -p ./src/addons/${addon}
    rsync -ar ./addons_available/addons/${addon}/ ./src/addons/${addon}
    if [[ -d ./addons_available/addons/${addon}/rootfiles ]];then
        rsync -ar ./addons_available/addons/${addon}/rootfiles/ ./
    fi
    if [[ -d ./addons_available/addons/${addon}/styles ]];then
        rsync -ar ./addons_available/addons/${addon}/styles/ ./styles
    fi
done

echo ""
echo "******************************************************************"
echo "=> Moving additionnal Addon files (as these addons are probably"
echo "=> already installed and won't reinstalled them)"
echo "******************************************************************"
rsync -azr ./addons_available/styles/ ./styles
rsync -azr ./addons_available/js/ ./js
if [[ -d ./src/addons/Purch/Shieldsquare/images ]]; then rsync -azr ./src/addons/Purch/Shieldsquare/images/* ./images/; fi;
if [[ -d ./src/addons/Purch/Shieldsquare/recaptcha_css ]]; then rsync -azr ./src/addons/Purch/Shieldsquare/recaptcha_css ./; fi;
if [[ -d ./src/addons/Purch/Shieldsquare/phpFiles ]]; then rsync -azr ./src/addons/Purch/Shieldsquare/phpFiles/* ./; fi;
if [[ -d ./src/addons/Purch/Anandtechsmilies/images ]]; then rsync -azr ./src/addons/Purch/Anandtechsmilies/images/* ./styles/default/xenforo/smilies/; fi;
if [[ -d ./src/addons/Purch/Onesignal/rootfiles ]]; then rsync -azr ./src/addons/Purch/Onesignal/rootfiles/* ./; fi;
if [[ -d ./src/addons/Purch/Tomsguide/styles ]]; then rsync -azr ./src/addons/Purch/Tomsguide/styles/ ./styles/; fi;
if [[ -d ./src/addons/Purch/Tomsguide/js ]]; then rsync -azr ./src/addons/Purch/Tomsguide/js/ ./js/; fi;
if [[ -d ./src/addons/Purch/Tomshardware/styles ]]; then rsync -azr ./src/addons/Purch/Tomshardware/styles/ ./styles/; fi;
if [[ -d ./src/addons/Purch/Tomshardware/js ]]; then rsync -azr ./src/addons/Purch/Tomshardware/js/ ./js/; fi;
if [[ -d ./src/addons/Future/Sso/sso ]]; then rsync -azr ./src/addons/Future/Sso/sso ./; fi;