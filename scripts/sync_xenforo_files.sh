#!/usr/bin/env bash

echo ""
echo "******************************************************************"
echo "=> Removing some files from tmp_xenforo_sources that we don't need"
echo "******************************************************************"
rm -rf tmp_xenforo_sources/README.md tmp_xenforo_sources/.git tmp_xenforo_sources/xenforo_*.zip tmp_xenforo_sources/composer.json

echo ""
echo "******************************************************************"
echo "=> Syncing files from tmp_xenforo_sources to root directory"
echo "******************************************************************"
rsync -azr tmp_xenforo_sources/* .

echo ""
echo "******************************************************************"
echo "=> Removing tmp_xenforo_sources files"
echo "******************************************************************"
rm -rf tmp_xenforo_sources