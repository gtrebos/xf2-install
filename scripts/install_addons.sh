#!/usr/bin/env bash

ENV_FILE=/home/deployuser/forum_app.env

if [ -f $ENV_FILE  ]; then
  source $ENV_FILE
fi

CURRENT_DIR=`dirname "$0"`
source $CURRENT_DIR/config/global.conf

update_addon () {
    #remove src/addons to get the name of the addon based on directory name
    emptys=""
    d_name=${1/.\/src\/addons\//$emptys}
    exceptions=("ThemeHouse/Core")

    if [ `echo ${exceptions[*]} | grep -wo $d_name | wc -w` -eq 0 ] && [ -z `echo $d_name | grep -e "XFES/.*" -e "CleanTalk/.*"` ]; then
        echo "=> Updating Addon: $d_name"
        php cmd.php xf:addon-install -n $d_name
        php cmd.php xf:addon-upgrade -n $d_name
    fi
}

# looping on the site config add-ons per sites (the order is important)
cat ./scripts/config/${SITE}/addons.conf | while read addon; do
    if [ -d "./src/addons/${addon}" ]; then
        update_addon ${addon}
    fi
done

exit 0;