#!/usr/bin/env bash

echo "=> Stopping/Removing Forum containers already running"
docker-compose down -v --remove-orphans

echo "=> Stopping/Removing Forum containers already running"
docker-compose -f docker-compose.local.with.stg.data.yml down -v --remove-orphans

echo "=> Building/Running Forum containers"
docker-compose -f docker-compose.local.with.stg.data.yml up -d

echo "=> Recompiling XenForo Staging Data"
docker-compose -f docker-compose.local.with.stg.data.yml run php php cmd.php xf-dev:recompile -n

docker-compose -f docker-compose.local.with.stg.data.yml run php ./scripts/local/save_stg_data.sh
