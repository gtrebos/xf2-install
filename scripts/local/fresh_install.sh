#!/usr/bin/env bash

CURRENT_DIR=`dirname "$0"`
source $CURRENT_DIR/../config/global.conf

echo "=> Stopping/Removing Forum containers already running"
docker-compose down -v --remove-orphans
docker-compose -f docker-compose.local.with.stg.data.yml down -v --remove-orphans

echo "=> Removing local mysqldata directory"
sudo rm -rf mysqldata

echo "=> Recreating local mysqldata directory"
mkdir -p mysqldata && chmod 777 mysqldata

echo "=> Building/Running AnandTech Forum containers"
docker-compose up -d --build

echo "=> Composer Install on PHP-FPM container"
docker-compose run php composer install

echo "=> Removing XenForo data and internal data"
docker-compose run php rm -rf data internal_data

echo "=> Installing XenForo"
docker-compose run php php cmd.php xf:install

echo "=> Prepare XenForo Addons"
docker-compose run php ./scripts/prepare_addons.sh ${SITE}

echo "=> Install XenForo Addons"
docker-compose run php ./scripts/install_addons.sh ${SITE}

echo "=> Copying internal_data to internal_data_local"
sudo chown $(id -u):$(id -g) . && sudo chmod -R 777 internal_data
cp -R internal_data internal_data_local
