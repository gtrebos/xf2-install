#!/usr/bin/env bash

echo "=> Stopping/Removing Forum containers already running"
docker-compose down -v --remove-orphans

echo "=> Stopping/Removing Forum containers already running"
docker-compose -f docker-compose.local.with.stg.data.yml down -v --remove-orphans

echo "=> Building/Running Forum containers"
docker-compose up -d

echo "=> Recompiling XenForo Local Data"
docker-compose run php php cmd.php xf-dev:recompile -n

docker-compose run php ./scripts/local/save_local_data.sh
