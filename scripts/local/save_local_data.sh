#!/usr/bin/env bash

echo "=> Copying internal_data to internal_data_local"
rm -rf internal_data_local
cp -R internal_data internal_data_local
