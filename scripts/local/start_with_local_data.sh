#!/usr/bin/env bash

echo "=> Stopping/Removing Forum containers already running"
docker-compose down -v --remove-orphans

echo "=> Stopping/Removing Forum containers already running"
docker-compose -f docker-compose.local.with.qa.data.yml down -v --remove-orphans

echo "=> Building/Running Forum containers"
docker-compose up -d

if [ -d internal_data_local ]; then
    echo "=> Loading Local Data"
    docker-compose run php rm -rf internal_data
    docker-compose run php cp -R internal_data_local internal_data
else
    echo "=> Recompiling XenForo Local Data"
    docker-compose run php php cmd.php xf-dev:recompile -n

    docker-compose run php ./scripts/local/save_local_data.sh
fi
