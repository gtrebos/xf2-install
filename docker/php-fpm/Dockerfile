FROM php:7.0-fpm

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y curl
RUN apt-get install -y g++
RUN apt-get install -y libicu-dev
RUN apt-get install -y libpng-dev
RUN apt-get install -y libjpeg-dev
RUN apt-get install -y libfreetype6-dev
RUN apt-get install -y python-setuptools
RUN apt-get install -y wget
RUN apt-get install -y zip
RUN apt-get install -y tar
RUN apt-get install -y gzip
RUN apt-get install -y vim

# Install PHP Extensions
RUN docker-php-ext-install iconv
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install zip
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install gd

# Set PHP Memory Limit and Timezone
RUN echo "memory_limit=-1" > /usr/local/etc/php/conf.d/memory_limit.ini
RUN echo -n "date.timezone=UTC" >> /usr/local/etc/php/conf.d/time_zone.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Start the server
WORKDIR /var/www/html/forum

ADD start.sh /start.sh
RUN chmod +x /start.sh

CMD /start.sh
