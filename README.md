# Xenforo Generic Install #

Generic Xenforo forum installation. 
Sites available:
* Tomsguide
* Tomshardware

### Install a fresh Xenforo environment in local ###

```
./scripts/local/fresh_install.sh <site>
```
### Deploy on remote env ###

```
./scripts/<env>/deploy.sh <site>
```
### Local docker running Local or Staging data ###
```
* Start with local data: ./scripts/local/start_with_local_data.sh
* Start with staging data: ./scripts/local/start_with_stg_data.sh
```

### Adding a site in the configuration ###
You can add a forum configuration by:
* updating ./scripts/config/global.conf to add [new-site] name
* adding a repository on the same model than Tomsguide in ./scripts/config/[new-site]


### Add-ons list available ###
This repository also contains the list of available add-ons that can be configured on a site.

```
* ThemeHouse/UIX
* ThemeHouse/Bookmarks
* ThemeHouse/Core
* ThemeHouse/Covers
* ThemeHouse/Nodes
* ThemeHouse/Reactions
* ThemeHouse/ThreadNav
* ThemeHouse/Trending
* ThemeHouse/WatchForums
* ThemeHouse/QAForums
* ThemeHouse/Thumbnails
* ThemeHouse/UserImprovements
* Andy/MemberNotes
* Andy/SimilarThreads
* CleanTalk
* Future/QuickThread
* KL/UserImprovements
* xenMade/SEO
* Purch/Anandtechsmilies
* Purch/Comscore
* Purch/Googleanalytics
* Purch/Onesignal
* Purch/Optimizedpager
* Purch/Purchadsmanagement
* Purch/Purchmetatags
* Purch/Shieldsquare
* Purch/Tomsguide
```

Example for Tomsguide, add an add-on in this file:
```
./scripts/config/Tomsguide/addons.conf
```