var themehouse = themehouse || {};

themehouse.watchforums = themehouse.watchforums || {};

!function ($, window, document, _undefined) {
    "use strict";

    XF.SwitchOverlayClick = XF.extend(XF.SwitchOverlayClick, {

        __backup: {
            'handleOverlaySubmit': '__handleOverlaySubmit_thwatchforums'
        },

        handleOverlaySubmit: function (e, data) {
            this.__handleOverlaySubmit_thwatchforums(e, data);

            if (data.status == 'ok') {
                themehouse.watchforums.handleSwitchResponse(this.$target, data);
            }
        },
    });

    themehouse.watchforums.handleSwitchResponse = function ($target, data) {
        if (data.remove) {
            $target.remove();
        }
    };
}(jQuery, window, document);
