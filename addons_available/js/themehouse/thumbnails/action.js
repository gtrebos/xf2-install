var themehouse = themehouse || {};

/** @param {jQuery} $ jQuery Object */
!function($, window, document, _undefined)
{
	"use strict";

	themehouse.ThumbnailClick = XF.Click.newHandler({
		eventNameSpace: 'ThemeHouseThumbnailClick',
		options: {
			thumbnailsUrl: ''
		},

		$form: null,
		href: null,
		loading: false,

		init: function()
		{
			var $form = this.$target.closest('form'),
				href = $form.data('thumbnails-url') || this.options.thumbnailsUrl || this.$target.attr('href');

			if (!href)
			{
				console.error('Thumbnails button must have a href');
				return;
			}

			this.href = href;
			this.$form = $form;
		},

		click: function(e)
		{
			if (!this.href)
			{
				return;
			}

			e.preventDefault();

			if (this.loading)
			{
				return;
			}

			var draftHandler = XF.Element.getHandler(this.$form, 'draft');
			if (draftHandler)
			{
				draftHandler.triggerSave();
			}

			var self = this,
				formData = XF.getDefaultFormData(this.$form);

			XF.ajax('post', this.href, formData, XF.proxy(this, 'onLoad'))
				.always(function() { self.loading = false; });
		},

		setupOverlay: function(overlay)
		{
			this.overlay = overlay;
			var self = this;

			overlay.on('overlay:hidden', function() {
				overlay.destroy();
				self.overlay = null;
			});

			var $form = overlay.getOverlay().find('form');

			$form.on('ajax-submit:response', XF.proxy(this, 'handleOverlaySubmit'));

			return overlay;
		},

		onLoad: function(data)
		{
			if (!data.html) {
				return;
			}

			var self = this;

			XF.setupHtmlInsert(data.html, function($html, container) {
				var overlay = new XF.Overlay(XF.getOverlayHtml({
					html: $html,
					title: container.title || container.h1
				}));

				var f = XF.proxy(self, 'setupOverlay');
				overlay = f(overlay);

				overlay.show();
			});
		},

		handleOverlaySubmit: function(e, data)
		{
			if (data.status == 'ok') {
				e.preventDefault();

				var overlay = this.overlay;
				if (overlay) {
					overlay.hide();
				}
			}
		}
    });

	XF.Click.register('thumbnail-click', 'themehouse.ThumbnailClick');
}
(jQuery, window, document);