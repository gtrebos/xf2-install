var future = future || {};

future.quickthread = future.quickthread || {};

!function($, window, document, _undefined)
{
	"use strict";

	XF.AjaxSubmit = XF.extend(XF.AjaxSubmit, {

		__backup: {
			'submit': '__submit'
		},

		submit: function(e)
		{
			var $form = this.$target;

			var draftHandler = XF.Element.getHandler($form, 'draft');
			if (draftHandler) {
				draftHandler.triggerSave();
			}

			this.__submit(e);
		},
	});
}
(jQuery, window, document);