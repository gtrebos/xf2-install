var Future = Future || {};

!function($, window, document, _undefined)
{
	"use strict";

	Future.QuickThreadWidget = XF.Element.newHandler({
		init: function()
		{
            var $form = this.$target;
            
			if (!$form.is('form')) {
				console.error('%o is not a form', $form[0]);
				return;
			}

			$form.on({
				submit: XF.proxy(this, 'submit'),
            });
            
            var $forumSelector = $form.find('select[name=node_id]');
            if ($forumSelector.length) {
                $forumSelector.on({
                    change: XF.proxy(this, 'changeForum'),
                });
            }
        },

        submit: function(e)
		{
            e.preventDefault();

            var $form = this.$target;

            var draftHandler = XF.Element.getHandler($form, 'draft');

			if (draftHandler) {
                draftHandler.$target.on('draft:complete', function() {
                    XF.redirect($form.attr('action'));
                });

                draftHandler.lastActionContent = null;
				draftHandler.triggerSave();
            } else {
                XF.redirect($form.attr('action'));
            }
        },

        changeForum: function(e)
		{
			var $form = this.$target;

            var $forumSelector = $(e.target);

            var selectedDraftUrl = $forumSelector.find(":selected").data('draft-url');
            var selectedActionUrl = $forumSelector.find(":selected").data('action-url');

            var draftHandler = XF.Element.getHandler($form, 'draft');
			if (draftHandler) {
                draftHandler.triggerDelete();
                draftHandler.lastActionContent = null;
                draftHandler.options.draftUrl = selectedDraftUrl;
            }
            $form.attr('data-draft-url', selectedDraftUrl);

            $form.attr('action', selectedActionUrl);
        },
    });

    XF.Element.register('future-quick-thread-widget', 'Future.QuickThreadWidget');
}
(jQuery, window, document);