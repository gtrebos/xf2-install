<!doctype html>
<html>
    <head>
        <title>Single Sign-On Ajax demo</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        
        <style>
            .state {
                display: none;
            }
            body.anonymous .state.anonymous,
            body.authenticated .state.authenticated {
                display: initial;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Single Sign-On Ajax demo</h1>

            <div id="error" class="alert alert-danger" style="display: none;"></div>


            <div class="state anonymous">
                <a href="http://local.forum.tomsguide.com/index.php?login/&_xfRedirect=<?php echo urlencode($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>">
                    <button type="submit" class="btn btn-default">Login</button>
                </a>
            </div>


            <div class="state authenticated">
                <h3>Logged in</h3>
                <dl id="user-info" class="dl-horizontal"></dl>
                
                <a id="logout" class="btn btn-default">Logout</a>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="app.js"></script>
    </body>
</html>

