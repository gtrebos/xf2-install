<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../src/XF.php';

use Future\Sso\src\SSOServer;

//Start application
XF::start(__DIR__);
$app = \XF::setupApp('XF\Pub\App');
$app->start();

//Server part
$ssoServer = new SSOServer(['brokers' => SSOServer::parseBrokersConfig($app->options()->future_sso_sites_and_secrets)]);
$command = isset($_REQUEST['command']) ? $_REQUEST['command'] : null;

if (!$command || !method_exists($ssoServer, $command)) {
    header("HTTP/1.1 404 Not Found");
    header('Content-type: application/json; charset=UTF-8');
    
    echo json_encode(['error' => 'Unknown command']);
    exit();
}

$result = $ssoServer->$command();

