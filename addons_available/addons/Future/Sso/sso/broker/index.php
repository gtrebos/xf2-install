<?php

require_once __DIR__ . '/Future/Sso/src/Exception.php';
require_once __DIR__ . '/Future/Sso/src/NotAttachedException.php';
require_once __DIR__ . '/Future/Sso/src/Broker.php';

use Future\Sso\src\NotAttachedException;
use Future\Sso\src\Broker;

if (isset($_GET['sso_error'])) {
    header("Location: error.php?sso_error=" . $_GET['sso_error'], true, 307);
    exit;
}

/*
 * SSO_SERVER = 'http://local.forum.tomsguide.com/sso/server/';
 * SSO_BROCKER_ID = 'tgu';
 * SSO_BROCKER_SECRET = '8iwzik1bwd';
 */

$broker = new Broker(getenv('SSO_SERVER'), getenv('SSO_BROKER_ID'), getenv('SSO_BROKER_SECRET'));
$broker->attach(true);

try {
    $user = $broker->getUserInfo();
} catch (NotAttachedException $e) {
    header('Location: ' . $_SERVER['REQUEST_URI']);
    exit;
} catch (\Exception $e) {
    header("Location: error.php?sso_error=" . $e->getMessage(), true, 307);
}
?>
<!doctype html>
<html>
    <head>
        <title><?= $broker->broker ?> (Single Sign-On demo)</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1><?= $broker->broker ?> <small>(Single Sign-On demo)</small></h1>

            <?php if ($user) { ?>
                <h3>Logged in</h3>
                <pre><?= json_encode($user, JSON_PRETTY_PRINT); ?></pre>
                <a id="logout" class="btn btn-default" href="http://local.forum.tomsguide.com/index.php?logout/">Logout</a>
            <?php } else { ?>
                <button class="btn btn-default">
                    <a href="http://local.forum.tomsguide.com/index.php?login/&_xfRedirect=<?php echo urlencode($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>">
                        Login
                    </a>
                </button>
            <?php } ?>
        </div>
    </body>
</html>

