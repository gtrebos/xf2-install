<?php
namespace Future\Sso\src;

/**
 * Exception thrown when a request is done while no session is attached
 *
 */
class NotAttachedException extends Exception
{

}
