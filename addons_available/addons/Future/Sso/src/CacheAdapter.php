<?php

namespace Future\Sso\src;

use Desarrolla2\Cache\Adapter\Mysqli;

/**
 * Mysqli Overwritten adapter
 */
class CacheAdapter extends Mysqli
{
    protected $database = 'xf_future_sso_cache';
}