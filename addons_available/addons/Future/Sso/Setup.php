<?php

namespace Future\Sso;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Create;
use XF\Db\Schema\Drop;


class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $this->schemaManager()->createTable('xf_future_sso_cache', function (Create $table) {
            $table->addColumn('k', 'varchar', 255);
            $table->addColumn('v', 'text');
            $table->addColumn('t', 'int', 11);
            $table->addPrimaryKey('k');
        });

        $db = $this->db();
        $query = "CREATE EVENT `future_sso_cache_cleaner` ON SCHEDULE EVERY 1 HOUR DO DELETE FROM `xf_future_sso_cache` WHERE `t` < NOW()";
        $db->rawQuery($query);
    }

    public function installStep2()
    {
        exec('cp -R '.__DIR__.'/sso '.__DIR__.'/../../../../');
    }

    public function uninstallStep1()
    {
        $db = $this->db();
        $this->schemaManager()->dropTable('xf_future_sso_cache');
        $db->rawQuery("DROP EVENT `future_sso_cache_cleaner`");
    }

    public function uninstallStep2()
    {
        @exec('rm -rf '.__DIR__.'/../../../../sso');
    }
}