<?php


namespace Future\Sso\XF\Admin\Controller;

use Firebase\JWT\JWT;
use Future\Sso\src\Broker;

class Login extends XFCP_Login
{
    public function actionLogin()
    {
        $redirect = parent::actionLogin();

        if($this->options()->offsetExists('future_sso_enable') && $this->options()->future_sso_enable){

            if($this->session()->get('userId')){
                //Get url back from real redirection class
                $originalRedirect = method_exists($redirect, 'getUrl') ? $redirect->getUrl() : null;

                $redirect = $this->setSsoSession($originalRedirect);
            }
        }

        return $redirect;
    }


    protected function setSsoSession($redirect)
    {
        require_once (__DIR__ . '/../../../../../../../vendor/autoload.php');

        $broker = new Broker(
            rtrim($this->options()->boardUrl, '/').'/sso/server/',
            getenv('SSO_BROKER_ID'),
            getenv('SSO_BROKER_SECRET')
        );

        $redirect = $broker->getAttachUrl(
            [
                'return_url' => $redirect,
                'autoSignIn' => JWT::encode([
                    'userId' => $this->session()->get('userId'),
                ], getenv('SSO_BROKER_SECRET'))]);

        return $this->redirect($redirect, '');
    }
}