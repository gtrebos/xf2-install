<?php


namespace Future\Sso\XF\Pub\Controller;

use Firebase\JWT\JWT;
use Future\Sso\src\Broker;

class Login extends XFCP_Login
{
    public function actionLogin()
    {
        //Before login and having a new sessionId
        //get _xfRedirect if exists
        if ($this->options()->offsetExists('future_sso_enable') && $this->options()->future_sso_enable){
            if($this->session()->offsetGet('_xfRedirect')){
                $redirectFromSession = $this->session()->get('_xfRedirect');
                $this->session()->offsetUnset('_xfRedirect');
            }
        }

        //Original login action
        $redirect = parent::actionLogin();

        if($this->options()->offsetExists('future_sso_enable') && $this->options()->future_sso_enable){

            if($this->session()->get('userId')){
                //Get url back from real redirection class
                $originalRedirect = method_exists($redirect, 'getUrl') ? $redirect->getUrl() : null;
                $redirect = $this->setSsoSession($redirectFromSession ?? $originalRedirect);
            } else {
                if($xfRedirect = $this->getXfRedirect($this->request->getReferrer())){
                    //if exists, save _xfRedirect on failed authentication
                    $this->session()->set('_xfRedirect', $xfRedirect);
                }else{
                    $this->session()->set('_xfRedirect', $this->request->getReferrer());
                }
            }
        }

        return $redirect;
    }

    protected function setSsoSession($redirect)
    {
        require_once (__DIR__ . '/../../../../../../../vendor/autoload.php');

        if($this->options()->offsetExists('future_sso_authorized_domains') && $this->request->getReferrer()){
            if($xfRedirect = $this->getXfRedirect($this->request->getReferrer())){
                $redirect = $xfRedirect;
            }
        }

        if (!$redirect || ($redirect && $this->isLoginOrLogoutUrl($redirect))){
            $redirect = $this->getDynamicRedirectIfNot($this->buildLink('login'), $this->options()->future_sso_fallback_redirection ?? null);
        }

        $broker = new Broker(
            rtrim($this->options()->boardUrl, '/').'/sso/server/',
            getenv('SSO_BROKER_ID'),
            getenv('SSO_BROKER_SECRET')
        );

        $redirect = $broker->getAttachUrl(
            [
                'return_url' => $redirect,
                'autoSignIn' => JWT::encode([
                    'userId' => $this->session()->get('userId'),
                ], getenv('SSO_BROKER_SECRET'))]);

        return $this->redirect($redirect, '');
    }

    function parseMultiLineTextField($text)
    {
        $arr = array_filter(explode("\n", $text));
        $arr = array_map('trim', $arr);

        return $arr;
    }

    function getAuthorizedDomains()
    {
        return array_unique(
            array_merge(
                $this->parseMultiLineTextField($this->options()->future_sso_authorized_domains),
                [parse_url($this->options()->boardUrl, PHP_URL_HOST)]
            )
        );
    }

    function getXfRedirect($url){
        parse_str(parse_url($url, PHP_URL_QUERY), $parsedQuery);
        if(isset($parsedQuery['_xfRedirect']) && in_array(parse_url($parsedQuery['_xfRedirect'], PHP_URL_HOST), $this->getAuthorizedDomains())){
            return $parsedQuery['_xfRedirect'];
        }

        return null;
    }
    
    function isLoginOrLogoutUrl($url)
    {
        if (strpos($url, $this->buildLink('login')) !== false){
            return true;
        }

        if (strpos($url, $this->buildLink('logout')) !== false){
            return true;
        }

        return false;
    }
}