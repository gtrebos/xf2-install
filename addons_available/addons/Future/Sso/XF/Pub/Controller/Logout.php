<?php

namespace Future\Sso\XF\Pub\Controller;

use Future\Sso\src\Broker;
use Future\Sso\src\Exception;
use Future\Sso\src\NotAttachedException;

class Logout extends XFCP_Logout
{
    public function actionIndex()
    {
        // I don't care !
        //$this->assertValidCsrfToken($this->filter('t', 'str'));
        if($this->request->get('logoutComplete', false)){
            header("Content-Type: application/json");
            http_response_code(204);
            die();
        }

        /** @var \XF\ControllerPlugin\Login $loginPlugin */
        $loginPlugin = $this->plugin('XF:Login');
        $loginPlugin->logoutVisitor();

        if ($this->options()->offsetExists('future_sso_enable') &&
            $this->options()->future_sso_enable && !$this->request->get('json'))
        {
            try{
                $broker = new Broker(getenv('SSO_SERVER'), getenv('SSO_BROKER_ID'), getenv('SSO_BROKER_SECRET'));
                $broker->logout(true);
            }
            catch (NotAttachedException $e){}
            catch (Exception $e){}
        }

        if($this->request->get('json', false)){
            return $this->redirect($this->buildLink('logout', null, ['logoutComplete' => 1]));
        }

        $redirect = $this->request->getReferrer() ?? $this->buildLink('index');

        return $this->redirect($redirect);
    }

}