<?php

namespace Future\TopPosters\XF\Service\MemberStat;

use XF\Mvc\Entity\Finder;

class Preparer extends XFCP_Preparer
{
    public function applyCallback($class, $method, Finder $finder, $additionalParams = [])
    {
        $currentTime = \XF::$time;

        $cutOff = new \DateTime('@' . $currentTime);
        if ($this->memberStat->sort_order == 'message_count_day') {
            $cutOff->modify('midnight');
            $dayCutOff = intval($cutOff->format('U'));
            $finder->where('message_count_last_updated', '>=', $dayCutOff);
        }

        if ($this->memberStat->sort_order == 'message_count_week') {
            $cutOff->modify('monday this week');
            $weekCutOff = intval($cutOff->format('U'));
            $finder->where('message_count_last_updated', '>=', $weekCutOff);
        }
        
        if ($this->memberStat->sort_order == 'message_count_month') {
            $cutOff->modify('first day of this month');
            $monthCutOff = intval($cutOff->format('U'));
            $finder->where('message_count_last_updated', '>=', $monthCutOff);
        }
        
        return parent::applyCallback($class, $method, $finder, $additionalParams);
    }
    
    protected function prepareCacheResults($order, array $cacheResults)
    {
        switch ($order) {
            case 'message_count_day':
            case 'message_count_week':
            case 'message_count_month':
                return array_map(function ($value) {
                    return \XF::language()->numberFormat($value);
                }, $cacheResults);
        }

        return parent::prepareCacheResults($order, $cacheResults);
    }
}
