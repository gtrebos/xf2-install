<?php

namespace Future\TopPosters\XF\Service\Post;

class Mover extends XFCP_Mover
{
    protected function updateUserCounters()
    {
        parent::updateUserCounters();

        $currentTime = \XF::$time;

        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('midnight');
        $dayCutOff = intval($cutOff->format('U'));

        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('monday this week');
        $weekCutOff = intval($cutOff->format('U'));

        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('first day of this month');
        $monthCutOff = intval($cutOff->format('U'));

        $target = $this->target;

        $targetMessagesCount = (
            $target->Forum && $target->Forum->count_messages
            && $target->discussion_state == 'visible'
        );

        $sourcesMessagesCount = [];
        foreach ($this->sourceThreads as $id => $sourceThread) {
            $sourcesMessagesCount[$id] = (
                $sourceThread->Forum && $sourceThread->Forum->count_messages
                && $sourceThread->discussion_state == 'visible'
            );
        }

        $userMessageCountDayAdjust = [];
        $userMessageCountWeekAdjust = [];
        $userMessageCountMonthAdjust = [];
    
        foreach ($this->sourcePosts as $id => $post) {
            if ($post['message_state'] != 'visible') {
                continue; // everything will stay the same in the new thread
            }

            $targetMessagesCountDay = 0;
            $sourceMessagesCountDay = 0;
            if ($post['post_date'] >= $dayCutOff) {
                $targetMessagesCountDay = $targetMessagesCount;
                $sourceMessagesCountDay = $sourcesMessagesCount[$post['thread_id']];
            }

            $targetMessagesCountWeek = 0;
            $sourceMessagesCountWeek = 0;
            if ($post['post_date'] >= $weekCutOff) {
                $targetMessagesCountWeek = $targetMessagesCount;
                $sourceMessagesCountWeek = $sourcesMessagesCount[$post['thread_id']];
            }

            $targetMessagesCountMonth = 0;
            $sourceMessagesCountMonth = 0;
            if ($post['post_date'] >= $monthCutOff) {
                $targetMessagesCountMonth = $targetMessagesCount;
                $sourceMessagesCountMonth = $sourcesMessagesCount[$post['thread_id']];
            }

            $userId = $post['user_id'];
            if ($userId) {
                if ($sourceMessagesCountDay && !$targetMessagesCountDay) {
                    if (!isset($userMessageCountDayAdjust[$userId])) {
                        $userMessageCountDayAdjust[$userId] = 0;
                    }
                    $userMessageCountDayAdjust[$userId]--;
                } elseif (!$sourceMessagesCountDay && $targetMessagesCountDay) {
                    if (!isset($userMessageCountDayAdjust[$userId])) {
                        $userMessageCountDayAdjust[$userId] = 0;
                    }
                    $userMessageCountDayAdjust[$userId]++;
                }
                if ($sourceMessagesCountWeek && !$targetMessagesCountWeek) {
                    if (!isset($userMessageCountWeekAdjust[$userId])) {
                        $userMessageCountWeekAdjust[$userId] = 0;
                    }
                    $userMessageCountWeekAdjust[$userId]--;
                } elseif (!$sourceMessagesCountWeek && $targetMessagesCountWeek) {
                    if (!isset($userMessageCountWeekAdjust[$userId])) {
                        $userMessageCountWeekAdjust[$userId] = 0;
                    }
                    $userMessageCountWeekAdjust[$userId]++;
                }
                if ($sourceMessagesCountMonth && !$targetMessagesCountMonth) {
                    if (!isset($userMessageCountMonthAdjust[$userId])) {
                        $userMessageCountMonthAdjust[$userId] = 0;
                    }
                    $userMessageCountMonthAdjust[$userId]--;
                } elseif (!$sourceMessagesCountMonth && $targetMessagesCountMonth) {
                    if (!isset($userMessageCountMonthAdjust[$userId])) {
                        $userMessageCountMonthAdjust[$userId] = 0;
                    }
                    $userMessageCountMonthAdjust[$userId]++;
                }
            }
        }

        foreach ($userMessageCountMonthAdjust as $userId => $monthAdjust) {
            if ($monthAdjust) {
                $weekAdjust = isset($userMessageCountWeekAdjust[$userId]) ? $userMessageCountWeekAdjust[$userId] : 0;
                $dayAdjust = isset($userMessageCountDayAdjust[$userId]) ? $userMessageCountDayAdjust[$userId] : 0;
                $this->db()->query("
					UPDATE xf_user
                    SET message_count_month = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_month + ?),
                            ?
                        ),
                        message_count_week = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_week + ?),
                            ?
                        ),
                        message_count_day = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_day + ?),
                            ?
                        ),
                        message_count_last_updated = ?
					WHERE user_id = ?
				", [
                    $monthCutOff,
                    $monthAdjust,
                    $monthAdjust,
                    $weekCutOff,
                    $weekAdjust,
                    $weekAdjust,
                    $dayCutOff,
                    $dayAdjust,
                    $dayAdjust,
                    $currentTime,
                    $userId
                ]);
            }
        }
    }
}
