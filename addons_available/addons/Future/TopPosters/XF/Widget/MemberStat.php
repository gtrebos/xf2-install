<?php

namespace Future\TopPosters\XF\Widget;

class MemberStat extends XFCP_MemberStat
{
    public function render()
    {
        $memberStatKeys = [
            'most_messages',
            'futuretopposters_most_messages_day',
            'futuretopposters_most_messages_week',
            'futuretopposters_most_messages_month'
        ];

        $hasForumContext = (
            isset($this->contextParams['forum'])
            && $this->contextParams['forum'] instanceof \XF\Entity\Forum
        );

        $hasThreadContext = (
            isset($this->contextParams['thread'])
            && $this->contextParams['thread'] instanceof \XF\Entity\Thread
        );

        if (!in_array($this->options['member_stat_key'], $memberStatKeys)
            || (!$hasForumContext && !$hasThreadContext)) {
            return parent::render();
        }
        
        if (!\XF::visitor()->canViewMemberList()) {
            return '';
        }

        /** @var \XF\Entity\MemberStat $memberStat */
        $memberStat = $this->findOne('XF:MemberStat', [
            'member_stat_key' => $this->options['member_stat_key']
        ]);
        if (!$memberStat || !$memberStat->canView()) {
            return '';
        }

        $currentTime = \XF::$time;
        $cutOff = null;
        switch ($memberStat->sort_order) {
            case 'message_count_day':
                $cutOff = new \DateTime('@' . $currentTime);
                $cutOff->modify('midnight');
                $cutOff = intval($cutOff->format('U'));
                break;
            case 'message_count_week':
                $cutOff = new \DateTime('@' . $currentTime);
                $cutOff->modify('monday this week');
                $cutOff = intval($cutOff->format('U'));
                break;
            case 'message_count_month':
                $cutOff = new \DateTime('@' . $currentTime);
                $cutOff->modify('first day of this month');
                $cutOff = intval($cutOff->format('U'));
                break;
        }

        $results = [];
        if ($cutOff) {
            if ($hasThreadContext) {
                $results = $this->db()->fetchPairs("
                        SELECT user_id, COUNT(*)
                        FROM xf_post
                        WHERE post_date > ?
                            AND thread_id = ?
                        GROUP BY user_id
                        ORDER BY COUNT(*) DESC
                        LIMIT ?
                    ", [$cutOff, $this->contextParams['thread']->thread_id, $this->options['limit'] * 2]);
            } else {
                $results = $this->db()->fetchPairs("
                        SELECT post.user_id, COUNT(*)
                        FROM xf_post AS post
                        INNER JOIN xf_thread AS thread ON (post.thread_id = thread.thread_id)
                        WHERE post.post_date > ?
                            AND thread.node_id = ?
                        GROUP BY post.user_id
                        ORDER BY COUNT(*) DESC
                        LIMIT ?
                    ", [$cutOff, $this->contextParams['forum']->node_id, $this->options['limit'] * 2]);
            }
        } else {
            if ($hasThreadContext) {
                $results = $this->db()->fetchPairs("
                        SELECT user_id, post_count
                        FROM xf_thread_user_post
                        WHERE thread_id = ?
                        ORDER BY post_count DESC
                        LIMIT ?
                    ", [$this->contextParams['thread']->thread_id, $this->options['limit'] * 2]);
            } else {
                $results = $this->db()->fetchPairs("
                        SELECT thread_user_post.user_id, SUM(post_count)
                        FROM xf_thread AS thread
                        INNER JOIN xf_thread_user_post AS thread_user_post
                            ON (thread_user_post.thread_id = thread.thread_id)
                        WHERE thread.node_id = ?
                        GROUP BY thread_user_post.user_id
                        ORDER BY SUM(post_count) DESC
                        LIMIT ?
                    ", [$this->contextParams['forum']->node_id, $this->options['limit'] * 2]);
            }
        }

        $userIds = array_keys($results);

        /** @var \XF\Finder\User $userFinder */
        $userFinder = $this->finder('XF:User');

        $users = $userFinder
            ->with('Option', true)
            ->with('Profile', true)
            ->where('user_id', array_unique($userIds))
            ->isValidUser()
            ->fetch();

        $count = 0;
        $resultsData = [];
        foreach ($results as $userId => $value) {
            if ($count == $this->options['limit']) {
                // we have enough for this stat
                break;
            }

            if (!isset($users[$userId])) {
                // no valid user record found
                continue;
            }

            $resultsData[$userId] = [
                'user' => $users[$userId],
                'value' => $value
            ];

            $count++;
        }

        $viewParams = [
            'title' => $this->getTitle() ?: $memberStat->title,
            'memberStat' => $memberStat,
            'results' => $resultsData
        ];
        return $this->renderer('widget_member_stat', $viewParams);
    }
}
