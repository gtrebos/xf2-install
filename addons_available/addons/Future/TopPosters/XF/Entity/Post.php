<?php

namespace Future\TopPosters\XF\Entity;

class Post extends XFCP_Post
{
    protected function adjustUserMessageCountIfNeeded($amount)
    {
        parent::adjustUserMessageCountIfNeeded($amount);

        if ($this->user_id
            && !empty($this->Thread->Forum->count_messages)
            && $this->Thread->discussion_state == 'visible'
        ) {
            $currentTime = \XF::$time;

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('midnight');
            $dayCutOff = intval($cutOff->format('U'));

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('monday this week');
            $weekCutOff = intval($cutOff->format('U'));

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('first day of this month');
            $monthCutOff = intval($cutOff->format('U'));

            $dayAdjust = 0;
            if ($this->post_date >= $dayCutOff) {
                $dayAdjust = $amount;
            }

            $weekAdjust = 0;
            if ($this->post_date >= $weekCutOff) {
                $weekAdjust = $amount;
            }

            $monthAdjust = 0;
            if ($this->post_date >= $monthCutOff) {
                $monthAdjust = $amount;
            }

            if ($monthAdjust) {
                $this->db()->query("
                    UPDATE xf_user
                    SET message_count_month = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_month + ?),
                            ?
                        ),
                        message_count_week = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_week + ?),
                            ?
                        ),
                        message_count_day = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, message_count_day + ?),
                            ?
                        ),
                        message_count_last_updated = ?
                    WHERE user_id = ?
                ", [
                    $monthCutOff,
                    $monthAdjust,
                    $monthAdjust,
                    $weekCutOff,
                    $weekAdjust,
                    $weekAdjust,
                    $dayCutOff,
                    $dayAdjust,
                    $dayAdjust,
                    $currentTime,
                    $this->user_id
                ]);
            }
        }
    }
}
