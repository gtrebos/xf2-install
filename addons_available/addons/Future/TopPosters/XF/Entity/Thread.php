<?php

namespace Future\TopPosters\XF\Entity;

class Thread extends XFCP_Thread
{
    protected function adjustUserMessageCountIfNeeded($direction, $forceChange = false)
    {
        parent::adjustUserMessageCountIfNeeded($direction, $forceChange);

        if ($this->discussion_type == 'redirect') {
            return;
        }

        if ($forceChange || !empty($this->Forum->count_messages)) {
            $currentTime = \XF::$time;

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('first day of this month');
            $monthCutOff = intval($cutOff->format('U'));

            $monthUpdates = $this->db()->fetchPairs("
				SELECT user_id, COUNT(*)
				FROM xf_post
				WHERE thread_id = ?
                    AND post_date > ?
                    AND user_id > 0
                    AND message_state = 'visible'
				GROUP BY user_id
            ", $this->thread_id, $monthCutOff);

            if (!$monthUpdates) {
                return;
            }

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('monday this week');
            $weekCutOff = intval($cutOff->format('U'));
 
            $weekUpdates = $this->db()->fetchPairs("
				SELECT user_id, COUNT(*)
				FROM xf_post
				WHERE thread_id = ?
                    AND post_date > ?
                    AND user_id > 0
                    AND message_state = 'visible'
				GROUP BY user_id
            ", $this->thread_id, $weekCutOff);

            $cutOff = new \DateTime('@' . $currentTime);
            $cutOff->modify('midnight');
            $dayCutOff = intval($cutOff->format('U'));

            $dayUpdates = $this->db()->fetchPairs("
				SELECT user_id, COUNT(*)
				FROM xf_post
				WHERE thread_id = ?
                    AND post_date > ?
                    AND user_id > 0
                    AND message_state = 'visible'
				GROUP BY user_id
            ", $this->thread_id, $dayCutOff);

            $operator = $direction > 0 ? '+' : '-';
            foreach ($monthUpdates as $userId => $monthAdjust) {
                $weekAdjust = isset($weekUpdates[$userId]) ? $weekUpdates[$userId] : 0;
                $dayAdjust = isset($dayUpdates[$userId]) ? $dayUpdates[$userId] : 0;
                $this->db()->query("
                    UPDATE xf_user
                    SET message_count_month = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, CAST(message_count_month AS SIGNED) {$operator} ?)
                            ?
                        ),
                        message_count_week = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, CAST(message_count_week AS SIGNED) {$operator} ?)
                            ?
                        ),
                        message_count_day = IF(
                            message_count_last_updated >= ?,
                            GREATEST(0, CAST(message_count_day AS SIGNED) {$operator} ?)
                            ?
                        ),
                        message_count_last_updated = ?
                    WHERE user_id = ?
                ", [
                    $monthCutOff,
                    $monthAdjust,
                    $monthAdjust,
                    $weekCutOff,
                    $weekAdjust,
                    $weekAdjust,
                    $dayCutOff,
                    $dayAdjust,
                    $dayAdjust,
                    $currentTime,
                    $userId
                ]);
            }
        }
    }
}
