<?php

namespace Future\TopPosters\XF\Entity;

/**
 * @property int message_count_day
 * @property int message_count_week
 * @property int message_count_month
 * @property int message_count_last_updated
 */
class User extends XFCP_User
{
    public function getMessageCountDay()
    {
        $currentTime = \XF::$time;

        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('midnight');
        
        if ($this->message_count_last_updated > $cutOff) {
            return $this->message_count_day;
        }

        return 0;
    }

    public function getMessageCountWeek()
    {
        $currentTime = \XF::$time;

        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('monday this week');
        
        if ($this->message_count_last_updated > $cutOff) {
            return $this->message_count_month;
        }

        return 0;
    }

    public function getMessageCountMonth()
    {
        $currentTime = \XF::$time;
        
        $cutOff = new \DateTime('@' . $currentTime);
        $cutOff->modify('first day of this month');
        
        if ($this->message_count_last_updated > $cutOff) {
            return $this->message_count_month;
        }

        return 0;
    }
}
