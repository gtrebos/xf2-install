<?php

namespace Future\TopPosters\Listener;

use XF\Entity\User;
use XF\Service\User\Merge;

class UserMergeCombine
{
    public static function userMergeCombine(User $target, User $source, Merge $mergeService)
    {
        $target->message_count_day = $target->getMessageCountDay() + $source->getMessageCountDay();
        $target->message_count_week = $target->getMessageCountWeek() + $source->getMessageCountWeek();
        $target->message_count_month = $target->getMessageCountMonth() + $source->getMessageCountMonth();
        $target->message_count_last_updated = \XF::$time;
    }
}
