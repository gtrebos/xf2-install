<?php

namespace Future\TopPosters\Listener;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class EntityStructure
{
    /**
     * @param Manager $em
     * @param Structure $structure
     */
    public static function xfUser(Manager $em, Structure &$structure)
    {
        $structure->columns['message_count_day'] = [
            'type' => Entity::UINT,
            'forced' => true,
            'default' => 0,
            'changeLog' => false
        ];

        $structure->columns['message_count_week'] = [
            'type' => Entity::UINT,
            'forced' => true,
            'default' => 0,
            'changeLog' => false
        ];

        $structure->columns['message_count_month'] = [
            'type' => Entity::UINT,
            'forced' => true,
            'default' => 0,
            'changeLog' => false
        ];

        $structure->columns['message_count_last_updated'] = [
            'type' => Entity::UINT,
            'forced' => true,
            'default' => 0,
            'changeLog' => false
        ];
    }
}
