<?php

namespace Future\TopPosters\Listener;

use XF\Entity\User;
use XF\Service\User\Merge;

class UserSearcherOrders
{
    public static function userSearcherOrders(\XF\Searcher\User $userSearcher, array &$sortOrders)
    {
        $sortOrders['message_count_day'] = \XF::phrase('futuretopposters_messages_today');
        $sortOrders['message_count_week'] = \XF::phrase('futuretopposters_messages_this_week');
        $sortOrders['message_count_month'] = \XF::phrase('futuretopposters_messages_this_month');
    }
}
