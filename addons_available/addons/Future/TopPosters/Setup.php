<?php

namespace Future\TopPosters;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_user', function (Alter $table) {
            $table->addColumn('message_count_day', 'int')
                ->setDefault(0);
            $table->addColumn('message_count_week', 'int')
                ->setDefault(0);
            $table->addColumn('message_count_month', 'int')
                ->setDefault(0);
            $table->addColumn('message_count_last_updated', 'int')
                ->setDefault(0);
            $table->addKey('message_count_day');
            $table->addKey('message_count_week');
            $table->addKey('message_count_month');
        });
    }
}
