<?php

namespace Future\QuickThread\Listener;

use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class EntityStructure
{
    /**
     * @param Manager $em
     * @param Structure $structure
     */
    public static function xfForum(Manager $em, Structure &$structure)
    {
        $user = \XF::visitor();

        if (!$user->user_id) {
            $session = \XF::session();

            $sessionId = $session->getSessionId();

            $structure->relations['DraftThreads']['conditions'] = [
                ['draft_key', '=', 'forum-', '$node_id', '-' . $sessionId]
            ];
        }
    }
}
