<?php

namespace Future\QuickThread\Widget;

use XF\Widget\AbstractWidget;

class QuickThread extends AbstractWidget
{
    protected $defaultOptions = [
        'description' => '',
        'show_title' => true,
        'show_message' => true,
        'title_placeholder' => '',
        'message_placeholder' => '',
        'node_id' => null,
        'show_node_list' => true,
    ];

    protected function getDefaultTemplateParams($context)
    {
        $params = parent::getDefaultTemplateParams($context);
        if ($context == 'options') {
            $nodeRepo = $this->app->repository('XF:Node');
            $params['nodeTree'] = $nodeRepo->createNodeTree($nodeRepo->getFullNodeList());
        }
        return $params;
    }
    
    public function render()
    {
        $options = $this->options;

        $description = $options['description'];
        $showTitle = $options['show_title'];
        $showMessage = $options['show_message'];
        $titlePlaceholder = $options['title_placeholder'];
        $messagePlaceholder = $options['message_placeholder'];
        $nodeId = $options['node_id'];
        $showNodeList = $options['show_node_list'];
        
        $forum = null;
        $nodeTree = null;
        if (isset($this->contextParams['forum'])) {
            $forum = $this->contextParams['forum'];
        }

        if ($showNodeList || !$forum) {
            /** @var \XF\Repository\Node $nodeRepo */
            $nodeRepo = $this->app()->repository('XF:Node');
            $nodes = $nodeRepo->getFullNodeList()->filterViewable();
            $nodeTree = $nodeRepo->createNodeTree($nodes);

            foreach ($nodeTree->getFlattened(0) as $treeEntry) {
                if ($nodeId && $treeEntry['record']['node_id'] === $nodeId) {
                    $forum = $treeEntry['record'];
                    break;
                }
                if ($treeEntry['record']['node_type_id'] === 'Forum') {
                    if (!$forum) {
                        $forum = $treeEntry['record'];
                    }
                    if (!$nodeId) {
                        break;
                    }
                }
            }
        }

        $viewParams = [
            'forum' => $forum,
            'nodeTree' => $nodeTree,
            'description' => $description,
            'showTitle' => $showTitle,
            'showMessage' => $showMessage,
            'titlePlaceholder' => $titlePlaceholder,
            'messagePlaceholder' => $messagePlaceholder,
            'showNodeList' => $showNodeList,
        ];

        return $this->renderer('future_widget_quick_thread', $viewParams);
    }
}
