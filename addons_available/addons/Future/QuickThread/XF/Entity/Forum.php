<?php

namespace Future\QuickThread\XF\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

class Forum extends XFCP_Forum
{
    protected $createThreadCheckOverride = false;

    public function canCreateThread(&$error = null)
    {
        if (!$this->createThreadCheckOverride) {
            return parent::canCreateThread($error);
        }

        if (!$this->allow_posting) {
            $error = \XF::phraseDeferred('you_may_not_perform_this_action_because_forum_does_not_allow_posting');
            return false;
        }

        return true;
    }

    public function canCreateThreadWithoutOverride(&$error = null)
    {
        $createThreadCheckOverride = $this->createThreadCheckOverride;
        $this->createThreadCheckOverride = false;

        $response = $this->canCreateThread($error);

        $this->createThreadCheckOverride = $createThreadCheckOverride;

        return $response;
    }

    /**
     * @param bool $createThreadCheckOverride
     */
    public function setCreateThreadCheckOverride(bool $createThreadCheckOverride)
    {
        $this->createThreadCheckOverride = $createThreadCheckOverride;
    }

    public function getDraftThread()
    {
        $user = \XF::visitor();
        if (!$user->user_id) {
            $user = $this->em()->find('XF:User', 1);

            return \XF\Draft::createFromEntity($this, 'DraftThreads', $user);
        }

        return parent::getDraftThread();
    }
}
