<?php

namespace Future\QuickThread\XF\Session;

class Session extends XFCP_Session
{
    public function changeUser(\XF\Entity\User $user)
    {
        $oldSessionId;
        if ($this->sessionId && !$this->__get('userId')) {
            $oldSessionId = $this->sessionId;
        }

        $session = parent::changeUser($user);

        if ($session && $oldSessionId && $this->sessionId !== $oldSessionId && $user->user_id) {
            $this->copyVisitorDraftThreadsToUser($oldSessionId, $user->user_id);
        }

        return $session;
    }

    protected function copyVisitorDraftThreadsToUser($oldSessionId, $userId)
    {
        $db = \XF::app()->db();

        $db->query('
                INSERT INTO xf_draft
                (draft_key, user_id, last_update, message, extra_data)
                    SELECT SUBSTR(draft_key, 1, LENGTH(draft_key) - 33), ?, last_update, message, extra_data
                    FROM xf_draft
                    WHERE user_id = 1 AND SUBSTR(draft_key, -32) = ?
                ON DUPLICATE KEY UPDATE
                    last_update = VALUES(last_update), message = VALUES(message), extra_data = VALUES(extra_data)
            ', [$userId, $oldSessionId]);
    }
}
