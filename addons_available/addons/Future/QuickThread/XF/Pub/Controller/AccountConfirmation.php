<?php

namespace Future\QuickThread\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\View;

class AccountConfirmation extends XFCP_AccountConfirmation
{
    public function actionEmail(ParameterBag $params)
    {
        $reply = parent::actionEmail($params);

        if (!($reply instanceof View) || $reply->getTemplateName() !== 'register_confirm') {
            return $reply;
        }

        $visitor = \XF::visitor();

        if ($visitor->user_state == 'moderated' || $visitor->getPreviousValue('user_state') == 'email_confirm_edit') {
            return $reply;
        }

        $drafts = $this->finder('XF:Draft')
            ->where('user_id', $visitor->user_id)
            ->where('draft_key', 'LIKE', 'forum-%')
            ->order('last_update', 'DESC')
            ->fetch();

        if (!$drafts->count()) {
            return $reply;
        }

        foreach ($drafts as $draft) {
            if (preg_match('/^forum-([0-9]+)$/', $draft->draft_key, $matches)) {
                $nodeId = $matches[1];
                $node = $this->finder('XF:Node')
                    ->where('node_id', $nodeId)
                    ->fetchOne();
                if ($node) {
                    return $this->redirect($this->buildLink('forums/post-thread', $node));
                }
            }
        }

        return $reply;
    }
}
