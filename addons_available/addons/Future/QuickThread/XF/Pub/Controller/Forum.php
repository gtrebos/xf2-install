<?php

namespace Future\QuickThread\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Forum extends XFCP_Forum
{
    protected $createThreadCheckOverride = false;

    public function actionPostThread(ParameterBag $params)
    {
        if (!$this->isPost()) {
            $this->createThreadCheckOverride = true;
        }

        return parent::actionPostThread($params);
    }

    protected function assertViewableForum($nodeIdOrName, array $extraWith = [])
    {
        $forum = parent::assertViewableForum($nodeIdOrName, $extraWith);

        if ($this->createThreadCheckOverride) {
            $forum->setCreateThreadCheckOverride(true);
        }

        return $forum;
    }
}
