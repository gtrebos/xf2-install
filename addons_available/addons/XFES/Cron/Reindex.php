<?php

namespace XFES\Cron;

class Reindex
{
	public static function reindex()
	{
		if (\XF::options()->xfesEnabled)
		{
			/** @var \XFES\Service\RetryFailed $retrier */
			$retrier = \XF::service('XFES:RetryFailed', \XFES\Listener::getElasticsearchApi());
			$retrier->retry(\XF::config('jobMaxRunTime'));
		}
	}
}