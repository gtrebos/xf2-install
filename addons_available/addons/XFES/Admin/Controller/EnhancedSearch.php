<?php

namespace XFES\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\Mvc\ParameterBag;

class EnhancedSearch extends AbstractController
{
	protected function preDispatchController($action, ParameterBag $params)
	{
		$this->assertAdminPermission('option');
	}

	public function actionIndex()
	{
		$configurer = $this->getConfigurer();

		$es = null;
		$version = null;
		$testError = null;
		$stats = null;
		$isOptimizable = false;
		$analyzerConfig = null;

		if ($configurer->hasActiveConfig())
		{
			$es = $configurer->getEsApi();

			try
			{
				$version = $es->version();

				if ($version && $es->test($testError))
				{
					if ($es->indexExists())
					{
						$isOptimizable = $this->getOptimizer($es)->isOptimizable();
						$stats = $this->service('XFES:Stats', $es)->getStats();
						$analyzerConfig = $this->getAnalyzer($es)->getCurrentConfig();
					}
					else
					{
						$isOptimizable = true;
					}
				}
			}
			catch (\XFES\Elasticsearch\Exception $e) {}
		}

		$viewParams = [
			'es' => $es,
			'version' => $version,
			'testError' => $testError,
			'stats' => $stats,
			'isOptimizable' => $isOptimizable,
			'analyzerConfig' => $analyzerConfig,
			'reindex' => $this->filter('reindex', 'bool')
		];
		return $this->view('XFES:EnhancedSearch\Index', 'xfes_index', $viewParams);
	}

	public function actionConfig()
	{
		if ($this->isPost())
		{
			$config = $this->filter([
				'host' => 'str',
				'port' => 'uint',
				'username' => 'str',
				'password' => 'str',
				'https' => 'bool',
				'index' => 'str'
			]);

			$configurer = $this->getConfigurer($config);

			if (!$configurer->test($error))
			{
				return $this->error($error);
			}

			if ($configurer->indexExists())
			{
				// don't manipulate any of this now
				$indexExists = true;
				$analyzerConfig = null;
			}
			else
			{
				$indexExists = false;
				$analyzerConfig = $configurer->getAnalyzerConfig();
			}

			$viewParams = [
				'config' => $config,
				'es' => $configurer->getEsApi(),
				'indexExists' => $indexExists,
				'analyzerConfig' => $analyzerConfig
			];
			return $this->view('XFES:EnhancedSearch\ConfigConfirm', 'xfes_config_confirm', $viewParams);
		}
		else
		{
			return $this->view('XFES:EnhancedSearch\Config', 'xfes_config');
		}
	}

	public function actionSetup()
	{
		$this->assertPostOnly();

		$input = $this->filter([
			'config' => 'json-array',
			'enable' => 'bool',
			'empty_mysql' => 'bool',
			'index' => 'array'
		]);

		$configurer = $this->getConfigurer($input['config']);

		if (!$configurer->test($error))
		{
			return $this->error($error);
		}

		if (!$configurer->indexExists())
		{
			$configurer->initializeIndex($input['index']);
		}

		$configurer->saveConfig();

		if ($input['enable'])
		{
			$configurer->enable($input['empty_mysql']);
		}
		else
		{
			$configurer->disable();
		}

		return $this->redirect($this->buildLink('enhanced-search', null, ['reindex' => 1]));
	}

	public function actionOptimize()
	{
		if ($this->isPost())
		{
			$optimizer = $this->getOptimizer();
			$optimizer->optimize([], true);

			return $this->redirect($this->buildLink('enhanced-search', null, ['reindex' => 1]));
		}
		else
		{
			$viewParams = [];
			return $this->view('XFES:EnhancedSearch\Optimize', 'xfes_optimize', $viewParams);
		}
	}

	public function actionToggle()
	{
		$configurer = $this->getConfigurer();

		if ($this->isPost())
		{
			$reindex = false;

			if ($configurer->isEnabled())
			{
				$configurer->disable();
			}
			else
			{
				$emptyMySql = $this->filter('empty_mysql', 'bool');
				$configurer->enable($emptyMySql);

				$reindex = true;
			}

			return $this->redirect($this->buildLink('enhanced-search', null, ['reindex' => $reindex]));
		}
		else
		{
			if (!$configurer->isEnabled() && !$configurer->getEsApi()->test($error))
			{
				return $this->error($error);
			}

			$viewParams = [
				'enabled' => $configurer->isEnabled()
			];
			return $this->view('XFES:EnhancedSearch\Toggle', 'xfes_toggle', $viewParams);
		}
	}

	public function actionIndexConfig()
	{
		$this->assertPostOnly();

		$config = $this->filter('index', 'array');

		$changer = $this->getAnalyzer();
		$changer->updateAnalyzer($config);

		return $this->redirect($this->buildLink('enhanced-search', null, ['reindex' => 1]));
	}

	public function actionOptions()
	{
		$this->assertPostOnly();

		$options = $this->getDynamicOptionValuesFromInput();
		$this->repository('XF:Option')->updateOptions($options);

		return $this->redirect($this->buildLink('enhanced-search'));
	}

	protected function getDynamicOptionValuesFromInput()
	{
		return [
			'xfesRecencyRelevance' => [
				'enabled' => $this->filter('recencyWeighted', 'bool'),
				'halfLife' => $this->filter('recencyHalfLife', 'uint')
			]
		];
	}

	/**
	 * @param array|null $config
	 *
	 * @return \XFES\Service\Configurer
	 */
	protected function getConfigurer(array $config = null)
	{
		return $this->service('XFES:Configurer', $config);
	}

	/**
	 * @param \XFES\Elasticsearch\Api|null $es
	 *
	 * @return \XFES\Service\Optimizer
	 */
	protected function getOptimizer(\XFES\Elasticsearch\Api $es = null)
	{
		return $this->service('XFES:Optimizer', $es ?: \XFES\Listener::getElasticsearchApi());
	}

	/**
	 * @param \XFES\Elasticsearch\Api|null $es
	 *
	 * @return \XFES\Service\Analyzer
	 */
	protected function getAnalyzer(\XFES\Elasticsearch\Api $es = null)
	{
		return $this->service('XFES:Analyzer', $es ?: \XFES\Listener::getElasticsearchApi());
	}
}