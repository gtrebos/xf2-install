<?php

namespace XFES\Elasticsearch;

class RequestException extends Exception
{
	/** @var \GuzzleHttp\Message\RequestInterface */
	protected $request;

	/** @var Response */
	protected $response;

	public function setRequest(\GuzzleHttp\Message\RequestInterface $request)
	{
		$this->request = $request;
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function setResponse(Response $response)
	{
		$this->response = $response;
	}

	public function getResponse()
	{
		return $this->response;
	}
}