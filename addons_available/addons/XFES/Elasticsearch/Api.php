<?php

namespace XFES\Elasticsearch;

class Api implements \ArrayAccess
{
	/**
	 * @var \GuzzleHttp\Client
	 */
	protected $client;

	protected $config = [
		'host' => 'localhost',
		'port' => 9200,
		'username' => '',
		'password' => '',
		'https' => false,
		'index' => '',
		'singleType' => false
	];

	protected $exists = null;

	protected $version = null;

	protected $isSingleType = null;
	protected $singleTypeName = 'xf';

	public function __construct(array $config)
	{
		$this->config = array_replace($this->config, $config);

		if (!$this->config['index'])
		{
			$xfConfig = \XF::app()->config();
			if (isset($xfConfig['db']['dbname']))
			{
				$this->config['index'] = strtolower($xfConfig['db']['dbname']);
			}
		}

		if (!empty($this->config['singleType']))
		{
			// if we know it's single type, then trust that; otherwise, we'll still check the version
			$this->isSingleType = true;
		}

		$this->client = \XF::app()->http()->createClient([
			'base_url' => $this->getBaseUrl(),
			'defaults' => [
				'timeout' => 20,
				'connect_timeout' => 3,
				'exceptions' => false
			]
		]);
	}

	public function test(&$error = null)
	{
		try
		{
			$result = $this->request('get', '/')->getBody();
		}
		catch (Exception $e)
		{
			$error = \XF::phrase('xfes_connection_could_not_be_made_to_elasticsearch_server');
			return false;
		}

		if (empty($result['version']['number']))
		{
			$error = \XF::phrase('xfes_does_not_appear_to_be_elasticsearch_server');
			return false;
		}

		preg_match('/^\d+/', $result['version']['number'], $match);
		if (!$match || intval($match[0]) < 2)
		{
			$error = \XF::phrase('xfes_elasticsearch_version_does_not_meet_requirements', ['version' => $result['version']['number']]);
			return false;
		}

		return true;
	}

	public function version()
	{
		if ($this->version === null)
		{
			$result = $this->request('get', '/')->getBody();
			if ($result && !empty($result['version']['number']))
			{
				$this->version = $result['version']['number'];
			}
			else
			{
				$this->version = 0;
				throw new RequestException("Could not find version number in request");
			}
		}

		return $this->version;
	}

	public function majorVersion()
	{
		return intval($this->version());
	}

	/**
	 * Returns true if the index only supports a single type.
	 *
	 * @return bool
	 */
	public function isSingleTypeIndex()
	{
		if ($this->isSingleType === null)
		{
			try
			{
				$this->isSingleType = ($this->majorVersion() >= 7);
			}
			catch (Exception $e)
			{
				$this->isSingleType = false;
			}
		}

		return $this->isSingleType;
	}

	public function isTypelessIndex()
	{
		return ($this->majorVersion() >= 7);
	}

	public function getSingleTypeName()
	{
		return $this->singleTypeName;
	}

	public function forceSingleType($force = true)
	{
		$this->config['singleType'] = $force;
		$this->isSingleType = $force;
	}

	public function index($type, $id, array $data)
	{
		$this->addTypeToDataForSingleTypeIndex($type, $data);

		return $this->requestById('put', $type, $id, $data)->getBody();
	}

	public function indexBulk(array $records)
	{
		if (!$records)
		{
			return [];
		}

		$items = [];

		foreach ($records AS $record)
		{
			$items[] = $this->getBulkActionEntry('index', $record['type'], $record['id'], $record['data']);
		}

		$query = implode("\n", $items) . "\n";

		$body = $this->bulkRequest($query)->getBody();

		return $body;
	}

	public function delete($type, $id)
	{
		try
		{
			return $this->requestById('delete', $type, $id)->getBody();
		}
		catch (RequestException $e)
		{
			$response = $e->getResponse();
			if ($response && $response->getCode() == 404)
			{
				// deleting something that has already been deleted shouldn't error
				return $response->getBody();
			}

			throw $e;
		}
	}

	public function deleteBulk($type, array $ids)
	{
		if (!$ids)
		{
			return [];
		}

		$deletes = [];

		foreach ($ids AS $id)
		{
			$deletes[] = $this->getBulkActionEntry('delete', $type, $id);
		}

		$query = implode("\n", $deletes) . "\n";

		return $this->bulkRequest($query)->getBody();
	}

	public function search(array $dsl)
	{
		return $this->requestFromIndex('get', '_search', $dsl)->getBody();
	}

	public function indexExists()
	{
		if ($this->exists !== null)
		{
			return $this->exists;
		}

		try
		{
			$response = $this->request('head', $this->config['index']);
			$this->exists = ($response->getCode() == 200);
		}
		catch (RequestException $e)
		{
			// probably a "no such index" error
			$this->exists = false;
		}

		return $this->exists;
	}

	public function getIndexInfo()
	{
		$name = $this->config['index'];

		$body = $this->request('get', $name)->getBody();
		return isset($body[$name]) ? $body[$name] : [];
	}

	public function getIndexStats()
	{
		$name = $this->config['index'];

		$body = $this->requestFromIndex('get', "_stats")->getBody();
		return isset($body['indices'][$name]['primaries']) ? $body['indices'][$name]['primaries'] : [];
	}

	public function deleteIndex()
	{
		$this->exists = null;

		return $this->request('delete', $this->config['index'])->getBody();
	}

	public function createIndex(array $config = [])
	{
		unset(
			$config['settings']['index']['creation_date'],
			$config['settings']['index']['uuid'],
			$config['settings']['index']['version'],
			$config['settings']['index']['provided_name']
		);

		if (empty($config['settings']['index']))
		{
			unset($config['settings']['index']);
		}

		foreach ($config AS $k => $value)
		{
			if (is_array($value) && !$value)
			{
				unset($config[$k]);
			}
		}

		$this->exists = null;

		$body = $this->request('put', $this->config['index'], $config)->getBody();

		return $body;
	}

	public function updateSettings(array $settings)
	{
		return $this->requestFromIndex('put', '_settings', $settings)->getBody();
	}

	public function closeIndex()
	{
		return $this->requestFromIndex('post', "_close")->getBody();
	}

	public function openIndex()
	{
		return $this->requestFromIndex('post', "_open")->getBody();
	}

	public function requestFromIndex($method, $path, $data = null)
	{
		$path = ltrim($path, '/');
		$index = $this->config['index'];

		return $this->request($method, "{$index}/{$path}", $data);
	}

	public function requestById($method, $type, $id, $data = null)
	{
		if ($this->isTypelessIndex())
		{
			$path = "$type-$id";
		}
		else if ($this->isSingleTypeIndex())
		{
			$path = $this->getSingleTypeName() . "/$type-$id";
		}
		else
		{
			$path = "$type/$id";
		}

		return $this->requestFromIndex($method, $path, $data);
	}

	public function request($method, $path, $data = null, array $options = [], &$request = null)
	{
		$request = $this->client->createRequest($method, $path);

		if ($data)
		{
			$contentType = !empty($options['contentType']) ? $options['contentType'] : 'application/json';
			$request->addHeader('Content-Type', $contentType);

			if (!is_string($data))
			{
				$data = json_encode($data, JSON_PRETTY_PRINT);
			}
			$request->setBody(\GuzzleHttp\Stream\Stream::factory($data));
		}

		try
		{
			$response = $this->client->send($request);
			$body = $response->getBody();

			if ($body)
			{
				$contents = @json_decode($body->getContents(), true);
				$result = is_array($contents) ? $contents : [];
			}
			else
			{
				$result = null;
			}

			$esResponse = new Response($response->getStatusCode(), $result);
		}
		catch (\GuzzleHttp\Exception\ConnectException $e)
		{
			throw new ConnectException($e->getMessage(), $e->getCode(), $e);
		}

		$responseCode = $esResponse->getCode();
		if ($responseCode >= 400 && $responseCode <= 599)
		{
			$body = $esResponse->getBody();
			$error = null;

			if ($body)
			{
				$error = $this->getErrorMessage($body);
			}

			if (!$error)
			{
				$error = "Unknown error (HTTP code: $responseCode)";
			}

			$e = new RequestException($error, $responseCode);
			$e->setRequest($request);
			$e->setResponse($esResponse);

			throw $e;
		}

		return $esResponse;
	}

	public function bulkRequest($data)
	{
		$options = [
			'contentType' => 'application/x-ndjson'
		];

		$esResponse = $this->request('post', '_bulk', $data, $options, $request);
		$body = $esResponse->getBody();

		if ($this->hasBulkActionErrors($body, $errors))
		{
			$e = new BulkRequestException($errors);
			$e->setRequest($request);
			$e->setResponse($esResponse);

			throw $e;
		}

		return $esResponse;
	}

	protected function getBulkActionEntry($action, $type, $id, array $source = null)
	{
		$index = $this->config['index'];
		$isSingleType = $this->isSingleTypeIndex();

		$actionLine = [
			$action => [
				'_index' => $index,
				'_type' => $isSingleType ? $this->singleTypeName : $type,
				'_id' => $isSingleType ? "$type-$id" : $id
			]
		];
		if ($this->isTypelessIndex())
		{
			// typeless indexes are always single type, so _id will have the type and ID in it
			unset($actionLine[$action]['_type']);
		}

		$bulk = json_encode($actionLine);

		if (is_array($source))
		{
			if ($isSingleType && ($action == 'index' || $action == 'create'))
			{
				$this->addTypeToDataForSingleTypeIndex($type, $source);
			}

			$bulk .= "\n" . json_encode($source);
		}

		return $bulk;
	}

	protected function addTypeToDataForSingleTypeIndex($type, array &$data)
	{
		if ($this->isSingleTypeIndex())
		{
			$data['type'] = $type;
		}
	}

	protected function getErrorMessage(array $body)
	{
		if (isset($body['error']['reason']))
		{
			return strval($body['error']['reason']);
		}

		return null;
	}

	protected function hasBulkActionErrors(array $body, &$errors = null)
	{
		$errors = [];

		if (empty($body['errors']) || empty($body['items']))
		{
			return false;
		}

		foreach ($body['items'] AS $itemContainer)
		{
			$item = reset($itemContainer);
			$action = key($itemContainer);

			$status = $item['status'];
			if ($status >= 400 && $status <= 599)
			{
				if ($action == 'delete' && $status == 404)
				{
					// don't treat a 404 for removal as an error since it's
					continue;
				}

				if (empty($item['_type']) || $item['_type'] == $this->singleTypeName)
				{
					$uniqueId = $item['_id'];
				}
				else
				{
					$uniqueId = "$item[_type]-$item[_id]";
				}
				$errors[$uniqueId] = $this->getErrorMessage($item);
			}
		}

		return (count($errors) > 0);
	}

	protected function getBaseUrl()
	{
		$config = $this->config;

		$protocol = $config['https'] ? 'https' : 'http';
		$host = $config['host'] ?: 'localhost';
		$port = $config['port'] ?: 9200;
		$username = $config['username'];

		if ($username)
		{
			return [
				"{$protocol}://{username}:{password}@{$host}:{$port}",
				['username' => $username, 'password' => $config['password']]
			];
		}
		else
		{
			return "{$protocol}://{$host}:{$port}";
		}
	}

	public function getPrintableBaseUrl()
	{
		$config = $this->config;

		$protocol = $config['https'] ? 'https' : 'http';
		$host = $config['host'];
		$port = $config['port'];
		$username = $config['username'];

		if ($username)
		{
			return "{$protocol}://{$username}:********@{$host}:{$port}";
		}
		else
		{
			return "{$protocol}://{$host}:{$port}";
		}
	}

	public function getTypeAndIdFromHit(array $hit)
	{
		if (empty($hit['_type']) || $hit['_type'] == $this->singleTypeName)
		{
			$typeAndId = explode('-', $hit['_id'], 2);
		}
		else
		{
			$typeAndId = [$hit['_type'], $hit['_id']];
		}

		// make sure the ID is an int
		$typeAndId[1] = intval($typeAndId[1]);

		return $typeAndId;
	}

	public function getConfig()
	{
		return $this->config;
	}

	public function offsetGet($offset)
	{
		switch ($offset)
		{
			case 'printableBaseUrl': return $this->getPrintableBaseUrl();
			case 'config': return $this->config;

			default:
				if (array_key_exists($offset, $this->config))
				{
					return $this->config[$offset];
				}
				else
				{
					return null;
				}
		}
	}

	public function offsetExists($offset)
	{
		switch ($offset)
		{
			case 'printableBaseUrl': return true;
			case 'config': return true;
			default: return array_key_exists($offset, $this->config);
		}
	}

	public function offsetSet($offset, $value)
	{
		throw new \LogicException("Cannot update Elasticsearch offsets");
	}

	public function offsetUnset($offset)
	{
		throw new \LogicException("Cannot update Elasticsearch offsets");
	}
}