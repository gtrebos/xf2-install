<?php

namespace XFES\Service;

class Stats extends \XF\Service\AbstractService
{
	protected $es;

	public function __construct(\XF\App $app, \XFES\Elasticsearch\Api $es)
	{
		parent::__construct($app);

		$this->es = $es;
	}

	public function getStats()
	{
		if ($this->es->indexExists())
		{
			return $this->es->getIndexStats();
		}
		else
		{
			return [];
		}
	}
}