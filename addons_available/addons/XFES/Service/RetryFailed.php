<?php

namespace XFES\Service;

class RetryFailed extends \XF\Service\AbstractService
{
	protected $es;

	public function __construct(\XF\App $app, \XFES\Elasticsearch\Api $es)
	{
		parent::__construct($app);

		$this->es = $es;
	}

	public function retry($maxRunTime = null)
	{
		/** @var \XFES\Repository\IndexFailed $indexFailedRepo */
		$indexFailedRepo = $this->repository('XFES:IndexFailed');

		$start = microtime(true);

		$failures = $indexFailedRepo->findRetryableRecords()->fetch(200);
		foreach ($failures AS $failure)
		{
			/** @var \XFES\Entity\IndexFailed $failure */

			$type = $failure->content_type;
			$id = $failure->content_id;

			try
			{
				if ($failure->action == 'delete')
				{
					$this->es->delete($type, $id);
				}
				else if ($failure->action == 'index')
				{
					$this->es->index($type, $id, $failure->data);
				}

				// if reached, action has been successful
				$failure->delete();
			}
			catch (\XFES\Elasticsearch\Exception $e)
			{
				if ($failure->fail_count >= 5)
				{
					$failure->delete();
					\XF::logException($e, false, "Indexing $type:$id failed 5 times (skipping): ");
				}
				else
				{
					// will delay by 1, 2, 4, 8, 16 hours
					$failure->reindex_date = \XF::$time + 3600 * pow(2, $failure->fail_count);
					$failure->fail_count++;
					$failure->save();
				}
			}

			if ($maxRunTime && microtime(true) - $start > $maxRunTime)
			{
				break;
			}
		}
	}
}