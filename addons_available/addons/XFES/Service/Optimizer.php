<?php

namespace XFES\Service;

use XF\Search\MetadataStructure;

class Optimizer extends \XF\Service\AbstractService
{
	protected $es;

	public function __construct(\XF\App $app, \XFES\Elasticsearch\Api $es)
	{
		parent::__construct($app);

		$this->es = $es;
	}

	public function getEsApi()
	{
		return $this->es;
	}

	public function isOptimizable()
	{
		try
		{
			$config = $this->es->getIndexInfo();
		}
		catch (\XFES\Elasticsearch\RequestException $e)
		{
			return true;
		}
		catch (\XFES\Elasticsearch\Exception $e)
		{
			return false;
		}

		if (!$config['mappings'])
		{
			return true;
		}

		$liveMappings = $config['mappings'];
		$mappingType = key($liveMappings);
		if ($mappingType == '_doc')
		{
			// ES7 may not have an explicit type, so we may _doc as the type, whereas we're expecting "xf".
			// When this happens, we won't be passing types into URLs, so we should be able to ignore it.
			$liveMappings = [$this->es->getSingleTypeName() => $liveMappings['_doc']];
		}

		$expectedMappings = $this->getExpectedMappingConfig();

		return ($liveMappings != $expectedMappings);
	}

	public function optimize(array $settings = [], $updateConfig = false)
	{
		$config = [];

		if ($this->es->indexExists())
		{
			$config = $this->es->getIndexInfo();
			$this->es->deleteIndex();
			$this->db()->emptyTable('xf_es_index_failed');
		}

		if ($settings)
		{
			if (isset($config['settings']))
			{
				$config['settings'] = array_replace_recursive($config['settings'], $settings);
			}
			else
			{
				$config['settings'] = $settings;
			}
		}

		// if we create an index in ES6+, we must force it to be single type
		$this->es->forceSingleType($this->es->majorVersion() >= 6);

		$config['mappings'] = $this->getExpectedMappingConfig();

		$this->es->createIndex($config);

		if ($updateConfig)
		{
			/** @var \XFES\Service\Configurer $configurer */
			$configurer = $this->service('XFES:Configurer', $this->es);
			$configurer->saveConfig();
		}

		return true;
	}

	public function getExpectedMappingConfig()
	{
		$baseMapping = $this->getBaseMapping();

		$isSingleType = $this->es->isSingleTypeIndex();
		$singleMapping = $baseMapping;

		$textType = ($this->es->majorVersion() >= 5 ? 'text' : 'string');

		$mappings = [];
		foreach ($this->app->search()->getValidHandlers() AS $type => $handler)
		{
			$mapping = $baseMapping;
			foreach ($handler->getMetadataStructure() AS $mdKey => $mdConfig)
			{
				switch ($mdConfig['type'])
				{
					case MetadataStructure::BOOL: $mdType = 'boolean'; break;
					case MetadataStructure::FLOAT: $mdType = 'double'; break;
					case MetadataStructure::INT: $mdType = 'long'; break;
					case MetadataStructure::KEYWORD: $mdType = 'keyword'; break;

					case MetadataStructure::STR:
					default:
						$mdType = $textType;
				}

				$mapping['properties'][$mdKey] = ['type' => $mdType];

				if ($singleMapping)
				{
					$singleMapping['properties'][$mdKey] = ['type' => $mdType];
				}
			}

			$mappings[$type] = $mapping;
		}

		if ($this->es->majorVersion() >= 8)
		{
			// In ES7, types become optional. However, mapping calls appear to default to include_type_name until ES8.
			// Thus, we can't remove the type name wrapping consistently until ES8 specifically.
			// See https://www.elastic.co/guide/en/elasticsearch/reference/6.0/removal-of-types.html
			return $singleMapping;
		}
		else if ($isSingleType)
		{
			return [$this->es->getSingleTypeName() => $singleMapping];
		}
		else
		{
			return $mappings;
		}
	}

	protected function getBaseMapping()
	{
		$version = $this->es->majorVersion();

		$textType = ($version >= 5 ? 'text' : 'string');

		$mapping = [
			'_source' => ['enabled' => false],
			'_all' => ['enabled' => false],
			'properties' => [
				'title' => ['type' => $textType],
				'message' => ['type' => $textType],
				'date' => ['type' => 'long', 'store' => true],
				'user' => ['type' => 'long', 'store' => true],
				'discussion_id' => ['type' => 'long', 'store' => true],
				'hidden' => ['type' => 'boolean'],
				'tag' => ['type' => 'long'] // this is actually an array of integers
			]
		];

		if ($version >= 6)
		{
			// this is disabled in 6+
			unset($mapping['_all']);
		}
		if ($this->es->isSingleTypeIndex())
		{
			$mapping['properties']['type'] = ['type' => 'keyword'];
		}

		return $mapping;
	}
}