<?php

namespace XFES\Search\Source;

use XF\Search\IndexRecord;
use XF\Search\Query;
use XFES\Elasticsearch\Exception as EsException;

class Elasticsearch extends \XF\Search\Source\AbstractSource
{
	/**
	 * @var \XFES\Elasticsearch\Api
	 */
	protected $es;

	protected $bulkIndexRecords = [];

	public function __construct(\XFES\Elasticsearch\Api $es)
	{
		$this->es = $es;
	}

	public function isRelevanceSupported()
	{
		return true;
	}

	public function index(IndexRecord $record)
	{
		$type = $record->type;
		$id = $record->id;

		$data = [
			'title' => $record->title,
			'message' => $record->message,
			'date' => $record->date,
			'user' => $record->userId,
			'discussion_id' => $record->discussionId
		];

		$data += $record->metadata;

		if ($record->hidden)
		{
			$data['hidden'] = true;
		}

		if ($this->bulkIndexing)
		{
			$this->bulkIndexRecords[] = [
				'type' => $type,
				'id' => $id,
				'data' => $data
			];
			if (count($this->bulkIndexRecords) >= 500)
			{
				$this->flushBulkIndexing();
			}

			return true;
		}
		else
		{
			try
			{
				$this->es->index($type, $id, $data);
				return true;
			}
			catch (EsException $e)
			{
				$this->logFailedIndexing($type, $id, $data);
				$this->logElasticsearchException($e, "Elasticsearch indexing error (queued): ");
				return false;
			}
		}
	}

	protected function flushBulkIndexing()
	{
		try
		{
			$this->es->indexBulk($this->bulkIndexRecords);
		}
		catch (EsException $e)
		{
			$this->logElasticsearchException($e, "Elasticsearch indexing error: ");
			throw new \XF\PrintableException(\XF::phrase('xfes_error_was_triggered_while_indexing_see_log'));
		}

		$this->bulkIndexRecords = [];
	}

	public function delete($type, $ids)
	{
		try
		{
			if (is_array($ids))
			{
				$this->es->deleteBulk($type, $ids);
			}
			else
			{
				$this->es->delete($type, $ids);
			}

			return true;
		}
		catch (EsException $e)
		{
			if (is_array($ids))
			{
				foreach ($ids AS $id)
				{
					$this->logFailedIndexing($type, $id);
				}
			}
			else
			{
				$this->logFailedIndexing($type, $ids);
			}

			$this->logElasticsearchException($e, "Elasticsearch indexing error (queued): ");

			return false;
		}
	}

	public function truncate($type = null)
	{
		if ($type)
		{
			// ES can't delete by type. It can delete by query but this could potentially delete millions of documents
			// which is likely to be problematic/slow.
		}
		else
		{
			/** @var \XFES\Service\Optimizer $optimizer */
			$optimizer = \XF::app()->service('XFES:Optimizer', $this->es);
			$optimizer->optimize([], true);
		}
	}

	public function search(Query\Query $query, $maxResults)
	{
		$dsl = $this->getDslFromQuery($query, $maxResults);

		try
		{
			$response = $this->es->search($dsl);
		}
		catch (EsException $e)
		{
			$this->logElasticsearchException($e);
			$response = null;
		}

		if (!$response || !isset($response['hits']['hits']))
		{
			throw \XF::phrasedException('xfes_search_could_not_be_completed_try_again_later');
		}

		$hits = $response['hits']['hits'];

		if ($query->hasQueryConstraints())
		{
			$matches = $this->getSqlResults($hits, $query, $maxResults);
		}
		else
		{
			$matches = [];
			$groupType = $query->getGroupByType();
			foreach ($hits AS $hit)
			{
				if ($groupType)
				{
					$discussionId = $hit['fields']['discussion_id'][0];
					$matches[$discussionId] = [
						'content_type' => $groupType,
						'content_id' => $discussionId
					];
				}
				else
				{
					$typeAndId = $this->es->getTypeAndIdFromHit($hit);

					$matches[] = [
						'content_type' => $typeAndId[0],
						'content_id' => intval($typeAndId[1])
					];
				}
			}
		}

		$matches = array_slice($matches, 0, $maxResults, false);

		return $matches;
	}

	protected function getDslFromQuery(Query\Query $query, $maxResults)
	{
		$dsl = [];

		$order = $query->getOrder();
		if ($order instanceof Query\SqlOrder)
		{
			$dsl['sort'] = [
				['date' => 'desc']
			];
		}
		else if ($order == 'relevance' && $query->getParsedKeywords())
		{
			$dsl['sort'] = [
				'_score',
				['date' => 'desc']
			];
		}
		else if ($order)
		{
			$dsl['sort'] = [
				['date' => 'desc']
			];
		}

		// TODO: use aggregations for grouping if no query constraints?
		// TODO: don't pull any fields if we don't need them
		if ($this->es->majorVersion() >= 5)
		{
			// fields is no longer accessible. stored_fields only works if explicitly stored. _source
			// only works if it hasn't been removed. docvalue_fields works consistently.
			$dsl['docvalue_fields'] = ['discussion_id', 'user', 'date'];
			$dsl['_source'] = false;
		}
		else
		{
			$dsl['fields'] = ['discussion_id', 'user', 'date'];
		}

		$fetchResults = $maxResults;

		if ($query->hasQueryConstraints())
		{
			$fetchResults *= 4;
		}
		if ($query->getGroupByType())
		{
			$fetchResults *= 4;
		}

		$dsl['size'] = min(10000, $fetchResults);

		$queryStringDsl = $this->getQueryStringDsl($query);

		$filters = [];
		$filtersNot = [];
		$this->applyDslFilters($query, $filters, $filtersNot);

		$dsl['query'] = $this->getDslQueryComponent($filters, $filtersNot, $queryStringDsl);

		return $dsl;
	}

	protected function applyDslFilters(Query\Query $query, array &$filters, array &$filtersNot)
	{
		$dateRange = [];
		if ($query->getMinDate())
		{
			$dateRange['gt'] = $query->getMinDate();
		}
		if ($query->getMaxDate())
		{
			$dateRange['lt'] = $query->getMaxDate();
		}
		if ($dateRange)
		{
			$filters[] = [
				'range' => ['date' => $dateRange]
			];
		}

		if (!$query->getAllowHidden())
		{
			$filtersNot[] = [
				'exists' => ['field' => 'hidden']
			];
		}

		$userIds = $query->getUserIds();
		if ($userIds)
		{
			$this->applyMetadataConstraint(
				new Query\MetadataConstraint('user', $userIds),
				$filters, $filtersNot
			);
		}

		$types = $query->getTypes();
		if ($types)
		{
			if ($this->es->isSingleTypeIndex())
			{
				// types are now stored in a field in the index directly
				$this->applyMetadataConstraint(
					new Query\MetadataConstraint('type', $types),
					$filters, $filtersNot
				);
			}
			else
			{
				// type matching is a special case -- we need a long winded approach for multiple types
				if (count($types) > 1)
				{
					$subBools = [];
					foreach ($types AS $type)
					{
						$subBools[] = [
							'type' => [
								'value' => $type
							]
						];
					}
					$filters[] = [
						'bool' => ['should' => $subBools]
					];
				}
				else
				{
					$filters[] = [
						'type' => ['value' => reset($types)]
					];
				}
			}
		}

		foreach ($query->getMetadataConstraints() AS $metadata)
		{
			$this->applyMetadataConstraint($metadata, $filters, $filtersNot);
		}

		foreach ($query->getPermissionConstraints() AS $perms)
		{
			foreach ($perms['constraints'] AS $permConstraint)
			{
				$this->applyMetadataConstraint($permConstraint, $filters, $filtersNot);
			}
		}
	}

	protected function getQueryStringDsl(Query\Query $query)
	{
		$searchQuery = $query->getParsedKeywords();
		if (!$searchQuery)
		{
			return null;
		}

		$queryStringDsl = [
			'simple_query_string' => [
				'query' => $searchQuery,
				'fields' => $query->getTitleOnly() ? ['title'] : ['title', 'message'],
				'default_operator' => 'and'
			]
		];

		if ($query->getOrder() == 'relevance')
		{
			$recencyRelevance = \XF::options()->xfesRecencyRelevance;
			if ($recencyRelevance['enabled'])
			{
				$queryStringDsl = [
					'function_score' => [
						'query' => $queryStringDsl,
						'functions' => [
							[
								'exp' => [
									'date' => [
										'origin' => \XF::$time,
										'decay' => 0.5,
										'scale' => 86400 * max(1, $recencyRelevance['halfLife'])
									]
								]
							]
						]
					]
				];
			}
		}

		return $queryStringDsl;
	}

	protected function getDslQueryComponent(array $filters, array $filtersNot, array $queryStringDsl = null)
	{
		if ($filters || $filtersNot)
		{
			$bools = [];
			if ($queryStringDsl)
			{
				$bools['must'] = $queryStringDsl;
			}
			if ($filters)
			{
				$bools['filter'] = $filters;
			}
			if ($filtersNot)
			{
				$bools['must_not'] = $filtersNot;
			}
			$queryDsl = ['bool' => $bools];
		}
		else if ($queryStringDsl)
		{
			$queryDsl = $queryStringDsl;
		}
		else
		{
			$queryDsl = ['match_all' => []];
		}

		return $queryDsl;
	}

	protected function applyMetadataConstraint(Query\MetadataConstraint $metadata, array &$filters, array &$filtersNot)
	{
		$values = $metadata->getValues();

		switch ($metadata->getMatchType())
		{
			case Query\MetadataConstraint::MATCH_ANY:
				if (count($values) > 1)
				{
					$filters[] = [
						'terms' => [$metadata->getKey() => $values]
					];
				}
				else
				{
					$filters[] = [
						'term' => [$metadata->getKey() => reset($values)]
					];
				}
				break;

			case Query\MetadataConstraint::MATCH_ALL:
				if (count($values) > 1)
				{
					$subBools = [];
					foreach ($values AS $value)
					{
						$subBools[] = [
							'term' => [
								$metadata->getKey() => $value
							]
						];
					}
					$filters[] = [
						'bool' => ['filter' => $subBools]
					];
				}
				else
				{
					$filters[] = [
						'term' => [$metadata->getKey() => reset($values)]
					];
				}
				break;

			case Query\MetadataConstraint::MATCH_NONE:
				if (count($values) > 1)
				{
					$filtersNot[] = [
						'terms' => [$metadata->getKey() => $values]
					];
				}
				else
				{
					$filtersNot[] = [
						'term' => [$metadata->getKey() => reset($values)]
					];
				}
				break;
		}
	}

	protected function getSqlResults(array $hits, Query\Query $query, $maxResults)
	{
		$db = \XF::db();

		$db->query('
			CREATE TEMPORARY TABLE xf_search_index_temp (
				hit_position int unsigned not null PRIMARY KEY,
				content_type varbinary(25) not null,
				content_id int unsigned not null,
				user_id int unsigned not null,
				item_date int unsigned not null,
				discussion_id int unsigned not null
			)
		');
		$bulkInsert = [];
		$bulkInsertLength = 0;
		$insertQuery = '
			INSERT INTO xf_search_index_temp
				(hit_position, content_type, content_id, user_id, item_date, discussion_id)
			VALUES
				%s
		';
		$hitPosition = 1;

		foreach ($hits AS $hit)
		{
			$typeAndId = $this->es->getTypeAndIdFromHit($hit);
			$fields = $hit['fields'];

			$row = '(' . $db->quote($hitPosition)
				. ', ' . $db->quote($typeAndId[0])
				. ', ' . $db->quote($typeAndId[1])
				. ', ' . $db->quote($fields['user'][0])
				. ', ' . $db->quote($fields['date'][0])
				. ', ' . $db->quote($fields['discussion_id'][0]) . ')';

			$bulkInsert[] = $row;
			$bulkInsertLength += strlen($row);

			if ($bulkInsertLength > 500000)
			{
				$db->query(sprintf($insertQuery, implode(',', $bulkInsert)));

				$bulkInsert = [];
				$bulkInsertLength = 0;
			}

			$hitPosition++;
		}

		if ($bulkInsert)
		{
			$db->query(sprintf($insertQuery, implode(',', $bulkInsert)));
		}

		/** @var Query\TableReference[] $tables */
		$tables = [];
		$where = [];

		foreach ($query->getSqlConstraints() AS $constraint)
		{
			$where[] = $constraint->getSql($db);
			$tables += $constraint->getTables();
		}

		$order = $query->getOrder();
		if ($order instanceof Query\SqlOrder)
		{
			$tables += $order->getTables();
			$orderByClause = 'ORDER BY ' . $order->getOrder() . ', search_index.hit_position ASC';
		}
		else
		{
			$orderByClause = 'ORDER BY search_index.hit_position ASC';
		}

		$joins = '';
		foreach ($tables AS $table)
		{
			$joins .= "INNER JOIN " . $table->getTable() . " AS " . $table->getAlias() . " ON (" . $table->getCondition() . ")\n";
		}

		if ($where)
		{
			$whereClause = 'WHERE ' . implode(' AND ', $where);
		}
		else
		{
			$whereClause = '';
		}

		$groupType = $query->getGroupByType();
		if ($groupType)
		{
			$selectFields = $db->quote($groupType) . ' AS content_type, search_index.discussion_id AS content_id';
			$groupByClause = 'GROUP BY search_index.discussion_id';
		}
		else
		{
			$selectFields = 'search_index.content_type, search_index.content_id';
			$groupByClause = '';
		}

		$maxResults = intval($maxResults);
		if ($maxResults <= 0)
		{
			$maxResults = 1;
		}

		$results = $db->fetchAllNum("
			SELECT {$selectFields}
			FROM xf_search_index_temp AS search_index
			{$joins}
			{$whereClause}
			{$groupByClause}
			{$orderByClause}
			LIMIT {$maxResults}
		");

		return $results;
	}

	public function getWordSplitRange()
	{
		return '\x00-\x21\x23-\x26\x28\x29\x2B\x2C\x2F\x3A-\x40\x5B-\x5E\x60\x7B-\x7F';
	}

	protected function finalizeParsedKeywords(array $parsed)
	{
		$query = '';
		foreach ($parsed AS $part)
		{
			if ($part[0] == '|')
			{
				$part[0] = '| ';
			}

			$query .= " $part[0]$part[1]";
		}

		return trim($query);
	}

	protected function logElasticsearchException(EsException $e, $errorPrefix = "Elasticsearch error: ")
	{
		\XF::logException($e, false, $errorPrefix);
	}

	protected function logFailedIndexing($type, $id, array $record = null)
	{
		/** @var \XFES\Repository\IndexFailed $indexFailed */
		$indexFailed = \XF::repository('XFES:IndexFailed');
		$indexFailed->logFailedIndexing($type, $id, $record);
	}
}