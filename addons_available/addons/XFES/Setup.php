<?php

namespace XFES;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
	use StepRunnerUpgradeTrait;

	public function install(array $stepParams = [])
	{
		$this->schemaManager()->createTable('xf_es_index_failed', function(Create $table)
		{
			$table->addColumn('index_failed_id', 'int')->autoIncrement();
			$table->addColumn('content_type', 'varbinary', 25);
			$table->addColumn('content_id', 'int');
			$table->addColumn('action', 'varchar', 25);
			$table->addColumn('data', 'mediumblob');
			$table->addColumn('fail_count', 'smallint')->setDefault(0);
			$table->addColumn('reindex_date', 'int')->setDefault(0);
			$table->addUniqueKey(['content_type', 'content_id'], 'content_type');
			$table->addKey('reindex_date');
		});
	}

	public function upgrade1010010Step1()
	{
		$this->schemaManager()->createTable('xf_es_search_failed', function(Create $table)
		{
			$table->addColumn('search_failed_id', 'int')->autoIncrement();
			$table->addColumn('content_type', 'varbinary', 25);
			$table->addColumn('content_id', 'int');
			$table->addColumn('action', 'varchar', 25);
			$table->addColumn('data', 'mediumblob');
			$table->addColumn('fail_count', 'smallint')->setDefault(0);
			$table->addColumn('reindex_date', 'int')->setDefault(0);
			$table->addUniqueKey(['content_type', 'content_id'], 'content_type');
			$table->addKey('reindex_date');
		});
	}

	public function upgrade2000010Step1()
	{
		// convert the old options to the new structure
		$options = $this->app->options();

		$this->query("
			REPLACE INTO xf_option
				(option_id, option_value, default_value,
				edit_format, edit_format_params, data_type, sub_options,
				validation_class, validation_method, addon_id)
			VALUES
				('xfesEnabled', ?, '', 
				'onoff', '', 'boolean', '',
				 '', '', 'XFES')
		", $options->enableElasticsearch ? 1 : 0);

		$newConfigValue = [
			'host' => $options->elasticSearchServer['host'],
			'port' => $options->elasticSearchServer['port'],
			'https' => false,
			'username' => '',
			'password' => '',
			'index' => $options->elasticSearchIndex,
			'singleType' => isset($options->elasticSearchSingleType) ? $options->elasticSearchSingleType : false
		];

		$this->query("
			REPLACE INTO xf_option
				(option_id, option_value, default_value,
				 edit_format, edit_format_params, data_type, sub_options,
				validation_class, validation_method, addon_id)
			VALUES
				('xfesConfig', ?, '', 
				'template', '', 'array', '*',
				 '', '', 'XFES')
		", [json_encode($newConfigValue)]);

		if (isset($options->esRecencyWeightedRelevance))
		{
			$newRecencyValue = [
				'enabled' => $options->esRecencyWeightedRelevance['enabled'] ? true : false,
				'halfLife' => $options->esRecencyWeightedRelevance['halfLifeDays']
			];

			$this->query("
				REPLACE INTO xf_option
					(option_id, option_value, default_value,
					 edit_format, edit_format_params, data_type, sub_options,
					validation_class, validation_method, addon_id)
				VALUES
					('xfesRecencyRelevance', ?, '', 
					'template', '', 'array', '*',
					 '', '', 'XFES')
			", [json_encode($newRecencyValue)]);
		}

		$this->db()->emptyTable('xf_es_search_failed');

		$this->schemaManager()->alterTable('xf_es_search_failed', function(Alter $alter)
		{
			$alter->renameTo('xf_es_index_failed');
			$alter->renameColumn('search_failed_id', 'index_failed_id');
			$alter->changeColumn('reindex_date')->unsigned(true);
		});
	}

	public function postUpgrade($previousVersion, array &$stateChanges)
	{
		if ($previousVersion < 2000010)
		{
			$app = \XF::app();
			$options = $app->options();

			if (!empty($options->xfesEnabled) && !empty($options->xfesConfig))
			{
				$es = new \XFES\Elasticsearch\Api($options->xfesConfig);
				try
				{
					if (!$es->test())
					{
						// doesn't meet requirements, so we need to disable
						$app->repository('XF:Option')->updateOption('xfesEnabled', false);
					}
				}
				catch (\Exception $e) {}
			}

			$stateChanges['redirect'] = $app->router('admin')->buildLink('enhanced-search');
		}
	}

	public function uninstall(array $stepParams = [])
	{
		$this->schemaManager()->dropTable('xf_es_index_failed');
	}
}