<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Create;
use XF\Db\Schema\Alter;

/**
 * Class Setup
 * @package KL\UserImprovements
 */
class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    /**
     * ----------------
     *   INSTALLATION
     * ----------------
     */

    /* CREATE xf_kl_ui_username_changes */
    public function installStep1()
    {
        $this->schemaManager()->createTable(
            'xf_kl_ui_username_changes',
            function (Create $table) {
                $table->addColumn('change_id', 'INT', 10)->autoIncrement();
                $table->addColumn('user_id', 'INT', 10);
                $table->addColumn('old_username', 'VARCHAR', 50);
                $table->addColumn('change_date', 'INT', 10);
            }
        );
    }

    /* CREATE xf_kl_ui_user_disables */
    public function installStep2()
    {
        $this->schemaManager()->createTable(
            'xf_kl_ui_user_disables',
            function (Create $table) {
                $table->addColumn('user_id', 'INT', 10)->primaryKey();
                $table->addColumn('disable_date', 'INT', 10);
                $table->addColumn('latest_restore_date', 'INT', 10);
            }
        );
    }

    /* ALTER xf_user */
    public function installStep3()
    {
        $this->schemaManager()->alterTable(
            'xf_user',
            function (Alter $table) {
                $table->addColumn('kl_ui_name_color_id', 'TINYINT')->setDefault(0);
                $table->addColumn('kl_ui_view_count', 'INT')->setDefault(0);
            }
        );
    }

    /* ALTER xf_user_privacy */
    public function installStep4()
    {
        $this->schemaManager()->alterTable(
            'xf_user_privacy',
            function (Alter $table) {
                $table->addColumn('kl_ui_view_profile_stats', 'ENUM', ['everyone', 'members', 'followed', 'none'])->setDefault('everyone');
                $table->addColumn('kl_ui_view_username_changes', 'ENUM', ['everyone', 'members', 'followed', 'none'])->setDefault('everyone');
                $table->addColumn('kl_ui_view_profile_views', 'ENUM', ['everyone', 'members', 'followed', 'none'])->setDefault('everyone');
            }
        );
    }

    /* CREATE xf_kl_ui_trophy_category */
    public function installStep5()
    {
        $this->schemaManager()->createTable(
            'xf_kl_ui_trophy_category',
            function (Create $table) {
                $table->addColumn('trophy_category_id', 'VARCHAR', 50)->primaryKey();
                $table->addColumn('display_order', 'INT', 10);
            }
        );
    }

    /* ALTER xf_trophy */
    public function installStep6()
    {
        $this->schemaManager()->alterTable(
            'xf_trophy',
            function (Alter $table) {
                $table->addColumn('kl_ui_trophy_category_id', 'VARCHAR', 50)->setDefault('');
                $table->addColumn('kl_ui_icon_type', 'VARCHAR', 25)->setDefault('');
                $table->addColumn('kl_ui_icon_value', 'VARCHAR', 100)->setDefault('');
                $table->addColumn('kl_ui_hidden', 'TINYINT', 1)->setDefault(0);
                $table->addColumn('kl_ui_predecessor', 'INT', 10)->setDefault(0);
                $table->addColumn('kl_ui_follower', 'INT', 10)->setDefault(0);
                $table->addColumn('kl_ui_icon_css', 'BLOB')->nullable();
                $table->addColumn('kl_ui_percentage', 'FLOAT')->setDefault(0);
            }
        );
    }

    /* INSERT INTO xf_connected_account_provider */
    public function installStep7()
    {
        $this->db()->insertBulk('xf_connected_account_provider', [
            [
                'provider_id' => 'kl_battlenet',
                'provider_class' => 'KL\\UserImprovements:Provider\\BattleNet',
                'display_order' => 80,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_deviantart',
                'provider_class' => 'KL\\UserImprovements:Provider\\DeviantArt',
                'display_order' => 90,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_dropbox',
                'provider_class' => 'KL\\UserImprovements:Provider\\Dropbox',
                'display_order' => 100, 'options' => '[]'
            ],
            [
                'provider_id' => 'kl_discord',
                'provider_class' => 'KL\\UserImprovements:Provider\\Discord',
                'display_order' => 110,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_amazon',
                'provider_class' => 'KL\\UserImprovements:Provider\\Amazon',
                'display_order' => 120,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_reddit',
                'provider_class' => 'KL\\UserImprovements:Provider\\Reddit',
                'display_order' => 130,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_pinterest',
                'provider_class' => 'KL\\UserImprovements:Provider\\Pinterest',
                'display_order' => 140,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_instagram',
                'provider_class' => 'KL\\UserImprovements:Provider\\Instagram',
                'display_order' => 150,
                'options' => '[]'
            ],
            [
                'provider_id' => 'kl_twitch',
                'provider_class' => 'KL\\UserImprovements:Provider\\Twitch',
                'display_order' => 160,
                'options' => '[]'
            ]
        ], 'provider_id');
    }

    /* CREATE xf_kl_ui_user_view */
    public function installStep8()
    {
        $this->schemaManager()->createTable(
            'xf_kl_ui_user_view',
            function (Create $table) {
                $table->engine('MEMORY');

                $table->addColumn('user_id', 'int');
                $table->addColumn('total', 'int');
                $table->addPrimaryKey('user_id');
            }
        );
    }

    /* ALTER xf_user_trophy */
    public function installStep9()
    {
        $this->schemaManager()->alterTable(
            'xf_user_trophy',
            function (Alter $table) {
                $table->addColumn('kl_ui_showcased', 'BOOL')->setDefault(0);
            }
        );
    }

    /**
     * ----------------
     *     UPGRADES
     * ----------------
     */

    /* 1.0.1 */
    /* UPDATE xf_connected_account_provider */
    public function upgrade1000171Step1()
    {
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_amazon'], "provider_id = 'amazon'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_battlenet'], "provider_id = 'battlenet'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_deviantart'], "provider_id = 'deviantart'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_discord'], "provider_id = 'discord'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_dropbox'], "provider_id = 'dropbox'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_instagram'], "provider_id = 'instagram'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_pinterest'], "provider_id = 'pinterest'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_reddit'], "provider_id = 'reddit'");
        $this->db()->update('xf_connected_account_provider', ['provider_id' => 'kl_twitch'], "provider_id = 'twitch'");
    }

    /* UPDATE xf_user_connected_account */
    public function upgrade1000171Step2()
    {
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_amazon'], "provider = 'amazon'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_battlenet'], "provider = 'battlenet'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_deviantart'], "provider = 'deviantart'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_discord'], "provider = 'discord'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_dropbox'], "provider = 'dropbox'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_instagram'], "provider = 'instagram'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_pinterest'], "provider = 'pinterest'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_reddit'], "provider = 'reddit'");
        $this->db()->update('xf_user_connected_account', ['provider' => 'kl_twitch'], "provider = 'twitch'");
    }

    /* 1.0.2 */
    /* We're doing this the hard way */
    /* UPDATE xf_connected_account_provider */
    public function upgrade1000270Step1()
    {
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_amazon' WHERE `provider_id`='amazon'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_battlenet' WHERE `provider_id`='battlenet'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_deviantart' WHERE `provider_id`='deviantart'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_discord' WHERE `provider_id`='discord'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_dropbox' WHERE `provider_id`='dropbox'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_instagram' WHERE `provider_id`='instagram'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_pinterest' WHERE `provider_id`='pinterest'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_reddit' WHERE `provider_id`='reddit'");
        $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`='kl_twitch' WHERE `provider_id`='twitch'");
    }

    /* UPDATE xf_user_connected_account */
    public function upgrade1000270Step2()
    {
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_amazon' WHERE `provider`='amazon'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_battlenet' WHERE `provider`='battlenet'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_deviantart' WHERE `provider`='deviantart'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_discord' WHERE `provider`='discord'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_dropbox' WHERE `provider`='dropbox'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_instagram' WHERE `provider`='instagram'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_pinterest' WHERE `provider`='pinterest'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_reddit' WHERE `provider`='reddit'");
        $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`='kl_twitch' WHERE `provider`='twitch'");
    }

    /* UPDATE xf_connected_account_provider */
    /* UPDATE xf_user_connected_account */
    public function upgrade1000270Step3()
    {
        if (!$this->db()->fetchOne("SELECT * FROM `xf_connected_account_provider` WHERE `provider_id`='kl_amazon'")) {
            $this->db()->rawQuery("UPDATE `xf_connected_account_provider` SET `provider_id`=CONCAT('kl_',`provider_id`) WHERE `provider_id` NOT LIKE 'kl%' AND `provider_class` LIKE 'KL%'");
            $this->db()->rawQuery("UPDATE `xf_user_connected_account` SET `provider`=CONCAT('kl_',`provider`) WHERE `provider` NOT LIKE 'kl%' AND `provider` NOT IN (SELECT `provider_id` FROM `xf_connected_account_provider`)");
        }
    }

    /* 1.0.3 */
    /* Rebuild Connected Account Provider Cache */
    public function upgrade1000370Step1()
    {
        /** @var \XF\Repository\ConnectedAccount $repo */
        $repo = $this->app->em()->getRepository('XF:ConnectedAccount');

        $conAccs = $this->app->em()->getFinder('XF:UserConnectedAccount')
            ->where('provider', 'LIKE', 'kl%')
            ->fetch()->groupBy('user_id');

        foreach ($conAccs as $key => $group) {
            $firstEntry = array_shift($group);
            $repo->rebuildUserConnectedAccountCache($firstEntry->User);
        }
    }

    /* 1.0.4 */
    public function upgrade1000470Step1()
    {
        $this->schemaManager()->alterTable(
            'xf_user_trophy',
            function (Alter $table) {
                $table->addColumn('kl_ui_showcased', 'BOOL')->setDefault(0);
            }
        );
    }

    /* 1.1.0 */
    public function upgrade1010071Step1()
    {
        $this->schemaManager()->alterTable(
            'xf_trophy',
            function (Alter $table) {
                $table->addColumn('kl_ui_icon_css', 'BLOB');
            }
        );
    }

    /* 1.1.1 */
    public function upgrade1010072Step1() {
        $this->schemaManager()->alterTable(
            'xf_trophy',
            function (Alter $table) {
                $table->changeColumn('kl_ui_icon_css')->nullable();
            }
        );
    }

    /* 1.2.4 */
    public function upgrade1020470Step1() {
        $this->schemaManager()->alterTable(
            'xf_trophy',
            function (Alter $table) {
                $table->addColumn('kl_ui_percentage', 'FLOAT')->setDefault(0);
            }
        );
    }

    /**
     * ----------------
     *  UNINSTALLATION
     * ----------------
     */

    /* DROP xf_kl_ui_username_changes */
    public function uninstallStep1()
    {
        $this->schemaManager()->dropTable('xf_kl_ui_username_changes');
    }

    /* DROP xf_kl_ui_user_disables */
    public function uninstallStep2()
    {
        $this->schemaManager()->dropTable('xf_kl_ui_user_disables');
    }

    /* DROP xf_kl_ui_trophy_category */
    public function uninstallStep3()
    {
        $this->schemaManager()->dropTable('xf_kl_ui_trophy_category');
    }

    /* ALTER xf_user */
    public function uninstallStep4()
    {
        $this->schemaManager()->alterTable(
            'xf_user',
            function (Alter $table) {
                $table->dropColumns([
                    'kl_ui_name_color_id'
                ]);
            }
        );
    }

    /* ALTER xf_user_privacy */
    public function uninstallStep5()
    {
        $this->schemaManager()->alterTable(
            'xf_user_privacy',
            function (Alter $table) {
                $table->dropColumns([
                    'kl_ui_view_profile_stats',
                    'kl_ui_view_username_changes',
                    'kl_ui_view_profile_views'
                ]);
            }
        );
    }

    /* ALTER xf_trophy */
    public function uninstallStep6()
    {
        $this->schemaManager()->alterTable(
            'xf_trophy',
            function (Alter $table) {
                $table->dropColumns([
                    'kl_ui_trophy_category_id',
                    'kl_ui_icon_type',
                    'kl_ui_icon_value',
                    'kl_ui_hidden',
                    'kl_ui_predecessor',
                    'kl_ui_follower',
                    'kl_ui_icon_css',
                    'kl_ui_percentage'
                ]);
            }
        );
    }

    /* DELETE FROM xf_connected_account_provider */
    public function uninstallStep7()
    {
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_amazon'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_battlenet'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_deviantart'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_discord'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_dropbox'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_instagram'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_pinterest'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_reddit'");
        $this->db()->delete('xf_connected_account_provider', "provider_id = 'kl_twitch'");
        $this->db()->rawQuery("DELETE FROM `xf_connected_account_provider` WHERE `provider_id` LIKE 'kl\_%'");
        $this->db()->rawQuery("DELETE FROM `xf_connected_account_provider` WHERE `provider_class` LIKE 'KL\\UserImprovements:%'");
    }

    /* DELETE FROM xf_user_connected_account */
    public function uninstallStep8()
    {
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_amazon'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_battlenet'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_deviantart'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_discord'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_dropbox'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_instagram'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_pinterest'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_reddit'");
        $this->db()->delete('xf_user_connected_account', "provider = 'kl_twitch'");
        $this->db()->rawQuery("DELETE FROM `xf_user_connected_account` WHERE `provider` LIKE 'kl\_%'");
    }

    /* DROP xf_kl_ui_user_view */
    public function uninstallStep9()
    {
        $this->schemaManager()->dropTable('xf_kl_ui_user_view');
    }
}