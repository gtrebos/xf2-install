<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\ConnectedAccount\ProviderData;

use \XF\ConnectedAccount\ProviderData\AbstractProviderData;

/**
 * Class Dropbox
 * @package KL\UserImprovements\ConnectedAccount\ProviderData
 */
class Dropbox extends AbstractProviderData
{
    /**
     * @return string
     */
    public function getDefaultEndpoint()
    {
        return 'users/get_current_account';
    }

    /**
     * @return mixed|null
     */
    public function getProviderKey()
    {
        return $this->requestFromEndpoint('account_id', 'POST');
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->requestFromEndpoint('name', 'POST')['display_name'];
    }

    /**
     * @return string
     */
    public function getAvatarUrl()
    {
        $key = $this->getProviderKey();
        $time = \XF::$time;

        return "https://dl-web.dropbox.com/account_photo/get/{$key}?vers={$time}&size=512x512";
    }
}