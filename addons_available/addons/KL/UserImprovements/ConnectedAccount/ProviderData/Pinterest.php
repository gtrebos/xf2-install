<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\ConnectedAccount\ProviderData;

use \XF\ConnectedAccount\ProviderData\AbstractProviderData;

/**
 * Class Pinterest
 * @package KL\UserImprovements\ConnectedAccount\ProviderData
 */
class Pinterest extends AbstractProviderData
{
    /**
     * @return string
     */
    public function getDefaultEndpoint()
    {
        return 'v1/me';
    }

    /**
     * @return mixed
     */
    public function getProviderKey()
    {
        return $this->requestFromEndpoint('data')['id'];
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        $data = $this->requestFromEndpoint('data');

        return trim($data['first_name'] . ' ' . $data['last_name']);
    }
}