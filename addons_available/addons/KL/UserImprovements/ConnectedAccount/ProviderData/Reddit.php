<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\ConnectedAccount\ProviderData;

use \XF\ConnectedAccount\ProviderData\AbstractProviderData;

/**
 * Class Reddit
 * @package KL\UserImprovements\ConnectedAccount\ProviderData
 */
class Reddit extends AbstractProviderData
{
    /**
     * @return string
     */
    public function getDefaultEndpoint()
    {
        return 'api/v1/me';
    }

    /**
     * @return mixed|null
     */
    public function getProviderKey()
    {

        return $this->requestFromEndpoint('id');
    }

    /**
     * @return mixed|null
     */
    public function getUsername()
    {
        return $this->requestFromEndpoint('name');
    }
}