<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\ConnectedAccount\Provider;

use XF\Entity\ConnectedAccountProvider;
use XF\ConnectedAccount\Http\HttpResponseException;
use XF\ConnectedAccount\Provider\AbstractProvider;

/**
 * Class BattleNet
 * @package KL\UserImprovements\ConnectedAccount\Provider
 */
class BattleNet extends AbstractProvider {
    /**
     * @return string
     */
    public function getOAuthServiceName() {
		return 'KL\UserImprovements:Service\BattleNet';
	}

    /**
     * @return string
     */
    public function getProviderDataClass() {
		return 'KL\UserImprovements:ProviderData\BattleNet';
	}

    /**
     * @return array
     */
    public function getDefaultOptions() {
		return [
			'api_key' => '',
			'api_secret' => ''
		];
	}

    /**
     * @param ConnectedAccountProvider $provider
     * @param null $redirectUri
     * @return array
     */
    public function getOAuthConfig(ConnectedAccountProvider $provider, $redirectUri = null) {
		return [
			'key' => $provider->options['api_key'],
			'secret' => $provider->options['api_secret'],
			'scopes' => [],
			'redirect' => $redirectUri ?: $this->getRedirectUri($provider)
		];
	}

    /**
     * @param HttpResponseException $e
     * @param null $error
     */
    public function parseProviderError(HttpResponseException $e, &$error = null) {
		$response = json_decode($e->getResponseContent(), true);
		if (is_array($response) && isset($response['error']['message'])) {
			$e->setMessage($response['error']['message']);
		}
		parent::parseProviderError($e, $error);
	}
}