<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\ControllerPlugin;

/**
 * Class Error
 * @package KL\UserImprovements\ControllerPlugin
 */
class Error extends XFCP_Error
{
    /**
     * @return \XF\Mvc\Reply\Error|\XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \Exception
     * @throws \XF\PrintableException
     */
    public function actionDisabled()
    {
        /** @var \XF\Mvc\Entity\Finder $finder */
        $finder = \XF::app()->em()->getFinder('KL\UserImprovements:UserDisabled');

        /** @var \KL\UserImprovements\Entity\UserDisabled $record */
        $record = $finder->where('user_id', \XF::visitor()->user_id)->fetchOne();

        if ($record && $record->latest_restore_date >= \XF::$time) {
            if ($this->isPost()) {
                $record->delete();
                $visitor = \XF::visitor();
                $visitor->user_state = 'valid';
                $visitor->save();
                return $this->redirect($this->getDynamicRedirect());
            } else {
                $params = [
                    'record' => $record
                ];
                return $this->view('KL\UserImprovements:SelfReactivate', 'kl_ui_self_reactivate', $params);
            }
        }

        return parent::actionDisabled();
    }
}