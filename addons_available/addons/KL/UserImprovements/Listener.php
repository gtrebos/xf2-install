<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements;

use KL\UserImprovements\Entity\User;

/**
 * Class Listener
 * @package KL\UserImprovements
 */
class Listener
{
    /**
     * @param $rule
     * @param array $data
     * @param User $user
     * @param $returnValue
     * @return bool
     */
    public static function criteriaUser($rule, array $data, User $user, &$returnValue)
    {
        switch ($rule) {
            /* User name color */
            case 'kl_ui_username_color':
                if ($user->kl_ui_name_color_id) {
                    $returnValue = true;
                }
                break;

            /* User name color */
            case 'kl_ui_no_username_color':
                if (!$user->kl_ui_name_color_id) {
                    $returnValue = true;
                }
                break;


            /* User name changes */
            case 'kl_ui_min_username_changes':
                if ($user->KL_UI_UsernameHistory->count() >= $data['count']) {
                    $returnValue = true;
                }
                break;

            /* User name changes */
            case 'kl_ui_max_username_changes':
                if ($user->KL_UI_UsernameHistory->count() <= $data['count']) {
                    $returnValue = true;
                }
                break;

            /* Profile views */
            case 'kl_ui_min_profile_views':
                if ($user->kl_ui_view_count >= $data['count']) {
                    $returnValue = true;
                }
                break;

            /* Max profile views */
            case 'kl_ui_max_profile_views':
                if ($user->kl_ui_view_count < $data['count']) {
                    $returnValue = true;
                }
                break;
        }
    }
}