<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class Trophy
 * @package KL\UserImprovements\Entity
 *
 * @property int kl_ui_trophy_category_id
 * @property string kl_ui_icon_type
 * @property string kl_ui_icon_value
 * @property bool kl_ui_hidden
 * @property int kl_ui_predecessor
 * @property int kl_ui_follower
 */
class Trophy extends XFCP_Trophy
{
    /**
     * @throws \Exception
     * @throws \XF\PrintableException
     */
    protected function _postSave()
    {
        parent::_postSave();

        /* Reset predecessor */

        /** @var \KL\UserImprovements\Entity\Trophy $trophy */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('kl_ui_follower', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->kl_ui_follower = 0;
            $trophy->save();
        }

        /* Add trophy to predecessor */
        if ($this->kl_ui_predecessor) {
            $trophy = \XF::app()->em()->getFinder('XF:Trophy')
                ->where('trophy_id', $this->kl_ui_predecessor)
                ->fetchOne();
            $trophy->kl_ui_follower = $this->trophy_id;
            $trophy->save();
        }
    }

    /**
     * @throws \Exception
     * @throws \XF\PrintableException
     */
    protected function _preDelete()
    {
        parent::_preDelete();

        /* Remove trophy from predecessor */
        /** @var \KL\UserImprovements\Entity\Trophy $trophy */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('kl_ui_follower', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->kl_ui_follower = 0;
            $trophy->save();
        }


        /* Remove Trophy from follower */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('kl_ui_predecessor', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->kl_ui_predecessor = 0;
            $trophy->save();
        }
    }

    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns = array_merge($structure->columns, [
            'kl_ui_trophy_category_id' => ['type' => self::STR, 'default' => '', 'maxLength' => 50],
            'kl_ui_icon_type' => ['type' => self::STR, 'default' => '', 'maxLength' => 25],
            'kl_ui_icon_value' => ['type' => self::STR, 'default' => '', 'maxLength' => 100],
            'kl_ui_hidden' => ['type' => self::UINT, 'default' => 0],
            'kl_ui_predecessor' => ['type' => self::UINT, 'default' => 0],
            'kl_ui_follower' => ['type' => self::UINT, 'default' => 0],
            'kl_ui_icon_css' => ['type' => self::STR, 'default' => '']
        ]);

        return $structure;
    }
}