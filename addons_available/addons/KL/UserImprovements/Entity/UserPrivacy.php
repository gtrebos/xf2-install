<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class UserPrivacy
 * @package KL\UserImprovements\Entity
 *
 * @property string kl_ui_view_username_changes
 * @property string kl_ui_view_profile_stats
 */
class UserPrivacy extends XFCP_UserPrivacy
{
    /**
     *
     */
    protected function _setupDefaults()
    {
        parent::_setupDefaults();

        $this->kl_ui_view_profile_stats = 'everyone';
        $this->kl_ui_view_username_changes = 'members';
        $this->kl_ui_view_profile_views = 'followed';
    }

    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns = array_merge($structure->columns, [
            'kl_ui_view_profile_stats' => [
                'type' => self::STR, 'default' => 'everyone',
                'allowedValues' => ['everyone', 'members', 'followed', 'none'],
                'verify' => 'verifyPrivacyChoice'
            ],
            'kl_ui_view_username_changes' => [
                'type' => self::STR, 'default' => 'members',
                'allowedValues' => ['everyone', 'members', 'followed', 'none'],
                'verify' => 'verifyPrivacyChoice'
            ],
            'kl_ui_view_profile_views' => [
                'type' => self::STR, 'default' => 'members',
                'allowedValues' => ['everyone', 'members', 'followed', 'none'],
                'verify' => 'verifyPrivacyChoice'
            ]
        ]);

        return $structure;
    }
}