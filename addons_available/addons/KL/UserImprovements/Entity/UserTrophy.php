<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class UserTrophy
 * @package KL\UserImprovements\Entity
 *
 * @property boolean kl_ui_showcased
 */
class UserTrophy extends XFCP_UserTrophy
{
    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns = array_merge($structure->columns, [
            'kl_ui_showcased' => ['type' => self::BOOL, 'default' => 0]
        ]);

        return $structure;
    }
}