<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Entity;

use XF\Mvc\Entity\ArrayCollection;
use XF\Mvc\Entity\Structure;

/**
 * Class User
 * @package KL\UserImprovements\Entity
 *
 * @property int kl_ui_name_color_id
 * @property int kl_ui_view_count
 * @property ArrayCollection Trophies
 * @property ArrayCollection|null KL_UI_UsernameHistory
 */
class User extends XFCP_User
{
    /**
     * @return bool
     */
    public function canViewProfileStatsBar()
    {
        $visitor = \XF::visitor();

        return (($this->user_id == $visitor->user_id || $this->isPrivacyCheckMet('kl_ui_view_profile_stats', $visitor))
                && $this->user_state != 'disabled')
            || $visitor->hasPermission('klUIModerator', 'klUIBypassPSBPrivacy')
            || !$this->hasPermission('klUI', 'klUIManageStatsPrivacy');
    }

    /**
     * @return bool
     */
    public function canViewUsernameHistory()
    {
        $visitor = \XF::visitor();

        return ($visitor->hasPermission('klUI', 'klUIViewUsernameChanges')
                && ($this->user_id == $visitor->user_id || $this->isPrivacyCheckMet('kl_ui_view_username_changes', $visitor))
                && $this->user_state != 'disabled')
            || $visitor->hasPermission('klUIModerator', 'klUIBypassUNCPrivacy')
            || !$this->hasPermission('klUI', 'klUIManageUsernamePrivacy');
    }

    /**
     * @return bool
     */
    public function canViewProfileViewCount()
    {
        $visitor = \XF::visitor();

        return $this->isPrivacyCheckMet('kl_ui_view_profile_views', $visitor);
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canViewFullProfile(&$error = null)
    {
        if ($this->user_state === "disabled" && !\XF::visitor()->hasPermission('klUIModerator', 'klUISeeDeactivatedProfile')) {
            $error = \XF::phraseDeferred('kl_ui_account_has_been_deactivated');
            return false;
        } else {
            return parent::canViewFullProfile($error);
        }
    }

    /**
     * @param null $position
     * @return bool|int
     */
    public function getTrophyShowcaseSize($position = null)
    {
        if (is_null($position)) {
            $a1 = $this->hasPermission('klUI', "klUITSS_profile");
            $a2 = $this->hasPermission('klUI', "klUITSS_postbit");
            $amount = max($a1, $a2);
        } else {
            $amount = $this->hasPermission('klUI', "klUITSS_{$position}");
        }

        if ($amount == -1) {
            return PHP_INT_MAX;
        } else {
            return $amount;
        }
    }

    /**
     * @param null $position
     * @return ArrayCollection
     */
    public function getLatestTrophies($position = null)
    {
        $finder = $this->em()->getFinder('XF:UserTrophy')
            ->where('user_id', $this->user_id)
            ->with('Trophy')
            ->order('award_date', 'DESC')
            ->limit($this->getTrophyShowcaseSize($position));

        return $finder->fetch();
    }

    /**
     * @param null $position
     * @return ArrayCollection
     */
    public function getHighestTrophies($position = null)
    {
        $finder = $this->em()->getFinder('XF:UserTrophy')
            ->where('user_id', $this->user_id)
            ->with('Trophy')
            ->order('Trophy.trophy_points', 'DESC')
            ->limit($this->getTrophyShowcaseSize($position));

        return $finder->fetch();
    }

    /**
     * @param null $position
     * @return ArrayCollection
     */
    public function getUserChoiceTrophies($position = null)
    {
        $finder = $this->em()->getFinder('XF:UserTrophy')
            ->where('user_id', $this->user_id)
            ->with('Trophy')
            ->where('kl_ui_showcased', true)
            ->limit($this->getTrophyShowcaseSize($position));

        $trophies = $finder->fetch();

        if (!$trophies->count()) {
            return $this->getLatestTrophies();
        }

        return $trophies;
    }

    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns = array_merge($structure->columns, [
            'kl_ui_name_color_id' => ['type' => self::UINT, 'max' => 27, 'default' => 0, 'changeLog' => false],
            'kl_ui_view_count' => ['type' => self::UINT, 'default' => 0]
        ]);

        $structure->relations = array_merge($structure->relations, [
            'Trophies' => [
                'entity' => 'XF:UserTrophy',
                'type' => self::TO_MANY,
                'conditions' => 'user_id',
                'key' => 'trophy_id'
            ],
            'KL_UI_UsernameHistory' => [
                'entity' => 'KL\UserImprovements:UsernameChange',
                'type' => self::TO_MANY,
                'conditions' => 'user_id',
                'primary' => true
            ]
        ]);

        return $structure;
    }
}