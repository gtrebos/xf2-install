<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Admin\Controller;

use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;
use \XF\Mvc\Reply\View;
use \KL\UserImprovements\Entity\TrophyCategory;

/**
 * Class Trophy
 * @package KL\UserImprovements\Admin\Controller
 */
class Trophy extends XFCP_Trophy {
    /**
     * @return View
     */
    public function actionIndex() {
		$view = parent::actionIndex();

		if($view instanceof View) {
			$categoryRepo = $this->getTrophyCategoryRepo();

			$trophies = $view->getParam('trophies')->groupBy('kl_ui_trophy_category_id');
			$categoryFinder = $categoryRepo->findTrophyCategoriesForList();
			$categories = $categoryFinder->fetch();

			if(isset($trophies[''])) {
				$trophies['uncategorized'] = $trophies[''];
				unset($trophies['']);
			}

			$view->setParam('trophies', $trophies);
			$view->setParam('trophyCategories', $categories);
		}

		return $view;
	}

    /**
     * @param \XF\Entity\Trophy $trophy
     * @return FormAction
     */
    protected function trophySaveProcess(\XF\Entity\Trophy $trophy) {
		$form = parent::trophySaveProcess($trophy);

        $trophyInput = $this->filter([
			'kl_ui_trophy_category_id' => 'str',
			'kl_ui_icon_type' => 'str',
			'kl_ui_hidden' => 'uint',
			'kl_ui_predecessor' => 'uint',
            'kl_ui_icon_css' => 'str'
		]);

		if($trophyInput['kl_ui_predecessor']) {
            /** @var \KL\UserImprovements\Entity\Trophy $predecessor */
            $predecessor = $this->assertTrophyExists($trophyInput['kl_ui_predecessor']);
			if($predecessor) {
				$trophyInput['kl_ui_trophy_category_id'] = $predecessor->kl_ui_trophy_category_id;
			}
			else {
				$tropyInput['kl_ui_predecessor'] = 0;
			}
		}

		if ($trophyInput['kl_ui_icon_type'] == 'fa') {
			$trophyInput['kl_ui_icon_value'] = $this->filter('kl_ui_icon_fa', 'str');
		}
		else if ($trophyInput['kl_ui_icon_type'] == 'image') {
			$trophyInput['kl_ui_icon_value'] = $this->filter('kl_ui_icon_image', 'str');
		}
		else {
			$trophyInput['kl_ui_icon_value'] = '';
		}

		$form->basicEntitySave($trophy, $trophyInput);

		return $form;
	}

    /**
     * @param \XF\Entity\Trophy $trophy
     * @return View
     */
    protected function trophyAddEdit(\XF\Entity\Trophy $trophy) {
		$view = parent::trophyAddEdit($trophy);

		if($view instanceof View) {
			$category = $this->filter(['trophy_category_id' => 'STR']);

			$trophy = $view->getParam('trophy');
			if($category['trophy_category_id'] && !$trophy->kl_ui_trophy_category_id) {
				$trophy->kl_ui_trophy_category_id = $category['trophy_category_id'];
			}

			$categoryRepo = $this->getTrophyCategoryRepo();
			/** @var \XF\Mvc\Entity\Finder $categoryFinder */
			$categoryFinder = $categoryRepo->findTrophyCategoriesForList();
			$categories = $categoryFinder->fetch();

			$trophyRepo = $this->getTrophyRepo();
			$trophyFinder = $trophyRepo->findTrophiesForList();

			$trophyFinder
				->where('kl_ui_predecessor', '!=', $trophy->trophy_id)
				->where('trophy_id', '!=', $trophy->trophy_id);

			$trophies = $trophyFinder->fetch();

			$view->setParam('categories', $categories);
			$view->setParam('trophies', $trophies);
		}

		return $view;
	}

    /**
     * @param TrophyCategory $category
     * @return View
     */
    protected function trophyCategoryAddEdit(TrophyCategory $category) {
		$viewParams = [
			'category' => $category
		];

		return $this->view('KL\UserImprovements:TrophyCategory\Edit', 'kl_ui_trophy_category_edit', $viewParams);
	}

    /**
     * @param ParameterBag $params
     * @return View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionCategoryEdit(ParameterBag $params) {
        /** @var \KL\UserImprovements\Entity\TrophyCategory $category */
        $category = $this->assertTrophyCategoryExists($params->trophy_category_id);

		return $this->trophyCategoryAddEdit($category);
	}

    /**
     * @return View
     */
    public function actionCategoryAdd() {
        /** @var \KL\UserImprovements\Entity\TrophyCategory $category */
        $category = $this->em()->create('KL\UserImprovements:TrophyCategory');

		return $this->trophyCategoryAddEdit($category);
	}

    /**
     * @param TrophyCategory $category
     * @return FormAction
     */
    protected function trophyCategorySaveProcess(TrophyCategory $category) {
		$form = $this->formAction();

		$categoryInput = $this->filter([
			'trophy_category_id' => 'str',
			'display_order' => 'uint'
		]);

		$form->basicEntitySave($category, $categoryInput);

		$phraseInput = $this->filter([
			'title' => 'str'
		]);

		$form->validate(function(FormAction $form) use ($phraseInput) {
			if ($phraseInput['title'] === '') {
				$form->logError(\XF::phrase('please_enter_valid_title'), 'title');
			}
		});

		$form->apply(function() use ($phraseInput, $category) {
			$masterTitle = $category->getMasterPhrase();
			$masterTitle->phrase_text = $phraseInput['title'];
			$masterTitle->save();
		});

		return $form;
	}

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect
     * @throws \XF\Mvc\Reply\Exception
     * @throws \XF\PrintableException
     */
    public function actionCategorySave(ParameterBag $params) {
		$this->assertPostOnly();

		if ($params->trophy_category_id) {
            /** @var \KL\UserImprovements\Entity\TrophyCategory $category */
			$category = $this->assertTrophyCategoryExists($params->trophy_category_id);
		}
		else {
            /** @var \KL\UserImprovements\Entity\TrophyCategory $category */
            $category = $this->em()->create('KL\UserImprovements:TrophyCategory');
		}

		$this->trophyCategorySaveProcess($category)->run();

		return $this->redirect($this->buildLink('trophies') . $this->buildLinkHash($category->trophy_category_id));
	}

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Error|\XF\Mvc\Reply\Redirect|View
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionCategoryDelete(ParameterBag $params) {
		$category = $this->assertTrophyCategoryExists($params->trophy_category_id);
		if (!$category->preDelete()) {
			return $this->error($category->getErrors());
		}

		if ($this->isPost()) {
			$category->delete();
			return $this->redirect($this->buildLink('trophies'));
		}
		else {
			$viewParams = [
				'category' => $category
			];

			return $this->view('KL\UserImprovements:Category\Delete', 'kl_ui_trophy_category_delete', $viewParams);
		}
	}

    /**
     * @return \KL\UserImprovements\Repository\TrophyCategory
     */
    protected function getTrophyCategoryRepo() {
        /** @var \KL\UserImprovements\Repository\TrophyCategory $repo */
        $repo = $this->repository('KL\UserImprovements:TrophyCategory');
		return $repo;
	}

    /**
     * @param string $id
     * @param array|string|null $with
     * @param null|string $phraseKey
     *
     * @return \KL\UserImprovements\Entity\Trophy
     * @throws \XF\Mvc\Reply\Exception
     */
	protected function assertTrophyCategoryExists($id, $with = null, $phraseKey = null) {
        /** @var \KL\UserImprovements\Entity\Trophy $trophy */
        $trophy = $this->assertRecordExists('KL\UserImprovements:TrophyCategory', $id, $with, $phraseKey);
		return $trophy;
	}
}