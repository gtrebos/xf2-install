<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Admin\Controller\Trophy;

use XF\Admin\Controller\AbstractController;

/**
 * Class Reward
 * @package KL\UserImprovements\Admin\Controller
 */
class Reward extends AbstractController {
    /**
     * @return \XF\Mvc\Reply\View
     */
    public function actionIndex() {

        $categoryRepo = $this->getTrophyCategoryRepo();
        $trophyRepo = $this->getTrophyRepo();

        $trophies = $trophyRepo->findTrophiesForList()->fetch()->groupBy('kl_ui_trophy_category_id');
        $categoryFinder = $categoryRepo->findTrophyCategoriesForList();
        $categories = $categoryFinder->fetch();

        if(isset($trophies[''])) {
            $trophies['uncategorized'] = $trophies[''];
            unset($trophies['']);
        }

        $viewParams = [
            'trophies' => $trophies,
            'categories' => $categories
        ];

        return $this->view('KL\UserImprovements:Trophy\Reward', 'kl_ui_reward_trophy', $viewParams);
    }

    public function actionSave() {
        $input = $this->filter([
            'users' => 'str',
            'trophies' => 'array-int'
        ]);

        $users = explode(',', $input['users']);
        $users = array_map('trim', $users);
        $users = array_filter($users);
        /** @var \XF\Repository\User $userRepo */
        $userRepo = $this->repository('XF:User');
        $users = $userRepo->getUsersByNames($users);

        $trophyRepo = $this->getTrophyRepo();

        $trophies = $this->finder('XF:Trophy')
            ->where('trophy_id', $input['trophies'])
            ->fetch();

        foreach($users as $user) {
            foreach($trophies as $trophy) {
                $trophyRepo->awardTrophyToUser($trophy, $user);
            }
        }

        return $this->redirect($this->buildLink('trophies/reward'), \XF::phrase('kl_ui_trophies_rewarded'));
    }

    /**
     * @return \KL\UserImprovements\Repository\TrophyCategory
     */
    protected function getTrophyCategoryRepo() {
        /** @var \KL\UserImprovements\Repository\TrophyCategory $repo */
        $repo = $this->repository('KL\UserImprovements:TrophyCategory');

        return $repo;
    }

    /**
     * @return \XF\Repository\Trophy
     */
    protected function getTrophyRepo() {
        /** @var \XF\Repository\Trophy $repo */
        $repo = $this->repository('XF:Trophy');

        return $repo;
    }
}