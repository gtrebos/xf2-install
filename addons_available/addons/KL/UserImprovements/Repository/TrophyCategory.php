<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Repository;

use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;

/**
 * Class TrophyCategory
 * @package KL\UserImprovements\Repository
 */
class TrophyCategory extends Repository
{
    /**
     * @return Finder
     */
    public function findTrophyCategoriesForList()
    {
        return $this->finder('KL\UserImprovements:TrophyCategory')->order('display_order');
    }
}