<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Repository;

use XF\Mvc\Entity\Repository;

/**
 * Class User
 * @package KL\UserImprovements\Repository
 */
class User extends Repository
{

    /**
     *
     */
    public function batchUpdateProfileViews()
    {
        $db = $this->db();
        $db->query("
			UPDATE
				xf_user AS u
			INNER JOIN
				xf_kl_ui_user_view AS uv
			ON
				(u.user_id = uv.user_id)
			SET
				u.kl_ui_view_count = u.kl_ui_view_count + uv.total
		");
        $db->emptyTable('xf_kl_ui_user_view');
    }

    public function logProfileView(\XF\Entity\User $user)
    {
        $this->db()->query("
			INSERT INTO xf_kl_ui_user_view
				(user_id, total)
			VALUES
				(? , 1)
			ON DUPLICATE KEY UPDATE
				total = total + 1
		", $user->user_id);
    }

    /**
     * Set the profile view counter back to 0.
     *
     * @param \XF\Entity\User $user
     */
    public function resetProfileViews(\XF\Entity\User $user)
    {
        /** @var \KL\UserImprovements\Entity\User $user */
        $user->kl_ui_view_count = 0;
        $user->saveIfChanged();

        $this->db()->query("
			DELETE FROM
				xf_kl_ui_user_view
			WHERE
				user_id = ?
		", $user->user_id);
    }
}