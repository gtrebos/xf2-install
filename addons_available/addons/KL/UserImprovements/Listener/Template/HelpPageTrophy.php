<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Listener\Template;

use XF\Template\Templater;

/**
 * Class HelpPageTrophy
 * @package KL\UserImprovements\Listener\Template
 */
class HelpPageTrophy
{
    /**
     * @param Templater $templater
     * @param $type
     * @param $template
     * @param array $params
     */
    public static function extend(Templater $templater, &$type, &$template, array &$params)
    {
        /** @var \XF\Repository\Trophy $trophyRepo */
        $trophyRepo = \XF::app()->em()->getRepository('XF:Trophy');
        $userId = \XF::visitor()->user_id;
        $userTrophies = $trophyRepo->findUserTrophies($userId)->fetch();

        $params['trophyCategories'] = \XF::app()->em()->getFinder('KL\UserImprovements:TrophyCategory')->order('display_order')->fetch();

        $trophies = [];

        foreach ($params['trophies'] as $trophyId => $trophy) {
            $cTrophy = [
                'earned' => isset($userTrophies["{$userId}-{$trophyId}"]),
                'level' => 0,
                'max_level' => 0,
                'entity' => $trophy,
                'predecessors' => [],
                'followers' => [],
                'predecessors_ids' => [],
                'followers_ids' => []
            ];

            if ($cTrophy['earned'] && $trophy->kl_ui_predecessor) {
                $pred = $trophy;
                while (
                    $pred->kl_ui_predecessor
                    && $pred = $params['trophies'][$pred->kl_ui_predecessor]
                ) {
                    $cTrophy['predecessors'][] = $pred;
                    $cTrophy['predecessors_ids'][] = $pred->trophy_id;
                }
            }
            if (($cTrophy['earned'] || !$trophy->kl_ui_predecessor) && $trophy->kl_ui_follower) {
                $follower = $trophy;
                while (
                    $follower->kl_ui_follower
                    && $follower = $params['trophies'][$follower->kl_ui_follower]
                ) {
                    $cTrophy['followers'][] = $follower;
                    $cTrophy['followers_ids'][] = $follower->trophy_id;
                }
            }


            $trophies[$trophyId] = $cTrophy;
        }

        foreach ($trophies as $trophy) {
            if ($trophy['earned']) {
                foreach ($trophy['predecessors_ids'] as $predId) {
                    unset($trophies[$predId]);
                }
            }

            if ($trophy['earned'] || !$trophy['entity']->kl_ui_predecessor) {
                foreach ($trophy['followers_ids'] as $follId) {
                    if (
                        isset($trophies[$follId]) &&
                        !$trophies[$follId]['earned']
                    ) {
                        unset($trophies[$follId]);
                    }
                }
            }
        }

        foreach ($trophies as &$trophy) {
            $trophy['level'] = count($trophy['predecessors']) + 1;
            $trophy['max_level'] = $trophy['level'] + count($trophy['followers']);
            usort($trophy['followers'], function ($a, $b) {
                return $a['trophy_points'] > $b['trophy_points'];
            });
            usort($trophy['predecessors'], function ($a, $b) {
                return $a['trophy_points'] > $b['trophy_points'];
            });
        }

        $trophiesCategorized = [];

        foreach ($trophies as $value) {
            $trophiesCategorized[$value['entity']->kl_ui_trophy_category_id ?: 'uncategorized'][] = $value;
        }

        $params['trophies'] = $trophiesCategorized;
    }
}