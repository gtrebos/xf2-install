<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Listener\Template;

use XF\Template\Templater;

/**
 * Class MemberAboutTrophies
 * @package KL\UserImprovements\Listener\Template
 */
class MemberAboutTrophies
{
    /**
     * @param Templater $templater
     * @param $type
     * @param $template
     * @param array $params
     */
    public static function extend(Templater $templater, &$type, &$template, array &$params)
    {
        $trophies = $params['trophies'];
        $categorizedTrophies = ['uncategorized' => []];

        foreach ($trophies as $value) {
            $follId = $value->Trophy->kl_ui_follower;
            $user = $params['user']->user_id;
            if (!$follId || !isset($trophies["{$user}-{$follId}"])) {
                if ($value->Trophy->kl_ui_trophy_category_id === '') {
                    $categorizedTrophies['uncategorized'][] = $value;
                } else {
                    $categorizedTrophies[$value->Trophy->kl_ui_trophy_category_id][] = $value;
                }
            }
        }

        $params['trophies'] = $categorizedTrophies;
        $params['trophyCategories'] = \XF::app()->em()->getFinder('KL\UserImprovements:TrophyCategory')->order('display_order')->fetch();
    }
}