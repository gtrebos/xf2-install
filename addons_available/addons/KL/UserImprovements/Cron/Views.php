<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Cron;

/**
 * Class Views
 * @package KL\UserImprovements\Cron
 */
class Views
{
    /**
     *
     */
    public static function runViewUpdate()
    {
        $app = \XF::app();

        /** @var \KL\userImprovements\Repository\User $userRepo */
        $userRepo = $app->repository('KL\UserImprovements:User');
        $userRepo->batchUpdateProfileViews();
    }
}