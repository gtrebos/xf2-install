<?php

/**
 * License https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
 * Copyright 2017-2018 Lukas Wieditz
 */

namespace KL\UserImprovements\Template;

use XF\Entity\User;

/**
 * Class Templater
 * @package KL\UserImprovements\Template
 */
class Templater extends XFCP_Templater
{
    /**
     * Extend the username templater function to support username colors.
     *
     * @param $templater
     * @param $escape
     * @param $user
     * @param bool $includeGroupStyling
     * @return string
     */
    public function fnUsernameClasses($templater, &$escape, $user, $includeGroupStyling = true)
    {
        $classes = parent::fnUsernameClasses($templater, $escape, $user, $includeGroupStyling);

        if ($user instanceof User) {
            if ($user->hasPermission('klUI', 'klUIChoseUsernameColor')) {
                /** @var \KL\UserImprovements\Entity\User $user */
                $classes .= " username--color-" . ($user->kl_ui_name_color_id ?: 0);
            }
        }

        if (!empty($user['user_state']) && $user['user_state'] == 'disabled') {
            $classes .= ' username--deactivated';
        }

        return trim($classes);
    }
}