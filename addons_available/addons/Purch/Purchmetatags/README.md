# Purchmetatags #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Set the titles and/or meta description of some pages (threads, categories, tags).

## The contents of this add-on are :  ##
* A new template *purch_head_tmnhead*
* A code event listener on the *templater_template_pre_render* event (*Listener.php*)
* XML files for installing options and phrases (*options.xml* , *phrases.xml*)

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.

## Additional Configuration ##
* Configuration and setup options are available at *Anandtech Xenforo Admin CP* at *Setup->Options->Purch Meta tags Management Options*
* You can set the title and/or description of different kinds of pages. Furthermore, there are some variables you can use in the fields: they are explicitly listed in each field description.
For example, to set the meta description of each 2+ page of a tag, you can enter *"Here is the page #[page] of tag #[tag] | welcome to #[boardTitle]".*
If you want to leave the title/description to its original value, just leave the field blank.
