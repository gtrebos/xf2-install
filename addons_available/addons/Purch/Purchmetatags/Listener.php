<?php

namespace Purch\Purchmetatags;

class Listener
{

  public static function templaterTemplatePreRender(
  \XF\Template\Templater $templater,
  &$type,
  &$template,
  array &$params
  ) {
      if($template == 'PAGE_CONTAINER')
      {
        $uri = isset($params['xf']) && isset($params['xf']['uri']) ? $params['xf']['uri'] : "";
        $page_section = isset($params['pageSection']) ? $params['pageSection'] : "";
        $page_number = isset($params['pageNumber']) ? $params['pageNumber'] : 1;
        $tag = isset($params['tag']) ? $params['tag'] : null;
        $page_title = isset($params['pageTitle']) ? $params['pageTitle'] : null;

        $metaTags = self::getMetaTags($uri, $page_section, $page_title, $page_number, $tag);

        // the "option" that gives us the description is sometimes blank - hmm
        if($metaTags['description'])
        {
          unset($params['head']['meta_description']);
          $params['pageDescriptionMeta'] = 1;
          $params['purchCustomDescription'] = $metaTags['description'];

        }
        if($metaTags['title'])
        {
          $params['purchCustomTitle'] = $metaTags['title'];
        }
      }

  }

  private static function _buildParams($title, $description) {
    return array('title' => $title, 'description' => $description);
  }

  private static function _parseDescription($description, $args) {
    foreach ($args as $key => $val) {
      $description = str_replace("#[$key]", $val, $description);
    }
    return $description;
  }

  //~ need to in depth test to make sure we are only overrwriting meta description and not page descriptions
  private static function getMetaTags($uri, $page_section, $page_title, $page_number, $tag) {
    ///exit ut if we don't know where we are
    if ($uri == "" && $page_section == "") {
      return self::_buildParams(false, false);
    }

    $options = \XF::options();
    $boardTitle = $options['boardTitle'];
    $description = false;
    $title = false;

    if ( strpos($uri, "tags") !== false || strpos($page_section, "tags") !== false ) {
      if ($tag) {
        if ($page_number == 1) {
          $description = self::_parseDescription($options['purch_purchmetatags_description_tags'], ['tag' => $tag, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_tags'], ['tag' => $tag, 'boardTitle' => $boardTitle]);
        } else {
          $description = self::_parseDescription($options['purch_purchmetatags_description_tags_page'], ['tag' => $tag, 'page' => $page_number, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_tags_page'], ['tag' => $tag, 'page' => $page_number, 'boardTitle' => $boardTitle]);
        }
      } else {
        $description = self::_parseDescription($options['purch_purchmetatags_description_tags_search'], ['boardTitle' => $boardTitle]);
        $title = $options['purch_purchmetatags_title_tags_search'];
      }
    } elseif (strpos($uri, "categories") !== false || strpos($uri, "forums") !== false || strpos($page_section, "forums") !== false || strpos($page_section, "forums") !== false) {
      if ($page_title) {
        if ($page_number == 1) {
          $description = self::_parseDescription($options['purch_purchmetatags_description_category'], ['category' => $page_title, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_category'], ['category' => $page_title, 'boardTitle' => $boardTitle]);
        } else {
          $description = self::_parseDescription($options['purch_purchmetatags_description_category_page'], ['category' => $page_title, 'page' => $page_number, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_category_page'], ['category' => $page_title, 'page' => $page_number, 'boardTitle' => $boardTitle]);
        }
      }
    } elseif ( (strpos($uri, "threads") !== false || strpos($page_section, "threads") !== false) && (strpos($uri, 'threads/multi-quote') === false && strpos($page_section, 'threads/multi-quote') === false) ) {
      if ($page_title) {
        if ($page_number == 1) {
          $description = self::_parseDescription($options['purch_purchmetatags_description_thread'], ['thread' => $page_title, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_thread'], ['thread' => $page_title, 'boardTitle' => $boardTitle]);
        } else {
          $description = self::_parseDescription($options['purch_purchmetatags_description_thread_page'], ['thread' => $page_title, 'page' => $page_number, 'boardTitle' => $boardTitle]);
          $title = self::_parseDescription($options['purch_purchmetatags_title_thread_page'], ['thread' => $page_title, 'page' => $page_number, 'boardTitle' => $boardTitle]);
        }
      }
    }

    return self::_buildParams($title, $description);
  }


}
