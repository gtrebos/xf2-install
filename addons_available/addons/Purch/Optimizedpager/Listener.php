<?php

namespace Purch\Optimizedpager;

class Listener
{

  public static function templaterTemplatePreRender(
  \XF\Template\Templater $templater,
  &$type,
  &$template,
  array &$params
  ) {

    ##if the param is already set then we don't need to redo the logic
    if(!isset($params['purch_optimized_pager_lines']))
    {
      $lines = [];
      $params['purch_optimized_pager_lines'] = [];
      //var_dump($params);
      if($template == 'page_nav')
      {
        $currentPage = $params['current'];
        $pageTotal = $params['totalPages'];
        
        $coeff = 1;
        $while_count=0;
        while ($pageTotal >= $coeff) {
          $line = [];
          $base = intdiv($currentPage, $coeff * 10) * $coeff * 10;

          $init = $base + $coeff;
          if (empty($lines) && $base + $coeff !== 1) {
            $init = $base;
          }

          $i_count = 0;
          for ($i = $init; $i <= $base + 9 * $coeff && $i <= $pageTotal; $i += $coeff) {
            $i_count++;
            if($while_count == 1 && $i_count==1){ continue; }//skip the first one of the second row
            $pageInfos = [
            'page' => $i
            ];

            $line[] = $pageInfos;
          }

          if($while_count > 0 || $pageTotal<10)
          {
            $lines[] = $line;
          } //don't want the first line

          $while_count++;
          $coeff *= 10;
        }
        $params['purch_optimized_pager_lines'] = $lines;
      }
    }

  }
}
