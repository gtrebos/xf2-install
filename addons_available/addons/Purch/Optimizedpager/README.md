# Optimizedpager #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Paginate results on threads and add a custom nav links to pages

## The contents of this add-on are :  ##

* A template modification of page_nav.
* A code event listener on the *templater_template_pre_render* event (*Listener.php*)


## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.
