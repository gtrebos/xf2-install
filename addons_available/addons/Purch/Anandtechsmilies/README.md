# Anandtechsmilies #
This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Install all the smileys used by the AnandTech Forums. It doesn't remove previously installed smileys but overloads potential conflicts.

## The contents of this add-on are :  ##
* A set-up file for copying the emoji sprite and importing smilies using the smilies.xml file (*Setup.php*, *smilies.xml*, *images/sprite_sheet_purchemoji.png*)

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.
