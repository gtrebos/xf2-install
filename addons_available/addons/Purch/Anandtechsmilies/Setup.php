<?php

namespace Purch\Anandtechsmilies;

use XF\AddOn\AbstractSetup;


class Setup extends AbstractSetup
{
	use \XF\AddOn\StepRunnerInstallTrait;
	use \XF\AddOn\StepRunnerUpgradeTrait;
	use \XF\AddOn\StepRunnerUninstallTrait;

	public function installStep1()
	{
		$smilieImporter = \XF::service('XF:Smilie\Import'); #get xenforo service to handle import
		$xml = \XF\Util\Xml::open(file_get_contents(__DIR__."/smilies.xml")); #open up the file we want to import

		$smilieData = $smilieImporter->getSmilieDataFromXml($xml); #parse out the xml

		#loop through all the smilies in the import file
		$smilies = [];
		foreach ($smilieData['smilies'] AS $k=>$one_smilie)
		{
			$temp_smilie = $one_smilie->getNewValues();#get the new smilies to import
			#smilie_category_id in the importer is complicated - we can only import into a category that is also being imported or represented within this file
			$temp_smilie['smilie_category_id'] = isset($smilieData['smilieCategoryMap'][$k]) ? $smilieData['smilieCategoryMap'][$k] : 0;
			$smilies[] = $this->setDefaultSmilieFields($temp_smilie); #make sure we have defaults so the import object doesn't blow up


			#find current smilies and delete them and then reimport them below because otherwise the importer will throw an error on duplicates and not import anything
			$finder = \XF::finder('XF:Smilie');
			$existing_smilie = $finder->where('smilie_text', $temp_smilie['smilie_text'])->fetchOne();
			if($existing_smilie)
			{
				$existing_smilie->delete();
			}

		}

		#run the import
		$smilieImporter->importSmilies($smilies, $smilieData['categories'],$error);

		#display any errors
		if (empty($error))
		{
			echo "\nSmilies imported successfully!\n\n";
		}
		else {
			echo "\n\nSmilies imported with some errors\n\n";
			foreach($error as $one_error)
			{
				echo "\nERROR : ".$one_error->getName()." FOR ".$one_error->getParams()['text']."\n\n";
			}
		}

		//copy smilie image
		$image_path_from = __DIR__."/images/*";
		$image_path_to = "./styles/default/xenforo/smilies/";

		`cp -r $image_path_from $image_path_to`;
	}

	#set defualts for required object fields
	protected function setDefaultSmilieFields($smilieInput)
	{

		$smilie_array = [];
		$smilie_fields = [
			'title' => '',
			'smilie_text' => '',
			'image_url' => '',
			'image_url_2x' => '',
			'sprite_mode' => 0,
			'sprite_params' => [],
			'smilie_category_id' => '',
			'display_order' => 0,
			'display_in_editor' => 0,
		];

		foreach($smilie_fields as $smilie_field_key=>$smilie_field_default)
		{
			if(!array_key_exists($smilie_field_key, $smilieInput))
			{
					$smilie_array[$smilie_field_key] = $smilie_field_default;
			}
			else {
					$smilie_array[$smilie_field_key] = $smilieInput[$smilie_field_key];
			}
		}
		return $smilie_array;
	}

	public function upgrade2000010Step1()
	{
		$this->installStep1();

	}

	//removing files that were moved into the web root
	public function uninstallStep1()
	{
		`rm -rf ./styles/default/xenforo/smilies/sprite_sheet_purchemoji.png`;
	}

}
