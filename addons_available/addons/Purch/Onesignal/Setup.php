<?php

namespace Purch\Onesignal;

use XF\AddOn\AbstractSetup;


class Setup extends AbstractSetup
{
	use \XF\AddOn\StepRunnerInstallTrait;
	use \XF\AddOn\StepRunnerUpgradeTrait;
	use \XF\AddOn\StepRunnerUninstallTrait;

	//moving some needid files into the web root
	public function installStep1()
	{
		//copy recaptcha images into images folder in web root
		$image_path_from = __DIR__."/rootfiles/*";
		$image_path_to = ".";

		`cp -r $image_path_from $image_path_to`;
	}

	public function upgrade2000010Step1()
	{
		$this->installStep1();

	}

	//removing files that were moved into the web root
	public function uninstallStep1()
	{
		`rm -rf ./manifest.json ./OneSignalSDKUpdaterWorker.js ./OneSignalSDKWorker.js`;
	}


}
