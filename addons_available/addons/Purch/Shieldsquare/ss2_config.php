<?php

namespace Purch\Shieldsquare;

class shieldsquare_config
{

    /*
     * @var string Path to write the cache file must have write access
     */
    public $_cahe_path = "/tmp/ss_nr_cache";

    /*
     *  Enter your Subscriber id  .
     */
    public $_sid = "bbe285d3-5a60-431a-89b9-98ee325874ab";

    /*
     * Please specify the mode in which you want to operate
     *
     * public $_mode = "Active";
     * or
     * public $_mode = "Monitor";
     */
    public $_mode = "Active";

    /*
     * Asynchronous HTTP Data Post
     * Setting this value to true will reduce the page load time when you are in Monitor mode.
     * This uses Linux CURL to POST the HTTP data using the EXEC command.
     * Note: Enable this only if you are hosting your applications on Linux environments.
     */
    public $_async_http_post = false;

    /*
     * Curl Timeout in Milliseconds
     */
    public $_timeout_value = 100;

    /*
     * PHPSESSID is the default session ID for PHP, please change it if needed
     */
    public $_sessid = 'PHPSESSID';

    /*
     * Change this value if your servers are behind a firewall or proxy
     */
    public $_ipaddress = 'REMOTE_ADDR';

    /*
     * Enter the relative URL of the JavaScript Data Collector
     */
    public $_js_url = '/getData.php';

    /*
     * Set the ShieldSquare domain based on your Server Locations
     *    Asia/India     -  'ss_sa.shieldsquare.net'
     *    Europe         -  'ss_ew.shieldsquare.net'
     *    Australia      -  'ss_au.shieldsquare.net'
     *    South America  -  'ss_br.shieldsquare.net'
     *    North America  -  'ss_scus.shieldsquare.net'
     */
    public $_ss2_domain = 'ss_neus.shieldsquare.net';

    /*
     * Set the DNA cache time
     * Default is one hour
     * Note: To use this feature your application server [Apache/Nginx]
     * should have write access to /dev/shm/ folder.
     */
    public $_domain_ttl = 3600;

    public $_domain_cache_file = '/tmp/';
}
