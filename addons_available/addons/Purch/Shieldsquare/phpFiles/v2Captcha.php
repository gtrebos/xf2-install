<?php
namespace Purch\Shieldsquare;
//Init Captcha Variables
include_once './src/addons/Purch/Shieldsquare/ss2.php';
include_once './src/addons/Purch/Shieldsquare/ss2_config.php';

$shieldsquare_config_data_v2Captcha   = new shieldsquare_config();

$siteKey = '6LdmXkIUAAAAAAHClGaYWqSw2_q4K_mF9iOesau0'; //Put your Public Site Key Here
$secret = '6LdmXkIUAAAAAGY4inUNqk5QfHcXSYpk1WRjv5Dk'; //Put your Secret key here

$lang = 'en';   //Language of website here -- See Google's Recaptcha V2 for options

//making a call to Shieldsquare server that captcha is shown
$shieldsquare_userid   = ""; // Enter the UserID of the user
$shieldsquare_calltype = 4;
$shieldsquare_pid      = "";
$shieldsquare_response = shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid);

if(isset($_POST['g-recaptcha-response'])) {
  $response = getCurlData("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']);
  $response = json_decode($response, true);
  if($response["success"] === true) {
    $redirPage = $_COOKIE["currentPagename"];
    setcookie(md5("captchaResponse"),md5("1".$redirPage.$_SERVER[$shieldsquare_config_data_v2Captcha->_ipaddress].$shieldsquare_config_data_v2Captcha->_sid),time()+60*60,"/");
    header("Location:".$redirPage);
    exit();
  }  else  {
    header("Location:".$_SERVER["PHP_SELF"]);
    exit();
  }
}

function getCurlData($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    $curlData = curl_exec($curl);
    curl_close($curl);
    return $curlData;
}
?>
<!-- The GET Request Captcha PAGE (Styles and Includes) -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ShieldSquare reCAPTCHA Page</title>
        <link rel="shortcut icon" href="https://cdn.perfdrive.com/icons/favicon.png" type="image/x-icon"/>
        <style type="text/css">
            body {
                margin: 1em 5em 0 5em;
                font-family: sans-serif;
            }
            fieldset {
                display: inline;
                padding: 1em;
            }
        </style>
    </head>
    <body align="center">
        <a href="//www.shieldsquare.com" target="_blank"><img src="https://cdn.perfdrive.com/icons/shieldsquarelogo.png">
        </a>
            <hr>
        <h1>Suspicious Activity Detected</h1>

    <!-- The Form Display -- You can Customize this form for Yourself! -->

    <p>Complete the reCAPTCHA then submit the form.</p>
        <fieldset>
            <legend>Solve Captcha</legend>
    <form action="<?php $_SERVER['PHP_SELF']; ?>"  method=POST>
            <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
            <script type="text/javascript"
                    src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
            </script><br>
            <input type=Submit Value="GO" />
        </fieldset>
    </form>
    <br><br><br><br> <hr> <br>
    <footer>
    <small>© Copyright 2015, All Rights Reserved. Kaalbi Technologies Pvt. Ltd. </small>
        </footer>

</body>
</html>
