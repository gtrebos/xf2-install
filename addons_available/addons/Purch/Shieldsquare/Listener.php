<?php
namespace Purch\Shieldsquare;

include_once __DIR__."/ss2.php";
include_once __DIR__.'/ss2_config.php';

class Listener
{

  private static $CAPTCHA_RETURNCODE = 2;
  private static $BLOCK_RETURNCODE = 3;
  private static $FEED_FAKE_DATA_RETURNCODE = 4;

  public static function templaterTemplatePreRender(
  \XF\Template\Templater $templater,
  &$type,
  &$template,
  array &$params
  ) {
      if($template == 'purch_body_shieldsquare'){

        $shieldsquare_config_data_header  = new shieldsquare_config();
        $visitor = \XF::visitor();
        $request = \XF::app()->request();
        $shieldsquare_userid = $visitor->user_id;
        $shieldsquare_calltype = 1;
        $shieldsquare_pid = "";
        $response = null;

        $prefix = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $current_page = $prefix.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        if(isset($_COOKIE[md5("captchaResponse")]) && $_COOKIE[md5("captchaResponse")] == md5("1".$current_page.$_SERVER[$shieldsquare_config_data_header->_ipaddress].$shieldsquare_config_data_header->_sid)){
          $shieldsquare_calltype = 5;
          //Unset the cookie variable
          setcookie(md5("captchaResponse"),null,-1,"/");
          setcookie("currentPagename",null,-1,"/");
        }

        $shieldsquare_response = shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid);

        if ($shieldsquare_response->responsecode === self::$CAPTCHA_RETURNCODE) {
          // Response code 2 => redirect to CAPTCHA
          //setting the current page name to the cookie for later use
          setcookie("currentPagename",$current_page,0,"/");
          $response = \XF::app()->response();
          $response->redirect($request->convertToAbsoluteUri("/v2Captcha.php"), 302);
        } elseif ($shieldsquare_response->responsecode === self::$BLOCK_RETURNCODE) {
          $response = \XF::app()->response();
          // Block response => stop script execution
          $response->httpCode(401);
        } elseif ($shieldsquare_response->responsecode === self::$FEED_FAKE_DATA_RETURNCODE) {
          $response = \XF::app()->response();
          // Feed fake data => stop script execution, but tell the bot that everything went fine
          $response->httpCode(200);
        }

        if ($response) {
          $response->send();
        }

        if ($params) {
          $params['shieldsquare_response'] = json_decode(json_encode($shieldsquare_response), true);
        }


      }

  }

}
