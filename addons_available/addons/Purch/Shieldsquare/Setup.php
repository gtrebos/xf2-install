<?php

namespace Purch\Shieldsquare;

use XF\AddOn\AbstractSetup;


class Setup extends AbstractSetup
{
	use \XF\AddOn\StepRunnerInstallTrait;
	use \XF\AddOn\StepRunnerUpgradeTrait;
	use \XF\AddOn\StepRunnerUninstallTrait;

	//removing old files from v.1 of Purch/Shieldsquare add-on
	public function installStep1()
	{
		$old_ss_files_array = array(
		"SSCaptcha.php",
		"VerifyCaptcha.php",
		"getData.php",
		"recaptchalib.php",
		"ss2.php",
		"ss2_active.php",
		"ss2_config.php",
		"ss2_config.php.devint",
		"ss2_config.php.prod",
		"ss2_monitor.php",
		"v2Captcha.php");

		foreach($old_ss_files_array as $afile)
		{
			@\unlink($afile);
		}
	}

	//moving some needid files into the web root
	public function installStep2()
	{
		//copy recaptcha images into images folder in web root
		$image_path_from = __DIR__."/images/recaptcha";
		$image_path_to = "./images";

		`cp -r $image_path_from $image_path_to`;

		//copy recaptcha css into folder in web root
		$css_path_from = __DIR__."/recaptcha_css";
		$css_path_to = "./";

		`cp -r $css_path_from $css_path_to`;

		//copy php files into web root
		$php_path_from = __DIR__."/phpFiles/*";
		$php_path_to = "./";

		`cp -r $php_path_from $php_path_to`;
	}

	public function upgrade2000010Step1()
	{
		$this->installStep1();

	}

	public function upgrade2000010Step2()
	{
		$this->installStep2();

	}

	//removing files that were moved into the web root
	public function uninstallStep1()
	{
		`rm -rf ./images/recaptcha`;
		`rm -rf ./recaptcha_css`;
		`rm -rf ./getData.php`;
		`rm -rf ./v2Captcha.php`;
	}


}
