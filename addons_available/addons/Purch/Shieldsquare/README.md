# Shieldsquare #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Add some ShieldSquare related javascript code to all pages in the Anandtech Xenforo Custom message board.
* Inject custom variable values into this javascript code.

## The contents of this add-on are :  ##

* A new template *purch_body_shieldsquare* .
* A template modification of the standard *PAGE_CONTAINER* xenforo template.
* A code event listener on the *templater_template_pre_render* event (*Listener.php*)
* A set of php files to handle ShiedSquare needs.
* A css file and directory of images used by this add-on.
* A set-up file for copying images, css and php files on installation and removing orphaned files from version 1.x.x of this add-on. The set-up file also removes these copied files in the event of an uninstall. (*Setup.php*)

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.
