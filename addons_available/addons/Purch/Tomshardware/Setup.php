<?php

namespace Purch\Tomshardware;

use XF\AddOn\AbstractSetup;

class Setup extends AbstractSetup
{
    use \XF\AddOn\StepRunnerInstallTrait;
    use \XF\AddOn\StepRunnerUpgradeTrait;
    use \XF\AddOn\StepRunnerUninstallTrait;

    private function removeOldStyles($exclude = []) {
        $style_finder = \XF::finder('XF:Style');
        $styles = $style_finder->fetch();

        foreach ($styles as $style) {
            if (!in_array(strtolower($style->title), $exclude)) {
                $style->delete();
            }
        }
    }

    private function installStyle($style, $xml, $parent) {
        $style_finder = \XF::finder('XF:Style');
        $parent_style = $style_finder->where('title', $parent)->fetchOne();
        $current_style = $style_finder->where('title', $style)->fetchOne();

        if ($parent_style && !$current_style) {
            $style_document = \XF\Util\Xml::openFile($xml);
            $style_importer = \XF::service('XF:Style\Import');
            $style_importer->setParentStyle($parent_style);
            $style_importer->importFromXml($style_document);
        }
    }

    private function activateStyle($name)
    {
        $style_finder = \XF::finder('XF:Style');
        $style = $style_finder->where('title', $name)->fetchOne();

        if ($style && isset($style->style_id)) {
            $style->set('user_selectable', 1);
            $style->save();

            \XF::repository('XF:Option')->updateOptions(['defaultStyleId' => $style->style_id]);
        }
    }

    public function installStep1()
    {
        $this->removeOldStyles(['default style']);

        $this->installStyle('UI.X', dirname(__FILE__).'/style-uix.xml', 'Default Style');
        $this->installStyle('UI.X Material', dirname(__FILE__).'/style-UI.X-Material.xml', 'UI.X');
        $this->installStyle('Tomshardware', dirname(__FILE__).'/style-Tomshardware.xml', 'UI.X Material');
        $this->activateStyle('Tomshardware');
    }

    public function upgrade2000020Step1() {
        $this->installStep1();
    }

    public function uninstall(array $stepParams = [])
    {
        ## Setting back the default style as default one
        $style_finder_out = \XF::finder('XF:Style');
        $default_style = $style_finder_out->where('title', "Default style")->fetchOne();

        if ($default_style){
            \XF::repository('XF:Option')->updateOptions(['defaultStyleId' => $default_style->style_id]);
            \XF::repository('XF:Option')->rebuildOptionCache();
        }

        $style_finder_out2 = \XF::finder('XF:Style');
        $th_style = $style_finder_out2->where('title', "Tomshardware")->fetchOne();

        if($th_style) {
            $th_style->set('user_selectable', 0);
            $th_style->save();
            $th_style->delete();
        }
    }
}