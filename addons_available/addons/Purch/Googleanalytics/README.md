# Googleanalytics #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Add some Googleanalytics related javascript code just after the body tag and in the footer of all pages in the Anandtech Xenforo Custom message board.
* Inject custom values into this javascript code.

### The contents of this add-on are :  ###

* Two new templates *purch_body_googleanalytics* and *purch_footer_google_analytics*.
* Two template modifications of the standard *PAGE_CONTAINER* xenforo template.
* A code event listener on the *templater_template_pre_render* event (_Listener.php_)

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.
