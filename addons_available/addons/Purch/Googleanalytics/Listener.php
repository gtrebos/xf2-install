<?php

namespace Purch\Googleanalytics;

class Listener
{

  public static function templaterTemplatePreRender(
  \XF\Template\Templater $templater,
  &$type,
  &$template,
  array &$params
  ) {

    if($template == 'purch_footer_googleanalytics') //only fire for the analytics footer
    {
      $uri = isset($params['xf']) && isset($params['xf']['uri']) ? $params['xf']['uri'] : "";
      $page_section = isset($params['pageSection']) ? $params['pageSection'] : "";

      # ok check uri first - if uri does not match then you can try page Section
      $omnitureValue = self::getGAValue($uri, $page_section);

      $params['prop4'] = $omnitureValue ? "D=v4" : "";
      $params['eVar4'] = $omnitureValue;

    }
  }

  private static function getGAValue($uri, $page_section)
  {
      $ga_value = "";

      if (strpos($uri, "forums/") !== false) {
          $ga_value = "Forum Listing";
      }

      if (strpos($uri, "threads/") !== false) {
          $ga_value = "Forum Thread";
      }

      if (strpos($uri, "members/") !== false) {
          $ga_value = "Public Profile";
      }

      if (strpos($uri, "faq") !== false || strpos($uri, "help") !== false) {
          $ga_value = "FAQ";
      }

      #if we don't find it in the URI then try the page section
      if($ga_value == "")
      {
        if ($page_section == "forums") {
            $ga_value = "Forum Listing";
        }

        if ($page_section == "threads") {
            $ga_value = "Forum Thread";
        }

        if ($page_section == "members") {
            $ga_value = "Public Profile";
        }

        if ($page_section =="faq" || $page_section =="help") {
            $ga_value = "FAQ";
        }
      }
      return $ga_value;
  }

}
