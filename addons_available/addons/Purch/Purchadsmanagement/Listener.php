<?php

namespace Purch\Purchadsmanagement;

class Listener
{
  /** @var string */
  protected static $content;

  public static function templaterTemplatePreRender(
  \XF\Template\Templater $templater,
  &$type,
  &$template,
  array &$params
  ) {
    $options  = \XF::options();
    $params['purchRaasHead'] = "";
    $params['purchRaasBody'] = "";

    //raas has to be enabled or else we don't want to make a call or inject code
    if(isset($options['purch_purchadsmanagement_enable_raas']) && $options['purch_purchadsmanagement_enable_raas'])
    {
      if($template == "purch_head_tmnhead")
      {
        $params['purchRaasHead'] = self::raasHead($params);
      }

      if($template == 'PAGE_CONTAINER')
      {
        $params['purchRaasBody'] = self::raasBody($params);
      }
    }
  }

  public static function raasBody($params) {
    if (is_null(self::$content)) {
      self::callRaasService($params);
    }

    $compactJson = json_encode(self::getRampDataCompact());
    return sprintf('<script type="text/javascript">var tmnramp = %s; %s</script>', $compactJson, self::$content['jsrenderer']);
  }

  public static function getRampDataCompact()
  {
    if (!is_null(self::$content)) {
      $compactData = self::$content;

      unset($compactData['jsrenderer']);
      unset($compactData['html_head']);

      return $compactData;
    }
  }

  public static function raasHead($params) {
    if (is_null(self::$content)) {
      self::callRaasService($params);
    }

    if (isset(self::$content['html_head']) && !empty(self::$content['html_head'])) {
      return self::$content['html_head'];
    }
  }

  public static function callRaasService($params)
  {
    $location = urlencode($params['xf']['fullUri']);
    $userAgent = urlencode( isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
    $clientIp = urlencode(self::getUserIp());
    $options  = \XF::options();

    $raasParams = [
      'ip' => $clientIp,
      'ua' => $userAgent,
      'p' => $options['purch_purchadsmanagement_id'],
      'l' => $location
    ];

    $raasHost = $options['purch_purchadsmanagement_raas_host'];
    $raasTimeout = $options['purch_purchadsmanagement_raas_timeout'];

    $uri = $raasHost . "raas/v1?o=" . urlencode(json_encode($raasParams));

    $client = \XF::app()->http()->client();

    try {
      $response = $client->get($uri, ['timeout' => $raasTimeout]);
      self::$content = $response->json();

    } catch(\GuzzleHttp\Exception\RequestException $e) {
      \XF::logException($e, false, sprintf('Error when trying to call RAAS: "%s"', $uri));
    }
  }

  protected static function getUserIp()
  {
  $userIp = (isset($_SERVER['HTTP_CONNECTING_CLIENT_IP'])) ? $_SERVER['HTTP_CONNECTING_CLIENT_IP'] :
    (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
  return $userIp;
  }


}
