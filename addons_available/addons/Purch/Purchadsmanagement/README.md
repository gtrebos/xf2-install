# Purchadsmanagement #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Integrate RAMP ads in xenForo forum

## The contents of this add-on are :  ##
* A new template *purch_head_tmnhead*
* Two template modification of the standard *PAGE_CONTAINER* xenforo template.
* A code event listener on the *templater_template_pre_render* event (*Listener.php*)
* A set-up file for adding some default ads that we might want to set up (*Setup.php*)
* XML file for installing options and phrases (*options.xml* , *phrases.xml*)

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.


## Additional Configuration ##
* Configuration and setup options are available at *Anandtech Xenforo Admin CP* at *Setup->Options->Purch Ads Management Options*
* Placement of actual ads are set up through the XF2 adds interface found at *Setup->Advertising*. Installing this add-on will start you with 6 ad placement positions. You can add more positions and modify existing positions as needed through the *Anandtech Xenforo Admin CP* interface at *Setup->Advertising*.
