<?php

namespace Purch\Purchadsmanagement;

use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends \XF\AddOn\AbstractSetup
{
	use \XF\AddOn\StepRunnerInstallTrait;
	use \XF\AddOn\StepRunnerUpgradeTrait;
	use \XF\AddOn\StepRunnerUninstallTrait;

	public function installStep1()
	{
		//some basic ads and positions to get started with
		$default_ads = array(
			"interstitial"=>"container_content_above",
			"out-of-page"=>"container_breadcrumb_bottom_below",
			"header_leaderboard"=>"container_breadcrumb_top_below",
			"footer_leaderboard"=>"container_breadcrumb_bottom_above",
			"rightcol_top"=>"container_sidebar_above",
			"rightcol_bottom"=>"container_sidebar_below"
		);

		$ad_entity = \XF::finder('XF:Advertising');
		$ads_already_in_db = $ad_entity->fetch()->pluckNamed('title');

		foreach($default_ads as $title=>$position)
		{
			if(in_array($title, $ads_already_in_db))
			{
				echo "Skipping ".$title." because it already exists.\n\n";
			}
			else {
				//add the types of ads we might use
				$creator = \XF::app()->em()->create('XF:Advertising');
				$creator->title = $title;
				$creator->position_id = $position;
				$creator->ad_html = "<div id=\"".$title."\"><script type=\"text/javascript\">window.tmntag && tmntag.cmd && tmntag.cmd.push(function(){ if (tmntag.adTag) {tmntag.adTag('".$title."',false);} });</script></div>";
				$creator->active = 0;
				$creator->save();
				echo "Setting up ".$title."\n\n";
			}
		}

	}

	public function upgrade2000010Step1()
	{
		$this->installStep1();

	}

}
