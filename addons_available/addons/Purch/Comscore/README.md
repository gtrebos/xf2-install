# Comscore #

This add-on is used in conjunction with [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom)

## The purpose of this add-on is to :  ##

* Add some comScore javascript code just after the body tag of all pages.

## The contents of this add-on are :  ##

* A new template *purch_comscore* containing the code to add to the pages.
* A template modification of the standard *PAGE_CONTAINER* xenforo template.

## Installation and Code Changes ##
For documentation on how to install or make changes to this add-on see the sections "Purch add-on Installation" and "Purch add-on Code Changes" in the [Anandtech Xenforo Custom](https://bitbucket.org/purchmedia/anandtech-xenforo-custom) README file.
