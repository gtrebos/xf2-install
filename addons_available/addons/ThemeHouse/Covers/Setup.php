<?php

namespace ThemeHouse\Covers;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Create;

/**
 * Class Setup
 * @package ThemeHouse\Covers
 */
class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    /**
     * Install Functions
     */
    public function installStep1()
    {
        $schemaManager = $this->db()->getSchemaManager();

        $schemaManager->createTable('xf_thcover_cover', function (Create $table) {
            $table->addColumn('cover_id', 'int')->autoIncrement();
            $table->addColumn('content_id', 'int');
            $table->addColumn('content_type', 'varbinary', 25);
            $table->addColumn('cover_user_id', 'int');
            $table->addColumn('cover_preset', 'int')->setDefault(0);
            $table->addColumn('cover_image', 'mediumblob');
            $table->addColumn('cover_styling', 'mediumblob');
            $table->addColumn('cover_date', 'int')->setDefault(0);
            $table->addColumn('cover_state', 'enum')->values([
                'visible',
                'moderated',
                'deleted'
            ])->setDefault('visible');
            $table->addKey(['content_type', 'content_id', 'cover_user_id'], 'content_type_id_cover_user_id');
            $table->addKey(['cover_user_id', 'content_type', 'content_id'], 'cover_user_content_type_id');
            $table->addKey(['cover_user_id', 'cover_date'], 'cover_user_id_cover_date');
            $table->addKey('cover_date', 'cover_date');
            $table->addKey('cover_preset', 'cover_preset');
        });
    }

    public function installStep2()
    {
        $schemaManager = $this->db()->getSchemaManager();

        $schemaManager->createTable('xf_thcover_preset', function (Create $table) {
            $table->addColumn('cover_preset_id', 'int')->autoIncrement();
            $table->addColumn('title', 'varchar', 50);
            $table->addColumn('cover_image', 'mediumblob');
            $table->addColumn('cover_styling', 'mediumblob');
            $table->addColumn('user_criteria', 'mediumblob');
            $table->addColumn('display_order', 'int')->setDefault(1);
            $table->addColumn('enabled', 'bool')->setDefault(1);
            $table->addColumn('default_for', 'mediumblob');
        });
    }

    public function installStep3()
    {
        $this->applyGlobalPermission('th_cover', 'view');
        $this->applyGlobalPermission('th_cover', 'edit');
        $this->applyGlobalPermission('th_cover', 'uploadImage');
        $this->applyGlobalPermission('th_cover', 'downloadImage');
        $this->applyGlobalPermission('th_cover', 'positionImage');
        # $this->applyGlobalPermission('th_cover', 'style');
        $this->applyGlobalPermission('th_cover', 'preset');
        $this->applyGlobalPermission('th_cover', 'delete');
    }

    /**
     * Uninstall Functions
     */
    public function uninstallStep1()
    {
        $schemaManager = $this->db()->getSchemaManager();

        $schemaManager->dropTable('xf_thcover_cover');
    }

    public function uninstallStep2()
    {
        $schemaManager = $this->db()->getSchemaManager();

        $schemaManager->dropTable('xf_thcover_preset');
    }
}