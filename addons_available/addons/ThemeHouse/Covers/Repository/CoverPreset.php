<?php

namespace ThemeHouse\Covers\Repository;

use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;

/**
 * Class CoverPreset
 * @package ThemeHouse\Covers\Repository
 */
class CoverPreset extends Repository
{
    /**
     * @return Finder
     */
    public function findCoverPresetsForList()
    {
        return $this->finder('ThemeHouse\Covers:CoverPreset')
            ->setDefaultOrder('display_order', 'ASC');
    }

    /**
     * @return \XF\Mvc\Entity\Entity[]
     */
    public function getCoverPresetList()
    {
        $coverPresets = $this->findCoverPresetsForList()->fetch();

        return $coverPresets->toArray();
    }

    /**
     * @return array
     */
    public function getCoverPresetCacheData()
    {
        $coverPresets = $this->finder('ThemeHouse\Covers:CoverPreset')
            ->order(['display_order', 'title'])
            ->fetch();

        $cache = [];

        foreach ($coverPresets AS $coverPresetId => $coverPreset) {
            $coverPreset = $coverPreset->toArray();

            $cache[$coverPresetId] = $coverPreset;
        }

        return $cache;
    }

    /**
     * @return array
     */
    public function rebuildCoverPresetCache()
    {
        $cache = $this->getCoverPresetCacheData();
        \XF::registry()->set('coverPresets', $cache);

        /** @var \XF\Repository\Style $styleRepo */
        $styleRepo = $this->repository('XF:Style');
        $styleRepo->updateAllStylesLastModifiedDate();

        return $cache;
    }
}