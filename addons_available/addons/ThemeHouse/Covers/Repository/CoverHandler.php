<?php

namespace ThemeHouse\Covers\Repository;

use XF\Mvc\Entity\Repository;

/**
 * Class CoverHandler
 * @package ThemeHouse\Covers\Repository
 */
class CoverHandler extends Repository
{
    protected $coverHandlers = null;

    /**
     * @return \ThemeHouse\Covers\Cover\AbstractHandler[]
     * @throws \Exception
     */
    public function getCoverHandlers()
    {
        /* Soft-pass to catch getCoverHandler quickSet */
        if ($this->coverHandlers === null || count($this->coverHandlers) === 1) {
            $handlers = [];

            foreach (\XF::app()->getContentTypeField('cover_handler_class') AS $contentType => $handlerClass) {
                if (class_exists($handlerClass)) {
                    $eclass = \XF::extendClass($handlerClass);
                    $handlers[$contentType] = [
                        'oclass' => $handlerClass,
                        'eclass' => $eclass,
                        'object' => new $eclass($contentType)
                    ];
                }
            }

            $this->coverHandlers = $handlers;
        }

        return $this->coverHandlers;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCoverHandlersList()
    {
        $handlers = $this->getCoverHandlers();

        $list = [];
        foreach ($handlers as $contentType => $handler) {
            /** @noinspection PhpUndefinedMethodInspection */
            $list[$contentType] = $handler['object']->getTitle();
        }

        return $list;
    }

    /**
     * @param string $type
     * @param bool $throw
     *
     * @return \ThemeHouse\Covers\Cover\AbstractHandler
     * @throws \Exception
     */
    public function getCoverHandler($type, $throw = false)
    {
        if (is_array($this->coverHandlers) && isset($this->coverHandlers[$type])) {
            $handlerClass = $this->coverHandlers[$type]['oclass'];
        } else {
            $handlerClass = \XF::app()->getContentTypeFieldValue($type, 'cover_handler_class');
        }

        if (!$handlerClass) {
            if ($throw) {
                throw new \InvalidArgumentException("No cover handler for '$type'");
            }

            return null;
        }

        if (!class_exists($handlerClass)) {
            if ($throw) {
                throw new \InvalidArgumentException("Cover handler for '$type' does not exist: $handlerClass");
            }

            return null;
        }

        $handlerClass = \XF::extendClass($handlerClass);

        if (!isset($this->coverHandlers[$type])) {
            $eclass = \XF::extendClass($handlerClass);
            $this->coverHandlers[$type] = [
                'oclass' => $handlerClass,
                'eclass' => $eclass,
                'object' => new $eclass($type)
            ];
        }

        return $this->coverHandlers[$type]['object'];

    }
}