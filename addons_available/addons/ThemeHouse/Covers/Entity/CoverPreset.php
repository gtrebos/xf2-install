<?php

namespace ThemeHouse\Covers\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * Class CoverPreset
 * @package ThemeHouse\Covers\Entity
 *
 * @property integer cover_preset_id
 * @property string title
 * @property array cover_image
 * @property array cover_styling
 * @property array user_criteria
 * @property integer display_order
 * @property boolean enabled
 * @property array default_for
 */
class CoverPreset extends Entity
{
    /**
     * @param $criteria
     * @return bool
     */
    protected function verifyUserCriteria(&$criteria)
    {
        $userCriteria = $this->app()->criteria('XF:User', $criteria);
        $criteria = $userCriteria->getCriteria();
        return true;
    }

    protected function _postSave()
    {
        $this->rebuildCoverPresetCache();
    }

    protected function _postDelete()
    {
        $this->rebuildCoverPresetCache();
    }

    protected function rebuildCoverPresetCache()
    {
        $repo = $this->getCoverPresetRepo();

        \XF::runOnce('coverPresetCache', function () use ($repo) {
            $repo->rebuildCoverPresetCache();
        });
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getDefaultNames() {
        $defaults = $this->default_for;

        $handlers = $this->getCoverHandlerRepo()->getCoverHandlers();

        $phrases = [];
        foreach($defaults as $default) {
            if(!empty($handlers[$default])) {
                /** @noinspection PhpUndefinedMethodInspection */
                $phrases[] = $handlers[$default]['object']->getContentTypePhrase();
            }
        }

        return $phrases;
    }

    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure->table = 'xf_thcover_preset';
        $structure->shortName = 'ThemeHouse\Covers:CoverPreset';
        $structure->primaryKey = 'cover_preset_id';
        $structure->columns = [
            'cover_preset_id' => ['type' => self::UINT, 'autoIncrement' => true, 'nullable' => true],
            'title' => ['type' => self::STR, 'maxLength' => 50, 'required' => true],
            'cover_image' => ['type' => self::SERIALIZED_ARRAY, 'default' => []],
            'cover_styling' => ['type' => self::SERIALIZED_ARRAY, 'default' => []],
            'user_criteria' => [
                'type' => self::SERIALIZED_ARRAY,
                'default' => [],
                'verify' => 'verifyUserCriteria'
            ],
            'display_order' => ['type' => self::UINT, 'default' => 10],
            'enabled' => ['type' => self::BOOL, 'default' => true],
            'default_for' => ['type' => self::LIST_COMMA, 'default' => []]
        ];

        $structure->getters = [
            'default_names' => true
        ];

        return $structure;
    }

    /**
     * @return \ThemeHouse\Covers\Repository\CoverPreset
     */
    protected function getCoverPresetRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Covers:CoverPreset');
    }

    /**
     * @return \ThemeHouse\Covers\Repository\CoverHandler
     */
    protected function getCoverHandlerRepo() {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Covers:CoverHandler');
    }
}