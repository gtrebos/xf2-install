<?php

namespace ThemeHouse\Covers\Entity;

use XF\Entity\ApprovalQueue;
use XF\Entity\DeletionLog;
use XF\Entity\User;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * Class Cover
 * @package ThemeHouse\Covers\Entity
 *
 * @property integer cover_id
 * @property integer content_id
 * @property string content_type
 * @property integer cover_user_id
 * @property integer cover_preset
 * @property array cover_image
 * @property array cover_styling
 * @property integer cover_date
 * @property integer cover_state
 *
 * @property User CoverUser
 * @property CoverPreset CoverPreset
 * @property DeletionLog DeletionLog
 * @property ApprovalQueue ApprovalQueue
 * @property Entity Content
 */
class Cover extends Entity
{
    protected $coverColorPattern = '/^(\#[\da-f]{3}|\#[\da-f]{6}|rgba\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)(,\s*(0\.\d+|1))\)|hsla\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)(,\s*(0\.\d+|1))\)|rgb\(((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*,\s*){2}((\d{1,2}|1\d\d|2([0-4]\d|5[0-5]))\s*)|hsl\(\s*((\d{1,2}|[1-2]\d{2}|3([0-5]\d|60)))\s*,\s*((\d{1,2}|100)\s*%)\s*,\s*((\d{1,2}|100)\s*%)\))$/';

    /**
     * @return null
     * @throws \Exception
     */
    public function getContentTypePhrase()
    {
        $handler = $this->getHandler();
        return $handler ? $handler->getContentTypePhrase() : null;
    }

    /**
     * @param null $error
     * @return bool
     * @throws \Exception
     */
    public function canView(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'view')) {
            $error = \XF::phrase('thcovers_no_view_permissions');
            return false;
        }

        $handler = $this->getHandler();
        $content = $this->Content;

        if ($handler && $content) {
            return $handler->canViewCover($content, $error);
        }

        return false;
    }

    /**
     * @param null $error
     * @return bool
     * @throws \Exception
     */
    public function canEdit(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'edit')) {
            $error = \XF::phrase('thcovers_no_edit_permissions');
            return false;
        }

        if (!$visitor->hasPermission('th_cover', 'use_' . $this->content_type)) {
            $error = \XF::phrase('thcovers_no_edit_permissions');
            return false;
        }

        $handler = $this->getHandler();
        $content = $this->Content;

        if ($handler && $content) {
            return $handler->canEditCover($content, $error);
        }

        return false;
    }

    /**
     * @param null $error
     * @return bool
     * @throws \Exception
     */
    public function canDelete(&$error = null)
    {
        if (empty($this->cover_image) && empty($this->cover_styling)) {
            $error = \XF::phrase('thcovers_only_custom_can_be_deleted');
            return false;
        }

        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'delete')) {
            $error = \XF::phrase('thcovers_no_delete_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        $handler = $this->getHandler();
        $content = $this->Content;

        if ($handler && $content) {
            return $handler->canDeleteCover($content, $error);
        }

        return false;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canSetImage(&$error = null)
    {
        if ($this->canUploadImage($error) || $this->canDownloadImage($error) || $this->canUseGlobalImage($error)) {
            return true;
        }

        return false;
    }

    /**
     * @param null $error
     * @return bool
     */
    protected function canUseGlobalImage(&$error = null)
    {
        // TODO
        return false;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canUploadImage(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'uploadImage')) {
            $error = \XF::phrase('thcovers_no_upload_image_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        return true;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canDownloadImage(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'downloadImage')) {
            $error = \XF::phrase('thcovers_no_download_image_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        return true;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canPositionImage(&$error = null)
    {
        if (empty($this->cover_image)) {
            $error = \XF::phrase('thcovers_only_custom_can_be_positioned');
            return false;
        }

        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'positionImage')) {
            $error = \XF::phrase('thcovers_no_position_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        return true;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canStyle(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'style')) {
            $error = \XF::phrase('thcovers_no_style_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        return true;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canUsePreset(&$error = null)
    {
        $visitor = \XF::visitor();
        if (!$visitor->hasPermission('th_cover', 'preset')) {
            $error = \XF::phrase('thcovers_no_preset_permissions');
            return false;
        }

        if ($this->Content->user_id != \XF::visitor()->user_id
            && !\XF::visitor()->hasPermission('th_cover', 'edit_all')) {
            return false;
        }

        return true;
    }

    /**
     * @param null $error
     * @return bool
     */
    public function canApproveUnapprove(&$error = null)
    {
        $visitor = \XF::visitor();
        return ($visitor->user_id && $visitor->hasPermission('th_cover', 'approveUnapprove'));
    }

    /**
     * @return string
     */
    public function getNewCoverState()
    {
        $visitor = \XF::visitor();

        if ($visitor->user_id && $visitor->hasPermission('th_cover', 'approveUnapprove')) {
            return 'visible';
        }

        if ($this->isChanged('cover_image')) {
            if (!empty($this->cover_image) && !$visitor->hasPermission('th_cover', 'imageWithoutApproval')) {
                return 'moderated';
            }
        }

        return 'visible';
    }

    /**
     * @return \ThemeHouse\Covers\Cover\AbstractHandler
     * @throws \Exception
     */
    public function getHandler()
    {
        return $this->getCoverHandlerRepo()->getCoverHandler($this->content_type);
    }

    /**
     * @return null|Entity
     * @throws \Exception
     */
    public function getContent()
    {
        $handler = $this->getHandler();
        return $handler ? $handler->getContent($this->content_id) : null;
    }

    /**
     * @param Entity|null $content
     */
    public function setContent(Entity $content = null)
    {
        $this->_getterCache['Content'] = $content;
    }


    /**
     * @param $coverColor
     * @return bool
     */
    protected function verifyCoverColor(&$coverColor)
    {
        // TODO ?
        /* if ($coverColor === $this->getExistingValue('cover_color')) {
         return true; // unchanged, always pass
         }

         if (preg_match($this->coverColorPattern, $coverColor) !== 1) {
         $this->error(\XF::phrase('th_invalid_color_covers'), 'cover_color');
         return false;
         }*/

        return true;
    }

    /**
     * @throws \XF\PrintableException
     * @throws \Exception
     */
    protected function _preSave()
    {
        $this->cover_date = \XF::$time;

        $visibilityChange = $this->isStateChanged('cover_state', 'visible');
        $approvalChange = $this->isStateChanged('cover_state', 'moderated');
        $deletionChange = $this->isStateChanged('cover_state', 'deleted');

        if ($this->isUpdate()) {
            if ($visibilityChange === 'enter') {
                $this->cover_state = 'visible';
            }

            if ($deletionChange === 'leave' && $this->DeletionLog) {
                $this->DeletionLog->delete();
            }

            if ($approvalChange === 'leave' && $this->ApprovalQueue) {
                $this->ApprovalQueue->delete();
            }
        }

        if ($approvalChange === 'enter') {
            $approvalQueue = $this->getRelationOrDefault('ApprovalQueue', false);
            $approvalQueue->content_date = $this->cover_date;
            $approvalQueue->save();
        } else {
            if ($deletionChange === 'enter' && $this->content_id && !$this->DeletionLog) {
                $delLog = $this->getRelationOrDefault('DeletionLog', false);
                $delLog->save();
            }
        }

        if ($this->isUpdate() && $this->getOption('log_moderator')) {
            $this->app()->logger()->logModeratorChanges('cover', $this->getContent());
        }
    }

    /**
     * @throws \XF\PrintableException
     * @throws \Exception
     */
    protected function _preDelete()
    {
        if ($this->cover_state == 'visible') {
            $this->cover_state = 'deleted';
        }

        if ($this->cover_state == 'deleted' && $this->DeletionLog) {
            $this->DeletionLog->delete();
        }

        if ($this->cover_state == 'moderated' && $this->ApprovalQueue) {
            $this->ApprovalQueue->delete();
        }

        if ($this->getOption('log_moderator') && $this->Content) {
            $this->app()->logger()->logModeratorAction('cover', $this->Content, 'deleted');
        }
    }

    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure->table = 'xf_thcover_cover';
        $structure->shortName = 'ThemeHouse\Covers:Cover';
        $structure->contentType = 'cover';
        $structure->primaryKey = 'cover_id';
        $structure->columns = [
            'cover_id' => ['type' => self::UINT, 'autoIncrement' => true, 'nullable' => true],
            'content_id' => ['type' => self::UINT, 'required' => true],
            'content_type' => ['type' => self::STR, 'maxLength' => 25, 'required' => true],
            'cover_user_id' => ['type' => self::UINT, 'required' => true, 'default' => \XF::visitor()->user_id],
            'cover_preset' => ['type' => self::UINT, 'required' => true, 'default' => 0],
            'cover_image' => ['type' => self::SERIALIZED_ARRAY, 'default' => []],
            'cover_styling' => ['type' => self::SERIALIZED_ARRAY, 'default' => []],
            'cover_date' => ['type' => self::UINT, 'default' => 0],
            'cover_state' => [
                'type' => self::STR,
                'default' => 'visible',
                'allowedValues' => ['visible', 'moderated', 'deleted']
            ],
        ];
        $structure->getters = [
            'Content' => true
        ];
        $structure->relations = [
            'CoverUser' => [
                'entity' => 'XF:User',
                'type' => self::TO_ONE,
                'conditions' => [
                    ['user_id', '=', '$cover_user_id']
                ],
                'primary' => true
            ],
            'CoverPreset' => [
                'entity' => 'ThemeHouse\Covers:CoverPreset',
                'type' => self::TO_ONE,
                'conditions' => [
                    ['cover_preset_id', '=', '$cover_preset']
                ],
                'primary' => true
            ],
            'DeletionLog' => [
                'entity' => 'XF:DeletionLog',
                'type' => self::TO_ONE,
                'conditions' => [
                    ['content_type', '=', 'cover'],
                    ['content_id', '=', '$cover_id']
                ],
                'primary' => true
            ],
            'ApprovalQueue' => [
                'entity' => 'XF:ApprovalQueue',
                'type' => self::TO_ONE,
                'conditions' => [
                    ['content_type', '=', 'cover'],
                    ['content_id', '=', '$cover_id']
                ],
                'primary' => true
            ]
        ];
        $structure->options = [
            'log_moderator' => true
        ];

        return $structure;
    }

    /**
     * @return \ThemeHouse\Covers\Repository\CoverHandler
     */
    protected function getCoverHandlerRepo()
    {
        return $this->repository('ThemeHouse\Covers:CoverHandler');
    }
}