<?php

namespace ThemeHouse\Covers\ApprovalQueue;

use XF\ApprovalQueue\AbstractHandler;
use XF\Mvc\Entity\Entity;

/**
 * Class Cover
 * @package ThemeHouse\Covers\ApprovalQueue
 */
class Cover extends AbstractHandler
{
    /**
     * @param Entity $content
     * @param null $error
     * @return bool
     */
    protected function canActionContent(Entity $content, &$error = null)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $content->canApproveUnapprove($error);
    }

    /**
     * @param \ThemeHouse\Covers\Entity\Cover $cover
     */
    public function actionApprove(\ThemeHouse\Covers\Entity\Cover $cover)
    {
        $this->quickUpdate($cover, 'cover_state', 'visible');
    }

    /**
     * @param \ThemeHouse\Covers\Entity\Cover $cover
     */
    public function actionDelete(\ThemeHouse\Covers\Entity\Cover $cover)
    {
        $this->quickUpdate($cover, 'cover_state', 'deleted');
    }
}