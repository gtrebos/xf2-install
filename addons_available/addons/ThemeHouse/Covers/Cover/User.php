<?php

namespace ThemeHouse\Covers\Cover;

use XF\Mvc\Entity\Entity;

/**
 * Class User
 * @package ThemeHouse\Covers\Cover
 */
class User extends AbstractHandler
{
    /**
     * @return string
     */
    public function getContentId()
    {
        return 'user_id';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'username';
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return 'user';
    }

    /**
     * @return string
     */
    public function getContentRoute()
    {
        return 'members';
    }

    /**
     * @param Entity $entity
     * @param null $error
     * @return bool
     */
    public function canEditCover(Entity $entity, &$error = null)
    {
        /** @var \XF\Entity\User $entity */
        return $entity->canEdit() || ($entity->user_id === \XF::visitor()->user_id && $entity->canEditProfile());
    }
}