<?php

namespace ThemeHouse\Covers\Cover;

use XF\Mvc\Entity\Entity;

/**
 * Class AbstractHandler
 * @package ThemeHouse\Covers\Cover
 */
abstract class AbstractHandler
{
    protected $content;

    abstract public function getContentId();

    abstract public function getContentTitle();

    abstract public function getContentType();

    abstract public function getContentRoute();

    /**
     * @param $id
     * @return null|\XF\Mvc\Entity\ArrayCollection|Entity
     */
    public function getContent($id)
    {
        return $this->content = \XF::app()->findByContentType($this->getContentType(), $id, $this->getEntityWith());
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getEntityWith()
    {
        return [];
    }

    public function getDefaultCover() {
        $finder = \XF::finder('ThemeHouse\Covers:CoverPreset');

        $finder->whereSql("FIND_IN_SET('{$this->getContentType()}', default_for)");

        return $finder->fetchOne();
    }

    /**
     * @param bool $content
     * @param string $prefix
     * @return mixed|string
     */
    public function getContentUrl($content = false, $prefix = '', array $params = [])
    {
        if (!$content) {
            $content = $this->content;
        }

        return \XF::app()->router((!empty($prefix) ? $prefix . ':' : '') . 'public')->buildLink($this->getContentRoute(),
            $content, $params);
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->content->user_id;
    }

    /**
     * @param Entity $entity
     * @param null $error
     * @return bool
     */
    public function canViewCover(Entity $entity, &$error = null)
    {
        if (method_exists($entity, 'canView')) {
            return $entity->canView($error);
        }

        return true;
    }

    /**
     * @param Entity $entity
     * @param null $error
     * @return bool
     */
    public function canEditCover(Entity $entity, &$error = null)
    {
        if (method_exists($entity, 'canEdit')) {
            return $entity->canEdit($error);
        }

        return true;
    }

    /**
     * @param Entity $entity
     * @param null $error
     * @return bool
     */
    public function canDeleteCover(Entity $entity, &$error = null)
    {
        if (method_exists($entity, 'canDelete')) {
            return $entity->canDelete($error);
        }

        return true;
    }

    /**
     * @return \XF\Phrase
     */
    public function getContentTypePhrase()
    {
        return \XF::app()->getContentTypePhrase($this->getContentType());
    }
}