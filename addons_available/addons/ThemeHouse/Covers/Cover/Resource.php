<?php

namespace ThemeHouse\Covers\Cover;

/**
 * Class Resource
 * @package ThemeHouse\Covers\Cover
 */
class Resource extends AbstractHandler
{
    /**
     * @return string
     */
    public function getContentId()
    {
        return 'resource_id';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'title';
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return 'resource';
    }

    /**
     * @return string
     */
    public function getContentRoute()
    {
        return 'resources';
    }
}