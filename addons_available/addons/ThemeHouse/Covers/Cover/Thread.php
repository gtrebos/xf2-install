<?php

namespace ThemeHouse\Covers\Cover;

/**
 * Class Thread
 * @package ThemeHouse\Covers\Cover
 */
class Thread extends AbstractHandler
{
    /**
     * @return string
     */
    public function getContentId()
    {
        return 'thread_id';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'title';
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return 'thread';
    }

    /**
     * @return string
     */
    public function getContentRoute()
    {
        return 'threads';
    }
}