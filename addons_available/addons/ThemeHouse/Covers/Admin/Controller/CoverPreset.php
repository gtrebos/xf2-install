<?php

namespace ThemeHouse\Covers\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\ControllerPlugin\Toggle;
use XF\Mvc\ParameterBag;

/**
 * Class CoverPreset
 * @package ThemeHouse\Covers\Admin\Controller
 */
class CoverPreset extends AbstractController
{
    /**
     * @param $action
     * @param ParameterBag $params
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function preDispatchController($action, ParameterBag $params)
    {
        $this->assertAdminPermission('th_cover');
    }

    /**
     * @return \XF\Mvc\Reply\View
     */
    public function actionIndex()
    {
        $coverPresets = $this->getCoverPresetRepo()->getCoverPresetList();

        $viewParams = [
            'coverPresets' => $coverPresets,
        ];

        return $this->view('ThemeHouse\Covers:CoverPreset\Listing', 'thcovers_cover_preset_list', $viewParams);
    }

    /**
     * @param \ThemeHouse\Covers\Entity\CoverPreset $coverPreset
     * @return \XF\Mvc\Reply\View
     */
    public function coverPresetAddEdit(\ThemeHouse\Covers\Entity\CoverPreset $coverPreset)
    {
        $userCriteria = $this->app->criteria('XF:User', $coverPreset->user_criteria);

        $viewParams = [
            'coverPreset' => $coverPreset,
            'userCriteria' => $userCriteria,
            'handlers' => $this->getCoverHandlerRepo()->getCoverHandlers()
        ];

        return $this->view('ThemeHouse\Covers:CoverPreset\Edit', 'thcovers_cover_preset_edit', $viewParams);
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionEdit(ParameterBag $params)
    {
        $coverPreset = $this->assertCoverPresetExists($params['cover_preset_id']);
        return $this->coverPresetAddEdit($coverPreset);
    }

    /**
     * @return \XF\Mvc\Reply\View
     */
    public function actionAdd()
    {
        /** @var \ThemeHouse\Covers\Entity\CoverPreset $coverPreset */
        $coverPreset = $this->em()->create('ThemeHouse\Covers:CoverPreset');

        return $this->coverPresetAddEdit($coverPreset);
    }

    /**
     * @param \ThemeHouse\Covers\Entity\CoverPreset $coverPreset
     * @return \XF\Mvc\FormAction
     */
    protected function coverPresetSaveProcess(\ThemeHouse\Covers\Entity\CoverPreset $coverPreset)
    {
        $entityInput = $this->filter([
            'title' => 'str',
            'cover_image' => 'array',
            'cover_styling' => 'array',
            'display_order' => 'str',
            'enabled' => 'bool',
            'user_criteria' => 'array'
        ]);

        $defaults = $this->filter('defaults', 'array');
        $entityInput['default_for'] = $defaults;

        $form = $this->formAction();
        $form->basicEntitySave($coverPreset, $entityInput);

        if($defaults) {
            $form->complete(function () use ($defaults, $coverPreset) {
                $finder = $this->finder('ThemeHouse\Covers:CoverPreset');

                $queries = [];

                foreach ($defaults as $default) {
                    $queries[] = "FIND_IN_SET('{$default}', default_for)";
                }

                $finder
                    ->where('cover_preset_id', '<>', $coverPreset->cover_preset_id)
                    ->whereSql(implode(' OR ', $queries));

                $coverPresets = $finder->fetch();

                foreach ($coverPresets as $coverPreset) {
                    $coverPreset->default_for = array_diff($coverPreset->default_for, $defaults);
                    $coverPreset->save();
                }
            });
        }

        return $form;
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect
     * @throws \XF\Mvc\Reply\Exception
     * @throws \XF\PrintableException
     */
    public function actionSave(ParameterBag $params)
    {
        $this->assertPostOnly();

        if ($params['cover_preset_id']) {
            $coverPreset = $this->assertCoverPresetExists($params['cover_preset_id']);
        } else {
            $coverPreset = $this->em()->create('ThemeHouse\Covers:CoverPreset');
        }

        $this->coverPresetSaveProcess($coverPreset)->run();

        if ($this->request->exists('exit')) {
            $redirect = $this->buildLink('cover-presets');
        } else {
            $redirect = $this->buildLink('cover-presets/edit', $coverPreset);
        }

        return $this->redirect($redirect);
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionDelete(ParameterBag $params)
    {
        $coverPreset = $this->assertCoverPresetExists($params['cover_preset_id']);

        if ($this->isPost()) {
            $coverPreset->delete();
            return $this->redirect($this->buildLink('cover-presets'));
        } else {
            $viewParams = [
                'coverPreset' => $coverPreset
            ];

            return $this->view('ThemeHouse\Covers:CoverPreset\Delete', 'thcovers_cover_preset_delete', $viewParams);
        }
    }

    /**
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     */
    public function actionSort()
    {
        $coverPresets = $this->getCoverPresetRepo()->getCoverPresetList();

        if ($this->isPost()) {

            $lastOrder = 0;
            foreach (json_decode($this->filter('presets', 'string')) as $presetValue) {
                $lastOrder += 10;

                /** @var \ThemeHouse\Covers\Entity\CoverPreset $preset */
                $preset = $coverPresets[$presetValue->id];
                $preset->display_order = $lastOrder;
                $preset->saveIfChanged();
            }

            return $this->redirect($this->buildLink('cover-presets'));
        } else {
            $viewParams = [
                'coverPresets' => $coverPresets
            ];
            return $this->view('ThemeHouse\Covers:CoverPreset\Sort', 'thcovers_cover_preset_sort', $viewParams);
        }
    }


    /**
     * @return \XF\Mvc\Reply\Message
     */
    public function actionToggle()
    {
        /** @var Toggle $plugin */
        $plugin = $this->plugin('XF:Toggle');
        return $plugin->actionToggle('ThemeHouse\Covers:CoverPreset', 'enabled');
    }

    /**
     * @param string $id
     * @param array|string|null $with
     * @param null|string $phraseKey
     *
     * @return \ThemeHouse\Covers\Entity\CoverPreset
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function assertCoverPresetExists($id, $with = null, $phraseKey = null)
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->assertRecordExists('ThemeHouse\Covers:CoverPreset', $id, $with, $phraseKey);
    }

    /**
     * @return \ThemeHouse\Covers\Repository\CoverPreset
     */
    protected function getCoverPresetRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Covers:CoverPreset');
    }

    /**
     * @return \ThemeHouse\Covers\Repository\CoverPreset
     */
    protected function getCoverHandlerRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Covers:CoverHandler');
    }
}