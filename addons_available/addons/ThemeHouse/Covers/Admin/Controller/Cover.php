<?php

namespace ThemeHouse\Covers\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\Mvc\ParameterBag;
use XF\Util\Color;

/**
 * Class Cover
 * @package ThemeHouse\Covers\Admin\Controller
 */
class Cover extends AbstractController
{
    /**
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\Reroute|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     * @throws \Exception
     */
    public function actionIndex()
    {
        $linkFilters = [];

        $page = $this->filterPage();
        $perPage = 20;

        if ($this->request->exists('delete_covers')) {
            return $this->rerouteController(__CLASS__, 'delete');
        }

        /** @var \ThemeHouse\Covers\Repository\Cover $coverRepo */
        $coverRepo = $this->getCoverRepo();

        $coverFinder = $coverRepo->findCoversForList()->limitByPage($page, $perPage);

        if ($contentType = $this->filter('content_type', 'str')) {
            $coverFinder->where('content_type', $contentType);
            $linkFilters['content_type'] = $contentType;
        }

        if ($username = $this->filter('username', 'str')) {
            /** @var \XF\Entity\User $user */
            $user = $this->finder('XF:User')->where('username', $username)->fetchOne();
            if ($user) {
                $coverFinder->where('cover_user_id', $user->user_id);
                $linkFilters['username'] = $user->username;
            }
        }

        if ($start = $this->filter('start', 'datetime')) {
            $coverFinder->where('cover_date', '>', $start);
            $linkFilters['start'] = $start;
        }

        if ($end = $this->filter('end', 'datetime')) {
            $coverFinder->where('cover_date', '<', $end);
            $linkFilters['end'] = $end;
        }

        if ($linkFilters && $this->isPost()) {
            return $this->redirect($this->buildLink('covers', null, $linkFilters), '');
        }

        $total = $coverFinder->total();
        $this->assertValidPage($page, $perPage, $total, 'covers');

        $covers = $coverFinder->fetch();
        foreach($covers as $key => $cover) {
            if(!$cover->Content) {
                $covers->offsetUnset($key);
                $cover->delete();
            }
        }

        $viewParams = [
            'covers' => $covers,
            'handlers' => $coverRepo->getCoverHandlers(),

            'page' => $page,
            'perPage' => $perPage,
            'total' => $total,

            'linkFilters' => $linkFilters,

            'datePresets' => \XF::language()->getDatePresets()
        ];
        return $this->view('ThemeHouse\Covers:Cover\Listing', 'thcovers_list', $viewParams);
    }


    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     * @throws \XF\PrintableException
     */
    public function actionPreset(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id);

        /** @var \ThemeHouse\Covers\Repository\CoverPreset $presetRepo */
        $presetRepo = $this->getCoverPresetRepo();

        $presets = $presetRepo->findCoverPresetsForList()->fetch();

        $csrfValid = $this->validateCsrfToken($this->filter('t', 'str'));

        if ($this->request->exists('cover_preset_id') && $csrfValid) {
            $presetId = $this->filter('cover_preset_id', 'int');

            if ($presetId != 0) {
                if (!$presets->offsetExists($presetId)) {
                    return $this->noPermission();
                }

                /** @var \ThemeHouse\Covers\Service\Cover\Deleter $deleter */
                $deleter = $this->service('ThemeHouse\Covers:Cover\Deleter', $cover);
                $deleter->delete();
            }

            $cover->cover_preset = $presetId;

            $cover->cover_state = $presetId || $cover->cover_image || $cover->cover_styling ? 'visible' : 'deleted';

            $cover->save();

            if($cover->cover_state === 'visible') {
                return $this->redirect($this->buildLink('covers/view', $cover));
            }
            else {
                return $this->redirect($this->buildLink('covers'));
            }
        } else {
            $viewParams = [
                'presets' => $presets,
                'cover' => $cover
            ];

            return $this->view('ThemeHouse\Covers:Cover\Preset', 'thcovers_cover_preset', $viewParams);
        }
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Error|\XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionImage(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id);

        /** @var \ThemeHouse\Covers\Repository\Cover $coverRepo */
        $coverRepo = $this->getCoverRepo();

        if ($this->isPost()) {
            /** @var \ThemeHouse\Covers\Service\Image $coverImageService */
            $coverImageService = $this->service('ThemeHouse\Covers:Image', $cover->content_id, $cover->content_type);
            $coverImageType = $this->filter('cover_image_type', 'str');
            $coverDetails = [];

            if ($coverImageType == 'custom') {
                $upload = $this->request->getFile('upload', false);
                if (!empty($upload)) {
                    if (!$coverImageService->setImageFromUpload($upload)) {
                        return $this->error($coverImageService->getError());
                    }
                }

                $coverImageUrl = $this->filter('cover_image_url', 'str');
                if (!empty($coverImageUrl)) {
                    if (!$coverImageService->downloadImage($coverImageUrl)) {
                        return $this->error($coverImageService->getError());
                    }
                }

                $coverImageDetails = $coverImageService->updateCoverImage();
                if (!$coverImageDetails) {
                    return $this->error(\XF::phrase('thcovers_new_cover_could_not_be_processed'));
                }

                $coverDetails = $coverDetails + $coverImageDetails;
            }

            $editor = $this->setupCoverEdit($cover, $coverDetails);
            $errors = null;
            if (!$editor->validate($errors)) {
                return $this->error($errors);
            }

            $editor->save();

            if ($this->filter('_xfWithData', 'bool')) {
                $covers = [];
                $coverCodes = array_keys($coverRepo->getCoverSizeMap());
                foreach ($coverCodes AS $code) {
                    $covers[$code] = $this->app->templater()->fn('cover', [$cover, $code]);
                }
            }
            return $this->redirect($this->buildLink('covers/view', $cover));
        } else {
            $viewParams = [
                'cover' => $cover,
                'maxSize' => $coverRepo->getCoverSizeMap()['m'],
            ];

            return $this->view('ThemeHouse\Covers:Cover\Image', 'thcovers_cover_image', $viewParams);
        }
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionDelete(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id);

        if ($this->isPost()) {
            /** @var \ThemeHouse\Covers\Service\Cover\Deleter $deleter */
            $deleter = $this->service('ThemeHouse\Covers:Cover\Deleter', $cover);
            $deleter->delete();

            return $this->redirect($this->buildLink('covers'));
        } else {
            $viewParams = [
                'cover' => $cover
            ];

            return $this->view('ThemeHouse\Covers:Cover\Delete', 'thcovers_cover_delete', $viewParams);
        }
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionStyle(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id);

        if ($this->isPost()) {
            $this->coverStylingProcess($cover)->run();
            return $this->redirect($this->buildLink('covers', $cover));

        } else {
            $viewParams = [
                'cover' => $cover
            ];

            return $this->view('ThemeHouse\Covers:Cover\Style', 'thcovers_cover_style', $viewParams);
        }
    }

    /**
     * @param \ThemeHouse\Covers\Entity\Cover $cover
     * @return \XF\Mvc\FormAction
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function coverStylingProcess(\ThemeHouse\Covers\Entity\Cover $cover)
    {
        $form = $this->formAction();

        if ($this->filter('delete', 'bool')) {
            $input = [
                'cover_styling' => [],
                'cover_state' => $cover->cover_image ? 'visible' : 'deleted'
            ];
        } else {
            $empty = true;

            $bgColor = $this->filter('background_color', 'str');

            if ($bgColor) {
                if ($bgColor && !Color::isValidColor($bgColor)) {
                    throw $this->errorException(\XF::phrase('thcovers_invalid_color'));
                }
                $empty = false;
            }

            $input = [
                'cover_styling' => [
                    'background_color' => $bgColor
                ],
                'cover_state' => $empty ? ($cover->cover_image ? 'visible' : 'deleted') : 'visible'
            ];
        }

        $form->basicEntitySave($cover, $input);

        return $form;
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Error|\XF\Mvc\Reply\Redirect
     * @throws \XF\PrintableException
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionPosition(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id);

        if (!$this->isPost()) {
            return $this->noPermission();
        }

        $coverDetails['cover_image'] = $cover->cover_image;

        $crop = $this->filter([
            'cropX' => 'float',
            'cropY' => 'float',
        ]);

        $coverDetails['cover_image']['cropX'] = $crop['cropX'];
        $coverDetails['cover_image']['cropY'] = $crop['cropY'];

        $editor = $this->setupCoverEdit($cover, $coverDetails);
        $errors = null;
        if (!$editor->validate($errors)) {
            return $this->error($errors);
        }

        $editor->save();

        return $this->redirect($this->buildLink('covers', $cover));
    }

    /**
     * @param \ThemeHouse\Covers\Entity\Cover $cover
     * @param array $coverDetails
     * @return \ThemeHouse\Covers\Service\Cover\Editor
     */
    protected function setupCoverEdit(\ThemeHouse\Covers\Entity\Cover $cover, array $coverDetails = [])
    {
        /** @var \ThemeHouse\Covers\Service\Cover\Editor $coverEditorService */
        $coverEditorService = $this->service('ThemeHouse\Covers:Cover\Editor', $cover);
        $coverEditorService->setDefaults();
        $coverEditorService->setCoverDetails($coverDetails);

        return $coverEditorService;
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionView(ParameterBag $params)
    {
        /** @var \ThemeHouse\Covers\Entity\Cover $cover */
        /** @noinspection PhpUndefinedFieldInspection */
        $cover = $this->assertCoverExists($params->cover_id, ['CoverUser']);

        $viewParams = [
            'cover' => $cover,
            'entity' => $cover->Content
        ];

        return $this->view('ThemeHouse\Covers:Cover\View', 'thcovers_cover_view', $viewParams);
    }

    /**
     * @param $id
     * @param null $with
     * @param null $phraseKey
     * @return \XF\Mvc\Entity\Entity
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function assertCoverExists($id, $with = null, $phraseKey = null)
    {
        return $this->assertRecordExists('ThemeHouse\Covers:Cover', $id, $with, $phraseKey);
    }

    /**
     * @return \XF\Mvc\Entity\Repository
     */
    protected function getCoverRepo()
    {
        return $this->repository('ThemeHouse\Covers:Cover');
    }

    /**
     *
     * @return \ThemeHouse\Covers\Repository\Cover
     */
    protected function getCoverPresetRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Covers:CoverPreset');
    }
}