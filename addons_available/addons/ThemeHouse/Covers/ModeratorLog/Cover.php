<?php

namespace ThemeHouse\Covers\ModeratorLog;

use XF\Entity\User;
use XF\ModeratorLog\AbstractHandler;
use XF\Entity\ModeratorLog;
use XF\Mvc\Entity\Entity;

/**
 * Class Cover
 * @package ThemeHouse\Covers\ModeratorLog
 */
class Cover extends AbstractHandler
{
    /**
     * @param Entity $content
     * @param $action
     * @param User $actor
     * @return bool
     */
    public function isLoggable(Entity $content, $action, User $actor)
    {
        switch ($action) {
            case 'edit':
                if ($actor->user_id == $content->user_id) {
                    return false;
                }
        }

        return parent::isLoggable($content, $action, $actor);
    }

    /**
     * @param Entity $content
     * @param $field
     * @param $newValue
     * @param $oldValue
     * @return array|bool|string
     */
    protected function getLogActionForChange(Entity $content, $field, $newValue, $oldValue)
    {
        switch ($field) {
            case 'cover_date':
                return 'edit';

            case 'cover_state':
                if ($newValue == 'visible' && $oldValue == 'moderated') {
                    return 'approve';
                } else {
                    if ($newValue == 'visible' && $oldValue == 'deleted') {
                        return 'undelete';
                    } else {
                        if ($newValue == 'deleted') {
                            /** @noinspection PhpUndefinedFieldInspection */
                            $reason = $content->DeletionLog ? $content->DeletionLog->delete_reason : '';
                            return ['delete_soft', ['reason' => $reason]];
                        } else {
                            if ($newValue == 'moderated') {
                                return 'unapprove';
                            }
                        }
                    }
                }

                break;
        }

        return false;
    }

    /**
     * @param ModeratorLog $log
     * @param Entity $content
     */
    protected function setupLogEntityContent(ModeratorLog $log, Entity $content)
    {
        /** @var \ThemeHouse\Covers\Cover\AbstractHandler $coverHandler */
        /** @noinspection PhpUndefinedMethodInspection */
        $coverHandler = \XF::Repository('ThemeHouse\Covers:CoverHandler')->getCoverHandler($content->getEntityContentType(),
            true);
        $cover = $content->Cover;

        $log->content_user_id = $cover->cover_user_id;
        $log->content_user_id = $cover->CoverUser->username;
        $log->content_type = $content->getEntityContentType();
        $log->content_id = $content->getEntityId();
        $log->content_url = $coverHandler->getContentUrl($content, 'nopath');
    }

    /**
     * @param ModeratorLog $log
     * @return null|string|string[]|\XF\Phrase
     */
    public function getContentTitle(ModeratorLog $log)
    {
        return \XF::phrase('th_x_cover_covers', [
            'content_type' => \XF::app()->stringFormatter()->censorText($log->content_title_)
        ]);
    }
}