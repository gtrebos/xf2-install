<?php

namespace ThemeHouse\Covers\Listener;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

/**
 * Class EntityStructure
 * @package ThemeHouse\Covers\Listener
 */
class EntityStructure
{
    /**
     * @param Manager $em
     * @param Structure $structure
     */
    public static function coverEntityHandlers(Manager $em, Structure &$structure)
    {

        /** @noinspection PhpUndefinedMethodInspection */
        if (isset($structure->contentType) && \XF::Repository('ThemeHouse\Covers:CoverHandler')->getCoverHandler($structure->contentType)) {
            $structure->relations['Cover'] = [
                'entity' => 'ThemeHouse\Covers:Cover',
                'type' => Entity::TO_ONE,
                'conditions' => [
                    ['content_type', '=', $structure->contentType],
                    ['content_id', '=', '$' . $structure->primaryKey]
                ],
                'key' => 'content_id'
            ];

            /* if (is_array($structure->defaultWith)) {
                 $structure->defaultWith[] = 'Cover';
             } else {
                 $structure->defaultWith = array($structure->defaultWith, 'Cover');
             }*/
        }
    }
}