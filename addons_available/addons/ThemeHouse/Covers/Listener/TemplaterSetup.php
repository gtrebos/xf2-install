<?php

namespace ThemeHouse\Covers\Listener;

use XF\Container;
use XF\Mvc\Entity\Entity;
use ThemeHouse\Covers\Entity\Cover;
use XF\Template\Templater;

/**
 * Class TemplaterSetup
 * @package ThemeHouse\Covers\Listener
 */
class TemplaterSetup
{
    protected static $templater;

    protected static $preset;

    public static function run(Container $container, Templater &$templater)
    {
        self::$templater = $templater;

        $templater->addFunction('get_cover', ['\ThemeHouse\Covers\Listener\TemplaterSetup', 'fnGetCover']);
        $templater->addFunction('cover', ['\ThemeHouse\Covers\Listener\TemplaterSetup', 'fnCover']);
        $templater->addFunction('cover_class', ['\ThemeHouse\Covers\Listener\TemplaterSetup', 'fnCoverClass']);
        $templater->addFunction('cover_styling', ['\ThemeHouse\Covers\Listener\TemplaterSetup', 'fnCoverStyling']);
    }

    public static function fnGetCover(Templater $templater, &$escape, Entity $entity)
    {
        return $entity->getRelationOrDefault('Cover', true);
        /* if (!$cover = self::isValidCover($entity)) {
         $cover = \XF::em()->create('ThemeHouse\Covers:Cover');
         $cover->bulkSet([
         'content_type' => $entity->getEntityContentType(),
         'content_id' => $entity->getEntityId()
         ]);

         return $cover;
         }

         return $entity->Cover;*/
    }

    public static function fnCover(Templater $templater, &$escape, Entity $entity)
    {
        if (!$cover = self::isValidCover($entity)) {
            return false;
        }

        return null;
    }

    public static function fnCoverClass(Templater $templater, &$escape, Entity $entity, $json = false)
    {
        $escape = true;

        $cover = self::isValidCover($entity);

        $default = null;

        if (!$cover || $cover->cover_state !== 'visible') {
            /** @var \ThemeHouse\Covers\Cover\AbstractHandler $handler */
            /** @noinspection PhpUndefinedMethodInspection */
            $handler = \XF::repository('ThemeHouse\Covers:CoverHandler')
                ->getCoverHandler($entity->getEntityContentType());

            $default = $handler->getDefaultCover();
        }

        if (!$default) {
            if (!$cover = self::isValidCover($entity)) {
                return false;
            }

            if ($cover->cover_state != 'visible') {
                return false;
            }

            if ($json) {
                return '.cover .cover-' . $cover->content_type . ' .cover-' . $cover->content_type . '-' . $cover->content_id . (!empty($cover->cover_image) ? ' .cover-hasImage' : '');
            }

            return 'cover cover-' . $cover->content_type . ' cover-' . $cover->content_type . '-' . $cover->content_id . (!empty($cover->cover_image) ? ' cover-hasImage' : '');
        } else {
            return 'cover cover-' . $entity->getEntityContentType() . (!empty($cover->cover_image) ? ' cover-hasImage' : '');
        }
    }

    public static function fnCoverStyling(
        Templater $templater,
        &$escape,
        Entity $entity,
        $sizeCode = 'l',
        $canonical = false
    ) {
        $escape = true;
        $cover = self::isValidCover($entity);

        $default = null;

        if (!$cover || $cover->cover_state !== 'visible') {
            /** @var \ThemeHouse\Covers\Cover\AbstractHandler $handler */
            /** @noinspection PhpUndefinedMethodInspection */
            $handler = \XF::repository('ThemeHouse\Covers:CoverHandler')
                ->getCoverHandler($entity->getEntityContentType());

            $default = $handler->getDefaultCover();
        }

        if (!$default) {
            /** @var Cover $cover */
            if (!$cover) {
                return false;
            }

            if ($cover->cover_state !== 'visible') {
                return false;
            }

            $preset = $cover->CoverPreset;
        } else {
            if ($cover->cover_state !== 'visible') {
                $cover = null;
            }

            $preset = $default;
        }

        $styling = '';

        /* User defined Cover */
        if ($cover->cover_image) {
            if (isset($cover->cover_image['cropY'])) {
                $styling .= "background-position-y: {$cover->cover_image['cropY']}%; ";
            }

            if ($cover_url = self::getCoverRepo()->getCoverUrl($cover->content_type,
                $cover->content_id, $cover->cover_image, $sizeCode, $canonical)) {

                $styling .= "background-image: url('{$cover_url}'); ";
            }
        } /* Preset Cover */
        else {
            if ($preset && $preset->cover_image) {
                if ($preset->cover_image['url']) {
                    $styling .= "background-image: url('{$preset->cover_image['url']}'); ";
                }
            }
        }

        /* User defined Cover */
        if ($cover->cover_styling) {
            foreach ($cover->cover_styling as $property => $value) {
                if ($value) {
                    $styling .= str_replace('_', '-', $property) . ': ' . $value . '; ';
                }
            }
        } /* Preset Cover */
        else {
            if ($preset && $preset->cover_styling) {
                foreach ($preset->cover_styling as $property => $value) {
                    if ($value) {
                        $styling .= str_replace('_', '-', $property) . ': ' . $value . '; ';
                    }
                }
            }
        }


        return $styling;
    }

    /**
     * @param $entity
     * @return bool|Cover
     */
    protected
    static function isValidCover(
        $entity
    ) {
        if ($entity instanceof Cover) {
            return $entity;
        }

        if (!empty($entity->Cover) && $entity->Cover instanceof Cover
            && $entity->Cover->canView() && !$entity->Cover->isInsert()) {
            return $entity->Cover;
        }

        return false;
    }

    /**
     * @return \ThemeHouse\Covers\Repository\Cover
     */
    protected
    static function getCoverRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return \XF::Repository('ThemeHouse\Covers:Cover');
    }
}
