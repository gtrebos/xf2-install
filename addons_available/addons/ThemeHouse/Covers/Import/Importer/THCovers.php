<?php

namespace ThemeHouse\Covers\Import\Importer;

use XF\Import\Importer\AbstractImporter;
use XF\Import\StepState;
use XF\Util\File;

class THCovers extends AbstractImporter
{

    public static function getListInfo()
    {
        return [
            'target' => '[TH] Covers',
            'source' => 'ThemeHouse Covers'
        ];
    }

    protected function getBaseConfigDefault()
    {
        return [];
    }

    public function renderBaseConfigOptions(array $vars)
    {
        return $this->app->templater()->renderTemplate('admin:thcovers_import_config', $vars);
    }

    public function validateBaseConfig(array &$baseConfig, array &$errors)
    {
        return true;
    }

    protected function getStepConfigDefault()
    {
        return [];
    }

    public function renderStepConfigOptions(array $vars)
    {
        return $this->app->templater()->renderTemplate('admin:thcovers_import_step_config', $vars);
    }

    public function validateStepConfig(array $steps, array &$stepConfig, array &$errors)
    {
        return true;
    }

    public function canRetainIds()
    {
        return false;
    }

    public function resetDataForRetainIds()
    {
        return false;
    }

    public function getSteps()
    {
        return [
            'profileCovers' => ['title' => 'Profile Covers'],
            'resourceCovers' => ['title' => 'Resource Covers']
        ];
    }

    public function getStepEndProfileCovers() {
        return $this->db()->fetchOne('SELECT max(user_id) FROM xf_user');
    }

    /**
     * @param StepState $state
     * @param array $stepConfig
     * @param $maxTime
     * @return StepState
     * @throws \XF\PrintableException
     */
    public function stepProfileCovers(StepState $state, array $stepConfig, $maxTime) {
        $limit = 15;

        $users = $this->db()->fetchAll("
            SELECT *
            FROM xf_user_profile
            WHERE user_id > ? AND user_id <= ?
            ORDER BY user_id
            LIMIT {$limit}
        ", [
            $state->startAfter,
            $state->end
        ]);

        /** @var \ThemeHouse\Covers\Repository\Cover $repository */
        $repository = \XF::repository('ThemeHouse\Covers:Cover');

        foreach($users as $user) {
            /** @var \ThemeHouse\Covers\Entity\Cover $cover */
            $cover = \XF::em()->create('ThemeHouse\Covers:Cover');

            if(!$user['profile_cover_image'] || $user['profile_cover_state'] !== 'visible') {
                $state->imported++;
                $state->startAfter = $user['user_id'];
                continue;
            }

            $filePath = $repository->getAbstractedCustomCoverPath('profile', $user['user_id'], 'l');
            $tempFile = File::copyAbstractedPathToTempFile($filePath);
            $newFilePath = $repository->getAbstractedCustomCoverPath('user', $user['user_id'], 'o');
            File::copyFileToAbstractedPath($tempFile, $newFilePath);
            unlink($tempFile);

            $cover->content_type = 'user';
            $cover->content_id = $user['user_id'];
            $cover->cover_date = $user['profile_cover_date'];

            /** @var \ThemeHouse\Covers\Service\Cover\Image $coverService */
            $coverService = \XF::service('ThemeHouse\Covers:Cover\Image', $user['user_id'], 'user');
            $coverService->setImageFromExisting();
            $imageDetails = $coverService->updateCoverImage();

            $imageDetails += [
                'cropX' => $user['profile_cover_crop_x'],
                'cropY' => $user['profile_cover_crop_y']
            ];

            $cover->cover_image = $imageDetails;
            $cover->cover_styling = [
                'background_color' => $user['profile_cover_color']
            ];
            $cover->save();

            $state->imported++;
            $state->startAfter = $user['user_id'];
        }

        if($state->startAfter === $state->end) {
            return $state->complete();
        }

        return $state;
    }

    /**
     * @param StepState $state
     * @param array $stepConfig
     * @param $maxTime
     * @return StepState
     * @throws \XF\PrintableException
     */
    public function stepResourceCovers(StepState $state, array $stepConfig, $maxTime) {
        $limit = 15;

        $resources = $this->db()->fetchAll("
            SELECT *
            FROM xf_rm_resource
            WHERE resource_id > ? AND resource_id <= ?
            ORDER BY resource_id
            LIMIT {$limit}
        ", [
            $state->startAfter,
            $state->end
        ]);

        /** @var \ThemeHouse\Covers\Repository\Cover $repository */
        $repository = \XF::repository('ThemeHouse\Covers:Cover');

        foreach($resources as $resource) {
            /** @var \ThemeHouse\Covers\Entity\Cover $cover */
            $cover = \XF::em()->create('ThemeHouse\Covers:Cover');

            if(!$resource['resource_cover_image'] || $resource['resource_cover_state'] !== 'visible') {
                $state->imported++;
                $state->startAfter = $resource['resource_id'];
                continue;
            }

            $filePath = $repository->getAbstractedCustomCoverPath('resource', $resource['resource_id'], 'l');
            $tempFile = File::copyAbstractedPathToTempFile($filePath);
            $newFilePath = $repository->getAbstractedCustomCoverPath('resource', $resource['resource_id'], 'o');
            File::copyFileToAbstractedPath($tempFile, $newFilePath);
            unlink($tempFile);

            $cover->content_type = 'resource';
            $cover->content_id = $resource['resource_id'];
            $cover->cover_date = $resource['resource_cover_date'];

            /** @var \ThemeHouse\Covers\Service\Cover\Image $coverService */
            $coverService = \XF::service('ThemeHouse\Covers:Cover\Image', $resource['resource_id'], 'resource');
            $coverService->setImageFromExisting();
            $imageDetails = $coverService->updateCoverImage();

            $imageDetails += [
                'cropX' => $resource['resource_cover_crop_x'],
                'cropY' => $resource['resource_cover_crop_y']
            ];

            $cover->cover_image = $imageDetails;
            $cover->cover_styling = [
                'background_color' => $resource['resource_cover_color']
            ];
            $cover->save();

            $state->imported++;
            $state->startAfter = $resource['resource_id'];
        }

        if($state->startAfter === $state->end) {
            return $state->complete();
        }

        return $state;
    }

    public function getStepEndResourceCovers() {
        return $this->db()->fetchOne('SELECT max(resource_id) FROM xf_rm_resource') ?: 0;
    }

    protected function doInitializeSource()
    {
        return true;
    }

    public function getFinalizeJobs(array $stepsRun)
    {
        return [];
    }
}