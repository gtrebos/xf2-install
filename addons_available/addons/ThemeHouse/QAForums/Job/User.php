<?php

namespace ThemeHouse\QAForums\Job;

use XF\Job\AbstractJob;

class User extends AbstractJob
{
    protected $defaultData = [
        'start' => 0,
        'batch' => 100
    ];

    public function run($maxRunTime)
    {
        $startTime = microtime(true);

        $db = $this->app->db();
        $em = $this->app->em();

        $ids = $db->fetchAllColumn($db->limit(
            "
				SELECT user_id
				FROM xf_user
				WHERE user_id > ?
				ORDER BY user_id
			",
            $this->data['batch']
        ), $this->data['start']);
        if (!$ids) {
            return $this->complete();
        }

        $done = 0;

        foreach ($ids as $id) {
            if (microtime(true) - $startTime >= $maxRunTime) {
                break;
            }

            $this->data['start'] = $id;

            $user = $em->find('XF:User', $id);
            if (!$user) {
                continue;
            }

            $db->beginTransaction();

            $user->rebuildQAVoteCounts();

            $db->commit();

            $done++;
        }

        $this->data['batch'] = $this->calculateOptimalBatch($this->data['batch'], $done, $startTime, $maxRunTime, 1000);

        return $this->resume();
    }

    public function getStatusMessage()
    {
        $actionPhrase = \XF::phrase('rebuilding');
        $typePhrase = \XF::phrase('users');
        return sprintf('%s... %s (%s)', $actionPhrase, $typePhrase, $this->data['start']);
    }

    public function canCancel()
    {
        return true;
    }

    public function canTriggerByChoice()
    {
        return true;
    }
}
