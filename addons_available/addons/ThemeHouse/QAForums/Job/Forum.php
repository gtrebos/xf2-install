<?php

namespace ThemeHouse\QAForums\Job;

use XF\Job\AbstractJob;

class Forum extends AbstractJob
{
    protected $defaultData = [
        'start' => 0,
        'batch' => 100
    ];

    public function run($maxRunTime)
    {
        $startTime = microtime(true);

        $db = $this->app->db();
        $em = $this->app->em();

        $ids = $db->fetchAllColumn($db->limit(
            "
				SELECT node_id
				FROM xf_forum
				WHERE node_id > ?
				ORDER BY node_id
			",
            $this->data['batch']
        ), $this->data['start']);
        if (!$ids) {
            return $this->complete();
        }

        $done = 0;

        foreach ($ids as $id) {
            if (microtime(true) - $startTime >= $maxRunTime) {
                break;
            }

            $this->data['start'] = $id;

            $forum = $em->find('XF:Forum', $id);
            if (!$forum) {
                continue;
            }

            $db->beginTransaction();

            $forum->rebuildQAVoteCounts();

            $db->commit();

            $done++;
        }

        $this->data['batch'] = $this->calculateOptimalBatch($this->data['batch'], $done, $startTime, $maxRunTime, 1000);

        return $this->resume();
    }

    public function getStatusMessage()
    {
        $actionPhrase = \XF::phrase('rebuilding');
        $typePhrase = \XF::phrase('forums');
        return sprintf('%s... %s (%s)', $actionPhrase, $typePhrase, $this->data['start']);
    }

    public function canCancel()
    {
        return true;
    }

    public function canTriggerByChoice()
    {
        return true;
    }
}
