<?php

namespace ThemeHouse\QAForums\Import\Importer;

use XF\Import\Importer\AbstractImporter;

abstract class AbstractQAForum extends AbstractImporter
{
    public function isBeta()
    {
        return true;
    }

    /**
     * @var \XF\Db\Mysqli\Adapter
     */
    protected $sourceDb;

    public function canRetainIds()
    {
        return false;
    }

    public function getFinalizeJobs(array $stepsRun)
    {
        return [
            'ThemeHouse\QAForums:User'
        ];
    }

    public function resetDataForRetainIds()
    {
        return false;
    }

    protected function getBaseConfigDefault()
    {
        return [];
    }

    public function renderBaseConfigOptions(array $vars) {}

    public function validateBaseConfig(array &$baseConfig, array &$errors)
    {
        return true;
    }

    protected function getStepConfigDefault()
    {
        return [];
    }

    public function renderStepConfigOptions(array $vars) {}

    public function validateStepConfig(array $steps, array &$stepConfig, array &$errors)
    {
        return true;
    }

    protected function doInitializeSource()
    {
        $this->sourceDb = $this->db();
    }
}