<?php

namespace ThemeHouse\QAForums\Import\Importer;

use XF\Import\StepState;

class NanocodeBestAnswer extends AbstractQAForum
{
    public static function getListInfo()
    {
        return [
            'target' => '[TH] Question & Answer Forums',
            'source' => '[n] Best Answer / Q&A System',
        ];
    }

    public function getSteps()
    {
        return [
            'votes' => [
                'title' => 'Votes',
            ],
        ];
    }

    public function getStepEndVotes()
    {
        return $this->db()->fetchOne('SELECT MAX(vote_id) FROM ba_votes') ?: 0;
    }

    public function stepVotes(StepState $state, array $stepConfig, $maxTime)
    {
        $limit = 100;

        $votes = $this->db()->fetchAll('
            SELECT vote.*, forum.th_qaforum, thread.bestanswer
            FROM ba_votes AS vote
            LEFT JOIN xf_post AS post ON post.post_id = vote.post_id
            LEFT JOIN xf_thread AS thread ON thread.thread_id = vote.thread_id
            LEFT JOIN xf_forum AS forum ON forum.node_id = thread.node_id
            WHERE vote.vote_id > ?
              AND vote.vote_id <= ?
              AND forum.th_qaforum = 1
            LIMIT ' . $limit, [
            $state->startAfter,
            $state->end,
        ]);
        if (!$votes) {
            return $state->complete();
        }

        foreach ($votes as $vote) {
            $state->startAfter = $vote['vote_id'];
            /** @var \XF\Entity\Post $post */
            $post = $this->app->finder('XF:Post')->with('Thread')->with('Thread.Forum')->where('post_id', '=', $vote['post_id'])->fetchOne();

            /** @var \XF\Entity\Thread $thread */
            $thread = $post->Thread;

            /** @var \XF\Entity\Forum $forum */
            $forum = $thread->Forum;

            $existingVote = $this->app->finder('ThemeHouse\QAForums:Vote')->where([
                'post_id' => $post->post_id,
                'user_id' => $vote['vote_user_id'],
            ])->fetch()->count();

            if ($existingVote) continue;
            if (!$forum->th_qaforum) continue;

            if (!$thread->th_is_qa_qaforum) {
                $thread->th_is_qa_qaforum = true;
            }
            if (!$thread->th_answered_qaforum && $vote['bestanswer']) {
                $thread->th_answered_qaforum = true;
            }
            $thread->save();

            if ($post->post_id === $vote['bestanswer'] && !$post->th_best_answer_qaforum) {
                $post->th_best_answer_qaforum = 1;
                $post->save();
            }

            $newVote = $this->em()->create('ThemeHouse\QAForums:Vote');
            $newVote->bulkSet([
                'user_id' => $vote['vote_user_id'],
                'post_id' => $post->post_id,
                'vote_type' => 'up',
                'vote_date' => $vote['vote_date'],
            ]);
            $newVote->save();

            $state->imported++;
        }

        return $state;
    }
}