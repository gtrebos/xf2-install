<?php

namespace ThemeHouse\QAForums\Option;

class ThreadPrefix extends \XF\Option\AbstractOption
{
    public static function renderSelect(\XF\Entity\Option $option, array $htmlParams)
    {
        $data = self::getSelectData($option, $htmlParams);

        return self::getTemplater()->formSelectRow(
            $data['controlOptions'], $data['choices'], $data['rowOptions']
        );
    }

    protected static function getSelectData(\XF\Entity\Option $option, array $htmlParams)
    {
        /** @var \XF\Repository\ThreadPrefix $prefixRepo */
        $prefixRepo = \XF::repository('XF:ThreadPrefix');

        $prefixes = $prefixRepo->findPrefixesForList()->fetch();

        $choices = [];

        $choices[] = [
            'value' => 0,
            'label' => \XF::phrase('no_prefix'),
        ];
        foreach ($prefixes as $prefix) {
            /** @var \XF\Entity\ThreadPrefix $prefix */
            $choices[] = [
                'value' => $prefix->prefix_id,
                'label' => $prefix->getTitle(),
            ];
        }

        return [
            'choices' => $choices,
            'controlOptions' => self::getControlOptions($option, $htmlParams),
            'rowOptions' => self::getRowOptions($option, $htmlParams)
        ];
    }
}