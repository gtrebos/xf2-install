<?php

namespace ThemeHouse\QAForums\Listener;

use XF\Container;

class ImportImporterClasses
{
    public static function importImporterClasses(\XF\SubContainer\Import $container, Container $parentContainer, array &$importers)
    {
        $importers[] = '\ThemeHouse\QAForums:NanocodeBestAnswer';
    }
}
