<?php

namespace ThemeHouse\QAForums\Listener\TemplaterMacroPreRender\Admin\HelperCriteria;

use XF\Template\Templater;

class UserPanes
{
    public static function templaterMacroPreRender(Templater $templater, &$type, &$template, &$name, array &$arguments, array &$globalVars)
    {
        $nodeRepo = \XF::repository('XF:Node');
        $nodeTree = $nodeRepo->createNodeTree($nodeRepo->getFullNodeList(null, 'NodeType'));

        $arguments['data']['thqaforums_nodetree'] = $nodeTree;
    }
}
