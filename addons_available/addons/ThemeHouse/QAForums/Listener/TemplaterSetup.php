<?php

namespace ThemeHouse\QAForums\Listener;

use XF\Container;
use XF\Template\Templater;
use ThemeHouse\QAForums\Util\Number;

class TemplaterSetup
{
    public static function templaterSetup(Container $container, Templater &$templater)
    {
        $templater->addFilter('th_friendlynumber_qaforum', function (Templater $templater, $value, &$escape) {
            return Number::formatFriendlyNumber($value);
        });
    }
}
