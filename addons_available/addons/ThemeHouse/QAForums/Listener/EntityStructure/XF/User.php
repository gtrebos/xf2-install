<?php

namespace ThemeHouse\QAForums\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class User
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $structure->columns['th_best_answers_qaforum'] = [
            'type' => Entity::UINT,
            'default' => 0,
        ];

        $structure->columns['th_points_qaforum'] = [
            'type' => Entity::INT,
            'default' => 0,
        ];

        $structure->columns['th_up_votes_qaforum'] = [
            'type' => Entity::UINT,
            'default' => 0,
        ];

        $structure->columns['th_down_votes_qaforum'] = [
            'type' => Entity::UINT,
            'default' => 0,
        ];

        $structure->relations['ForumUserBestAnswers'] = [
            'type' => Entity::TO_MANY,
            'entity' => 'ThemeHouse\QAForums:ForumUserBestAnswers',
            'conditions' => [
                'user_id',
            ],
        ];
    }
}
