<?php

namespace ThemeHouse\QAForums\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Post
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $visitor = \XF::visitor();

        $structure->columns['th_best_answer_qaforum'] = [
            'type' => Entity::BOOL,
            'default' => false,
        ];

        $structure->columns['th_points_qaforum'] = [
            'type' => Entity::INT,
            'default' => 0,
        ];

        $structure->columns['th_up_votes_qaforum'] = [
            'type' => Entity::UINT,
            'default' => 0,
        ];

        $structure->columns['th_down_votes_qaforum'] = [
            'type' => Entity::UINT,
            'default' => 0,
        ];

        $structure->relations['CurrentVote'] = [
            'type' => Entity::TO_ONE,
            'entity' => 'ThemeHouse\QAForums:Vote',
            'conditions' => [
                'post_id',
                ['user_id', '=', $visitor->user_id],
            ],
        ];

        $structure->relations['Thread']['with'][] = 'Forum.ForumUserBestAnswers';
    }
}
