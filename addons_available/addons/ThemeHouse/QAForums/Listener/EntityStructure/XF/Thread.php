<?php

namespace ThemeHouse\QAForums\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Thread
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $structure->columns['th_answered_qaforum'] = [
            'type' => Entity::BOOL,
            'default' => false,
        ];

        $structure->columns['th_is_qa_qaforum'] = [
            'type' => Entity::BOOL,
            'default' => false,
        ];
    }
}
