<?php

namespace ThemeHouse\QAForums\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Forum
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $visitor = \XF::visitor();

        $structure->columns['th_qaforum'] = [
            'type' => Entity::BOOL,
            'default' => false,
        ];

        $structure->columns['th_force_qa_qaforum'] = [
            'type' => Entity::BOOL,
            'default' => false,
        ];

        $structure->relations['ForumUserBestAnswers'] = [
            'type' => Entity::TO_ONE,
            'entity' => 'ThemeHouse\QAForums:ForumUserBestAnswers',
            'conditions' => [
                'node_id',
                ['user_id', '=', $visitor->user_id],
            ],
        ];
    }
}
