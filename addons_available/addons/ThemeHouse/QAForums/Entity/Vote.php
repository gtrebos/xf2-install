<?php

namespace ThemeHouse\QAForums\Entity;

use XF\Entity\Post;
use XF\Entity\User;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * Class Vote
 * @package ThemeHouse\QAForums\Entity
 *
 * @property integer vote_id
 * @property integer post_id
 * @property integer user_id
 * @property string vote_type
 * @property integer vote_date
 *
 * @property User User
 * @property Post Post
 */
class Vote extends Entity
{
    protected function _postSave()
    {
        $this->rebuildPostVoteCounts();
    }

    protected function _postDelete()
    {
        $this->rebuildPostVoteCounts();
    }

    public function rebuildPostVoteCounts()
    {
        if ($this->Post) {
            $this->Post->rebuildQAVoteCounts();
        }

        if ($this->User) {
            $this->User->rebuildQAVoteCounts();
        }
    }

    public static function getStructure(Structure $structure)
    {
        $structure->table = 'xf_th_vote_qaforum';
        $structure->shortName = 'ThemeHouse\QAForums:Vote';
        $structure->primaryKey = 'vote_id';
        $structure->columns = [
            'vote_id' => ['type' => self::UINT, 'autoIncrement' => true],
            'post_id' => ['type' => self::UINT],
            'user_id' => ['type' => self::UINT],
            'vote_type' => ['type' => self::STR],
            'vote_date' => ['type' => self::UINT, 'default' => \XF::$time],
        ];
        $structure->getters = [];
        $structure->relations = [
            'User' => [
                'entity' => 'XF:User',
                'type' => self::TO_ONE,
                'conditions' => 'user_id',
            ],
            'Post' => [
                'entity' => 'XF:Post',
                'type' => self::TO_ONE,
                'conditions' => 'post_id',
            ],
        ];

        return $structure;
    }
}
