<?php

namespace ThemeHouse\QAForums\XF\InlineMod;

use XF\Mvc\Entity\Entity;

class Thread extends XFCP_Thread
{
    public function getPossibleActions()
    {
        /** @var array $actions */
        $actions = parent::getPossibleActions();

        $actions['th_addqa_qaforums'] = $this->getSimpleActionHandler(
            \XF::phrase('th_add_question_status_qaForums'),
            'canAddQAStatus',
            function(Entity $entity) {
                if ($entity->discussion_type != 'redirect' && !$entity->th_is_qa_qaforum && $entity->Forum->th_qaforum) {
                    /** @var \XF\Entity\Thread $entity */
                    $entity->th_is_qa_qaforum = true;
                    $entity->save();
                }
            }
        );

        $actions['th_removeqa_qaforums'] = $this->getSimpleActionHandler(
            \XF::phrase('th_remove_question_status_qaForums'),
            'canRemoveQAStatus',
            function(Entity $entity) {
                if ($entity->discussion_type != 'redirect' && $entity->th_is_qa_qaforum && !$entity->Forum->th_force_qa_qaforum) {
                    /** @var \XF\Entity\Thread $entity */
                    $entity->th_is_qa_qaforum = false;
                    $entity->save();
                }
            }
        );

        return $actions;
    }
}