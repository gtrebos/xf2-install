<?php

namespace ThemeHouse\QAForums\XF\Service\Thread;

class Creator extends XFCP_Creator
{
    public function setQaState($qaState)
    {
        $this->thread->th_is_qa_qaforum = $qaState;
    }
}