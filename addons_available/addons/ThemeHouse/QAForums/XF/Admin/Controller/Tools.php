<?php

namespace ThemeHouse\QAForums\XF\Admin\Controller;

class Tools extends XFCP_Tools
{
    public function actionRebuild()
    {
        $response = parent::actionRebuild();

        if ($response instanceof \XF\Mvc\Reply\View) {
            $installedAddOns = $this->finder('XF:AddOn')->fetch();

            if (!empty($installedAddOns['ApanticBestAnswer'])) {
                $response->setParam('th_apantticBestAnswer_qaForum', true);
            }
        }

        return $response;
    }

    public function actionUninstallNanocodeBestAnswer()
    {
        $schemaManager = $this->app()->db()->getSchemaManager();

        try {
            $schemaManager->dropTable('ba_votes');
            $schemaManager->alterTable('xf_user', function (\XF\Db\Schema\Alter $table) {
                $table->dropColumns(['bestanswers']);
            });
            $schemaManager->alterTable('xf_thread', function (\XF\Db\Schema\Alter $table) {
                $table->dropColumns(['bestanswer', 'ba_alternativeanswers']);
            });
            $schemaManager->alterTable('xf_post', function (\XF\Db\Schema\Alter $table) {
                $table->dropColumns(['ba_votes']);
            });
        } catch (\Exception $e) {
        }

        return $this->redirect($this->buildLink('add-ons/ApanticBestAnswer/uninstall'));
    }
}
