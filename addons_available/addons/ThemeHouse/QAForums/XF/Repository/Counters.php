<?php

namespace ThemeHouse\QAForums\XF\Repository;

class Counters extends XFCP_Counters
{
    public function getForumStatisticsCacheData()
    {
        $cache = parent::getForumStatisticsCacheData();

        $cache += $this->getTHQAAnsweredTotals();
        $cache += $this->getTHQAUnansweredTotals();
        return $cache;
    }

    public function getTHQAUnansweredTotals()
    {
        return $this->db()->fetchRow("
			SELECT COUNT(*) AS thqaunanswered
			FROM xf_thread
			WHERE th_is_qa_qaforum = 1
			  AND th_answered_qaforum = 0
		");
    }

    public function getTHQAAnsweredTotals()
    {
        return $this->db()->fetchRow("
			SELECT COUNT(*) AS thqaanswered
			FROM xf_thread
			WHERE th_is_qa_qaforum = 1
			  AND th_answered_qaforum = 1
		");
    }
}