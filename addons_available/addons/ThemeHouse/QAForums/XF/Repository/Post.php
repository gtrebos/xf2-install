<?php

namespace ThemeHouse\QAForums\XF\Repository;

class Post extends XFCP_Post
{
    public function findPostsForThreadVotesView(\XF\Entity\Thread $thread, $limits = [])
    {
        $finder = $this->findPostsForThreadView($thread, $limits);

        $finder->where('post_id', '!=', $thread->first_post_id);

        $finder->resetOrder();
        $finder->orderByQAVotes();

        return $finder;
    }

    public function sendBestAnswerAlert(\XF\Entity\Post $post, \XF\Entity\User $viewingUser = null)
    {
        if (!$viewingUser) {
            $viewingUser = \XF::visitor();
        }

        if (!$post->User) {
            return false;
        }

        /** @var \XF\Repository\UserAlert $alertRepo */
        $alertRepo = \XF::repository('XF:UserAlert');
        return $alertRepo->alertFromUser(
            $post->User, $viewingUser, 'post', $post->post_id, 'best_answer'
        );
    }

    public function removeBestAnswerAlert(\XF\Entity\Post $post)
    {
        /** @var \XF\Repository\UserAlert $alertRepo */
        $alertRepo = \XF::repository('XF:UserAlert');
        $alertRepo->fastDeleteAlertsToUser($post->user_id, 'post', $post->post_id, 'best_answer');
    }
}