<?php

namespace ThemeHouse\QAForums\XF\ControllerPlugin;

class Thread extends XFCP_Thread
{
    public function getPostLink(\XF\Entity\Post $post)
    {
        $thread = $post->Thread;
        if (!$thread) {
            throw new \LogicException("Post has no thread");
        }

        /** @var \XF\Entity\Forum $forum */
        $forum = $thread->Forum;
        $options = \XF::options();

        if ($forum->th_qaforum && $options->th_defaultThreadView_qaForums === 'votes') {
            $page = floor($post->position / $this->options()->messagesPerPage) + 1;
            return $this->buildLink('threads', $thread, ['page' => $page, 'view' => 'date']) . '#post-' . $post->post_id;
        }

        return parent::getPostLink($post);
    }
}