<?php

namespace ThemeHouse\QAForums\XF\Entity;

class Thread extends XFCP_Thread
{
    public function getBestAnswer()
    {
        return $this->finder('XF:Post')->where('thread_id', '=', $this->thread_id)
            ->where('th_best_answer_qaforum', '=', true)->fetchOne();
    }

    protected function _preSave()
    {
        parent::_preSave();

        $options = \XF::options();

        if ($this->isInsert() && $this->th_is_qa_qaforum && !$this->prefix_id && $options->th_qaPrefix_qaForums) {
            $prefix = $this->em()->find('XF:ThreadPrefix', $options->th_qaPrefix_qaForums);
            if ($prefix && $this->Forum->isPrefixValid($prefix)) {
                $this->prefix_id = $options->th_qaPrefix_qaForums;
            }
        }

        if ($this->isUpdate() && $this->isChanged('th_answered_qaforum')) {
            $answered = $this->th_answered_qaforum;
            $prefix = false;
            if ($answered && $options->th_answeredPrefix_qaForums) {
                $prefix = $this->em()->find('XF:ThreadPrefix', $options->th_answeredPrefix_qaForums);
            }

            if (!$answered && $options->th_qaPrefix_qaForums) {
                $prefix = $this->em()->find('XF:ThreadPrefix', $options->th_qaPrefix_qaForums);
            }

            if ($prefix && $this->Forum->isPrefixValid($prefix)) {
                $this->prefix_id = $prefix->prefix_id;
            }
        }

        if (!$this->isInsert() && $this->isChanged('node_id')) {
            if ($this->th_is_qa_qaforum && !$this->Forum->th_qaforum) {
                $this->th_is_qa_qaforum = 0;
                $this->th_answered_qaforum = 0;
            }

            if (!$this->th_is_qa_qaforum && $this->Forum->th_qaforum && $this->Forum->th_force_qa_qaforum) {
                $this->th_is_qa_qaforum = 1;
            }
        }
    }

    protected function _postSave()
    {
        parent::_postSave();

        if ($this->isChanged('th_is_qa_qaforum') && !$this->th_is_qa_qaforum) {
            $this->db()->update('xf_post', [
                'th_best_answer_qaforum' => 0,
                'th_points_qaforum' => 0,
                'th_up_votes_qaforum' => 0,
                'th_down_votes_qaforum' => 0,
            ], 'thread_id=?', [$this->thread_id]);
        }
    }

    public function canAddQAStatus()
    {
        if (!$this->canUseInlineModeration()) {
            return false;
        }

        return true;
    }

    public function canRemoveQAStatus()
    {
        if (!$this->canUseInlineModeration()) {
            return false;
        }

        return true;
    }
}