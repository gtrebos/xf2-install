<?php

namespace ThemeHouse\QAForums\XF\Entity;

class User extends XFCP_User
{
    public function rebuildQAVoteCounts()
    {
        $upVotes = $this->finder('ThemeHouse\QAForums:Vote')->with('Post')->where('Post.user_id', '=', $this->user_id)->where('vote_type', '=', 'up')->total();
        $downVotes = $this->finder('ThemeHouse\QAForums:Vote')->with('Post')->where('Post.user_id', '=', $this->user_id)->where('vote_type', '=', 'down')->total();
        $bestAnswers = $this->finder('XF:Post')->with('Thread')->with('Thread.Forum')->where('user_id', '=', $this->user_id)->where('th_best_answer_qaforum', '=', 1)->where('Thread.Forum.th_qaforum', '=', 1)->total();

        $voteCount = $upVotes - $downVotes;

        $this->th_up_votes_qaforum = $upVotes;
        $this->th_down_votes_qaforum = $downVotes;
        $this->th_points_qaforum = $voteCount;
        $this->th_best_answers_qaforum = $bestAnswers;
        $this->save();
    }
}
