<?php

namespace ThemeHouse\QAForums\XF\Entity;

class Forum extends XFCP_Forum
{
    public function rebuildQAVoteCounts()
    {
        $db = $this->db();

        $db->beginTransaction();
        $db->delete('xf_th_qaforums_forum_user_best_answers', 'node_id = ?', $this->node_id);
        $db->query("
			INSERT INTO xf_th_qaforums_forum_user_best_answers (node_id, user_id, best_answers)
			SELECT node_id, post.user_id, COUNT(*)
            FROM xf_post AS post
            INNER JOIN xf_thread AS thread ON (post.thread_id = thread.thread_id)
			WHERE thread.node_id = ?
				AND message_state = 'visible'
                AND post.user_id > 0
                AND th_best_answer_qaforum = 1
			GROUP BY user_id
		", $this->node_id);
        $db->commit();
    }
}
