<?php

namespace ThemeHouse\QAForums\XF\Entity;

class Post extends XFCP_Post
{
    /**
     * @throws \XF\PrintableException
     */
    public function rebuildQAVoteCounts()
    {
        $upVotes = $this->finder('ThemeHouse\QAForums:Vote')
            ->where('post_id', '=', $this->post_id)
            ->where('vote_type', '=', 'up')
            ->total();
        $downVotes = $this->finder('ThemeHouse\QAForums:Vote')
            ->where('post_id', '=', $this->post_id)
            ->where('vote_type', '=', 'down')
            ->total();

        $voteCount = $upVotes - $downVotes;

        $this->th_up_votes_qaforum = $upVotes;
        $this->th_down_votes_qaforum = $downVotes;
        $this->th_points_qaforum = $voteCount;
        $this->save();
    }

    public function canMarkAsBestAnswer(&$error = null)
    {
        $visitor = \XF::visitor();
        $thread = $this->Thread;
        $forum = $thread->Forum;
        $nodeId = $thread->node_id;

        $canAccept = false;

        if (!$forum->th_qaforum || !$thread->th_is_qa_qaforum) {
            return false;
        }

        if ($this->isFirstPost()) {
            return false;
        }

        if (!$this->canView($error)) {
            return false;
        }

        if (!$visitor->user_id || !$thread) {
            return false;
        }

        if ($thread->user_id === $visitor->user_id && $visitor->hasNodePermission($nodeId, 'th_bestAnswerOwnThread')) {
            $canAccept = true;
        }

        if ($visitor->hasNodePermission($nodeId, 'th_bestAnswerAnyThread')) {
            $canAccept = true;
        }

        return $canAccept;
    }

    public function canVoteAnswer($returnFalseIfVoted = false, &$error = null)
    {
        $user = \XF::visitor();

        // User must be logged in to vote
        if (!$user->user_id) {
            return false;
        }

        if ($this->user_id === $user->user_id) {
            return false;
        }

        if ($this->Thread->user_id === $user->user_id && !$user->hasNodePermission($this->Thread->node_id, 'th_voteAnswerOwnThread')) {
            return false;
        }

        if ($this->Thread->user_id !== $user->user_id && !$user->hasNodePermission($this->Thread->node_id, 'th_voteAnswer')) {
            return false;
        }

        return true;
    }

    public function canViewVoteDetails(&$error = null)
    {
        $user = \XF::visitor();

        return $user->hasNodePermission($this->Thread->node_id, 'th_viewVotes');
    }

    public function isBelowPointThreshold()
    {
        if ($this->Thread->th_is_qa_qaforum && !$this->isFirstPost()) {
            $pointThreshold = \XF::options()->th_hidePostThreshold_qaForums;
            if (!empty($pointThreshold['enabled'])) {
                if ($this->th_points_qaforum < $pointThreshold['points']) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @throws \XF\PrintableException
     */
    protected function _postSave()
    {
        parent::_postSave();

        if (!$this->isInsert()) {
            if ($this->isChanged('th_best_answer_qaforum')) {
                if ($this->th_best_answer_qaforum) {
                    $bestAnswers = $this->User->th_best_answers_qaforum + 1;
                    $this->adjustQAForumUserBestAnswerCount(1);
                } else {
                    $bestAnswers = $this->User->th_best_answers_qaforum - 1;
                    $this->adjustQAForumUserBestAnswerCount(-1);
                }
                if ($bestAnswers < 0) {
                    $bestAnswers = 0;
                }

                $this->User->th_best_answers_qaforum = $bestAnswers;
                $this->User->save();
            }
        }
    }

    protected function adjustQAForumUserBestAnswerCount($amount)
    {
        if ($this->user_id) {
            $db = $this->db();

            if ($amount > 0) {
                $db->insert('xf_th_qaforums_forum_user_best_answers', [
                    'node_id' => $this->Thread->node_id,
                    'user_id' => $this->user_id,
                    'best_answers' => $amount
                ], false, 'best_answers = best_answers + VALUES(best_answers)');
            } else {
                $existingValue = $db->fetchOne("
					SELECT best_answers
					FROM xf_th_qaforums_forum_user_best_answers
					WHERE node_id = ?
						AND user_id = ?
				", [$this->Thread->node_id, $this->user_id]);
                if ($existingValue !== null) {
                    $newValue = $existingValue + $amount;
                    if ($newValue <= 0) {
                        $this->db()->delete(
                            'xf_th_qaforums_forum_user_best_answers',
                            'node_id = ? AND user_id = ?',
                            [$this->Thread->node_id, $this->user_id]
                        );
                    } else {
                        $this->db()->update(
                            'xf_th_qaforums_forum_user_best_answers',
                            ['best_answers' => $newValue],
                            'node_id = ? AND user_id = ?',
                            [$this->Thread->node_id, $this->user_id]
                        );
                    }
                }
            }
        }
    }

    /**
     * @throws \XF\PrintableException
     */
    protected function _postDelete()
    {
        parent::_postDelete();

        if ($this->th_best_answer_qaforum) {
            $bestAnswers = $this->User->th_best_answers_qaforum - 1;
            $this->adjustQAForumUserBestAnswerCount(-1);
            if ($bestAnswers < 0) {
                $bestAnswers = 0;
            }

            $this->User->th_best_answers_qaforum = $bestAnswers;
            $this->User->save();
        }
    }
}
