<?php

namespace ThemeHouse\QAForums\XF\Pub\Controller;

class Forum extends XFCP_Forum
{
    protected function setupThreadCreate(\XF\Entity\Forum $forum)
    {
        $creator = parent::setupThreadCreate($forum);

        if ($forum->th_qaforum) {
            if ($forum->th_force_qa_qaforum) {
                $creator->setQaState(1);
            } else {
                $qaState = $this->filter('th_is_qa_qaforum', 'bool');
                $creator->setQaState($qaState);
            }
        }

        return $creator;
    }
}