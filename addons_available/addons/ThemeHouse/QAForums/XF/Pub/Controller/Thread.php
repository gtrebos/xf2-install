<?php

namespace ThemeHouse\QAForums\XF\Pub\Controller;

use XF\Mvc\Entity\ArrayCollection;
use XF\Mvc\ParameterBag;

class Thread extends XFCP_Thread
{
    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Reroute|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionIndex(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id, $this->getThreadViewExtraWith());

        /** @var \XF\Entity\Forum $forum */
        $forum = $thread->Forum;

        if ($thread->th_is_qa_qaforum) {
            $options = \XF::options();
            $view = $this->filter('view', 'string', $options->th_defaultThreadView_qaForums);

            if ($view == 'votes') {
                return $this->rerouteController(__CLASS__, 'threadVotes', $params);
            } else {
                $response = parent::actionIndex($params);

                if ($response instanceof \XF\Mvc\Reply\View) {
                    $pageNavParams = $response->getParam('pageNavParams');
                    if (!$pageNavParams) {
                        $pageNavParams = [];
                    }
                    $pageNavParams['view'] = 'date';

                    $response->setParam('pageNavParams', $pageNavParams);
                }

                return $response;

            }
        }

        return parent::actionIndex($params);
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Error|\XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionThreadVotes(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id, $this->getThreadViewExtraWith());

        if ($thread->discussion_type == 'redirect') {
            if (!$thread->Redirect) {
                return $this->noPermission();
            }

            return $this->redirectPermanently($this->request->convertToAbsoluteUri($thread->Redirect->target_url));
        }

        $threadRepo = $this->getThreadRepo();
        /** @var \ThemeHouse\QAForums\XF\Repository\Post $postRepo */
        $postRepo = $this->getPostRepo();

        $page = $params->page;
        $perPage = $this->options()->messagesPerPage;

        $this->assertValidPage($page, $perPage, $thread->reply_count + 1, 'threads', $thread);
        $this->assertCanonicalUrl($this->buildLink('threads', $thread, ['page' => $page]));

        /** @noinspection PhpUndefinedMethodInspection */
        /** @var \XF\Mvc\Entity\Finder $postList */
        $postList = $postRepo->findPostsForThreadVotesView($thread)->onVotesPage($page, $perPage);
        $posts = $postList->fetch();

        /** @var \XF\Repository\Attachment $attachmentRepo */
        $attachmentRepo = $this->repository('XF:Attachment');
        $attachmentRepo->addAttachmentsToContent($posts, 'post');

        $lastPost = $thread->LastPost;
        if (!$lastPost) {
            if ($page > 1) {
                return $this->redirect($this->buildLink('threads', $thread));
            } else {
                // should never really happen
                return $this->error(\XF::phrase('something_went_wrong_please_try_again'));
            }
        }

        $firstPost = $posts->first();

        /** @var \XF\Entity\Post $post */

        $canInlineMod = false;
        foreach ($posts AS $post) {
            if ($post->canUseInlineModeration()) {
                $canInlineMod = true;
                break;
            }
        }

        $firstUnread = null;
        foreach ($posts AS $post) {
            if ($post->isUnread()) {
                $firstUnread = $post;
                break;
            }
        }

        $poll = ($thread->discussion_type == 'poll' ? $thread->Poll : null);

        $threadRepo->markThreadReadByVisitor($thread, $lastPost->post_date);
        $threadRepo->logThreadView($thread);

        $viewParams = [
            'thread' => $thread,
            'forum' => $thread->Forum,
            'posts' => $posts,
            'firstPost' => $firstPost,
            'lastPost' => $lastPost,
            'firstUnread' => $firstUnread,

            'poll' => $poll,

            'canInlineMod' => $canInlineMod,

            'page' => $page,
            'perPage' => $perPage,

            'pageNavParams' => [
                'view' => 'votes'
            ],

            'attachmentData' => $this->getReplyAttachmentData($thread),

            'pendingApproval' => $this->filter('pending_approval', 'bool'),

            'th_view_qaForum' => 'votes',
        ];
        return $this->view('XF:Thread\View', 'thread_view', $viewParams);
    }

    protected function getNewPostsReply(\XF\Entity\Thread $thread, $lastDate)
    {
        $response = parent::getNewPostsReply($thread, $lastDate);
        if ($thread->Forum->th_qaforum) {
            $response->setParam('firstUnshownPost', false);
            $postsOrig = $response->getParam('posts');

            if($postsOrig) {
                $posts = new ArrayCollection([
                    $postsOrig->last(),
                ]);
            }
            else {
                $posts = [];
            }
            $response->setParam('posts', $posts);
        }

        return $response;
    }
}