<?php

namespace ThemeHouse\QAForums\XF\Pub\Controller;

use ThemeHouse\QAForums\Util\Number;
use XF\Mvc\ParameterBag;

class Post extends XFCP_Post
{
    public function actionBestAnswer(ParameterBag $params)
    {
        $this->assertValidCsrfToken();

        $visitor = \XF::visitor();

        /** @var \ThemeHouse\QAForums\XF\Entity\Post $post */
        $post = $this->assertViewablePost($params['post_id']);

        /** @var \ThemeHouse\QAForums\XF\Entity\Thread $thread */
        $thread = $post->Thread;

        /** @var \ThemeHouse\QAForums\XF\Repository\Post $postRepo */
        $postRepo = $this->getPostRepo();

        if (!$thread->th_is_qa_qaforum) {
            return $this->notFound();
        }

        if (!$post->canMarkAsBestAnswer($error)) {
            return $this->noPermission($error);
        }

        if ($post->th_best_answer_qaforum) {
            $post->th_best_answer_qaforum = false;
            $post->save();

            $thread->th_answered_qaforum = false;
            $thread->save();

            $postRepo->removeBestAnswerAlert($post);
        } elseif ($thread->th_answered_qaforum) {
            $bestAnswer = $thread->getBestAnswer();
            if ($bestAnswer) {
                $bestAnswer->th_best_answer_qaforum = false;
                $bestAnswer->save();
            }

            $post->th_best_answer_qaforum = true;
            $post->save();

            $thread->th_answered_qaforum = true;
            $thread->save();

            $postRepo->removeBestAnswerAlert($bestAnswer);
            $postRepo->sendBestAnswerAlert($post);
        } else {
            $post->th_best_answer_qaforum = true;
            $post->save();

            $thread->th_answered_qaforum = true;
            $thread->save();

            $postRepo->sendBestAnswerAlert($post);
        }

        return $this->redirect($this->buildLink('posts', $post));
    }

    public function actionUpvoteAnswer(ParameterBag $params)
    {
        return $this->voteAnswerProcess($params['post_id'], 'up');
    }

    public function actionDownvoteAnswer(ParameterBag $params)
    {
        return $this->voteAnswerProcess($params['post_id'], 'down');
    }

    public function actionQaVotes(ParameterBag $params)
    {
        $post = $this->assertViewablePost($params->post_id);

        if (!$post->canViewVoteDetails()) {
            return $this->noPermission();
        }

        $breadcrumbs = $post->Thread->getBreadcrumbs();

        $page = $this->filterPage();
        $perPage = 50;

        $votes = $this->finder('ThemeHouse\QAForums:Vote')->where('post_id', '=', $post->post_id)->with('User')->order('vote_date', 'desc')
            ->limitByPage($page, $perPage, 1)->fetch();

        $hasNext = count($votes) > $perPage;
        $votes = $votes->slice(0, $perPage);

        $viewParams = [
            'post' => $post,

            'votes' => $votes,
            'hasNext' => $hasNext,
            'page' => $page,

            'breadcrumbs' => $breadcrumbs,
        ];
        return $this->view('ThemeHouse\QAForums:Post\Votes', 'th_vote_list_qaForums', $viewParams);

    }

    protected function voteAnswerProcess($postId, $voteType='up')
    {
        $this->assertValidCsrfToken();

        $visitor = \XF::visitor();
        // This should never happen, so no need for a phrase.
        if (!in_array($voteType, ['up', 'down'])) {
            throw $this->errorException('Invalid vote type', 400);
        }

        /** @var \ThemeHouse\QAForums\XF\Entity\Post $post */
        $post = $this->assertViewablePost($postId);

        /** @var \XF\Entity\Thread $thread */
        $thread = $post->Thread;

        /** @var \XF\Entity\Forum $forum */
        $forum = $thread->Forum;

        if (!$forum->th_qaforum) {
            return $this->notFound();
        }

        if (!$post->canVoteAnswer()) {
            return $this->noPermission();
        }

        if ($post->CurrentVote) {
            $vote = $post->CurrentVote;

            if ($vote->vote_type === $voteType) {
                $vote->delete();
            } else {
                $vote->vote_type = $voteType;
                $vote->save();
            }
        } else {
            $vote = $this->em()->create('ThemeHouse\QAForums:Vote');
            $vote->bulkSet([
                'user_id' => $visitor->user_id,
                'post_id' => $post->post_id,
                'vote_type' => $voteType,
            ]);
            $vote->save();
        }

        if ($this->filter('_xfWithData', 'bool')) {
            if ($vote->isDeleted()) {
                $post->hydrateRelation('CurrentVote', null);
            } else {
                $post->hydrateRelation('CurrentVote', $vote);
            }
            $viewParams = [
                'post' => $post,
            ];

            return $this->view('ThemeHouse\QAForums:Post\VoteAnswer', 'th_post_vote_count_qaForums', $viewParams);
        }

        return $this->redirect($this->buildLink('posts', $post));
    }
}