<?php

namespace ThemeHouse\Nodes\Listener;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

/**
 * Class EntityStructure
 * @package ThemeHouse\Nodes\Listener
 */
class EntityStructure
{
    public static function xfNode(Manager $em, Structure &$structure)
    {
        $structure->relations['NodeStyling'] = [
            'entity' => 'ThemeHouse\Nodes:NodeStyling',
            'type' => Entity::TO_MANY,
            'conditions' => 'node_id',
        ];
    }
}