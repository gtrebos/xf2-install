<?php

namespace ThemeHouse\WatchForums\XF\Entity;

class User extends XFCP_User
{
    public function canViewThreadsInWatchedForums()
    {
        return $this->user_id && $this->hasPermission('general', 'th_threadsInWatchedForums');
    }
}
