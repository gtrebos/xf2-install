<?php

namespace ThemeHouse\WatchForums\XF\Entity;

class Forum extends XFCP_Forum
{
    public function canWatch(&$error = null)
    {
        $visitor = \XF::visitor();

        if (empty($this->Watch[$visitor->user_id]) && !$visitor->hasNodePermission($this->node_id, 'th_watchForum')) {
            return false;
        }

        return parent::canWatch($error);
    }
}
