<?php

namespace ThemeHouse\WatchForums\XF\Pub\Controller;

class Watched extends XFCP_Watched
{
    public function actionForumsWatch()
    {
        $visitor = \XF::visitor();

        $nodeRepo = $this->repository('XF:Node');
        $nodes = $nodeRepo->getNodeList();

        $nodeIds = $this->filter('node_ids', 'array');

        if ($this->isPost()) {
            foreach ($nodes as $nodeId => $node) {
                if (!in_array($nodeId, $nodeIds)) {
                    continue;
                }

                if ($node->node_type_id != 'Forum') {
                    continue;
                }

                /** @var \XF\Entity\Forum $forum */
                $forum = $node->Data;
                if ($forum->canWatch()) {
                    $notifyType = $this->filter('notify', 'str');
                    if ($notifyType != 'thread' && $notifyType != 'message') {
                        $notifyType = '';
                    }

                    if ($forum->allowed_watch_notifications == 'none') {
                        $notifyType = '';
                    } elseif ($forum->allowed_watch_notifications == 'thread' && $notifyType == 'message') {
                        $notifyType = 'thread';
                    }

                    $sendAlert = $this->filter('send_alert', 'bool');
                    $sendEmail = $this->filter('send_email', 'bool');

                    /** @var \XF\Repository\ForumWatch $watchRepo */
                    $watchRepo = $this->repository('XF:ForumWatch');
                    $watchRepo->setWatchState($forum, $visitor, $notifyType, $sendAlert, $sendEmail);
                }
            }

            return $this->redirect($this->buildLink('watched/forums'));
        }

        $canWatch = false;
        foreach ($nodes as $nodeId => $node) {
            if ($node->node_type_id != 'Forum') {
                continue;
            }

            /** @var \XF\Entity\Forum $forum */
            $forum = $node->Data;
            if ($forum->canWatch()) {
                $canWatch = true;
                break;
            }
        }

        if (!$canWatch) {
            return $this->noPermission();
        }

        $nodeTree = $nodeRepo->createNodeTree($nodes);
        $nodeTree = $nodeTree->filter(null, function ($id, \XF\Entity\Node $node, $depth, $children, \XF\Tree $tree) {
            if ($children) {
                return true;
            }
            if ($node->node_type_id == 'Forum' && $node->Data->canWatch()) {
                return true;
            }
            return false;
        });

        $nodeExtras = $nodeRepo->getNodeListExtras($nodeTree);

        $viewParams = [
            'nodeTree' => $nodeTree,
            'nodeExtras' => $nodeExtras
        ];
        return $this->view('ThemeHouse\WatchForums:Watched\ForumsWatch', 'thwatchforums_watch_forums', $viewParams);
    }
}
