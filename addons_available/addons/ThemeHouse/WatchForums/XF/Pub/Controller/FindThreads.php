<?php

namespace ThemeHouse\WatchForums\XF\Pub\Controller;

class FindThreads extends XFCP_FindThreads
{
    public function actionIndex()
    {
        switch ($this->filter('type', 'str')) {
            case 'watched-forums':
                return $this->redirectPermanently($this->buildLink('find-threads/watched-forums'));
            case 'watched-forums-unread':
                return $this->redirectPermanently($this->buildLink('find-threads/watched-forums/unread'));
            default:
                return parent::actionIndex();
        }
    }

    public function actionWatchedForums()
    {
        $visitor = \XF::visitor();

        if (!$visitor->canViewThreadsInWatchedForums()) {
            return $this->noPermission();
        }

        $userId = $visitor->user_id;

        $threadFinder = $this->getThreadRepo()->findThreadsInWatchedForums($userId);

        return $this->getThreadResults($threadFinder, 'watched-forums');
    }

    public function actionWatchedForumsUnread()
    {
        $visitor = \XF::visitor();

        if (!$visitor->canViewThreadsInWatchedForums()) {
            return $this->noPermission();
        }

        $userId = $visitor->user_id;

        $threadFinder = $this->getThreadRepo()->findUnreadThreadsInWatchedForums($userId);

        return $this->getThreadResults($threadFinder, 'watched-forums-unread');
    }
}
