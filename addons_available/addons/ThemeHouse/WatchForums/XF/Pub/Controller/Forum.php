<?php

namespace ThemeHouse\WatchForums\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Forum extends XFCP_Forum
{
    public function actionWatch(ParameterBag $params)
    {
        $reply = parent::actionWatch($params);

        $visitor = \XF::visitor();

        if ($this->isPost()) {
            $jsonParams = $reply->getJsonParams();

            if (!empty($jsonParams['switchKey']) && $jsonParams['switchKey'] == 'watch') {
                if ($params->node_id) {
                    $nodeId = $params->node_id;
                } else {
                    $forum = $this->assertViewableForum($params->node_name);
                    $nodeId = $forum->node_id;
                }

                if (!$visitor->hasNodePermission($nodeId, 'th_watchForum')) {
                    $reply->setJsonParam('remove', true);
                }
            }
        }

        return $reply;
    }
}
