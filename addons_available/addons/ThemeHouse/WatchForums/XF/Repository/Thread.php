<?php

namespace ThemeHouse\WatchForums\XF\Repository;

class Thread extends XFCP_Thread
{
    public function findThreadsInWatchedForums($userId, $unread = false)
    {
        $finder = $this->finder('XF:Thread')
            ->forFullView(true)
            ->with(['Forum', 'User'])
            ->where('Forum.Watch|' . $userId . '.user_id', '!=', null)
            ->where('discussion_type', '<>', 'redirect')
            ->setDefaultOrder('last_post_date', 'DESC');

        if ($unread) {
            $finder->where('Forum.find_new', 1)
                ->where('last_post_date', '>', $this->getReadMarkingCutOff())
                ->whereOr(
                    ["Read|{$userId}.thread_id", null],
                    [$finder->expression('%s > %s', 'last_post_date', "Read|{$userId}.thread_read_date")]
                );
        }

        return $finder;
    }

    public function findUnreadThreadsInWatchedForums($userId)
    {
        return $this->findThreadsInWatchedForums($userId, true);
    }
}
