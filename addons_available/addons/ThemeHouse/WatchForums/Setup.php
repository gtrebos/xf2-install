<?php

namespace ThemeHouse\WatchForums;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $this->applyGlobalPermission('forum', 'th_watchForum', 'forum', 'viewOthers');
        $this->applyGlobalPermission('general', 'th_threadsInWatchedForums', 'general', 'view');
    }
}
