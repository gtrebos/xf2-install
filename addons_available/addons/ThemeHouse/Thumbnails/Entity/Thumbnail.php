<?php

namespace ThemeHouse\Thumbnails\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 * @property int|null thumbnail_id
 * @property string content_type
 * @property int content_id
 * @property int cache_date
 * @property int selected
 * @property string temp_hash
 * @property bool unassociated
 *
 * GETTERS
 * @property Entity|null Container
 */
class Thumbnail extends Entity
{
    public function getContainerLink()
    {
        $container = $this->getContainer();
        if ($container) {
            $handler = $this->getHandler();
            return $handler ? $handler->getContainerLink($this->getContainer()) : null;
        }

        return null;
    }

    public function getContentTypePhrase()
    {
        $handler = $this->getHandler();
        return $handler ? $handler->getContentTypePhrase() : null;
    }

    public function getHandler()
    {
        return $this->getThumbnailRepo()->getThumbnailHandler($this->content_type);
    }

    /**
     * @return Entity|null
     */
    public function getContainer()
    {
        $handler = $this->getHandler();
        return $handler ? $handler->getContainerEntity($this->content_id) : null;
    }

    public function setContainer(Entity $content = null)
    {
        $this->_getterCache['Container'] = $content;
    }

    public function getAbstractedPath($sizeCode)
    {
        $group = floor($this->thumbnail_id / 1000);

        return sprintf(
            'data://thumbnails/%s/%d/%d.jpg',
            $sizeCode,
            $group,
            $this->thumbnail_id
        );
    }

    protected function _preSave()
    {
        if (!$this->content_id) {
            if (!$this->temp_hash) {
                throw new \LogicException('Temp hash must be specified if no content is specified.');
            }

            $this->unassociated = true;
        } else {
            $this->temp_hash = '';
            $this->unassociated = false;
        }
    }

    protected function _postSave()
    {
        if ($this->isChanged('selected') && $this->get('selected')) {
            if ($this->content_id) {
                $this->db()->query("
                    UPDATE xf_th_thumbnails_thumbnail
                    SET selected = 0
                    WHERE content_type = ?
                        AND content_id = ?
                        AND thumbnail_id <> ?
                ", [$this->content_type, $this->content_id, $this->thumbnail_id]);
                $this->onSelected();
            } else {
                $this->db()->query("
                    UPDATE xf_th_thumbnails_thumbnail
                    SET selected = 0
                    WHERE temp_hash = ?
                        AND thumbnail_id <> ?
                ", [$this->temp_hash, $this->thumbnail_id]);
            }
        }
    }

    protected function onSelected()
    {
        $handler = $this->getHandler();
        $container = $handler->getContainerEntity($this->content_id);
        $this->getHandler()->onSelected($container, $this);
    }

    protected function _preDelete()
    {
        if ($this->content_id) {
            /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
            $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
            $handler = $thumbnailRepo->getThumbnailHandler($this->content_type);
            if ($handler) {
                $container = $handler->getContainerEntity($this->content_id);
                $handler->beforeThumbnailDelete($this, $container);
            }
        }
    }

    protected function _postDelete()
    {
        if ($this->content_id) {
            /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
            $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
            $handler = $thumbnailRepo->getThumbnailHandler($this->content_type);
            if ($handler) {
                $container = $handler->getContainerEntity($this->content_id);
                $handler->onThumbnailDelete($this, $container);
            }
        }

        $path = $this->getAbstractedPath();
        \XF\Util\File::deleteFromAbstractedPath($path);
    }

    public static function getStructure(Structure $structure)
    {
        $structure->table = 'xf_th_thumbnails_thumbnail';
        $structure->shortName = 'ThemeHouse\Thumbnails:Thumbnail';
        $structure->primaryKey = 'thumbnail_id';
        $structure->columns = [
            'thumbnail_id' => ['type' => self::UINT, 'autoIncrement' => true, 'nullable' => true],
            'content_type' => ['type' => self::STR, 'maxLength' => 25, 'default' => ''],
            'content_id' => ['type' => self::UINT, 'default' => 0],
            'cache_date' => ['type' => self::UINT, 'default' => \XF::$time],
            'selected' => ['type' => self::BOOL, 'default' => 0],
            'temp_hash' => ['type' => self::STR, 'maxLength' => 32, 'default' => ''],
            'unassociated' => ['type' => self::BOOL, 'default' => true],
            'file_hash' => ['type' => self::STR, 'maxLength' => 32, 'required' => true],
            'width' => ['type' => self::UINT, 'default' => 0],
            'height' => ['type' => self::UINT, 'default' => 0],
            'type' => ['type' => self::STR, 'maxLength' => 25, 'default' => ''],
            'extra_data' => ['type' => self::JSON_ARRAY, 'default' => []],
        ];
        $structure->getters = [
            'Container' => true,
        ];

        return $structure;
    }

    /**
     * @return \ThemeHouse\Thumbnails\Repository\Thumbnail
     */
    protected function getThumbnailRepo()
    {
        return $this->repository('ThemeHouse\Thumbnails:Thumbnail');
    }
}
