<?php

namespace ThemeHouse\Thumbnails\Util;

use XF\Mvc\Entity\Entity;

class BbCode
{
    public static function getImageUrls(Entity $entity, $message)
    {
        /** @var \ThemeHouse\Thumbnails\Service\Message\Preparer $messagePreparer */
        $messagePreparer = \XF::service('ThemeHouse\Thumbnails:Message\Preparer', 'post');
        $messagePreparer->prepare($message);

        $images = $messagePreparer->getImages();

        return $images;
    }
}
