<?php

namespace ThemeHouse\Thumbnails\Repository;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;

class Thumbnail extends Repository
{
    public function getEditorData($contentType, Entity $entity = null, &$tempHash = null, array $extraContext = [])
    {
        if (!$tempHash) {
            $tempHash = md5(microtime(true) . \XF::generateRandomString(8, true));
        }

        $thumbnails = $this->getThumbnailsForEntity($contentType, $entity, $tempHash, $extraContext);

        return $this->groupThumbnails($thumbnails);
    }

    public function groupThumbnails(array $thumbnails)
    {
        $groupedThumbnails = [];
        foreach ($thumbnails as $thumbnailId => $thumbnail) {
            $groupedThumbnails[$thumbnail->type][$thumbnailId] = $thumbnail;
        }

        return $groupedThumbnails;
    }

    public function getThumbnailsForEntity($contentType, Entity $entity = null, &$tempHash = null, array $extraContext = [])
    {
        $handler = $this->getThumbnailHandler($contentType);
        if (!$handler) {
            throw new \InvalidArgumentException("No thumbnail handler found for content type '$contentType'");
        }
        $context = $handler->getContext($entity, $extraContext);

        $hashThumbnails = [];
        if ($tempHash) {
            $hashThumbnails = $this->findThumbnailsByTempHash($tempHash)->fetch()->toArray();
        }

        $containerId = $handler->getContainerIdFromContext($context);
        $contentThumbnails = [];
        if ($containerId) {
            $contentThumbnails = $this->findThumbnailsByContent($contentType, $containerId)->fetch()->toArray();
        }

        $thumbnails = $contentThumbnails + $hashThumbnails;

        $handler->extractFromContainerEntity($entity, $tempHash, $thumbnails);

        $selected = array_filter(array_column($thumbnails, 'selected'));
        if (!$selected) {
            $handler->selectDefaultThumbnail($entity, $thumbnails);
        }

        return $thumbnails;
    }

    /**
     * @return Finder
     */
    public function findThumbnailsForList()
    {
        return $this->finder('ThemeHouse\Thumbnails:Thumbnail')
            ->setDefaultOrder('cache_date', 'DESC');
    }

    /**
     * @param string $hash
     *
     * @return Finder
     */
    public function findThumbnailsByTempHash($hash)
    {
        return $this->finder('ThemeHouse\Thumbnails:Thumbnail')
            ->where('temp_hash', $hash)
            ->order('cache_date');
    }

    /**
     * @param string $contentType
     * @param int $contentId
     *
     * @return Finder
     */
    public function findThumbnailsByContent($contentType, $contentId)
    {
        return $this->finder('ThemeHouse\Thumbnails:Thumbnail')
            ->where('content_type', $contentType)
            ->where('content_id', $contentId)
            ->order('cache_date');
    }

    /**
     * @return \ThemeHouse\Thumbnails\Thumbnail\AbstractHandler[]
     */
    public function getThumbnailHandlers()
    {
        $handlers = [];

        foreach (\XF::app()->getContentTypeField('ththumbnail_handler_class') as $contentType => $handlerClass) {
            if (class_exists($handlerClass)) {
                $handlerClass = \XF::extendClass($handlerClass);
                $handlers[$contentType] = new $handlerClass($contentType);
            }
        }

        return $handlers;
    }

    /**
     * @param string $type
     *
     * @return \ThemeHouse\Thumbnails\Thumbnail\AbstractHandler|null
     */
    public function getThumbnailHandler($type)
    {
        $handlerClass = \XF::app()->getContentTypeFieldValue($type, 'ththumbnail_handler_class');
        if (!$handlerClass) {
            return null;
        }

        if (!class_exists($handlerClass)) {
            return null;
        }

        $handlerClass = \XF::extendClass($handlerClass);
        return new $handlerClass($type);
    }

    public function markThumbnailSelected(\ThemeHouse\Thumbnails\Entity\Thumbnail $thumbnail, $tempHash = null)
    {
        if ($thumbnail->temp_hash) {
            if ($tempHash !== $thumbnail->temp_hash) {
                return false;
            }
        } elseif ($thumbnail->content_type && $thumbnail->content_id) {
            $handler = $this->getThumbnailHandler($thumbnail->content_type);
            if (!$handler) {
                throw new \InvalidArgumentException("No thumbnail handler found for content type '$contentType'");
            }
            $entity = $handler->getContainerEntity($thumbnail->content_id);
            $context = $handler->getContext($entity);
            if (!$handler->canMarkThumbnailSelected($thumbnail, $context)) {
                return false;
            }
        } else {
            return false;
        }

        $thumbnail->selected = 1;
        $thumbnail->save();

        return true;
    }

    public function deleteUnassociatedThumbnails($cutOff = null)
    {
        if ($cutOff === null) {
            $cutOff = \XF::$time - 86400;
        }

        $db = $this->db();

        return $db->delete(
            'xf_th_thumbnails_thumbnail',
            'unassociated = 1
                AND cache_date < ?',
            $cutOff
        );
    }

    public function fastDeleteContentThumbnails($contentType, $contentIds)
    {
        if (!is_array($contentIds)) {
            $contentIds = [$contentIds];
        }

        if (!$contentIds) {
            return 0;
        }

        $db = $this->db();

        return $db->delete(
            'xf_th_thumbnails_thumbnail',
            'content_type = ?
                AND content_id IN (' . $db->quote($contentIds) . ')',
            $contentType
        );
    }

    public function getThumbnailUrl($thumbnailId, $sizeCode, $canonical = false)
    {
        $app = $this->app();

        $sizeMap = $app->container('avatarSizeMap');
        if (!isset($sizeMap[$sizeCode])) {
            // Always fallback to 's' in the event of an unknown size (e.g. 'xs', 'xxs' etc.)
            $sizeCode = 's';
        }

        if ($thumbnailId) {
            $group = floor($thumbnailId / 1000);
            return $app->applyExternalDataUrl(
                "thumbnails/{$sizeCode}/{$group}/{$thumbnailId}.jpg",
                $canonical
            );
        } else {
            return null;
        }
    }

    public function getThumbnailUrl2x($thumbnailId, $sizeCode, $canonical = false)
    {
        $sizeMap = $this->app()->container('avatarSizeMap');

        switch ($sizeCode) {
            case 'xs':
            case 's':
                return $this->getThumbnailUrl($thumbnailId, 'm', $canonical);
                break;

            case 'm':
                return $this->getThumbnailUrl($thumbnailId, 'l', $forceType, $canonical);
                break;

            case 'l':
                return $this->getThumbnailUrl($thumbnailId, 'h', $forceType, $canonical);
                break;
        }

        return '';
    }
}
