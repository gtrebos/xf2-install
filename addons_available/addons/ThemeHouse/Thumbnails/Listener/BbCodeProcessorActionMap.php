<?php

namespace ThemeHouse\Thumbnails\Listener;

class BbCodeProcessorActionMap
{
    public static function bbCodeProcessorActionMap(array &$processorActionMap)
    {
        $processorActionMap['images'] = 'ThemeHouse\Thumbnails:Images';
    }
}
