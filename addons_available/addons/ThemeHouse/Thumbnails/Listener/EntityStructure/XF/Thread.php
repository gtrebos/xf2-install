<?php

namespace ThemeHouse\Thumbnails\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Thread
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $structure->columns['th_first_post_thumbnail_id'] = ['type' => Entity::UINT, 'default' => 0];
        $structure->columns['th_last_post_thumbnail_id'] = ['type' => Entity::UINT, 'default' => 0];
        $structure->relations['FirstPostThumbnails'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_MANY,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$first_post_id'],
            ],
            'order' => 'cache_date'
        ];
        $structure->relations['FirstPostThumbnail'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_ONE,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$first_post_id'],
                ['selected', '=', 1],
            ]
        ];
        $structure->relations['LastPostThumbnails'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_MANY,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$last_post_id'],
            ],
            'order' => 'cache_date'
        ];
        $structure->relations['LastPostThumbnail'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_ONE,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$last_post_id'],
                ['selected', '=', 1],
            ]
        ];
    }
}
