<?php

namespace ThemeHouse\Thumbnails\Listener\EntityStructure\XF;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Post
{
    public static function entityStructure(Manager $em, Structure &$structure)
    {
        $structure->relations['Thumbnails'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_MANY,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$post_id']
            ],
            'order' => 'cache_date'
        ];
        $structure->relations['Thumbnail'] = [
            'entity' => 'ThemeHouse\Thumbnails:Thumbnail',
            'type' => Entity::TO_ONE,
            'conditions' => [
                ['content_type', '=', 'post'],
                ['content_id', '=', '$post_id'],
                ['selected', '=', 1]
            ]
        ];
    }
}
