<?php

namespace ThemeHouse\Thumbnails\Thumbnail;

use ThemeHouse\Thumbnails\Entity\Thumbnail;
use XF\Mvc\Entity\Entity;
use ThemeHouse\Thumbnails\Util\BbCode;

class Post extends AbstractHandler
{
    public function getContainerWith()
    {
        $visitor = \XF::visitor();

        return ['Thread', 'Thread.Forum', 'Thread.Forum.Node.Permissions|' . $visitor->permission_combination_id];
    }

    public function canManageThumbnails(array $context, &$error = null)
    {
        $em = \XF::em();

        if (!empty($context['post_id'])) {
            /** @var \XF\Entity\Post $post */
            $post = $em->find('XF:Post', intval($context['post_id']), ['Thread', 'Thread.Forum']);
            if (!$post || !$post->canView() || !$post->canEdit()) {
                return false;
            }

            return $post->canManageThumbnails();
        }

        if (!empty($context['thread_id'])) {
            /** @var \XF\Entity\Thread $thread */
            $thread = $em->find('XF:Thread', intval($context['thread_id']), ['Forum']);
            if (!$thread || !$thread->canView()) {
                return false;
            }

            $forum = $thread->Forum;
        } elseif (!empty($context['node_id'])) {
            /** @var \XF\Entity\Forum $forum */
            $forum = $em->find('XF:Forum', intval($context['node_id']));
            if (!$forum || !$forum->canView()) {
                return false;
            }
        } else {
            return false;
        }

        return $forum->canManageThumbnails();
    }

    public function canMarkThumbnailSelected(Thumbnail $thumbnail, array $context, &$error = null)
    {
        return $this->canManageThumbnails($context, $error);
    }

    public function getContainerIdFromContext(array $context)
    {
        return isset($context['post_id']) ? intval($context['post_id']) : null;
    }

    public function getContainerLink(Entity $container, array $extraParams = [])
    {
        return \XF::app()->router('public')->buildLink('posts', $container, $extraParams);
    }

    public function getContext(Entity $entity = null, array $extraContext = [])
    {
        if ($entity instanceof \XF\Entity\Post) {
            $extraContext['post_id'] = $entity->post_id;
        } elseif ($entity instanceof \XF\Entity\Thread) {
            $extraContext['thread_id'] = $entity->thread_id;
        } elseif ($entity instanceof \XF\Entity\Forum) {
            $extraContext['node_id'] = $entity->node_id;
        } else {
            throw new \InvalidArgumentException("Entity must be post, thread or forum");
        }

        return $extraContext;
    }

    public function extractFromContainerEntity(Entity $entity, &$hash, array &$thumbnails)
    {
        $thumbnailPreparer = \XF::service('ThemeHouse\Thumbnails:Thumbnail\Preparer');

        $allowDownloading = $this->options['allow_downloading_images'];

        $bbCodeThumbnails = [];
        if (isset($entity->message)) {
            $type = 'message_img';
            $images = BbCode::getImageUrls($entity, $entity->message);
            foreach ($images as $url) {
                $thumbnail = $thumbnailPreparer
                    ->insertThumbnailFromUrl($this, $entity, $url, $hash, $type, $thumbnails, $allowDownloading);
                if ($thumbnail) {
                    $thumbnails[$thumbnail->thumbnail_id] = $thumbnail;
                }
            }
        }
    }

    public function onSelected(Entity $container, Thumbnail $thumbnail)
    {
        $thread = $container->Thread;

        $updated = false;
        if ($thread->last_post_id === $container->post_id) {
            $thread->set('th_last_post_thumbnail_id', $thumbnail->thumbnail_id, [
                'forceSet' => true
            ]);
            $updated = true;
        }
        if ($thread->first_post_id === $container->post_id) {
            $thread->set('th_first_post_thumbnail_id', $thumbnail->thumbnail_id, [
                'forceSet' => true
            ]);
            $updated = true;
        }

        if ($updated) {
            $thread->save();
        }
    }
}
