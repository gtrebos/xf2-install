<?php

namespace ThemeHouse\Thumbnails\Thumbnail;

use ThemeHouse\Thumbnails\Entity\Thumbnail;
use XF\Mvc\Entity\Entity;

abstract class AbstractHandler
{
    protected $contentType;

    protected $options = [
        'allow_downloading_images' => true,
    ];

    public function __construct($contentType)
    {
        $this->contentType = $contentType;
    }

    abstract public function canManageThumbnails(array $context, &$error = null);
    abstract public function canMarkThumbnailSelected(Thumbnail $thumbnail, array $context, &$error = null);
    abstract public function getContainerIdFromContext(array $context);
    abstract public function getContainerLink(Entity $container, array $extraParams = []);
    abstract public function getContext(Entity $entity = null, array $extraContext = []);
    abstract public function extractFromContainerEntity(Entity $entity, &$hash, array &$thumbnails);
    abstract public function onSelected(Entity $container, Thumbnail $thumbnail);

    public function setOption($key, $value)
    {
        if (isset($this->options[$key])) {
            $this->options[$key] = $value;
        }
    }

    public function getContainerFromContext(array $context)
    {
        $id = $this->getContainerIdFromContext($context);
        return $id ? $this->getContainerEntity($id) : null;
    }

    public function getContainerEntity($id)
    {
        return \XF::app()->findByContentType($this->contentType, $id, $this->getContainerWith());
    }

    public function getContainerWith()
    {
        return [];
    }

    public function getContentType()
    {
        return $this->contentType;
    }

    public function getContentTypePhrase()
    {
        return \XF::app()->getContentTypePhrase($this->contentType);
    }

    public function selectDefaultThumbnail(Entity $entitiy, array $thumbnails)
    {
        if ($thumbnails) {
            $thumbnail = reset($thumbnails);
            $thumbnail->selected = 1;
            $thumbnail->save();
        }
    }
}
