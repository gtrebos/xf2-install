<?php

namespace ThemeHouse\Thumbnails;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->createTable('xf_th_thumbnails_thumbnail', function (Create $table) {
            $table->addColumn('thumbnail_id', 'int')->autoIncrement();
            $table->addColumn('content_type', 'varbinary', 25);
            $table->addColumn('content_id', 'int');
            $table->addColumn('cache_date', 'int');
            $table->addColumn('selected', 'tinyint', 3)->setDefault(0);
            $table->addColumn('temp_hash', 'varchar', 32)->setDefault('');
            $table->addColumn('unassociated', 'tinyint', 3);
            $table->addColumn('file_hash', 'varchar', 32);
            $table->addColumn('width', 'int')->setDefault(0);
            $table->addColumn('height', 'int')->setDefault(0);
            $table->addColumn('type', 'varbinary', 25);
            $table->addColumn('extra_data', 'blob')->nullable();
            $table->addKey(['content_type', 'content_id', 'cache_date'], 'content_type_id_date');
            $table->addKey(['content_type', 'content_id', 'selected'], 'content_type_id_selected');
            $table->addKey(['temp_hash', 'cache_date']);
            $table->addKey(['unassociated', 'cache_date']);
        });
    }

    public function installStep2()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_thread', function (Alter $table) {
            $table->addColumn('th_first_post_thumbnail_id', 'int')
                ->setDefault(0);
            $table->addColumn('th_last_post_thumbnail_id', 'int')
                ->setDefault(0);
        });
    }

    public function uninstallStep1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->dropTable('xf_th_thumbnails_thumbnail');
    }

    public function uninstallStep2()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_thread', function (Alter $table) {
            $table->dropColumns([
                'th_first_post_thumbnail_id',
                'th_last_post_thumbnail_id',
            ]);
        });
    }

    public function uninstallStep3()
    {
        \XF\Util\File::deleteAbstractedDirectory('data://thumbnails');
    }
}
