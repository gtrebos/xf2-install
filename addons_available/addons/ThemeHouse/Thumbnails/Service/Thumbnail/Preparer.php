<?php

namespace ThemeHouse\Thumbnails\Service\Thumbnail;

use XF\Util\File;
use XF\Mvc\Entity\Entity;

class Preparer extends \XF\Service\AbstractService
{
    public function insertThumbnailFromUrl(
        \ThemeHouse\Thumbnails\Thumbnail\AbstractHandler $handler,
        Entity $entity,
        $url,
        &$hash,
        $type,
        array &$existingThumbnails = [],
        $allowDownloading = true
    ) {
        $url = $this->cleanUrlForFetch($url);
        if (!preg_match('#^https?://#i', $url)) {
            return null;
        }

        foreach ($existingThumbnails as $existingThumbnail) {
            if ($existingThumbnail->type == $type) {
                if (isset($existingThumbnail->extra_data['url'])) {
                    if ($existingThumbnail->extra_data['url'] == $url) {
                        return null;
                    }
                }
            }
        }

        if (!$allowDownloading) {
            // TODO: use image proxy (if available) instead of downloading
            return null;
        }

        $file = $this->fetchImageDataFromUrl($url);

        if (!$file) {
            return null;
        }

        $thumbnail = $this->generateThumbnail($file);
        @unlink($file);

        if (!$thumbnail) {
            return null;
        }

        $hashes = array_column($existingThumbnails, 'file_hash');
        $extraData = [
            'url' => $url
        ];

        $context = $handler->getContext($entity);
        return $this->insertTemporaryThumbnail($handler, $context, $hash, $thumbnail, $type, $extraData, $hashes);
    }

    protected function fetchImageDataFromUrl($url)
    {
        $urlParts = @parse_url($url);

        $validImage = false;
        $fileName = !empty($urlParts['path']) ? basename($urlParts['path']) : null;
        $mimeType = null;
        $streamFile = \XF\Util\File::getTempDir() . '/' . strtr(md5($url) . '-' . uniqid(), '/\\.', '---') . '.temp';
        $externalImageMaxSize = $this->app->options()->ththumbnails_externalImageMaxSize * 1024;

        try {
            $options = [
                'headers' => [
                    'Accept' => 'image/*,*/*;q=0.8'
                ]
            ];
            $limits = [
                'time' => 8,
                'bytes' => $externalImageMaxSize ?: -1
            ];
            $response = $this->app->http()->reader()->getUntrusted($url, $limits, $streamFile, $options, $error);
        } catch (\Exception $e) {
            $response = null;
        }

        if ($response) {
            $response->getBody()->close();

            if ($response->getStatusCode() == 200) {
                $disposition = $response->getHeader('Content-Disposition');
                if ($disposition && preg_match('/filename=(\'|"|)(.+)\\1/siU', $disposition, $match)) {
                    $fileName = $match[2];
                }
                if (!$fileName) {
                    $fileName = 'image';
                }

                $imageInfo = filesize($streamFile) ? @getimagesize($streamFile) : false;
                if ($imageInfo) {
                    $imageType = $imageInfo[2];

                    $extension = \XF\Util\File::getFileExtension($fileName);
                    $extensionMap = [
                        IMAGETYPE_GIF => ['gif'],
                        IMAGETYPE_JPEG => ['jpg', 'jpeg', 'jpe'],
                        IMAGETYPE_PNG => ['png']
                    ];
                    if (isset($extensionMap[$imageType])) {
                        $mimeType = $imageInfo['mime'];

                        $validExtensions = $extensionMap[$imageType];
                        if (!in_array($extension, $validExtensions)) {
                            $extensionStart = strrpos($fileName, '.');
                            $fileName = (
                                $extensionStart
                                    ? substr($fileName, 0, $extensionStart)
                                    : $fileName
                                ) . '.' . $validExtensions[0];
                        }

                        $validImage = true;
                    }
                }
            }
        }

        if (!$validImage) {
            @unlink($streamFile);
        }

        return $validImage ? $streamFile : null;
    }

    public function cleanUrlForFetch($url)
    {
        $url = preg_replace('/#.*$/s', '', $url);
        if (preg_match_all('/[^A-Za-z0-9._~:\/?#\[\]@!$&\'()*+,;=%-]/', $url, $matches)) {
            foreach ($matches[0] as $match) {
                $url = str_replace($match[0], '%' . strtoupper(dechex(ord($match[0]))), $url);
            }
        }
        $url = preg_replace('/%(?![a-fA-F0-9]{2})/', '%25', $url);

        return $url;
    }

    /**
     * @param string $sourceFile
     * @return \XF\Image\AbstractDriver
     */
    public function generateThumbnail($sourceFile)
    {
        $image = $this->app->imageManager()->imageFromFile($sourceFile);
        if (!$image) {
            return null;
        }

        if ($image instanceof \XF\Image\Imagick) {
            // Workaround to only use the first frame of a multi-frame image for the thumb
            foreach ($image->getImage() as $imagick) {
                $image->setImage($imagick->getImage());
                break;
            }
        }

        $thumbWidthHeight = $this->app->container('avatarSizeMap')['o'];

        $image->resizeAndCrop($thumbWidthHeight, $thumbWidthHeight)
            ->unsharpMask();

        return $image;
    }

    public function insertTemporaryThumbnail(
        \ThemeHouse\Thumbnails\Thumbnail\AbstractHandler $handler,
        array $context,
        $tempHash,
        \XF\Image\AbstractDriver $image,
        $type,
        array $extraData = [],
        array $existingFileHashes = []
    ) {
        $tempFile = \XF\Util\File::getTempFile();
        if (!$tempFile || !$image->save($tempFile)) {
            return null;
        }

        $fileHash = md5_file($tempFile);
        if (in_array($fileHash, $existingFileHashes)) {
            return null;
        }

        $tempFiles['o'] = $tempFile;
        $originalWidth = $image->getWidth();
        $originalHeight = $image->getHeight();

        $sizeMap = $this->app->container('avatarSizeMap');
        foreach ($sizeMap as $sizeCode => $thumbWidthHeight) {
            if ($sizeCode === 'o') {
                continue;
            }
            if ($originalWidth !== $thumbWidthHeight) {
                $image->resize($thumbWidthHeight, $thumbWidthHeight);
            }
            $tempFile = \XF\Util\File::getTempFile();
            if (!$tempFile || !$image->save($tempFile)) {
                return null;
            }
            $tempFiles[$sizeCode] = $tempFile;
        }

        /** @var \XF\Entity\Thumbnail $thumbnail */
        $thumbnail = $this->app->em()->create('ThemeHouse\Thumbnails:Thumbnail');

        $thumbnail->content_type = $handler->getContentType();
        if (!$tempHash && $contentId = $handler->getContainerIdFromContext($context)) {
            $thumbnail->content_id = $contentId;
            $thumbnail->unassociated = 0;
        } else {
            if (!$tempHash) {
                $tempHash = md5(microtime(true) . \XF::generateRandomString(8, true));
            }
            $thumbnail->temp_hash = $tempHash;
        }
        $thumbnail->file_hash = $fileHash;
        $thumbnail->width = $originalWidth;
        $thumbnail->height = $originalHeight;
        $thumbnail->type = $type;
        $thumbnail->extra_data = $extraData;
        $thumbnail->save();

        foreach ($tempFiles as $sizeCode => $tempFile) {
            File::copyFileToAbstractedPath($tempFile, $thumbnail->getAbstractedPath($sizeCode));
        }

        return $thumbnail;
    }

    public function associateThumbnailsWithContent($tempHash, $contentType, $contentId)
    {
        $thumbnailFinder = $this->finder('ThemeHouse\Thumbnails:Thumbnail')
            ->where('temp_hash', $tempHash);

        /** @var \ThemeHouse\Thumbnails\Entity\Thumbnail $thumbnail */
        foreach ($thumbnailFinder->fetch() as $thumbnail) {
            $thumbnail->content_type = $contentType;
            $thumbnail->content_id = $contentId;
            $thumbnail->temp_hash = '';
            $thumbnail->unassociated = 0;

            $thumbnail->save();

            $container = $thumbnail->getContainer();
        }
    }
}
