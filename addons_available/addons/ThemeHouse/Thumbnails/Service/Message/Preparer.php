<?php

namespace ThemeHouse\Thumbnails\Service\Message;

use XF\Mvc\Entity\Entity;

class Preparer extends \XF\Service\AbstractService
{
    protected $context;

    /**
     * @var Entity|null
     */
    protected $messageEntity;

    /**
     * @var \XF\BbCode\Processor
     */
    protected $bbCodeProcessor;

    protected $filters = [
        'images' => true
    ];

    protected $images = [];

    public function __construct(\XF\App $app, $context, Entity $messageEntity = null)
    {
        $this->context = $context;
        $this->messageEntity = $messageEntity;
        parent::__construct($app);
    }

    public function getMessageEntity()
    {
        return $this->messageEntity;
    }

    public function setMessageEntity(Entity $messageEntity = null)
    {
        $this->messageEntity = $messageEntity;
    }

    public function prepare($message)
    {
        $message = $this->processMessage($message);

        /** @var \ThemeHouse\Thumbnails\BbCode\ProcessorAction\Images|null $images */
        $images = $this->bbCodeProcessor->getFilterer('images');

        $this->images = $images ? $images->getImages() : [];

        return $message;
    }

    protected function processMessage($message)
    {
        $this->bbCodeProcessor = $this->getBbCodeProcessor();

        $bbCodeContainer = $this->app->bbCode();

        $renderContext = $this->context . ':prepare';
        $options = $bbCodeContainer->getFullRenderOptions($this->messageEntity, $renderContext, null);

        return $this->bbCodeProcessor->render(
            $message,
            $bbCodeContainer->parser(),
            $bbCodeContainer->rules($renderContext),
            $options
        );
    }

    protected function getBbCodeProcessor()
    {
        $bbCodeContainer = $this->app->bbCode();

        $processor = $bbCodeContainer->processor();

        if ($this->filters['images']) {
            $processor->addProcessorAction('images', $bbCodeContainer->processorAction('images'));
        }

        return $processor;
    }

    public function getImages()
    {
        return $this->images;
    }
}
