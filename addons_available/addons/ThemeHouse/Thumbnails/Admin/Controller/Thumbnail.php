<?php

namespace ThemeHouse\Thumbnails\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\Mvc\ParameterBag;

class Thumbnail extends AbstractController
{
    protected function preDispatchController($action, ParameterBag $params)
    {
        $this->assertAdminPermission('ththumbnails');
    }

    public function actionIndex()
    {
        $options = [
            $this->em()->find('XF:Option', 'ththumbnails_externalImageMaxSize'),
        ];

        $viewParams = [
            'options' => $options,
            'success' => $this->filter('success', 'bool'),
            'hasStoppedManualJobs' => $this->app->jobManager()->hasStoppedManualJobs()
        ];

        return $this->view('ThemeHouse\Thumbnails:Thumbnail\Index', 'ththumbnails_index', $viewParams);
    }

    public function actionRebuild()
    {
        $this->assertAdminPermission('rebuildCache');

        if ($this->isPost()) {
            $job = $this->filter('job', 'str');
            $options = $this->filter('options', 'array');

            $runner = $this->app->job($job, null, $options);
            if ($runner && $runner->canTriggerByChoice()) {
                $uniqueId = 'Rebuild' . $job;
                $id = $this->app->jobManager()->enqueueUnique(
                    $uniqueId,
                    $job,
                    $options
                );

                $reply = $this->redirect(
                    $this->buildLink('tools/run-job', null, [
                        'only_id' => $id,
                        '_xfRedirect' => $this->buildLink('thumbnails', null, ['success' => 1])
                    ])
                );
                $reply->setPageParam('skipManualJobRun', true);
                return $reply;
            } else {
                return $this->error(\XF::phrase('this_cache_could_not_be_rebuilt'), 500);
            }
        } else {
            return $this->redirect($this->buildLink('thumbnails'));
        }
    }
}
