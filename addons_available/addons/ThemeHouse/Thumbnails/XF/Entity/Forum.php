<?php

namespace ThemeHouse\Thumbnails\XF\Entity;

class Forum extends XFCP_Forum
{
    public function canManageThumbnails(&$error = null)
    {
        return $this->canCreateThread($error);
    }
}
