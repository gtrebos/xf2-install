<?php

namespace ThemeHouse\Thumbnails\XF\Entity;

class Post extends XFCP_Post
{
    public function canManageThumbnails(&$error = null)
    {
        return $this->canEdit($error);
    }
}
