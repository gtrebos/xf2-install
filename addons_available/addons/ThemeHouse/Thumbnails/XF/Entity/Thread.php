<?php

namespace ThemeHouse\Thumbnails\XF\Entity;

class Thread extends XFCP_Thread
{
    public function canManageThumbnails(&$error = null)
    {
        return $this->Forum->canManageThumbnails($error);
    }

    public function rebuildThumbnailCache()
    {
        $lastPostUpdated = $this->rebuildLastPostThumbnailCache();
        $firstPostUpdated = $this->rebuildFirstPostThumbnailCache();

        return ($lastPostUpdated || $firstPostUpdated);
    }

    protected function rebuildLastPostThumbnailCache()
    {
        $updated = false;
        if ($this->last_post_id) {
            $finder = $this->getThumbnailRepo()->findThumbnailsByContent('post', $this->last_post_id);
            $thumbnail = $finder->where('selected', 1)->fetchOne();
        } else {
            $thumbnail = null;
        }
        if ($thumbnail) {
            $updated = true;
            $this->th_last_post_thumbnail_id = $thumbnail->thumbnail_id;
            $this->hydrateRelation('LastPostThumbnail', $thumbnail);
        } elseif ($this->th_last_post_thumbnail_id) {
            $updated = true;
            $this->th_last_post_thumbnail_id = 0;
            $this->hydrateRelation('LastPostThumbnail', null);
        }
        return $updated;
    }

    protected function rebuildFirstPostThumbnailCache()
    {
        $updated = false;
        if ($this->first_post_id) {
            $finder = $this->getThumbnailRepo()->findThumbnailsByContent('post', $this->first_post_id);
            $thumbnail = $finder->where('selected', 1)->fetchOne();
        } else {
            $thumbnail = null;
        }
        if ($thumbnail) {
            $updated = true;
            $this->th_first_post_thumbnail_id = $thumbnail->thumbnail_id;
            $this->hydrateRelation('FirstPostThumbnail', $thumbnail);
        } elseif ($this->th_first_post_thumbnail_id) {
            $updated = true;
            $this->th_first_post_thumbnail_id = 0;
            $this->hydrateRelation('FirstPostThumbnail', null);
        }
        return $updated;
    }

    protected function _preSave()
    {
        parent::_preSave();

        if ($this->isInsert() || $this->isChanged('last_post_id')) {
            $this->rebuildLastPostThumbnailCache();
        }

        if ($this->isInsert() || $this->isChanged('first_post_id')) {
            $this->rebuildFirstPostThumbnailCache();
        }
    }

    /**
     * @return \XF\Repository\Thumbnail
     */
    protected function getThumbnailRepo()
    {
        return $this->repository('ThemeHouse\Thumbnails:Thumbnail');
    }
}
