<?php

namespace ThemeHouse\Thumbnails\XF\Job;

class Thread extends XFCP_Thread
{
    protected function setupData(array $data)
    {
        $defaultData = [
            'ththumbnails_rebuild' => false,
        ];

        $this->defaultData = array_merge($this->defaultData, $defaultData);

        return parent::setupData($data);
    }

    protected function rebuildById($id)
    {
        parent::rebuildById($id);

        if ($this->data['ththumbnails_rebuild']) {
            /** @var \XF\Entity\Thread $thread */
            $thread = $this->app->em()->find('XF:Thread', $id);
            if (!$thread) {
                return;
            }

            if ($thread->rebuildThumbnailCache()) {
                $thread->save();
            }
        }
    }
}
