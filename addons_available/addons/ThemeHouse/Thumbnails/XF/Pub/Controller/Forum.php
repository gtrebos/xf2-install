<?php

namespace ThemeHouse\Thumbnails\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply;

class Forum extends XFCP_Forum
{
    public function actionPostThread(ParameterBag $params)
    {
        $reply = parent::actionPostThread($params);

        if ($reply instanceof Reply\View && $reply->getParam('forum')) {
            $forum = $reply->getParam('forum');
            if ($forum->canManageThumbnails()) {
                /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
                $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
                $tempHash = isset($forum->draft_thread['thumbnail_hash']) ? $forum->draft_thread['thumbnail_hash'] : null;
                $thumbnailData = $thumbnailRepo->getEditorData(
                    'post',
                    $forum,
                    $tempHash
                );
            } else {
                $thumbnailData = null;
            }

            $reply->setParam('thumbnail_hash', $tempHash);
        }

        return $reply;
    }

    protected function setupThreadCreate(\XF\Entity\Forum $forum)
    {
        $creator = parent::setupThreadCreate($forum);

        if ($forum->canManageThumbnails()) {
            $creator->setThumbnailHash($this->filter('thumbnail_hash', 'str'));
        }

        return $creator;
    }

    public function actionThreadThumbnails(ParameterBag $params)
    {
        $this->assertPostOnly();

        $forum = $this->assertViewableForum($params->node_id ?: $params->node_name);
        if (!$forum->canManageThumbnails($error)) {
            return $this->noPermission($error);
        }

        $tempHash = $this->filter('thumbnail_hash', 'str');
        if (!$tempHash) {
            return $this->noPermission($error);
        }

        $creator = $this->setupThreadCreate($forum);
        if (!$creator->validate($errors)) {
            return $this->error($errors);
        }

        $thread = $creator->getThread();
        $post = $creator->getPost();

        /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
        $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
        $groupedThumbnails = $thumbnailRepo->getEditorData(
            'post',
            $post,
            $tempHash
        );

        $viewParams = [
            'groupedThumbnails' => $groupedThumbnails,
            'tempHash' => $tempHash
        ];

        return $this->view(
            'ThemeHouse\Thumbnails:Forum\ThreadThumbnails',
            'ththumbnails_thumbnail_chooser',
            $viewParams
        );
    }
}
