<?php

namespace ThemeHouse\Thumbnails\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Thread extends XFCP_Thread
{
    public function actionThumbnails(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id);
        $params->post_id = $thread->first_post_id;

        return $this->rerouteController('XF:Post', 'thumbnails', $params);
    }
}
