<?php

namespace ThemeHouse\Thumbnails\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Post extends XFCP_Post
{
    public function actionThumbnails(ParameterBag $params)
    {
        $post = $this->assertViewablePost($params->post_id);
        if (!$post->canManageThumbnails($error)) {
            return $this->noPermission($error);
        }

        /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
        $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
        $thumbnails = $thumbnailRepo->getThumbnailsForEntity('post', $post);
        $groupedThumbnails = $thumbnailRepo->groupThumbnails($thumbnails);

        $viewParams = [
            'groupedThumbnails' => $groupedThumbnails,
            'contentId' => $post->post_id,
            'contentType' => 'post'
        ];

        return $this->view('ThemeHouse\Thumbnails:Thread\Thumbnails', 'ththumbnails_thumbnail_chooser', $viewParams);
    }
}
