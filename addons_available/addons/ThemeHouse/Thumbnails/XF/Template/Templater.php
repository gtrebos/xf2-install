<?php

namespace ThemeHouse\Thumbnails\XF\Template;

use ThemeHouse\Thumbnails\Entity\Thumbnail;

class Templater extends XFCP_Templater
{
    public function addDefaultHandlers()
    {
        parent::addDefaultHandlers();

        $this->addFunctions([
            'thumbnail' => 'fnThumbnail',
        ]);
    }

    protected function getButtonPhraseFromIcon($icon, $fallback = '')
    {
        switch ($icon) {
            case 'thumbnail':
                $fallback = 'button.' . $icon;
                break;
        }

        return parent::getButtonPhraseFromIcon($icon, $fallback);
    }

    public function fnThumbnail(
        $templater,
        &$escape,
        $thumbnailId,
        $size
    ) {
        $templater->includeCss('public:ththumbnails.less');

        $escape = false;

        /** @var \ThemeHouse\Thumbnails\XF\Str\Formatter $formatter */
        $formatter = $this->app->stringFormatter();
        return $formatter->getThumbnailHtml($thumbnailId, $size);
    }
}
