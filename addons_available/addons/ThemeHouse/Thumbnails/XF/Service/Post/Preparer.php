<?php

namespace ThemeHouse\Thumbnails\XF\Service\Post;

class Preparer extends XFCP_Preparer
{
    protected $thumbnailHash;

    public function setThumbnailHash($hash)
    {
        $this->thumbnailHash = $hash;
    }

    public function afterInsert()
    {
        if ($this->thumbnailHash) {
            $post = $this->post;

            $thumbnails = $this->getThumbnailsForEntity();
            $this->associateThumbnails($this->thumbnailHash);

            $selectedIds = array_keys(array_filter(array_column($thumbnails, 'selected', 'thumbnail_id')));
            $selectedId = reset($selectedIds);

            if ($selectedId && $post->isFirstPost()) {
                $thread = $this->post->Thread;
                $thread->fastUpdate([
                    'th_first_post_thumbnail_id' => $selectedId,
                    'th_last_post_thumbnail_id' => $selectedId,
                ]);
            }
        }

        parent::afterInsert();
    }

    public function afterUpdate()
    {
        $this->getThumbnailsForEntity();
        $this->associateThumbnails($this->thumbnailHash);

        parent::afterUpdate();
    }

    protected function getThumbnailsForEntity()
    {
        $post = $this->post;

        /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
        $thumbnailRepo = $this->repository('ThemeHouse\Thumbnails:Thumbnail');
        return $thumbnailRepo->getThumbnailsForEntity('post', $post, $this->thumbnailHash);
    }

    protected function associateThumbnails($hash)
    {
        $post = $this->post;

        /** @var \ThemeHouse\Thumbnails\Service\Thumbnail\Preparer $preparer */
        $preparer = $this->service('ThemeHouse\Thumbnails:Thumbnail\Preparer');
        $preparer->associateThumbnailsWithContent($hash, 'post', $post->post_id);
    }
}
