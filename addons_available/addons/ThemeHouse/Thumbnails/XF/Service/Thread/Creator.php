<?php

namespace ThemeHouse\Thumbnails\XF\Service\Thread;

class Creator extends XFCP_Creator
{
    public function setThumbnailHash($hash)
    {
        $this->postPreparer->setThumbnailHash($hash);
    }
}
