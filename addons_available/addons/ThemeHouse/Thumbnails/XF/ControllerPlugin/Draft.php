<?php

namespace ThemeHouse\Thumbnails\XF\ControllerPlugin;

class Draft extends XFCP_Draft
{
    public function actionDraftMessage(
        \XF\Draft $draft,
        array $extraData = [],
        $messageInput = 'message',
        &$actionTaken = null
    ) {
        $thumbnailHash = $this->filter('thumbnail_hash', 'str');
        if ($thumbnailHash) {
            $extraData['thumbnail_hash'] = $thumbnailHash;
        }

        return parent::actionDraftMessage($draft, $extraData, $messageInput, $actionTaken);
    }
}
