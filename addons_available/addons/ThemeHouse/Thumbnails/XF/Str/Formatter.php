<?php

namespace ThemeHouse\Thumbnails\XF\Str;

class Formatter extends XFCP_Formatter
{
    public function getThumbnailHtml($thumbnailId, $size)
    {
        /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
        $thumbnailRepo = \XF::repository('ThemeHouse\Thumbnails:Thumbnail');

        $thumbnailUrl = $thumbnailRepo->getThumbnailUrl($thumbnailId, $size);

        if (!$thumbnailUrl) {
            return '';
        }

        $thumbnailUrl2x = $thumbnailRepo->getThumbnailUrl2x($thumbnailId, $size);
        $srcSet = $thumbnailUrl2x ? " srcset=\"{$thumbnailUrl2x} 2x\"" : '';

        return "<img src=\"{$thumbnailUrl}\"{$srcSet} class=\"thThumbnail thThumbnail--{$size}\" />";
    }
}
