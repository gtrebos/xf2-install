<?php

namespace ThemeHouse\Thumbnails\BbCode\ProcessorAction;

use XF\BbCode\ProcessorAction\FiltererInterface;
use XF\BbCode\ProcessorAction\FiltererHooks;
use XF\BbCode\Processor;

class Images implements FiltererInterface
{
    /**
     * @var \XF\App
     */
    protected $app;

    protected $images = [];

    public function __construct(\XF\App $app)
    {
        $this->app = $app;
    }

    public function addFiltererHooks(FiltererHooks $hooks)
    {
        $hooks->addTagHook('img', 'filterImageTag');
    }

    public function filterImageTag(array $tag, array $options, Processor $processor)
    {
        $image = $processor->renderSubTreePlain($tag['children']);
        if (empty($tag['option']) || $tag['option'] == $image) {
            $this->images[] = $image;
        }

        return null;
    }

    public function getImages()
    {
        return $this->images;
    }

    public static function factory(\XF\App $app)
    {
        return new static($app);
    }
}
