<?php

namespace ThemeHouse\Thumbnails\Job;

use XF\Mvc\Entity\Entity;

class Post extends AbstractThumbnailJob
{
    protected function getIdsToRebuild()
    {
        $db = $this->app->db();

        return $db->fetchAllColumn($db->limit(
            "
				SELECT post_id
				FROM xf_post
				WHERE post_id > ?
				ORDER BY post_id
			",
            $this->data['batch']
        ), $this->data['start']);
    }

    protected function getRecordToRebuild($id)
    {
        return $this->app->em()->find('XF:Post', $id);
    }

    protected function getContentType()
    {
        return 'post';
    }

    protected function getActionDescription()
    {
        $rebuildPhrase = \XF::phrase('rebuilding');
        $type = \XF::phrase('posts');
        return sprintf('%s... %s', $rebuildPhrase, $type);
    }
}
