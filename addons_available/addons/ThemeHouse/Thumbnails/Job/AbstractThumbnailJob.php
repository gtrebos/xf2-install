<?php

namespace ThemeHouse\Thumbnails\Job;

use XF\Mvc\Entity\Entity;
use XF\Job\AbstractJob;

abstract class AbstractThumbnailJob extends AbstractJob
{
    protected $defaultData = [
        'steps' => 0,
        'start' => 0,
        'batch' => 1000,
        'allow_downloading_images' => false
    ];

    abstract protected function getIdsToRebuild();
    abstract protected function getRecordToRebuild($id);
    abstract protected function getContentType();
    abstract protected function getActionDescription();

    public function run($maxRunTime)
    {
        $startTime = microtime(true);

        $this->data['steps']++;

        $ids = $this->getIdsToRebuild();
        if (!$ids) {
            return $this->complete();
        }

        $done = 0;

        /** @var \ThemeHouse\Thumbnails\Repository\Thumbnail $thumbnailRepo */
        $thumbnailRepo = \XF::repository('ThemeHouse\Thumbnails:Thumbnail');

        $contentType = $this->getContentType();
        $handler = $thumbnailRepo->getThumbnailHandler($contentType);
        if (!$handler) {
            throw new \InvalidArgumentException("No thumbnail handler found for content type '$contentType'");
        }

        $handler->setOption('allow_downloading_images', $this->data['allow_downloading_images']);

        foreach ($ids as $id) {
            if (microtime(true) - $startTime >= $maxRunTime) {
                break;
            }

            $this->data['start'] = $id;

            $record = $this->getRecordToRebuild($id);
            if (!$record) {
                continue;
            }

            $thumbnails = $thumbnailRepo->findThumbnailsByContent($contentType, $id)->fetch()->toArray();

            $handler->extractFromContainerEntity($record, $tempHash, $thumbnails);

            $selected = array_filter(array_column($thumbnails, 'selected'));
            if (!$selected) {
                $handler->selectDefaultThumbnail($record, $thumbnails);
            }

            $done++;
        }

        $this->data['batch'] = $this->calculateOptimalBatch($this->data['batch'], $done, $startTime, $maxRunTime, 1000);

        return $this->resume();
    }

    public function getStatusMessage()
    {
        $description = $this->getActionDescription();
        return sprintf('%s (%s)', $description, $this->data['start']);
    }

    public function canCancel()
    {
        return true;
    }

    public function canTriggerByChoice()
    {
        return true;
    }
}
