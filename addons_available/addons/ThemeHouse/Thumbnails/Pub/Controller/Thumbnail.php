<?php

namespace ThemeHouse\Thumbnails\Pub\Controller;

use XF\Pub\Controller\AbstractController;
use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\AbstractReply;

class Thumbnail extends AbstractController
{
    public function actionIndex(ParameterBag $params)
    {
        $this->assertPostOnly();

        $thumbnailId = $this->filter('thumbnail_id', 'int');
        $tempHash = $this->filter('thumbnail_hash', 'string');

        if (!$thumbnailId) {
            throw $this->exception($this->notFound());
        }

        /** @var \ThemeHouse\Thumbnails\Entity\Thumbnail $thumbnail */
        $thumbnail = $this->em()->find('ThemeHouse\Thumbnails:Thumbnail', $thumbnailId);
        if (!$thumbnail) {
            throw $this->exception($this->notFound());
        }

        if ($this->getThumbnailRepo()->markThumbnailSelected($thumbnail, $tempHash)) {
            return $this->redirect($this->getDynamicRedirect(null, true));
        } else {
            return $this->noPermission();
        }
    }

    /**
     * @return \XF\Repository\Thumbnail
     */
    protected function getThumbnailRepo()
    {
        return $this->repository('ThemeHouse\Thumbnails:Thumbnail');
    }
}
