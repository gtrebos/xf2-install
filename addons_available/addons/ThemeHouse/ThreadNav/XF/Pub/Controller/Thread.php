<?php

namespace ThemeHouse\ThreadNav\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Thread extends XFCP_Thread
{
    public function actionNext(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id);

        if (\XF::options()->ththreadnav_reverseNextPrevious) {
            $thread = $thread->getPrevious();
        } else {
            $thread = $thread->getNext();
        }

        return $this->redirect($this->buildLink('threads', $thread));
    }

    public function actionPrevious(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id);

        if (\XF::options()->ththreadnav_reverseNextPrevious) {
            $thread = $thread->getNext();
        } else {
            $thread = $thread->getPrevious();
        }

        return $this->redirect($this->buildLink('threads', $thread));
    }
    
    public function actionPreviewNext(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id);

        if (\XF::options()->ththreadnav_reverseNextPrevious) {
            $thread = $thread->getPrevious(['FirstPost']);
        } else {
            $thread = $thread->getNext(['FirstPost']);
        }

        $firstPost = $thread->FirstPost;

        $viewParams = [
            'thread' => $thread,
            'firstPost' => $firstPost
        ];
        return $this->view('XF:Thread\Preview', 'thread_preview', $viewParams);
    }
    
    public function actionPreviewPrevious(ParameterBag $params)
    {
        $thread = $this->assertViewableThread($params->thread_id);

        if (\XF::options()->ththreadnav_reverseNextPrevious) {
            $thread = $thread->getNext(['FirstPost']);
        } else {
            $thread = $thread->getPrevious(['FirstPost']);
        }

        $firstPost = $thread->FirstPost;

        $viewParams = [
            'thread' => $thread,
            'firstPost' => $firstPost
        ];
        return $this->view('XF:Thread\Preview', 'thread_preview', $viewParams);
    }
}
