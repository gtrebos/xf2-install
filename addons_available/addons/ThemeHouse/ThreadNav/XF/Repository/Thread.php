<?php

namespace ThemeHouse\ThreadNav\XF\Repository;

use XF\Entity\Forum;

class Thread extends XFCP_Thread
{
    /**
     * @return XF\Entity\Thread
     */
    public function getFirstThread(Forum $forum, $sticky, array $extraWith = [])
    {
        $finder = \XF::finder('XF:Thread')
            ->inForum($forum)
            ->where('sticky', $sticky);

        if ($extraWith) {
            $finder->with($extraWith);
        }

        return $finder->fetchOne();
    }

    /**
     * @return XF\Entity\Thread
     */
    public function getLastThread(Forum $forum, $sticky, array $extraWith = [])
    {
        $sortDirection = strtoupper($forum->default_sort_direction) === 'ASC' ? 'DESC' : 'ASC';

        $finder = \XF::finder('XF:Thread')
            ->inForum($forum)
            ->where('sticky', $sticky)
            ->setDefaultOrder($forum->default_sort_order, $sortDirection);

        if ($extraWith) {
            $finder->with($extraWith);
        }

        return $finder->fetchOne();
    }
}
