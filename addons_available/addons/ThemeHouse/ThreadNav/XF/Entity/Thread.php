<?php

namespace ThemeHouse\ThreadNav\XF\Entity;

class Thread extends XFCP_Thread
{
    /**
     * @return XF\Entity\Thread
     */
    public function getNext(array $extraWith = [], $noLoop = false)
    {
        $thread = $this->getNextPrevious('desc', $extraWith);

        if ($thread) {
            return $thread;
        }

        $threadRepo = \XF::repository('XF:Thread');

        if ($this->sticky && $noLoop) {
            return null;
        }

        $thread = $threadRepo->getLastThread($this->Forum, !$this->sticky, $extraWith);

        if ($thread) {
            return $thread;
        }

        if ($noLoop) {
            return null;
        }

        return $threadRepo->getLastThread($this->Forum, $this->sticky, $extraWith);
    }

    /**
     * @return XF\Entity\Thread
     */
    public function getPrevious(array $extraWith = [], $noLoop = false)
    {
        $thread = $this->getNextPrevious('asc', $extraWith);

        if ($thread) {
            return $thread;
        }

        $threadRepo = \XF::repository('XF:Thread');

        if (!$this->sticky && $noLoop) {
            return null;
        }

        $thread = $threadRepo->getFirstThread($this->Forum, !$this->sticky, $extraWith);

        if ($thread) {
            return $thread;
        }

        if ($noLoop) {
            return null;
        }

        return $threadRepo->getFirstThread($this->Forum, $this->sticky, $extraWith);
    }

    /**
     * @param string $sortOrder
     * @return XF\Entity\Thread|null
     */
    protected function getNextPrevious($sortOrder, array $extraWith = [])
    {
        $forum = $this->Forum;

        $finder = \XF::finder('XF:Thread')
            ->inForum($forum)
            ->where('sticky', $this->sticky);

        if ($extraWith) {
            $finder->with($extraWith);
        }

        if ($forum->default_sort_direction === $sortOrder) {
            $finder->where($forum->default_sort_order, '>', $this->{$forum->default_sort_order});
            $finder->order($forum->default_sort_order, $sortOrder == 'asc' ? 'desc' : 'asc');
        } else {
            $finder->where($forum->default_sort_order, '<', $this->{$forum->default_sort_order});
        }

        $thread = $finder->fetchOne();

        if ($thread || $thread->{$forum->default_sort_order} !== $this->{$forum->default_sort_order}) {
            return $thread;
        }

        $finder = \XF::finder('XF:Thread')
            ->inForum($forum)
            ->where('sticky', $this->sticky);

        if ($extraWith) {
            $finder->with($extraWith);
        }

        $finder->where($forum->default_sort_order, '=', $this->{$forum->default_sort_order});
        if ($forum->default_sort_direction === $sortOrder) {
            $finder->where('thread_id', '>', $this->thread_id);
            $finder->order($forum->default_sort_order, $sortOrder == 'asc' ? 'desc' : 'asc');
        } else {
            $finder->where('thread_id', '<', $this->thread_id);
        }

        return $finder->fetchOne();
    }
}
