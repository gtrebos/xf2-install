<?php

namespace ThemeHouse\ThreadNav\Listener;

use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class EntityStructure
{
    public static function xfThread(Manager $em, Structure &$structure)
    {
        $structure->getters['next'] = true;
        $structure->getters['previous'] = true;
    }
}
