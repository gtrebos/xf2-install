<?php

namespace ThemeHouse\Trending\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * Class Trending
 * @package ThemeHouse\Trending\Entity
 *
 * @property string trending_id
 * @property string title
 * @property string content_type
 * @property integer max_age_days
 * @property string max_age_column
 * @property string algorithm
 * @property array category_ids
 * @property array config
 * @property integer display_order
 * @property boolean active
 * @property boolean show_Tab
 */
class Trending extends Entity
{
    protected $handler;

    public static function getStructure(Structure $structure)
    {
        $structure->table = 'xf_th_trending';
        $structure->shortName = 'ThemeHouse\Trending:Trending';
        $structure->primaryKey = 'trending_id';

        $structure->columns = [
            'trending_id' => ['type' => self::UINT, 'autoIncrement' => true],
            'trending_url_bit' => ['type' => self::STR, 'maxLength' => 50, 'required' => true],
            'title' => ['type' => self::STR, 'maxLength' => 50, 'required' => true],
            'content_type' => ['type' => self::STR, 'maxLength' => 25, 'required' => true],
            'max_age_days' => ['type' => self::UINT, 'default' => 30],
            'max_age_column' => ['type' => self::STR, 'required' => true],
            'algorithm' => ['type' => self::STR, 'required' => true],
            'category_ids' => ['type' => self::LIST_COMMA, 'default' => []],
            'config' => ['type' => self::JSON_ARRAY, 'default' => []],
            'display_order' => ['type' => self::UINT, 'default' => 1],
            'active' => ['type' => self::BOOL, 'default' => true],
            'show_tab' => ['type' => self::BOOL, 'default' => true],
        ];
        $structure->getters = [
            'Handler' => true
        ];

        return $structure;
    }

    /**
     * @param bool $plural
     * @return \XF\Phrase
     * @throws \Exception
     */
    public function getContentTypePhrase($plural = false)
    {
        return $this->getHandler()->getContentTypePhrase($plural);
    }

    /**
     * @return \ThemeHouse\Trending\Trending\AbstractHandler
     * @throws \Exception
     */
    public function getHandler()
    {
        if (!$this->handler) {
            $this->handler = $this->getTrendingRepo()->getHandlerForContentType($this->content_type, $this);
        }

        return $this->handler;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function renderTrendingContent()
    {
        return $this->getHandler()->renderTrendingContent($this);
    }

    /**
     * @param \ThemeHouse\Trending\Widget\Trending $widget
     * @return mixed
     * @throws \Exception
     */
    public function renderTrendingWidget(\ThemeHouse\Trending\Widget\Trending $widget)
    {
        return $this->getHandler()->renderTrendingWidget($this, $widget);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function renderAdditionalConfig()
    {
        return $this->getHandler()->renderAdditionalConfig($this);
    }

    /**
     * @param $trendingId
     * @return bool
     */
    public function verifyTrendingId(&$trendingId)
    {
        if ($this->isInsert()) {
            if (!preg_match('#^[a-z0-9_]+$#i', $trendingId)) {
                $this->error(\XF::phrase('thtrending_trending_ids_may_only_contain_alphanumeric_underscore'));
                return false;
            }

            $existingTrendingId = $this->db()->fetchOne('SELECT count(trending_id) FROM xf_th_trending WHERE trending_id =?', $trendingId);
            if ($existingTrendingId) {
                $this->error(\XF::phrase('thtrending_trending_ids_must_be_unique'));
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $config
     * @return bool
     * @throws \Exception
     */
    public function verifyConfig(array &$config)
    {
        if (!$this->getHandler()->validateAdditionalConfig($config, $error)) {
            $this->error($error);
            return false;
        }

        return true;
    }

    /**
     * @return \ThemeHouse\Trending\Repository\Trending
     */
    protected function getTrendingRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Trending:Trending');
    }
}