<?php

namespace ThemeHouse\Trending\Repository;

use XF\Db\Schema\Alter;
use XF\Mvc\Entity\Repository;

/**
 * Class Trending
 * @package ThemeHouse\Trending\Repository
 */
class Trending extends Repository
{
    protected $handlerClassContentField = 'thtrending_handler_class';

    /**
     * @return \XF\Mvc\Entity\ArrayCollection
     */
    public function getTrendingConfigsForListing($viewCheck = true)
    {
        /** @var \XF\Mvc\Entity\Finder $finder */
        $finder = $this->finder('ThemeHouse\Trending:Trending');
        $finder->where('show_tab', '=', 1);
        $finder->where('active', '=', 1);
        $finder->order('display_order', 'asc');

        $list = $finder->fetch();

        foreach($list as $key => $item) {
            if(!\XF::visitor()->hasTrendingPermission($item->trending_id, 'view')) {
                $list->offsetUnset($key);
            }
        }

        return $list;
    }

    /**
     * @param $contentType
     * @param \ThemeHouse\Trending\Entity\Trending|null $trending
     * @return \ThemeHouse\Trending\Trending\AbstractHandler
     * @throws \Exception
     */
    public function getHandlerForContentType($contentType, \ThemeHouse\Trending\Entity\Trending $trending = null)
    {
        $handlerClass = $this->app()->getContentTypeFieldValue($contentType, $this->handlerClassContentField);
        if (!$handlerClass || !class_exists($handlerClass)) {
            throw new \Exception('Invalid trending handler loaded: ' . $contentType);
        }

        $handlerClass = \XF::extendClass($handlerClass);
        return new $handlerClass($this->app(), $trending);
    }

    /**
     * @param $contentType
     * @param bool $onlyUsable
     * @return bool
     * @throws \Exception
     */
    public function isValidContentType($contentType, $onlyUsable = true)
    {
        $contentTypes = $this->getValidContentTypes($onlyUsable);

        if (in_array($contentType, $contentTypes)) {
            return true;
        }

        return false;
    }

    /**
     * @param bool $onlyUsable
     * @param bool $withPhrase
     * @param bool $plural
     * @return array
     * @throws \Exception
     */
    public function getValidContentTypes($onlyUsable=true, $withPhrase=false, $plural=false)
    {
        $contentTypes = $this->app()->getContentTypeField($this->handlerClassContentField);
        $contentTypes = array_keys($contentTypes);

        if ($onlyUsable) {
            foreach ($contentTypes as $contentType) {
                $handler = $this->getHandlerForContentType($contentType);

                if (!$handler->canUse()) {
                    $key = array_search($contentType, $contentTypes);
                    unset($contentTypes[$key]);
                }
            }

            $contentTypes = array_values($contentTypes);
        }

        if ($withPhrase) {
            $contentTypesWithPhrases = [];
            foreach ($contentTypes as $contentType) {
                $handler = $this->getHandlerForContentType($contentType);

                $contentTypesWithPhrases[$contentType] = [
                    'content_type' => $contentType,
                    'phrase' => $handler->getContentTypePhrase($plural),
                ];
            }
            $contentTypes = $contentTypesWithPhrases;
        }

        return $contentTypes;
    }

    /**
     * @param $contentType
     */
    public function rebuildTrendingValues($contentType)
    {
        try {
            $handler = $this->getHandlerForContentType($contentType);
            $handler->rebuildTrendingValues();
        } catch (\Exception $e) {}
    }

    /**
     * @param $addOnId
     */
    public function setupColumnsForAddon($addOnId)
    {
        $schemaManager = $this->app()->db()->getSchemaManager();

        if ($addOnId === 'XFRM') {
            $schemaManager->alterTable('xf_rm_resource', function(Alter $table) {
                $table->addColumn('thtrending_positive_rating_count', 'int')->setDefault(0);
                $table->addColumn('thtrending_positive_ratings_per_minute', 'decimal', '20,6')->setDefault(0);
            });
        }

        if ($addOnId === 'XFMG') {
            $schemaManager->alterTable('xf_mg_media_item', function(Alter $table) {
                $table->addColumn('thtrending_positive_rating_count', 'int')->setDefault(0);
                $table->addColumn('thtrending_views_per_minute', 'decimal', '20,6')->setDefault(0);
                $table->addColumn('thtrending_positive_ratings_per_minute', 'decimal', '20,6')->setDefault(0);
                $table->addColumn('thtrending_positive_ratings_views_per_minute', 'decimal', '20,6')->setDefault(0);
            });
        }
    }
}