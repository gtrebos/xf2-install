<?php

namespace ThemeHouse\Trending\XFRM\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class ResourceItem
 * @package ThemeHouse\Trending\XFRM\Entity
 */
class ResourceItem extends XFCP_ResourceItem
{
    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns['thtrending_positive_rating_count'] = [
            'type' => self::UINT,
            'default' => 0,
        ];
        $structure->columns['thtrending_positive_ratings_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];

        return $structure;
    }

    public function rebuildRating()
    {
        parent::rebuildRating();

        $positiveRatings = $this->db()->fetchOne("
            SELECT COUNT(resource_rating_id)
            FROM xf_rm_resource_rating
            WHERE resource_id=?
              AND rating >= 4
              AND count_rating = 1
              AND rating_state = 'visible'
          ", $this->resource_id);

        $this->thtrending_positive_rating_count = $positiveRatings;
    }
}