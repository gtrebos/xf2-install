<?php

namespace ThemeHouse\Trending\Cron;

/**
 * Class CalculateTrendingValues
 * @package ThemeHouse\Trending\Cron
 */
class CalculateTrendingValues
{
    public static function run()
    {
        /** @var \ThemeHouse\Trending\Repository\Trending $trendingRepo */
        $trendingRepo = \XF::app()->repository('ThemeHouse\Trending:Trending');

        $addOns = \XF::app()->registry()['addOns'];
        $trendingRepo->rebuildTrendingValues('thread');

        if (!empty($addOns['XFRM'])) {
            $trendingRepo->rebuildTrendingValues('resource');
        }

        if (!empty($addOns['XFMG'])) {
            $trendingRepo->rebuildTrendingValues('xfmg_media');
        }
    }
}