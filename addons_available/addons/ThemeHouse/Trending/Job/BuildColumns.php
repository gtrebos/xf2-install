<?php

namespace ThemeHouse\Trending\Job;

use XF\Job\AbstractJob;

/**
 * Class BuildColumns
 * @package ThemeHouse\Trending\Job
 */
class BuildColumns extends AbstractJob
{
    protected $defaultData = [
        'addon_id' => null
    ];

    /**
     * @param $maxRunTime
     * @return \XF\Job\JobResult
     */
    public function run($maxRunTime)
    {
        /** @var \ThemeHouse\Trending\Repository\Trending $repo */
        $repo = \XF::repository('ThemeHouse\Trending:Trending');
        $repo->setupColumnsForAddon($this->data['addon_id']);

        return $this->complete();
    }

    public function getStatusMessage()
    {
        // TODO: Implement getStatusMessage() method.
    }

    /**
     * @return bool
     */
    public function canCancel()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canTriggerByChoice()
    {
        return false;
    }
}