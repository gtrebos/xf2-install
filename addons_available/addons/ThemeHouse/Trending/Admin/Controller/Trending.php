<?php

namespace ThemeHouse\Trending\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\Mvc\ParameterBag;

/**
 * Class Trending
 * @package ThemeHouse\Trending\Admin\Controller
 */
class Trending extends AbstractController
{
    public function actionIndex()
    {
        $trendingConfigs = $this->finder('ThemeHouse\Trending:Trending')->order('display_order')->fetch();

        /** @noinspection PhpUndefinedMethodInspection */
        $customPermissions = $this->repository('XF:PermissionEntry')->getContentWithCustomPermissions('thtrending');

        $viewParams = [
            'trendingConfigs' => $trendingConfigs,
            'customPermissions' => $customPermissions
        ];

        return $this->view('ThemeHouse\Trending:Trending\Index', 'thtrending_trending_list', $viewParams);
    }

    /**
     * @return \XF\Mvc\Reply\View
     * @throws \Exception
     */
    public function actionAdd()
    {
        $trendingRepo = $this->getTrendingRepository();
        /** @var \ThemeHouse\Trending\Entity\Trending $trending */
        $trending = $this->em()->create('ThemeHouse\Trending:Trending');

        $contentType = $this->filter('content_type', 'str');

        if (!$contentType || !$trendingRepo->isValidContentType($contentType)) {
            $viewParams = [
                'contentTypes' => $trendingRepo->getValidContentTypes(true, true),
            ];
            return $this->view('ThemeHouse\Trending:Trending\SelectContentType', 'thtrending_trending_add', $viewParams);
        }

        $trending->content_type = $contentType;

        return $this->addEditTrending($trending);
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionEdit(ParameterBag $params)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $trending = $this->assertTrendingExists($params->trending_id);

        return $this->addEditTrending($trending);
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     * @throws \XF\PrintableException
     */
    public function actionDelete(ParameterBag $params)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $trending = $this->assertTrendingExists($params->trending_id);

        if ($this->isPost()) {
            $trending->delete();

            return $this->redirect($this->buildLink('trending'));
        }

        $viewParams = [
            'trending' => $trending,
        ];

        return $this->view('ThemeHouse\Trending:Trending\Delete', 'thtrending_trending_delete', $viewParams);
    }

    protected function trendingSaveProcess(\ThemeHouse\Trending\Entity\Trending $trending)
    {
        $form = $this->formAction();

        $input = $this->filter([
            'title' => 'str',
            'max_age_days' => 'uint',
            'max_age_column' => 'str',
            'algorithm' => 'str',
            'category_ids' => 'array-int',
            'config' => 'array',
            'active' => 'bool',
            'show_tab' => 'bool',
        ]);

        $form->basicEntitySave($trending, $input);

        return $form;
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect
     * @throws \XF\Mvc\Reply\Exception
     * @throws \XF\PrintableException
     */
    public function actionSave(ParameterBag $params)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        if ($params->trending_id) {
            /** @noinspection PhpUndefinedFieldInspection */
            $trending = $this->assertTrendingExists($params->trending_id);
        } else {
            /** @var \ThemeHouse\Trending\Entity\Trending $trending */
            $trending = $this->em()->create('ThemeHouse\Trending:Trending');

            $contentType = $this->filter('content_type', 'str');
            $trendingUrlBit = $this->filter('trending_url_bit', 'str');

            $trending->trending_url_bit = $trendingUrlBit;
            $trending->content_type = $contentType;
        }

        $this->trendingSaveProcess($trending)->run();

        return $this->redirect($this->buildLink('trending') . $this->buildLinkHash($trending->trending_id));
    }

    protected function addEditTrending(\ThemeHouse\Trending\Entity\Trending $trending)
    {
        $viewParams = [
            'trending' => $trending,
        ];

        return $this->view('ThemeHouse\Trending:Trending\Edit', 'thtrending_trending_edit', $viewParams);
    }

    /**
     * @return \ThemeHouse\Trending\ControllerPlugin\TrendingPermission
     */
    protected function getTrendingPermissionPlugin()
    {
        /** @var \ThemeHouse\Trending\ControllerPlugin\TrendingPermission $plugin */
        $plugin = $this->plugin('ThemeHouse\Trending:TrendingPermission');
        $plugin->setFormatters('ThemeHouse\Trending\TrendingPermission%s', 'trending_permission_%s');
        $plugin->setRoutePrefix('trending/permissions');

        return $plugin;
    }

    public function actionPermissions(ParameterBag $params)
    {
        return $this->getTrendingPermissionPlugin()->actionList($params);
    }

    public function actionPermissionsEdit(ParameterBag $params)
    {
        return $this->getTrendingPermissionPlugin()->actionEdit($params);
    }

    public function actionPermissionsSave(ParameterBag $params)
    {
        return $this->getTrendingPermissionPlugin()->actionSave($params);
    }

    /**
     * @param $id
     * @param null $with
     * @param null $phraseKey
     *
     * @return \ThemeHouse\Trending\Entity\Trending
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function assertTrendingExists($id, $with = null, $phraseKey = null)
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->assertRecordExists('ThemeHouse\Trending:Trending', $id, $with, $phraseKey);
    }

    /**
     * @return \ThemeHouse\Trending\Repository\Trending
     */
    protected function getTrendingRepository()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Trending:Trending');
    }
}