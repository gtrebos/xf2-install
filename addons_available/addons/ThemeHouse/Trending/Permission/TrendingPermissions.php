<?php

namespace ThemeHouse\Trending\Permission;

use XF\Entity\Permission;
use XF\Permission\FlatContentPermissions;

class TrendingPermissions extends FlatContentPermissions
{
    protected function getContentType()
    {
        return 'thtrending';
    }

    public function getAnalysisTypeTitle()
    {
        return \XF::phrase('thtrending_trending_permissions');
    }

    public function isValidPermission(Permission $permission)
    {
        if ($permission->permission_group_id == 'general' && $permission->permission_id == 'viewTHTrending')
        {
            return true;
        }

        return false;
    }

    public function getContentList()
    {
        return \XF::finder('ThemeHouse\Trending:Trending')->fetch();
    }

    protected function getFinalPerms($contentId, array $calculated, array &$childPerms)
    {
        $final['view'] = $calculated['general']['viewTHTrending'];
        return $final;
    }

    protected function getFinalAnalysisPerms($contentId, array $calculated, array &$childPerms)
    {
        $finalize = [
            'general' => ['viewTHTrending' => $calculated['general']['viewTHTrending']],
        ];

        $final = $this->builder->finalizePermissionValues($finalize);

        if (!$final['general']['viewTHTrending'])
        {
            $childPerms['general']['viewTHTrending'] = 'deny';
        }

        return $final;
    }
}