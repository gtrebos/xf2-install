<?php

namespace ThemeHouse\Trending\Trending;

use ThemeHouse\Trending\Entity\Trending;

/**
 * Class Thread
 * @package ThemeHouse\Trending\Trending
 */
class Thread extends AbstractHandler
{
    protected $contentType = 'thread';

    /**
     * @param bool $plural
     * @return \XF\Phrase
     */
    protected function _getCategoryPhrase($plural = false)
    {
        if ($plural) {
            return \XF::phrase('nodes');
        }

        return \XF::phrase('node');
    }

    /**
     * @return array
     */
    protected function _getAlgorithmOptions()
    {
        return [
            'thtrending_replies_views_per_minute' => [
                'value' => 'thtrending_replies_views_per_minute',
                'label' => \XF::phrase('thtrending_replies_views_per_minute'),
            ],
            'thtrending_replies_per_minute' => [
                'value' => 'thtrending_replies_per_minute',
                'label' => \XF::phrase('thtrending_replies_per_minute'),
            ],
            'thtrending_views_per_minute' => [
                'value' => 'thtrending_views_per_minute',
                'label' => \XF::phrase('thtrending_views_per_minute'),
            ],
        ];
    }

    /**
     * @return array
     */
    protected function _getCategoryOptions()
    {
        /** @var \XF\Repository\Node $nodeRepo */
        $nodeRepo = \XF::repository('XF:Node');

        $choices = $nodeRepo->getNodeOptionsData(false, 'Forum', 'option');
        $choices = array_map(function ($v) {
            $v['label'] = \XF::escapeString($v['label']);
            return $v;
        }, $choices);

        return $choices;
    }

    /**
     * @param Trending $trending
     *
     * @return \XF\Finder\Thread
     */
    protected function _getTrendingContentFinder(Trending $trending)
    {
        $visitor = \XF::visitor();

        /** @var \XF\Finder\Thread $finder */
        $finder = $this->app->finder('XF:Thread');
        $finder->forFullView(true);
        $finder->with('Forum.Node.Permissions|' . $visitor->permission_combination_id);
        if ($trending->max_age_days) {
            $cutoff = \XF::$time - ($trending->max_age_days * 86400);
            $finder->where($trending->max_age_column, '>', $cutoff);
        }
        $finder->order($trending->algorithm, 'desc');

        if (!empty($trending->config['min_replies'])) {
            $finder->where('reply_count', '>=', $trending->config['min_replies']);
        }

        if (!empty($trending->config['min_views'])) {
            $finder->where('view_count', '>=', $trending->config['min_views']);
        }

        if (!empty($trending->category_ids)) {
            $finder->where('node_id', '=', $trending->category_ids);
        }

        return $finder;
    }

    /**
     * @return array
     */
    protected function _getMaxAgeColumns()
    {
        return [
            'last_post_date' => [
                'value' => 'last_post_date',
                'label' => \XF::phrase('last_message'),
            ],
            'post_date' => [
                'value' => 'post_date',
                'label' => \XF::phrase('first_message'),
            ],
        ];
    }

    /**
     *
     */
    protected function _rebuildTrendingValues()
    {
        $db = $this->app->db();
        $db->query("
            UPDATE xf_thread AS t
            SET t.thtrending_views_per_minute = (t.view_count / floor((? - t.post_date) / 60)),
              t.thtrending_replies_per_minute = (t.reply_count / floor((? - t.post_date) / 60)),
              t.thtrending_replies_views_per_minute = t.thtrending_views_per_minute + t.thtrending_replies_per_minute
        ", [
            \XF::$time,
            \XF::$time,
        ]);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderTrendingContent(Trending $trending)
    {
        $templater = $this->app->templater();

        $page = $this->filterPage();
        $threadsPerPage = (int)\XF::app()->options()->discussionsPerPage;

        $threadFinder = $this->getTrendingContentFinder($trending);

        $this->applyFinderFilters($threadFinder);

        $filters = $this->getFilterInput();
        $this->applyFilters($threadFinder, $filters);

        $this->applyDateLimitFilters($threadFinder, $filters);

        $threads = $threadFinder->limitByPage($page, $threadsPerPage)->fetch();

        $canInlineMod = false;
        foreach ($threads as $threadId => $thread) {
            /** @var \XF\Entity\Thread $thread */

            if (!$thread->canView()) {
                unset($threads[$threadId]);
            }
            else if ($thread->canUseInlineModeration())
            {
                $canInlineMod = true;
                break;
            }
        }

        if (!empty($filters['starter_id'])) {
            $starterFilter = \XF::app()->find('XF:User', $filters['starter_id']);
        } else {
            $starterFilter = null;
        }

        $viewParams = [
            'filters' => $filters,
            'starterFilter' => $starterFilter,
            'trending' => $trending,
            'threads' => $threads,
            'page' => $page,
            'perPage' => $threadsPerPage,
            'total' => $threadFinder->total(),
            'canInlineMod' => $canInlineMod
        ];

        return $templater->renderTemplate('public:thtrending_trending_view_thread', $viewParams);
    }

    protected function getAvailableDateLimits()
    {
        return [-1, 7, 14, 30, 60, 90, 182, 365];
    }

    protected function applyFilters(\XF\Finder\Thread $threadFinder, array $filters)
    {
        if (!empty($filters['prefix_id'])) {
            $threadFinder->where('prefix_id', intval($filters['prefix_id']));
        }

        if (!empty($filters['starter_id'])) {
            $threadFinder->where('user_id', intval($filters['starter_id']));
        }
    }

    protected function applyDateLimitFilters(\XF\Finder\Thread $threadFinder, array $filters)
    {
        if (!empty($filters['last_days']) && empty($filters['no_date_limit'])) {
            if ($filters['last_days'] > 0) {
                $threadFinder->where('last_post_date', '>=', \XF::$time - ($filters['last_days'] * 86400));
            }
        }
    }

    protected function getFilterInput()
    {
        $filters = [];

        $input = \XF::app()->request()->filter([
            'prefix_id' => 'uint',
            'starter' => 'str',
            'starter_id' => 'uint',
            'last_days' => 'int',
            'order' => 'str',
            'direction' => 'str',
            'no_date_limit' => 'bool'
        ]);

        if ($input['no_date_limit']) {
            $filters['no_date_limit'] = $input['no_date_limit'];
        }

        if ($input['prefix_id']) {
            $filters['prefix_id'] = $input['prefix_id'];
        }

        if ($input['starter_id']) {
            $filters['starter_id'] = $input['starter_id'];
        } else {
            if ($input['starter']) {
                $user = \XF::app()->em()->findOne('XF:User', ['username' => $input['starter']]);
                if ($user) {
                    $filters['starter_id'] = $user->user_id;
                }
            }
        }

        if (in_array($input['last_days'], $this->getAvailableDateLimits())) {
            $filters['last_days'] = $input['last_days'];
        }

        return $filters;
    }

    protected function applyFinderFilters($threadFinder)
    {
        return $threadFinder;
    }

    /**
     * @param Trending $trending
     * @param \ThemeHouse\Trending\Widget\Trending $widget
     * @return \XF\Widget\WidgetRenderer
     */
    protected function _renderTrendingWidget(Trending $trending, \ThemeHouse\Trending\Widget\Trending $widget)
    {
        $limit = $widget->getOptions()['limit'];

        $threadFinder = $this->getTrendingContentFinder($trending);
        $threadFinder->limit($limit);

        $threads = $threadFinder->fetch();

        foreach ($threads as $threadId => $thread) {
            /** @var \XF\Entity\Thread $thread */

            if (!$thread->canView()) {
                unset($threads[$threadId]);
            }
        }

        $viewParams = [
            'trending' => $trending,
            'threads' => $threads,
        ];
        return $widget->renderer('thtrending_trending_widget_thread', $viewParams);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderAdditionalConfig(Trending $trending)
    {
        $templater = $this->app->templater();

        $viewParams = [
            'trending' => $trending,
            'config' => $trending->config,
        ];

        return $templater->renderTemplate('admin:thtrending_edit_config_thread', $viewParams);
    }

    /**
     * @param array $config
     * @param $error
     * @return bool
     */
    protected function _validateAdditionalConfig(array &$config, &$error)
    {
        $config = [
            'min_replies' => $config['min_replies'] ?: 0,
            'min_views' => $config['min_views'] ?: 0,
        ];
        if ($config['min_replies'] < 0) {
            $error = \XF::phrase('thtrending_minium_replies_must_be_positive_integer_or_zero');
            return false;
        }
        if ($config['min_views'] < 0) {
            $error = \XF::phrase('thtrending_minimum_views_must_be_positive_integer_or_zero');
            return false;
        }

        return true;
    }
}