<?php

namespace ThemeHouse\Trending\Trending;

use ThemeHouse\Trending\Entity\Trending;

/**
 * Class ResourceItem
 * @package ThemeHouse\Trending\Trending
 */
class ResourceItem extends AbstractHandler
{
    protected $contentType = 'resource';

    /**
     * @return bool
     */
    public function canUse()
    {
        $addOns = $this->app->registry()['addOns'];

        if (!empty($addOns['XFRM'])) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    protected function _getAlgorithmOptions()
    {
        return [
            'thtrending_positive_ratings_per_minute' => [
                'value' => 'thtrending_positive_ratings_per_minute',
                'label' => \XF::phrase('thtrending_positive_ratings_per_minute'),
            ],
        ];
    }

    /**
     * @return array
     */
    protected function _getCategoryOptions()
    {
        /** @var \XFRM\Repository\Category $categoryRepo */
        $categoryRepo = \XF::repository('XFRM:Category');

        $choices = $categoryRepo->getCategoryOptionsData(false);
        $choices = array_map(function($v) {
            $v['label'] = \XF::escapeString($v['label']);
            return $v;
        }, $choices);

        return $choices;
    }

    /**
     * @param Trending $trending
     *
     * @return \XFRM\Finder\ResourceItem
     */
    protected function _getTrendingContentFinder(Trending $trending)
    {
        $visitor = \XF::visitor();

        /** @var \XFRM\Finder\ResourceItem $finder */
        $finder = $this->app->finder('XFRM:ResourceItem');
        $finder->forFullView(true);
        $finder->with('Category.Permissions|' . $visitor->permission_combination_id);
        if ($trending->max_age_days) {
            $cutoff = \XF::$time - ($trending->max_age_days * 86400);
            $finder->where($trending->max_age_column, '>', $cutoff);
        }
        $finder->order($trending->algorithm, 'desc');

        if (!empty($trending->config['min_positive_ratings'])) {
            $finder->where('thtrending_positive_rating_count', '>=', $trending->config['min_positive_ratings']);
        }

        if (!empty($trending->category_ids)) {
            $finder->where('resource_category_id', '=', $trending->category_ids);
        }

        return $finder;
    }

    /**
     * @return array
     */
    protected function _getMaxAgeColumns()
    {
        return [
            'last_update' => [
                'value' => 'last_update',
                'label' => \XF::phrase('xfrm_last_update'),
            ],
            'resource_date' => [
                'value' => 'resource_date',
                'label' => \XF::phrase('xfrm_first_release'),
            ],
        ];
    }

    protected function _rebuildTrendingValues()
    {
        $db = $this->app->db();
        $db->query("
            UPDATE xf_rm_resource AS r
            SET r.thtrending_positive_ratings_per_minute = (r.thtrending_positive_rating_count / floor((? - r.resource_date) / 60))
        ", [
            \XF::$time
        ]);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderTrendingContent(Trending $trending)
    {
        $templater = $this->app->templater();

        $page = $this->filterPage();
        $threadsPerPage = (int) \XF::app()->options()->discussionsPerPage;

        $resourceFinder = $this->getTrendingContentFinder($trending);

        $resources = $resourceFinder->limitByPage($page, $threadsPerPage)->fetch();

        foreach ($resources as $resourceId => $resource) {
            /** @var \XFRM\Entity\ResourceItem $resource */

            if (!$resource->canView()) {
                unset($resources[$resourceId]);
            }
        }

        $viewParams = [
            'trending' => $trending,
            'resources' => $resources,
            'page' => $page,
            'perPage' => $threadsPerPage,
            'total' => $resourceFinder->total(),
        ];

        return $templater->renderTemplate('public:thtrending_trending_view_resource', $viewParams);
    }

    /**
     * @param Trending $trending
     * @param \ThemeHouse\Trending\Widget\Trending $widget
     * @return \XF\Widget\WidgetRenderer
     */
    protected function _renderTrendingWidget(Trending $trending, \ThemeHouse\Trending\Widget\Trending $widget)
    {
        $limit = $widget->getOptions()['limit'];

        $resourceFinder = $this->getTrendingContentFinder($trending);
        $resourceFinder->limit($limit);

        $resources = $resourceFinder->fetch();

        foreach ($resources as $resourceId => $resource) {
            /** @var \XFRM\Entity\ResourceItem $resource */

            if (!$resource->canView()) {
                unset($resources[$resourceId]);
            }
        }

        $viewParams = [
            'trending' => $trending,
            'resources' => $resources,
        ];
        return $widget->renderer('thtrending_trending_widget_resource', $viewParams);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderAdditionalConfig(Trending $trending)
    {
        $templater = $this->app->templater();

        $viewParams = [
            'trending' => $trending,
            'config' => $trending->config,
        ];

        return $templater->renderTemplate('admin:thtrending_edit_config_resource', $viewParams);
    }

    /**
     * @param array $config
     * @param $error
     * @return bool
     */
    protected function _validateAdditionalConfig(array &$config, &$error)
    {
        $config = [
            'min_positive_ratings' => $config['min_positive_ratings'] ?: 0,
        ];
        if ($config['min_positive_ratings'] < 0) {
            $error = \XF::phrase('thtrending_minium_positive_ratings_must_be_positive_integer_or_zero');
            return false;
        }

        return true;
    }
}