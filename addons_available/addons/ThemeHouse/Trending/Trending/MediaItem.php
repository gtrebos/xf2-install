<?php

namespace ThemeHouse\Trending\Trending;

use ThemeHouse\Trending\Entity\Trending;

/**
 * Class MediaItem
 * @package ThemeHouse\Trending\Trending
 */
class MediaItem extends AbstractHandler
{
    protected $contentType = 'xfmg_media';

    /**
     * @return bool
     */
    public function canUse()
    {
        $addOns = $this->app->registry()['addOns'];

        if (!empty($addOns['XFMG'])) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    protected function _getAlgorithmOptions()
    {
        return [
            'thtrending_positive_ratings_views_per_minute' => [
                'value' => 'thtrending_positive_ratings_views_per_minute',
                'label' => \XF::phrase('thtrending_positive_ratings_views_per_minute'),
            ],
            'thtrending_positive_ratings_per_minute' => [
                'value' => 'thtrending_positive_ratings_per_minute',
                'label' => \XF::phrase('thtrending_positive_ratings_per_minute'),
            ],
            'thtrending_views_per_minute' => [
                'value' => 'thtrending_views_per_minute',
                'label' => \XF::phrase('thtrending_views_per_minute'),
            ],
        ];
    }

    /**
     * @return array
     */
    protected function _getCategoryOptions()
    {
        /** @var \XFMG\Repository\Category $categoryRepo */
        $categoryRepo = \XF::repository('XFMG:Category');

        $choices = $categoryRepo->getCategoryOptionsData(false);
        $choices = array_map(function($v) {
            $v['label'] = \XF::escapeString($v['label']);
            return $v;
        }, $choices);

        return $choices;
    }

    /**
     * @param Trending $trending
     *
     * @return \XFMG\Finder\MediaItem
     */
    protected function _getTrendingContentFinder(Trending $trending)
    {
        $visitor = \XF::visitor();

        /** @var \XFMG\Finder\MediaItem $finder */
        $finder = $this->app->finder('XFMG:MediaItem');
        $finder->with('Category.Permissions|' . $visitor->permission_combination_id);
        if ($trending->max_age_days) {
            $cutoff = \XF::$time - ($trending->max_age_days * 86400);
            $finder->where($trending->max_age_column, '>', $cutoff);
        }
        $finder->order($trending->algorithm, 'desc');

        if (!empty($trending->config['min_positive_ratings'])) {
            $finder->where('thtrending_positive_rating_count', '>=', $trending->config['min_positive_ratings']);
        }

        if (!empty($trending->config['min_views'])) {
            $finder->where('view_count', '>=', $trending->config['min_views']);
        }

        if (!empty($trending->category_ids)) {
            $finder->where('category_id', '=', $trending->category_ids);
        }

        return $finder;
    }

    /**
     * @return array
     */
    protected function _getMaxAgeColumns()
    {
        return [
            'media_date' => [
                'value' => 'media_date',
                'label' => \XF::phrase('xfmg_date_added'),
            ],
        ];
    }

    protected function _rebuildTrendingValues()
    {
        $db = $this->app->db();
        $db->query("
            UPDATE xf_mg_media_item AS m
            SET m.thtrending_positive_ratings_per_minute = (m.thtrending_positive_rating_count / floor((? - m.media_date) / 60)),
              m.thtrending_views_per_minute = (m.view_count / floor((? - m.media_date) / 60)),
              m.thtrending_positive_ratings_views_per_minute = (m.thtrending_positive_ratings_per_minute + m.thtrending_views_per_minute)
        ", [
            \XF::$time,
            \XF::$time,
        ]);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderTrendingContent(Trending $trending)
    {
        $templater = $this->app->templater();

        $page = $this->filterPage();
        $threadsPerPage = (int) \XF::app()->options()->discussionsPerPage;

        $mediaFinder = $this->getTrendingContentFinder($trending);

        $mediaItems = $mediaFinder->limitByPage($page, $threadsPerPage)->fetch();

        foreach ($mediaItems as $mediaId => $media) {
            /** @var \XFMG\Entity\MediaItem $media */

            if (!$media->canView()) {
                unset($mediaItems[$mediaId]);
            }
        }

        $viewParams = [
            'trending' => $trending,
            'mediaItems' => $mediaItems,
            'page' => $page,
            'perPage' => $threadsPerPage,
            'total' => $mediaFinder->total(),
        ];

        return $templater->renderTemplate('public:thtrending_trending_view_xfmg_media', $viewParams);
    }

    /**
     * @param Trending $trending
     * @param \ThemeHouse\Trending\Widget\Trending $widget
     * @return \XF\Widget\WidgetRenderer
     */
    protected function _renderTrendingWidget(Trending $trending, \ThemeHouse\Trending\Widget\Trending $widget)
    {
        $limit = $widget->getOptions()['limit'];

        $mediaFinder = $this->getTrendingContentFinder($trending);
        $mediaFinder->limit($limit);

        $mediaItems = $mediaFinder->fetch();

        foreach ($mediaItems as $mediaId => $media) {
            /** @var \XFMG\Entity\MediaItem $media */

            if (!$media->canView()) {
                unset($mediaItems[$mediaId]);
            }
        }

        $viewParams = [
            'trending' => $trending,
            'mediaItems' => $mediaItems,
        ];
        return $widget->renderer('thtrending_trending_widget_xfmg_media', $viewParams);
    }

    /**
     * @param Trending $trending
     * @return string
     */
    protected function _renderAdditionalConfig(Trending $trending)
    {
        $templater = $this->app->templater();

        $viewParams = [
            'trending' => $trending,
            'config' => $trending->config,
        ];

        return $templater->renderTemplate('admin:thtrending_edit_config_xfmg_media', $viewParams);
    }

    /**
     * @param array $config
     * @param $error
     * @return bool
     */
    protected function _validateAdditionalConfig(array &$config, &$error)
    {
        $config = [
            'min_positive_ratings' => $config['min_positive_ratings'] ?: 0,
            'min_views' => $config['min_views'] ?: 0,
        ];
        if ($config['min_positive_ratings'] < 0) {
            $error = \XF::phrase('thtrending_minium_positive_ratings_must_be_positive_integer_or_zero');
            return false;
        }
        if ($config['min_views'] < 0) {
            $error = \XF::phrase('thtrending_minimum_views_must_be_positive_integer_or_zero');
            return false;
        }

        return true;
    }
}