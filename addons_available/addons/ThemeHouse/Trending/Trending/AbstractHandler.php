<?php

namespace ThemeHouse\Trending\Trending;

use ThemeHouse\Trending\Entity\Trending;
use XF\App;

/**
 * Class AbstractHandler
 * @package ThemeHouse\Trending\Trending
 */
abstract class AbstractHandler
{
    /**
     * @var App
     */
    protected $app;

    /**
     * @var Trending
     */
    protected $trending;

    protected $contentType;

    protected $categoryPhrase;

    /**
     * AbstractHandler constructor.
     * @param App $app
     * @param Trending|null $trending
     * @throws \Exception
     */
    public function __construct(App $app, Trending $trending = null)
    {
        $this->app = $app;
        $this->trending = $trending;

        $this->categoryPhrase = $this->getCategoryPhrase();

        if (!$this->contentType) {
            throw new \Exception('No content type specified');
        }
    }

    /**
     * @param bool $plural
     * @return \XF\Phrase
     */
    public function getCategoryPhrase($plural = false)
    {
        return $this->_getCategoryPhrase($plural);
    }

    /**
     * @param bool $plural
     * @return \XF\Phrase
     */
    public function getContentTypePhrase($plural = false)
    {
        return $this->app->getContentTypePhrase($this->contentType, $plural);
    }

    /**
     * @return mixed
     */
    public function getAlgorithmOptions()
    {
        return $this->_getAlgorithmOptions();
    }

    /**
     * @return mixed
     */
    public function getCategoryOptions()
    {
        return $this->_getCategoryOptions();
    }

    /**
     * @param Trending $trending
     * @return mixed
     */
    public function getTrendingContentFinder(Trending $trending)
    {
        return $this->_getTrendingContentFinder($trending);
    }

    /**
     * @param bool $keysOnly
     * @return array
     */
    public function getMaxAgeColumns($keysOnly=false)
    {
        $columns = $this->_getMaxAgeColumns();

        if ($keysOnly) {
            return array_keys($columns);
        }

        return $columns;
    }

    /**
     * @return mixed
     */
    public function rebuildTrendingValues()
    {
        return $this->_rebuildTrendingValues();
    }

    /**
     * @param Trending $trending
     * @return mixed
     */
    public function renderTrendingContent(Trending $trending)
    {
        return $this->_renderTrendingContent($trending);
    }

    /**
     * @param Trending $trending
     * @param \ThemeHouse\Trending\Widget\Trending $widget
     * @return mixed
     */
    public function renderTrendingWidget(Trending $trending, \ThemeHouse\Trending\Widget\Trending $widget)
    {
        return $this->_renderTrendingWidget($trending, $widget);
    }

    /**
     * @param Trending $trending
     * @return mixed
     */
    public function renderAdditionalConfig(Trending $trending)
    {
        return $this->_renderAdditionalConfig($trending);
    }

    /**
     * @param array $config
     * @param $error
     * @return mixed
     */
    public function validateAdditionalConfig(array &$config, &$error)
    {
        return $this->_validateAdditionalConfig($config, $error);
    }

    /**
     * @return array
     */
    protected function getEntityWith()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function canUse()
    {
        return true;
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function filterPage($page = 0)
    {
        $page = (int) $_GET['page'];
        return max(1, intval($page) ?: $page);
    }

    /**
     * @param bool $plural
     * @return \XF\Phrase
     */
    protected function _getCategoryPhrase($plural = false)
    {
        if ($plural) return \XF::phrase('categories');

        return \XF::phrase('category');
    }

    protected abstract function _getAlgorithmOptions();
    protected abstract function _getCategoryOptions();
    protected abstract function _getTrendingContentFinder(Trending $trending);
    protected abstract function _getMaxAgeColumns();
    protected abstract function _rebuildTrendingValues();
    protected abstract function _renderTrendingContent(Trending $trending);
    protected abstract function _renderTrendingWidget(Trending $trending, \ThemeHouse\Trending\Widget\Trending $widget);
    protected abstract function _renderAdditionalConfig(Trending $trending);
    protected abstract function _validateAdditionalConfig(array &$config, &$error);
}