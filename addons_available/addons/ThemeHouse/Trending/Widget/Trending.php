<?php

namespace ThemeHouse\Trending\Widget;

use XF\Widget\AbstractWidget;

/**
 * Class Trending
 * @package ThemeHouse\Trending\Widget
 */
class Trending extends AbstractWidget
{
    protected $defaultOptions = [
        'limit' => 5,
        'trending_id' => '',
    ];

    /**
     * @param $context
     * @return array
     */
    protected function getDefaultTemplateParams($context)
    {
        $params = parent::getDefaultTemplateParams($context);
        if ($context == 'options') {
            $trendingConfigs = $this->finder('ThemeHouse\Trending:Trending')->where('active', 1)->fetch();
            $params['trendingConfigs'] = $trendingConfigs;
        }
        return $params;
    }

    /**
     * @return bool|mixed
     * @throws \Exception
     */
    public function render()
    {
        /** @var \XFRM\XF\Entity\User $visitor */
//        $visitor = \XF::visitor();

        $options = $this->options;
//        $limit = $options['limit'];
        $trendingId = $options['trending_id'];

        /** @var \ThemeHouse\Trending\Entity\Trending $trending */
        $trending = $this->em()->find('ThemeHouse\Trending:Trending', $trendingId);
        if (!$trending) return false;

        return $trending->renderTrendingWidget($this);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }
}