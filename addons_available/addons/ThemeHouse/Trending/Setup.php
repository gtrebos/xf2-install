<?php

namespace ThemeHouse\Trending;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

/**
 * Class Setup
 * @package ThemeHouse\Trending
 */
class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->createTable('xf_th_trending', function (Create $table) {
            $table->addColumn('trending_id', 'integer')->autoIncrement()->primaryKey();
            $table->addColumn('trending_url_bit', 'varbinary', 50);
            $table->addColumn('title', 'varchar', 100);
            $table->addColumn('content_type', 'varbinary', 25);
            $table->addColumn('max_age_days', 'int')->setDefault(0);
            $table->addColumn('max_age_column', 'varbinary', 50);
            $table->addColumn('algorithm', 'varbinary', 50);
            $table->addColumn('category_ids', 'mediumblob')->nullable();
            $table->addColumn('config', 'mediumblob')->nullable();
            $table->addColumn('display_order', 'int')->setDefault(1);
            $table->addColumn('active', 'boolean')->setDefault(1);
            $table->addColumn('show_tab', 'boolean')->setDefault(1);
        });
    }

    public function installStep2()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_thread', function (Alter $table) {
            $table->addColumn('thtrending_replies_per_minute', 'decimal', '20,6')->setDefault(0);
            $table->addColumn('thtrending_views_per_minute', 'decimal', '20,6')->setDefault(0);
            $table->addColumn('thtrending_replies_views_per_minute', 'decimal', '20,6')->setDefault(0);
        });
    }

    public function installStep3()
    {
        $addOns = $this->app->registry()['addOns'];
        /** @var \ThemeHouse\Trending\Repository\Trending $repository */
        $repository = $this->app->repository('ThemeHouse\Trending:Trending');

        if (!empty($addOns['XFRM'])) {
            $repository->setupColumnsForAddon('XFRM');
        }

        if (!empty($addOns['XFMG'])) {
            $repository->setupColumnsForAddon('XFMG');
        }
    }

    public function upgrade1000132Step1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_th_trending', function (Alter $table) {
            $table->dropPrimaryKey();
            $table->renameColumn('trending_id', 'trending_url_bit');
        });

        $schemaManager->alterTable('xf_th_trending', function (Alter $table) {
            $table->addColumn('trending_id', 'integer')->autoIncrement();
        });
    }

    /**
     * @param array $stateChanges
     * @throws \XF\PrintableException
     */
    public function postInstall(array &$stateChanges)
    {
        $trending = \XF::em()->create('ThemeHouse\Trending:Trending');
        $trending->bulkSet([
            'trending_url_bit' => 'threads',
            'title' => 'Trending threads',
            'content_type' => 'thread',
            'max_age_days' => 30,
            'max_age_column' => 'last_post_date',
            'algorithm' => 'thtrending_replies_views_per_minute',
            'category_ids' => [],
            'config' => [
                'min_replies' => 0,
                'min_views' => 0,
            ],
            'display_order' => 1,
            'active' => 1,
            'show_tab' => 1,
        ]);
        $trending->save();
    }

    public function uninstallStep1()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->dropTable('xf_th_trending');
    }

    public function uninstallStep2()
    {
        $schemaManager = $this->schemaManager();

        $schemaManager->alterTable('xf_thread', function (Alter $table) {
            $table->dropColumns([
                'thtrending_replies_per_minute',
                'thtrending_views_per_minute',
                'thtrending_replies_views_per_minute'
            ]);
        });
    }

    public function uninstallStep3()
    {
        $schemaManager = $this->schemaManager();
        $addOns = $this->app->registry()['addOns'];

        if (!empty($addOns['XFRM'])) {
            $schemaManager->alterTable('xf_rm_resource', function (Alter $table) {
                $table->dropColumns([
                    'thtrending_positive_rating_count',
                    'thtrending_positive_ratings_per_minute'
                ]);
            });
        }

        if (!empty($addOns['XFMG'])) {
            $schemaManager->alterTable('xf_mg_media_item', function (Alter $table) {
                $table->dropColumns([
                    'thtrending_positive_rating_count',
                    'thtrending_views_per_minute',
                    'thtrending_positive_ratings_per_minute',
                    'thtrending_positive_ratings_views_per_minute'
                ]);
            });
        }
    }

}