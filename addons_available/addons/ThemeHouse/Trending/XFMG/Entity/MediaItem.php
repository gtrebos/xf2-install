<?php

namespace ThemeHouse\Trending\XFMG\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class MediaItem
 * @package ThemeHouse\Trending\XFMG\Entity
 */
class MediaItem extends XFCP_MediaItem
{
    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns['thtrending_positive_rating_count'] = [
            'type' => self::UINT,
            'default' => 0,
        ];
        $structure->columns['thtrending_positive_ratings_views_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];
        $structure->columns['thtrending_positive_ratings_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];
        $structure->columns['thtrending_views_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];

        return $structure;
    }

    public function rebuildRating()
    {
        parent::rebuildRating();

        $positiveRatings = $this->db()->fetchOne("
            SELECT COUNT(rating_id)
            FROM xf_mg_rating
            WHERE content_id=?
              AND content_type = 'xfmg_media'
              AND rating >= 4
          ", $this->media_id);

        $this->thtrending_positive_rating_count = $positiveRatings;
    }
}