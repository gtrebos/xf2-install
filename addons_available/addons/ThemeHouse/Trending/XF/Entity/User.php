<?php

namespace ThemeHouse\Trending\XF\Entity;

class User extends XFCP_User
{
    public function hasTrendingPermission($contentId, $permission)
    {
        return true;

        // Keep for eventual future usage
//        $contentPermission = $this->hasContentPermission('thtrending', $contentId, $permission);
//
//        switch($contentPermission) {
//            case 'unset':
//            case 'content_allow':
//                return true;
//
//            case 'deny':
//            case 'reset':
//            default:
//                return false;
//        }
    }
}