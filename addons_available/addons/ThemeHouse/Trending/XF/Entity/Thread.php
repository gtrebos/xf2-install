<?php

namespace ThemeHouse\Trending\XF\Entity;

use XF\Mvc\Entity\Structure;

/**
 * Class Thread
 * @package ThemeHouse\Trending\XF\Entity
 */
class Thread extends XFCP_Thread
{
    /**
     * @param Structure $structure
     * @return Structure
     */
    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns['thtrending_replies_views_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];
        $structure->columns['thtrending_replies_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];
        $structure->columns['thtrending_views_per_minute'] = [
            'type' => self::FLOAT,
            'default' => 0,
        ];

        return $structure;
    }
}