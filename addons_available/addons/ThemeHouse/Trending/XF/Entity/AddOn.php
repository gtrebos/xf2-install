<?php

namespace ThemeHouse\Trending\XF\Entity;

/**
 * Class AddOn
 * @package ThemeHouse\Trending\XF\Entity
 */
class AddOn extends XFCP_AddOn
{
    protected function _postSave()
    {
        parent::_postSave();

        if ($this->isInsert()) {
            \XF::app()->jobManager()->enqueueUnique("thtrending-{$this->addon_id}", 'ThemeHouse\Trending:BuildColumns', [
                'addon_id' => $this->addon_id
            ], false);
        }
    }
}