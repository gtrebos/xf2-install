<?php

namespace ThemeHouse\Trending\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\View;

class Forum extends XFCP_Forum
{
    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Reroute|View
     */
    public function actionIndex(ParameterBag $params)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        if ($params->node_id || $params->node_name) {
            return $this->rerouteController('XF:Forum', 'Forum', $params);
        }

        if ($this->responseType == 'rss') {
            return $this->getForumRss();
        }

        switch ($this->options()->forumsDefaultPage) {
            case 'thtrending_trending_list':
                return $this->rerouteController('ThemeHouse\Trending:Trending', 'index');
                break;
            default:
                return parent::actionIndex($params);
        }
    }

}