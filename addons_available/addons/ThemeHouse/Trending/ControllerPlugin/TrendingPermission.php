<?php

namespace ThemeHouse\Trending\ControllerPlugin;

use XF\ControllerPlugin\AbstractPermission;
use XF\Mvc\ParameterBag;

class TrendingPermission extends AbstractPermission
{
	protected $viewFormatter = 'ThemeHouse\Trending:Trending\Permission';
	protected $templateFormatter = 'permission_trending';
	protected $routePrefix = 'trending/permissions';
	protected $contentType = 'thtrending';
	protected $entityIdentifier = 'ThemeHouse\Trending:Trending';
	protected $primaryKey = 'trending_id';
	protected $privatePermissionGroupId = 'general';
	protected $privatePermissionId = 'viewTHTrending';
}