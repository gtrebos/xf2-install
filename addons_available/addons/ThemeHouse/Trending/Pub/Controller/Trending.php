<?php

namespace ThemeHouse\Trending\Pub\Controller;

use XF\Finder\Thread;
use XF\Mvc\ParameterBag;
use XF\Pub\Controller\AbstractController;

/**
 * Class Trending
 * @package ThemeHouse\Trending\Pub\Controller
 */
class Trending extends AbstractController
{
    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionIndex(ParameterBag $params)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        if ($params->trending_id) {
            /** @noinspection PhpUndefinedFieldInspection */
            $trending = $this->assertTrendingExistsAndViewable($params->trending_id);
        } else {
            $trendingRepo = $this->getTrendingRepo();
            $trendingConfigs = $trendingRepo->getTrendingConfigsForListing();
            $trending = $trendingConfigs->first();
        }

        if (!$trending) {
            return $this->notFound();
        }

        return $this->renderTrending($trending);
    }

    protected function adjustContext(\ThemeHouse\Trending\Entity\Trending $trending) {
        switch($trending->content_type) {
            case 'thread':
                $this->setSectionContext('forums');
                break;

            case 'resource':
                $this->setSectionContext('xfrm');
                break;

            case 'media':
                $this->setSectionContext('xfmg');
                break;
        }
    }

    /**
     * @param \ThemeHouse\Trending\Entity\Trending $trending
     * @return \XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     * @throws \Exception
     */
    protected function renderTrending(\ThemeHouse\Trending\Entity\Trending $trending)
    {
        if (!$trending->getHandler()->canUse()) {
            return $this->notFound();
        }
        $this->assertCanonicalUrl($this->buildLink('trending', $trending));

        $trendingRepo = $this->getTrendingRepo();
        $trendingConfigs = $trendingRepo->getTrendingConfigsForListing();

        $this->adjustContext($trending);

        $viewParams = [
            'trending' => $trending,
            'trendingConfigs' => $trendingConfigs,
        ];
        return $this->view('ThemeHouse\Trending:Trending\View', 'thtrending_trending_view', $viewParams);
    }

    protected function getAvailableDateLimits()
    {
        return [-1, 7, 14, 30, 60, 90, 182, 365];
    }

    protected function getFilterInput()
    {
        $filters = [];

        $input = \XF::app()->request()->filter([
            'prefix_id' => 'uint',
            'starter' => 'str',
            'starter_id' => 'uint',
            'last_days' => 'int',
            'order' => 'str',
            'direction' => 'str',
            'no_date_limit' => 'bool'
        ]);

        if ($input['no_date_limit']) {
            $filters['no_date_limit'] = $input['no_date_limit'];
        }

        if ($input['prefix_id']) {
            $filters['prefix_id'] = $input['prefix_id'];
        }

        if ($input['starter_id']) {
            $filters['starter_id'] = $input['starter_id'];
        } else {
            if ($input['starter']) {
                $user = $this->em()->findOne('XF:User', ['username' => $input['starter']]);
                if ($user) {
                    $filters['starter_id'] = $user->user_id;
                }
            }
        }

        if (in_array($input['last_days'], $this->getAvailableDateLimits())) {
            $filters['last_days'] = $input['last_days'];
        }

        return $filters;
    }

    /**
     * @param ParameterBag $params
     * @return \XF\Mvc\Reply\Redirect|\XF\Mvc\Reply\View
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionFilters(ParameterBag $params)
    {
        $trending = $this->assertTrendingExists($params->trending_id);

        $filters = $this->getFilterInput();

        if ($this->filter('apply', 'bool'))
        {
            if (!empty($filters['last_days']))
            {
                unset($filters['no_date_limit']);
            }
            return $this->redirect($this->buildLink('trending', $trending, $filters));
        }

        if (!empty($filters['starter_id']))
        {
            $starterFilter = $this->em()->find('XF:User', $filters['starter_id']);
        }
        else
        {
            $starterFilter = null;
        }


        $prefixes = $this->finder('XF:ThreadPrefix')
            ->order('materialized_order')
            ->fetch();

        $viewParams = [
            'trending' => $trending,
            'prefixes' => $prefixes->groupBy('prefix_group_id'),
            'filters' => $filters,
            'starterFilter' => $starterFilter
        ];
        return $this->view('ThemeHouse\Trending:Trending\Filters', 'thtrending_trending_filter', $viewParams);
    }

    /**
     * @param $id
     * @param null $with
     * @param null $phraseKey
     *
     * @return \ThemeHouse\Trending\Entity\Trending
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function assertTrendingExists($id, $with = null, $phraseKey = null)
    {
        /** @var \ThemeHouse\Trending\Entity\Trending $trending */
        $trending = $this->assertRecordExists('ThemeHouse\Trending:Trending', $id, $with, $phraseKey);

        if (!$trending->active) {
            throw $this->exception($this->notFound());
        }

        return $trending;
    }

    /**
     * @param $id
     * @param null $with
     * @param null $phraseKey
     * @return \ThemeHouse\Trending\Entity\Trending
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function assertTrendingExistsAndViewable($id, $with = null, $phraseKey = null) {
        $trending = $this->assertTrendingExists($id, $with, $phraseKey);

        /** @noinspection PhpUndefinedMethodInspection */
        if(!\XF::visitor()->hasTrendingPermission($trending->trending_id, 'view')) {
            throw $this->exception($this->noPermission());
        }

        return $trending;
    }

    /**
     * @return \ThemeHouse\Trending\Repository\Trending
     */
    protected function getTrendingRepo()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repository('ThemeHouse\Trending:Trending');
    }
}