<?php

namespace ThemeHouse\UserImprovements\Listener\TemplaterTemplatePreRender\Pub;

use XF\Template\Templater;

class MemberTrophies
{
    public static function templaterTemplatePreRender(Templater $templater, &$type, &$template, array &$params)
    {
        $trophies = $params['trophies'];
        $categorizedTrophies = ['uncategorized' => []];

        foreach ($trophies as $value) {
            $follId = $value->Trophy->th_follower;
            $user = $params['user']->user_id;
            if (!$follId || !isset($trophies["{$user}-{$follId}"])) {
                if ($value->Trophy->th_trophy_category_id === '') {
                    $categorizedTrophies['uncategorized'][] = $value;
                } else {
                    $categorizedTrophies[$value->Trophy->th_trophy_category_id][] = $value;
                }
            }
        }

        $params['trophies'] = $categorizedTrophies;
        $params['trophyCategories'] = \XF::app()->em()->getFinder('ThemeHouse\UserImprovements:TrophyCategory')->order('display_order')->fetch();
    }
}
