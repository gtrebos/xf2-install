<?php

namespace ThemeHouse\UserImprovements\XF\Pub\Controller;

use \XF\Mvc\ParameterBag;
use \XF\Mvc\Reply\View;

class Member extends XFCP_Member
{
    /**
     * @param ParameterBag $params
     * @return View
     */
    public function actionView(ParameterBag $params)
    {
        $return = parent::actionView($params);

        if ($return instanceof View) {
            $changeRecords = $this->em()->getFinder('ThemeHouse\UserImprovements:UsernameChange')->
            order('change_date', 'DESC')->where('user_id', $params->user_id)->fetch(10);

            $return->setParam('username_changes', $changeRecords);

            if ($this->app->options()->klUiProfileViews) {
                /** @var \ThemeHouse\UserImprovements\Repository\User $repo */
                $repo = $this->repository('ThemeHouse\UserImprovements:User');
                $repo->logProfileView($return->getParam('user'));
            }
        }

        return $return;
    }

    /**
     * @return \XF\Mvc\Reply\Redirect|View
     */
    public function actionTrophiesShowcaseSelect()
    {
        /** @var \ThemeHouse\UserImprovements\Entity\User $visitor */
        $visitor = \XF::visitor();
        $options = \XF::app()->options();
        $trophies = $this->finder('XF:UserTrophy')
            ->where('user_id', $visitor->user_id)
            ->with('Trophy', true)
            ->order('Trophy.trophy_points')
            ->fetch();

        if ($options->klUIProfileTrophyShowcase != 3) {
            return $this->noPermission();
        }

        if (!$visitor->getTrophyShowcaseSize()) {
            return $this->noPermission();
        }

        if ($this->isPost()) {
            $trophyIds = $this->filter('trophy_ids', 'array-int');
            $trophyIds = array_slice($trophyIds, 0, 5);

            foreach ($trophies as $trophy) {
                $trophy->th_showcased = in_array($trophy->trophy_id, $trophyIds);
                $trophy->saveIfChanged();
            }

            return $this->redirect($this->buildLink('members', $visitor));
        } else {
            $group = $trophies->groupBy('th_showcased');

            if (isset($group[1])) {
                $selected = count($group[1]);
            } else {
                $selected = 0;
            }

            $viewParams = [
                'amountSelected' => $selected,
                'amount' => $visitor->getTrophyShowcaseSize(),
                'trophies' => $trophies
            ];

            return $this->view('ThemeHouse\UserImprovements:TrophyShowcase', 'thuserimprovements_trophy_showcase_select', $viewParams);
        }
    }
}
