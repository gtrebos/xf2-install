<?php

namespace ThemeHouse\UserImprovements\XF\Entity;

use XF\Mvc\Entity\Structure;

/**
 * @property int th_trophy_category_id
 * @property string th_icon_type
 * @property string th_icon_value
 * @property bool th_hidden
 * @property int th_predecessor
 * @property int th_follower
 */
class Trophy extends XFCP_Trophy
{
    /**
     * @throws \Exception
     * @throws \XF\PrintableException
     */
    protected function _postSave()
    {
        parent::_postSave();

        /* Reset predecessor */

        /** @var \ThemeHouse\UserImprovements\Entity\Trophy $trophy */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('th_follower', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->th_follower = 0;
            $trophy->save();
        }

        /* Add trophy to predecessor */
        if ($this->th_predecessor) {
            $trophy = \XF::app()->em()->getFinder('XF:Trophy')
                ->where('trophy_id', $this->th_predecessor)
                ->fetchOne();
            $trophy->th_follower = $this->trophy_id;
            $trophy->save();
        }
    }

    /**
     * @throws \Exception
     * @throws \XF\PrintableException
     */
    protected function _preDelete()
    {
        parent::_preDelete();

        /* Remove trophy from predecessor */
        /** @var \ThemeHouse\UserImprovements\Entity\Trophy $trophy */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('th_follower', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->th_follower = 0;
            $trophy->save();
        }


        /* Remove Trophy from follower */
        $trophy = \XF::app()->em()->getFinder('XF:Trophy')
            ->where('th_predecessor', $this->trophy_id)
            ->fetchOne();

        if ($trophy) {
            $trophy->th_predecessor = 0;
            $trophy->save();
        }
    }

    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns = array_merge($structure->columns, [
            'th_trophy_category_id' => ['type' => self::STR, 'default' => '', 'maxLength' => 50],
            'th_icon_type' => ['type' => self::STR, 'default' => '', 'maxLength' => 25],
            'th_icon_value' => ['type' => self::STR, 'default' => '', 'maxLength' => 100],
            'th_hidden' => ['type' => self::UINT, 'default' => 0],
            'th_predecessor' => ['type' => self::UINT, 'default' => 0],
            'th_follower' => ['type' => self::UINT, 'default' => 0],
            'th_icon_css' => ['type' => self::STR, 'default' => '']
        ]);

        return $structure;
    }
}
