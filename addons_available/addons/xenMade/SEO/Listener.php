<?php

namespace xenMade\SEO;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;

class Listener
{
    public static function entity_structur(\XF\Mvc\Entity\Manager $em, \XF\Mvc\Entity\Structure &$structure)
    {
        switch($structure->table)
        {
            case 'xf_category':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;

            case 'xf_forum':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;

            case 'xf_thread':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;

            case 'xf_thread_prefix':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;

            case 'xf_attachment_data':
                $structure->columns['seo_title'] = ['type' => Entity::STR, 'nullable' => true, 'default' => null];
                $structure->columns['seo_alt'] = ['type' => Entity::STR, 'nullable' => true, 'default' => null];
                $structure->columns['seo_description'] = ['type' => Entity::STR, 'nullable' => true, 'default' => null];
                break;

            case 'xf_rm_category':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;

            case 'xf_rm_resource':
                $structure->columns['seo'] = ['type' => Entity::SERIALIZED_ARRAY, 'default' => []];
                break;
        }
    }

    public static function templater_setup(\XF\Container $container, \XF\Template\Templater &$templater)
    {
        $templater->addFunction('seo_image_data', function(\XF\Template\Templater $templater, &$escape, $attachment, $src = '', $alt = '')
        {
            if(!empty(\XF::options()->seo_image_attachments['internal']))
            {
                /** @var $imageRepo \xenMade\SEO\Repository\Image */
                $imageRepo = \XF::app()->repository('xenMade\SEO:Image');
                return $imageRepo->getImageSeoData($attachment, $src);
            }

            return $alt;
        });

        $templater->addFunction('seocanonical', function(\XF\Template\Templater $templater, &$escape, $data = '', $original = '')
        {
            if(!empty($data->seo['canonical']))
            {
                return $data->seo['canonical'];
            }

            return $original;
        });

        $templater->addFunction('change_thread_title', function(\XF\Template\Templater $templater, &$escape, $thread = '', $page = '')
        {
            $page = intval($page);

            $pageAppend = '';
            if ($page > 1)
            {
                $pageAppend = \XF::app()->language()->phrase('seo_title_page_x', ['page' => $page]);
            }

            if($thread instanceof \XF\Entity\Thread)
            {

                if(!empty($thread->seo['meta_title']))
                {
                    return $templater->escape($thread->seo['meta_title']);
                }

                if (isset($thread->title) && strlen($thread->title))
                {
                    $forumTitle = '';
                    if(!empty($thread->Forum->title))
                    {
                        $forumTitle = $thread->Forum->title;
                    }

                    $nodes = [];
                    if(!empty($thread->Forum->Node))
                    {
                        $node = $thread->Forum->Node;
                        $nodeTypes = \XF::app()->container('nodeTypes');

                        if ($node->breadcrumb_data)
                        {
                            foreach ($node->breadcrumb_data AS $crumb)
                            {
                                if (!isset($nodeTypes[$crumb['node_type_id']]))
                                {
                                    continue;
                                }

                                $nodes[] = $crumb['title'];
                            }
                        }
                    }

                    $prefixTitle = $templater->fnPrefix($templater, $escape, 'thread', $thread,'plain', '');


                    $newThreadTitle = $thread->title . ' | ' . \XF::options()->boardTitle;
                    $newThreadTitleFormat = \XF::options()->seo_threadTitleFormat;

                    if(!empty($newThreadTitleFormat))
                    {
                        $seperator = \XF::options()->seo_threadTitleSeperator ;
                        $newThreadTitleFormat = preg_replace('/\s+/', '', $newThreadTitleFormat);

                        $titleSegements = @explode('%', $newThreadTitleFormat);

                        if($titleSegements)
                        {
                            $segementArray = [
                                'thread_title' => $thread->title,
                                'prefix_title' => $prefixTitle,
                                'forum_title' => $forumTitle,
                                'board_title' => \XF::options()->boardTitle,
                                'page_no' => $pageAppend,
                                'all_parent_nodes' => ($nodes ? implode(" $seperator ", $nodes) : ''),
                            ];

                            //CustomFields

                            $customFiels = [];

                            if($thread->custom_fields)
                            {
                                foreach ($thread->custom_fields as $key => $value)
                                {
                                    if(empty($value) && \XF::options()->seo_emptyCustomField)
                                        continue;

                                    $customFiels['field_' . $key] = $value;
                                }
                            }

                            $segementArray = array_merge($segementArray, $customFiels);

                            $titleSegements = str_replace(array_keys($segementArray), array_values($segementArray), $titleSegements);
                            $titleSegements = array_filter($titleSegements);

                            $newThreadTitle = implode(" $seperator ", $titleSegements);
                        }
                    }

                    return $templater->escape($newThreadTitle, $escape);
                }
            }

            return sprintf('%s | %s',
                $templater->escape($thread->title . $pageAppend, $escape),
                $templater->escape(\XF::options()->boardTitle, $escape)
            );
        });
    }

    public static function templater_template_post_render(\XF\Template\Templater $templater, $type, $template, &$output)
    {
        //$controllerName = \XF::app()->router()->routeToController(\XF::app()->request()->getRoutePath())->getController();

        $visitor = \XF::visitor();

        if(!$visitor->user_id && \XF::options()->seo_removeUserLinks)
        {
            $output = preg_replace_callback('#<a([^>]+class="username(.*?)")([^>]*)>(.*?)<\/a>#is', array('self', 'removeUsernameLink'), $output);
            $output = preg_replace_callback('#<a([^>]+class="avatar(.*?)")([^>]*)>(.*?)<\/a>#is', array('self', 'removeAvatarLink'), $output);
        }

        if(\XF::options()->seo_minifyHtml)
        {
            $output = self::compressOutput($output);
        }
    }

    public static function ttprForumView(\XF\Template\Templater $templater, &$type, &$template, array &$params)
    {
        if(!empty($params['forum']['seo']['meta_title']))
        {
            $templater->pageParams['pageH1'] = $params['forum']['title'];
        }
    }

    public static function ttprCategoryView(\XF\Template\Templater $templater, &$type, &$template, array &$params)
    {
        if(!empty($params['category']['seo']['meta_title']))
        {
            $templater->pageParams['pageH1'] = $params['category']['title'];
        }
    }

    public static function templater_macro_pre_render(\XF\Template\Templater $templater, &$type, &$template, &$name, array &$arguments, array &$globalVars)
    {
        if ($arguments['group']->group_id == 'seo_settings')
        {
            $template = 'seo_macros_seo_settings';
        }
    }

    public static function entity_pre_save(\XF\Mvc\Entity\Entity $entity)
    {
        // FIX FOR NULLTABLE :(
        if (isset($entity->seo) && $entity->seo === null)
        {
            $entity->seo = [];
        }

        return $entity;
    }

    public static function app_pub_start_begin(\XF\Pub\App $app)
    {
        if(\XF::options()->seo_googleNews)
        {
            $request = $app->request();
            $extendedUrl = ltrim($request->getExtendedUrl(), '/');

            if ($extendedUrl == 'googlenews.xml')
            {
                $response = $app->response();

                $googleNews = new \xenMade\SEO\Sitemap\GoogleNews($app);
                $googleNews->buildSitemap($response, $request);
                exit;
            }
        }
    }

    protected static function removeUsernameLink($match)
    {
        $itemprop = '';
        if(strpos($match[3], 'itemprop') !== false)
        {
            $itemprop = ' itemprop="name"';
        }

        $match[2] = trim($match[2]);

        return '<span class="username' . $match[2] . '"' . $itemprop . '>' . $match[4] . '</span>';
    }

    protected static function removeAvatarLink($match)
    {
        $html = $match[0];

        $style = preg_match('#(style=".+?")#ims', $html, $styleMatch);
        $style = (!empty($styleMatch[1]) ? 'style="' . $styleMatch[1] . '"' : '');

        return '<span class="avatar ' . $match[2] . ' ' . $style . '">' . $match[4] . '</span>';
    }

    protected static function compressOutput($buffer)
    {

        //https://gist.github.com/dr-dimitru/9317130
        $replace = '%
                    (?>            
                    [^\S ]\s*    
                    | \s{2,}       
                    ) 
                    (?=           
                    [^<]*+    
                    (?:           
                    <         
                    (?!/?(?:textarea|pre|script)\b)
                    [^<]*+   
                    )*+          
                    (?:          
                    <           
                    (?>textarea|pre|script)\b
                            | \z 
                    )
                    )
                    %Six';

        $buffer = preg_replace($replace, ' ', $buffer);

        return $buffer;
    }

    public static function getCustomFieldTitle($title, $thread)
    {
        $regEx = '#\{field_(.*?)\}#is';
        preg_match_all($regEx, $title, $match);

        foreach ($match[0] as $key => $fieldId)
        {
            $fieldId = $match[1][$key];
            $replace = (isset($thread->custom_fields[$fieldId]) ? $thread->custom_fields[$fieldId] : '');

            $title = str_replace($match[0], $replace, $title);
        }

        return $title;
    }
}