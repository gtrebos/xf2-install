<?php

namespace xenMade\SEO\XF\Repository;

use XF\Mvc\Entity\Repository;

class Thread extends XFCP_Thread
{
    public function countWordsOnFirstPage(\XF\Entity\Thread $thread, $posts, $page = 0)
    {
        /* Veraltet da ich es im Thread direkt mache
        $postRepo = $this->getPostRepo();

        if($page)
        {
            $postList = $postRepo->findPostsForThreadView($thread)->onPage($page, \XF::options()->messagesPerPage);
            $posts = $postList->fetch();
        }
        */

        $words = 0;
        if($posts)
        {
            foreach($posts as $post)
            {
                $message = preg_replace('#\[[^\]]+\]#', '', $post->message);
                $words += str_word_count($message, 0);
            }
        }

        return $words;
    }

    public function countWordsOnAllPages(\XF\Entity\Thread $thread)
    {
        $db = \XF::db();

        $words = $db->fetchOne('
                                        SELECT 
                                            SUM(LENGTH(message) - LENGTH(REPLACE(message, \' \', \'\')) + 1) as words 
                                        FROM xf_post 
                                            WHERE thread_id = ' . $thread->thread_id
        );

        return $words;
    }

    public function countAttachmentsFirstPost(\XF\Entity\Thread $thread)
    {
        return $thread->FirstPost->attach_count;
    }

    public function countAttachmentsAllPost(\XF\Entity\Thread $thread)
    {
        $db = \XF::db();

        return (int)$db->fetchOne('SELECT SUM(attach_count) 
                                   FROM xf_thread as thread 
                                   LEFT JOIN xf_post as post USING(thread_id) 
                                       WHERE discussion_state=\'visible\' 
                                        AND thread_id = ?', $thread->thread_id);
    }

    public function changeThreadTitle($title)
    {
        if(\XF::options()->seo_lowerCaseThreadTitle)
        {
            if(\XF::options()->seo_lowerCaseThreadTitle == 'all_lowercase')
            {
                $title = utf8_strtolower($title);
            }
            elseif(\XF::options()->seo_lowerCaseThreadTitle == 'except_first_letter')
            {
                $title = utf8_ucfirst(utf8_strtolower($title));
            }
        }

        return $title;
    }
}
if(false)
{
    class XFCP_Thread extends \XF\Repository\Thread {};
}