<?php

namespace xenMade\SEO\XF\Entity;

class Forum extends XFCP_Forum
{
    public function canSetMetaTitle(&$error = null)
    {
        $visitor = \XF::visitor();
        return $visitor->hasNodePermission($this->node_id, 'canSetMetaTitle');
    }

    public function canSetMetaDesc(&$error = null)
    {
        $visitor = \XF::visitor();
        return $visitor->hasNodePermission($this->node_id, 'canSetMetaDesc');
    }

    public function canSetMetaRobot(&$error = null)
    {
        $visitor = \XF::visitor();
        return $visitor->hasNodePermission($this->node_id, 'canSetMetaRobot');
    }
}
if(false)
{
    class XFCP_Forum extends \XF\Entity\Forum {};
}