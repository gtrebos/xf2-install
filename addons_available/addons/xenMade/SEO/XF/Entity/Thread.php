<?php

namespace xenMade\SEO\XF\Entity;

class Thread extends XFCP_Thread
{
    public function canSetMetaTitle(&$error = null)
    {
        if($this->thread_id)
        {
            if($this->checkOwnThread($error))
            {
                $visitor = \XF::visitor();
                return $visitor->hasNodePermission($this->node_id, 'canSetMetaTitle');
            }
        }
        else
        {
            $forum = $this->Forum;
            return $forum ? $forum->canSetMetaTitle($this, $error) : false;
        }

        return false;
    }

    public function canSetMetaDesc(&$error = null)
    {
        if($this->thread_id)
        {
            if($this->checkOwnThread($error))
            {
                $visitor = \XF::visitor();
                return $visitor->hasNodePermission($this->node_id, 'canSetMetaDesc');
            }
        }
        else
        {
            $forum = $this->Forum;
            return $forum ? $forum->canSetMetaDesc($this, $error) : false;
        }

        return false;
    }

    public function canSetMetaRobot(&$error = null)
    {
        if($this->thread_id)
        {
            if($this->checkOwnThread($error))
            {
                $visitor = \XF::visitor();
                return $visitor->hasNodePermission($this->node_id, 'canSetMetaRobot');
            }
        }
        else
        {
            $forum = $this->Forum;
            return $forum ? $forum->canSetMetaRobot($this, $error) : false;
        }

        return false;
    }


    protected function checkOwnThread(&$error)
    {
        $visitor = \XF::visitor();
        if (!$visitor->user_id)
        {
            return false;
        }

        $nodeId = $this->node_id;

        if (!$this->discussion_open && !$this->canLockUnlock())
        {
            $error = \XF::phraseDeferred('you_may_not_perform_this_action_because_discussion_is_closed');
            return false;
        }

        if ($this->user_id == $visitor->user_id && $visitor->hasNodePermission($nodeId, 'editOwnPost'))
        {
            $editLimit = $visitor->hasNodePermission($nodeId, 'editOwnPostTimeLimit');
            if ($editLimit != -1 && (!$editLimit || $this->post_date < \XF::$time - 60 * $editLimit))
            {
                $error = \XF::phraseDeferred('message_edit_time_limit_expired', ['minutes' => $editLimit]);
                return false;
            }

            if (!$this->Forum || !$this->Forum->allow_posting)
            {
                $error = \XF::phraseDeferred('you_may_not_perform_this_action_because_forum_does_not_allow_posting');
                return false;
            }

            return $visitor->hasNodePermission($nodeId, 'editOwnThreadTitle');
        }

        return false;
    }
}
if(false)
{
    class XFCP_Thread extends \XF\Entity\Thread {};
}