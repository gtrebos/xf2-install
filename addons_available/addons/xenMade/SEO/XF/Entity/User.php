<?php

namespace xenMade\SEO\XF\Entity;

use XF\Mvc\Entity\Structure;

class User extends XFCP_User
{
    public function canSetAltTitle(&$error = null)
    {
        return (
            $this->user_id &&
            $this->app()->options()->seo_image_attachments['internal'] &&
            $this->hasPermission('seo', 'canSetAltTitle')
        );
    }

    public function canSetAttachmentDesc(&$error = null)
    {
        return (
            $this->user_id &&
            $this->hasPermission('seo', 'canSetAttachmentDesc')
        );
    }

    public function canSetSeo(&$error = null)
    {
        return (
            $this->user_id &&
            $this->hasPermission('seo', 'canSetSeo')
        );
    }
}
if(false)
{
    class XFCP_User extends \XF\Entity\User {};
}