<?php

namespace xenMade\SEO\XF\Entity;

use XF\Mvc\Entity\Structure;

class Node extends XFCP_Node
{
    public function getSeoOgImagePath($seo, $mode = 'node')
    {
        if(empty($seo))
            return false;

        if(empty($seo['ogimage_link']) && empty($seo['ogimage_upload']))
            return false;

        if(!empty($seo['ogimage_link']))
        {
            return $seo['ogimage_link'];
        }

        if(!empty($seo['ogimage_upload']))
        {
            switch ($mode)
            {
                case 'node':
                    return sprintf('data://seo_node_images/%s', $seo['ogimage_upload']);
                    break;

                case 'thread':
                    return sprintf('data://seo_thread_images/%s', $seo['ogimage_upload']);
                    break;
            }
        }

        return false;
    }

    public function getSeoOgImageUrl($seo, $mode = 'node')
    {
        $app = $this->app();

        if(empty($seo))
            return false;

        if(!empty($seo['ogimage_attachment']) && (empty($seo['ogimage_upload']) && $mode == 'thread'))
        {
            return $seo['ogimage_attachment'];
        }

        if(empty($seo['ogimage_link']) && empty($seo['ogimage_upload']))
            return false;

        if(!empty($seo['ogimage_link']))
        {
            return $seo['ogimage_link'];
        }

        if(!empty($seo['ogimage_upload']))
        {
            switch ($mode)
            {
                case 'node':
                    return $app->applyExternalDataUrl(sprintf('seo_node_images/%s', $seo['ogimage_upload']), true);
                    break;

                case 'thread':
                    return $app->applyExternalDataUrl(sprintf('seo_thread_images/%s', $seo['ogimage_upload']), true);
                    break;
            }
        }

        return false;
    }
}
if(false)
{
    class XFCP_Node extends \XF\Entity\Node {};
}