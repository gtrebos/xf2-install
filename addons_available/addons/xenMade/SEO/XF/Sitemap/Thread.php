<?php

namespace xenMade\SEO\XF\Sitemap;

class Thread extends XFCP_Thread
{
	public function isIncluded($record)
	{
        $forum = $record->Forum;

        if(empty($forum) || empty($record))
            return parent::isIncluded($record);

        $posts = [];

        if(!empty($forum->seo['words']))
        {
            $postList = $this->getPostRepo()->findPostsForThreadView($record)->onPage(1, \XF::options()->messagesPerPage);
            $posts = $postList->fetch();
        }

        list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex($forum, $record, $posts);

        if(strpos($metaNoIndex, 'noindex') !== false)
            return false;

	    return parent::isIncluded($record);
	}

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return \XF::repository('xenMade\SEO:NoIndex');
    }


    /**
     * @return \XF\Repository\Post
     */
    protected function getPostRepo()
    {
        return \XF::repository('XF:Post');
    }

}
if(false)
{
    class XFCP_Thread extends \XF\Sitemap\Thread {}
}