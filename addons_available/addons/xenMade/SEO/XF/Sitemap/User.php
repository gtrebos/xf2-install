<?php

namespace xenMade\SEO\XF\Sitemap;

class User extends XFCP_User
{
    public function isIncluded($record)
    {
        $options = \XF::options();

        if($options->seo_robotsProfile['robot_index'] == 'noindex')
        {
            return false;
        }

        return parent::isIncluded($record);
    }
}
if(false)
{
    class XFCP_User extends \XF\Sitemap\User {}
}