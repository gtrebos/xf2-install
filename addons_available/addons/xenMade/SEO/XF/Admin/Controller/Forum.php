<?php

namespace xenMade\SEO\XF\Admin\Controller;

use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;

class Forum extends XFCP_Forum
{
    protected function saveTypeData(FormAction $form, \XF\Entity\Node $node, \XF\Entity\AbstractNode $data)
    {
        $res = parent::saveTypeData($form, $node, $data);

        $seo = $this->filter('seo', 'array');

        $deleteImage = $this->filter('delete_ogimage', 'bool');
        $ogImageExists = $this->filter('ogimage_exists', 'str');

        $newFileName = null;
        $ogImage = false;

        // Zero Feld check
        if(!empty($seo['zero_post']))
        {
            if($seo['zero_post_days'] == 0)
            {
                $node->error(\XF::phrase('seo_the_option_noIndex_for_threads_with_zero_replys_after_x_days_must_not_be_0'), 'zero_post_days');
            }
        }

        /** @var \xenMade\SEO\Service\OgImage $ogService */
        $ogService = $this->service('xenMade\SEO:OgImage', $node, null, 'node');

        $upload = $this->request->getFile('ogimage_upload', false, false);
        if ($upload)
        {
            if (!$ogService->setImageFromUpload($upload))
            {
                throw $this->exception($ogService->getError());
            }

            if (!$newFileName = $ogService->updateOgImage($ogImageExists))
            {
                throw $this->exception($this->error(\XF::phrase('seo_new_ogimage_could_not_be_processed')));
            }

            if($newFileName !== null )
            {
                $seo['ogimage_link'] = '';
                $seo['ogimage_upload'] = $newFileName;
                $ogImage = true;
            }
        }
        elseif ($deleteImage)
        {
            $ogService->deleteOgImage($ogImageExists);
        }
        elseif ($newFileName === null && $ogImageExists)
        {
            $seo['ogimage_upload'] = $ogImageExists;
            $ogImage = true;
        }

        if(!empty($seo['ogimage_link']))
        {
            $ogImage = true;

            if($ogImageExists)
                $ogService->deleteOgImage($seo['ogimage_upload']);
        }

        $seo['ogimage'] = $ogImage;

        if(isset($seo['ogimage_attachment']))
            $seo['ogimage'] = true;

        $data->set('seo', $seo);

        if($upload)
        {
            $form->complete(function () use ($seo, $ogService, $node)
            {
                $ogService->moveOgImage($seo, $node);
            });
        }

        return $res;
    }

    protected function nodeAddEdit(\XF\Entity\Node $node)
    {
        $reply = parent::nodeAddEdit($node);

        if ($reply instanceof \XF\Mvc\Reply\View)
        {
            $forum = $reply->getParam('forum');
            $node = $reply->getParam('node');

            if(!empty($forum) && empty($node->node_id) && empty($forum->seo['ogimage']))
            {
                if(!is_array($forum->seo))
                    $forum->seo = [];

                $forum->seo = array_merge($forum->seo,
                    [
                        'ogimage' => 1,
                        'ogimage_attachment' => 1,
                        'ogimage_attachment_fallback' => 1,
                        'ogimage_attachment_avatar' => 1,
                    ]
                );
            }
        }

        return $reply;
    }
}
if(false)
{
    class XFCP_Forum extends \XF\Admin\Controller\Forum {};
}


