<?php

namespace xenMade\SEO\XF\Admin\Controller;

use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;

class Category extends XFCP_Category
{
    protected function saveTypeData(FormAction $form, \XF\Entity\Node $node, \XF\Entity\AbstractNode $data)
    {
        $res = parent::saveTypeData($form, $node, $data);

        $seo = $this->filter('seo', 'array');

        $deleteImage = $this->filter('delete_ogimage', 'bool');
        $ogImageExists = $this->filter('ogimage_exists', 'str');

        $newFileName = null;
        $ogImage = false;

        /** @var \xenMade\SEO\Service\OgImage $ogService */
        $ogService = $this->service('xenMade\SEO:OgImage', $node);

        $upload = $this->request->getFile('ogimage_upload', false, false);
        if ($upload)
        {
            if (!$ogService->setImageFromUpload($upload))
            {
                throw $this->exception($ogService->getError());
            }

            if (!$newFileName = $ogService->updateOgImage($ogImageExists))
            {
                throw $this->exception($this->error(\XF::phrase('seo_new_ogimage_could_not_be_processed')));
            }

            if($newFileName !== null )
            {
                $seo['ogimage_link'] = '';
                $seo['ogimage_upload'] = $newFileName;
                $ogImage = true;
            }
        }
        elseif ($deleteImage)
        {
            $ogService->deleteOgImage($ogImageExists);
        }
        elseif ($newFileName === null && $ogImageExists)
        {
            $seo['ogimage_upload'] = $ogImageExists;
            $ogImage = true;
        }

        if(!empty($seo['ogimage_link']))
        {
            $ogImage = true;

            if($ogImageExists)
                $ogService->deleteOgImage($seo['ogimage_upload']);
        }

        $seo['ogimage'] = $ogImage;

        if(isset($seo['ogimage_attachment']))
            $seo['ogimage'] = true;

        $data->set('seo', $seo);

        if($upload)
        {
            $form->complete(function () use ($seo, $ogService, $node)
            {
                $ogService->moveOgImage($seo, $node);
            });
        }

        return $res;
    }
}
if(false)
{
    class XFCP_Category extends \XF\Admin\Controller\Category {};
}