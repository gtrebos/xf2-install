<?php

namespace xenMade\SEO\XF\Admin\Controller;

use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;

class ThreadPrefix extends XFCP_ThreadPrefix
{
    protected function prefixSaveProcess(\XF\Entity\AbstractPrefix $prefix)
    {
        $res = parent::prefixSaveProcess($prefix);

        $entityInput = $this->filter([
            'seo' => 'array',
        ]);

        $res->basicEntitySave($prefix, $entityInput);

        return $res;
    }
}
if(false)
{
    class XFCP_ThreadPrefix extends \XF\Admin\Controller\ThreadPrefix {};
}