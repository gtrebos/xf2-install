<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\ConnectedAccount\Provider\AbstractProvider;
use XF\ConnectedAccount\ProviderData\AbstractProviderData;
use XF\Mvc\ParameterBag;

class Post extends XFCP_Post
{
    public function actionEdit(ParameterBag $params)
    {
        $res = parent::actionEdit($params);

        // SEO ATTACHMENT
        if ($this->isPost() && \XF::options()->seo_image_attachments['internal'])
        {
            if($res instanceof \XF\Mvc\Reply\View)
            {
                /** @var $imageRepo \xenMade\SEO\Repository\Image */
                $imageRepo = $this->repository('xenMade\SEO:Image');
                $imageRepo->_manageData($res);

                // Attachments Update
                $db = \XF::db();
                $seoAlt = $this->filter('seo_image_alt', 'array');
                $seoTitle = $this->filter('seo_image_title', 'array');

                $seoDesc = $this->filter('seo_description', 'array');

                if($seoAlt && \XF::visitor()->canSetAltTitle())
                {
                    foreach ($seoAlt as $key => $data)
                    {
                        if(!isset($seoTitle[$key]))
                            continue;

                        $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_title=?, seo_alt=? WHERE attachment_id=?', [$seoTitle[$key], $data, $key]);
                    }
                }

                if($seoDesc && \XF::visitor()->canSetAttachmentDesc())
                {
                    foreach ($seoDesc as $key => $data)
                    {
                        $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_description=? WHERE attachment_id=?', [$data, $key]);
                    }
                }
            }
        }

        return $res;
    }

    protected function setupFirstPostThreadEdit(\XF\Entity\Thread $thread, &$threadChanges)
    {
        $editor = parent::setupFirstPostThreadEdit($thread, $threadChanges);

        if($thread->canSetMetaTitle() || $thread->canSetMetaDesc() || $thread->canSetMetaRobot())
        {
            $thread = $editor->getThread();

            $seo = $this->filter('seo', 'array-str');

            $thread['seo'] = $seo;
        }

        return $editor;
    }
}
if(false)
{
    class XFCP_Post extends \XF\Pub\Controller\Post {};
}