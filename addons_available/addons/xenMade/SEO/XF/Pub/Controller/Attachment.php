<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\ConnectedAccount\Provider\AbstractProvider;
use XF\ConnectedAccount\ProviderData\AbstractProviderData;
use XF\Mvc\ParameterBag;

class Attachment extends XFCP_Attachment
{
    public function actionUpload()
    {
        $res = parent::actionUpload();

        // SEO ATTACHMENT
        if($res instanceof \XF\Mvc\Reply\Redirect && \XF::options()->seo_image_attachments['internal'])
        {
            $json = $res->getJsonParams();

            if(isset($json['attachment']))
            {
                $type = $this->filter('type', 'str');
                $json['attachment']['content_' . $type] = true;
            }

            $res->setJsonParams($json);
        }

        return $res;
    }
}
if(false)
{
    class XFCP_Attachment extends \XF\Pub\Controller\Attachment {};
}