<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\AbstractReply;
use XF\Mvc\Reply\View;

class WhatsNew extends XFCP_WhatsNew
{
    protected function postDispatchController($action, ParameterBag $params, AbstractReply &$reply)
    {
        if ($reply instanceof \XF\Mvc\Reply\View)
        {
            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex(null, null, null, null, \XF::options()->seo_robotsWhatsNew);
            $reply->setParam('meta_noindex', $metaNoIndex);
        }
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }
}
if(false)
{
    class XFCP_WhatsNew extends \XF\Pub\Controller\WhatsNew {};
}