<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\AbstractReply;
use XF\Mvc\Reply\View;

class Logout extends XFCP_Logout
{
    protected function postDispatchController($action, ParameterBag $params, AbstractReply &$reply)
    {
        if ($reply instanceof \XF\Mvc\Reply\View)
        {
            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex(null, null, null, null, \XF::options()->seo_robotsOthers);
            $reply->setParam('meta_noindex', $metaNoIndex);
        }
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }
}
if(false)
{
    class XFCP_Logout extends \XF\Pub\Controller\Logout {};
}