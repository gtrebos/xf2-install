<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\View;

class Help extends XFCP_Help
{
    protected function addWrapperParams(View $view, $selected)
    {
        $res = parent::addWrapperParams($view, $selected);

        // SEO
        if($res instanceof \XF\Mvc\Reply\View && !empty($res->getParam('page')))
        {
            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex(null, null, null, null, \XF::options()->seo_robotsHelp);

            $res->setParam('meta_noindex', $metaNoIndex);
        }

        return $res;
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }

}
if(false)
{
    class XFCP_Help extends \XF\Pub\Controller\Help {};
}