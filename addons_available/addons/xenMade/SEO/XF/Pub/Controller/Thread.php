<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Thread extends XFCP_Thread
{
    public function actionIndex(ParameterBag $params)
    {
        $res = parent::actionIndex($params);

        $page = $params->page;

        // SEO ATTACHMENT
        if($res instanceof \XF\Mvc\Reply\View && \XF::options()->seo_image_attachments['internal'])
        {
            /** @var $imageRepo \xenMade\SEO\Repository\Image */
            $imageRepo = $this->repository('xenMade\SEO:Image');
            $imageRepo->_manageData($res);
        }

        // SEO
        if($res instanceof \XF\Mvc\Reply\View && !empty($res->getParam('thread')))
        {
            $thread = $res->getParam('thread');
            $forum = $res->getParam('forum');
            $posts = $res->getParam('posts');

            if(empty($posts) || empty($thread))
                return $res;

            if(empty($thread->seo))
                $thread->seo = [];

            // Besorge FirstPagePosts!
            if($page)
            {
                $postRepo = $this->getPostRepo();

                $postList = $postRepo->findPostsForThreadView($thread)->onPage(1, \XF::options()->messagesPerPage);
                $posts = $postList->fetch();
            }

            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex($forum, $thread, $posts, $page);

            // check Card Empty?
            if(empty($thread->seo['twitter_card']))
            {
                $thread->seo = array_merge($thread->seo,
                    [
                        'twitter_card' => 'summary'
                    ]
                );
            }

            //Check Other Thread Changes
            if(!empty($thread->seo['meta_title']))
                $_seoFlag = true;
            if(!empty($thread->seo['meta_description']))
                $_seoFlag = true;

            $ogImagePath = null;
            $ogImageSettings = [];

            if(!empty(\XF::options()->seo_threadDefault['ogimage']))
            {
                $ogImageSettings = [
                    'ogimage_attachment' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment']) ? 1 : 0),
                    'ogimage_attachment_fallback' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment_fallback']) ? 1 : 0),
                    'ogimage_attachment_avatar' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment_avatar']) ? 1 : 0),
                ];
            }

            if(!empty($forum->seo['ogimage']))
            {
                $ogImageSettings = [
                    'ogimage_attachment' => (!empty($forum->seo['ogimage_attachment']) ? 1 : 0),
                    'ogimage_attachment_fallback' => (!empty($forum->seo['ogimage_attachment_fallback']) ? 1 : 0),
                    'ogimage_attachment_avatar' => (!empty($forum->seo['ogimage_attachment_avatar']) ? 1 : 0)
                ];
            }

            if(!empty($ogImageSettings['ogimage_attachment']) && $ogImagePath == null)
            {
                $firstPost = $thread->FirstPost;
                $attachments = $firstPost->Attachments;

                foreach($attachments as $attachment)
                {
                    if($attachment->has_thumbnail)
                    {
                        $imageUrl = rtrim($this->request()->getHostUrl(), '/') . '/' . ltrim($attachment->thumbnail_url, '/');

                        if($thread->canViewAttachments())
                        {
                            $imageUrl = $this->buildLink('full:attachments', $attachment);
                        }

                        $ogImagePath = $imageUrl;

                        break;
                    }
                }
            }

            if(!empty($ogImageSettings['ogimage_attachment_fallback']) && $ogImagePath == null)
            {
                $firstPost = $thread->FirstPost;
                $attachments = $firstPost->Attachments;

                preg_match('#\[img\]([^\[\]\'"]+)\[\/img\]#is', $firstPost->message, $matches);

                if(!empty($matches[1]))
                {
                    $ogImagePath = $matches[1];
                }
                else
                {
                    preg_match('#(\[ATTACH[^\]]*\])([\d]+)(\[\/ATTACH\])#is', $firstPost->message, $matches);

                    if(!empty($matches[2]))
                    {
                        $id = $matches[2];

                        if(!empty($attachments[$id]))
                        {
                            $attachment = $attachments[$id];

                            if($attachment->has_thumbnail)
                            {
                                $imageUrl = rtrim($this->request()->getHostUrl(), '/') . '/' . ltrim($attachment->thumbnail_url, '/');

                                if($thread->canViewAttachments())
                                {
                                    $imageUrl = $this->buildLink('full:attachments', $attachment);
                                }

                                $ogImagePath = $imageUrl;
                            }
                        }
                    }
                }
            }

            if(!empty($ogImageSettings['ogimage_attachment_avatar']) && $ogImagePath == null)
            {
                $firstPost = $posts->first();

                // Check iF avatar exists :)
                if(!empty($firstPost->User) && $firstPost->User instanceof \XF\Entity\User)
                {
                    $userAvatar = '';
                    $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('h');

                    if(!\XF\Util\File::abstractedPathExists($avatarPath))
                    {
                        $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('l');
                        if(!\XF\Util\File::abstractedPathExists($avatarPath))
                        {
                            $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('m');
                            if(\XF\Util\File::abstractedPathExists($avatarPath))
                            {
                                $userAvatar = $firstPost->User->getAvatarUrl('m', '', true);
                            }
                        }
                        else
                        {
                            $userAvatar = $firstPost->User->getAvatarUrl('l', '', true);
                        }
                    }
                    else
                    {
                        $userAvatar = $firstPost->User->getAvatarUrl('h', '', true);
                    }

                    if($userAvatar)
                    {
                        $ogImagePath = $userAvatar;
                    }
                }
            }


            if($ogImagePath == null)
            {
                if(!empty($thread->seo['ogimage_link']) || !empty($thread->seo['ogimage_upload']))
                {
                    if(!empty($thread->seo['ogimage_link']))
                    {
                        $ogImagePath = $thread->seo['ogimage_link'];
                    }
                    elseif(!empty($thread->seo['ogimage_upload']))
                    {
                        $ogImagePath = \XF::app()->applyExternalDataUrl(sprintf('seo_thread_images/%s', $thread->seo['ogimage_upload']), true);
                    }
                }
                elseif(!empty($forum->seo['ogimage_link']) || !empty($forum->seo['ogimage_upload']))
                {
                    if(!empty($forum->seo['ogimage_link']))
                    {
                        $ogImagePath = $forum->seo['ogimage_link'];
                    }
                    elseif(!empty($forum->seo['ogimage_upload']))
                    {
                        $ogImagePath = \XF::app()->applyExternalDataUrl(sprintf('seo_node_images/%s', $forum->seo['ogimage_upload']), true);
                    }
                }
                elseif(!empty(\XF::options()->seo_threadDefault['ogimage_link']))
                {
                    $ogImagePath = \XF::options()->seo_threadDefault['ogimage_link'];
                }
            }


            if($ogImagePath !== null)
            {
                $thread->seo = array_merge($thread->seo,
                    [
                        'og_image_link' => $ogImagePath
                    ]
                );

                $res->setParam('thread', $thread);
            }



            // Überschreibe Forum SEO
            $seo = [];
            if(!empty($forum->seo['meta_description']))
                $seo['meta_description'] = \xenMade\SEO\Listener::getCustomFieldTitle($forum->seo['meta_description'], $thread);

            if(!empty($forum->seo['meta_title']))
                $seo['meta_title'] = \xenMade\SEO\Listener::getCustomFieldTitle($forum->seo['meta_title'], $thread);

            if($seo)
            {
                $seo = array_merge($forum->seo, $seo);
                $forum->set('seo', $seo);
                unset($seo);
            }


            // Überschreibe Thread SEO
            $seo = [];
            if(!empty($thread->seo['meta_description']))
                $seo['meta_description'] = \xenMade\SEO\Listener::getCustomFieldTitle($thread->seo['meta_description'], $thread);

            if(!empty($thread->seo['meta_title']))
                $seo['meta_title'] = \xenMade\SEO\Listener::getCustomFieldTitle($thread->seo['meta_title'], $thread);

            if($seo)
            {
                $seo = array_merge($thread->seo, $seo);
                $thread->set('seo', $seo);
                unset($seo);
            }

            $res->setParam('meta_noindex', $metaNoIndex);
            $res->setParam('_seoFlag', $_seoFlag);
        }

        return $res;
    }

    protected function getNewPostsReply(\XF\Entity\Thread $thread, $lastDate)
    {
        $res = parent::getNewPostsReply($thread, $lastDate);

        // SEO Attachment
        if($res instanceof \XF\Mvc\Reply\View && \XF::options()->seo_image_attachments['internal'])
        {
            /** @var $imageRepo \xenMade\SEO\Repository\Image */
            $imageRepo = $this->repository('xenMade\SEO:Image');
            $imageRepo->_manageData($res);
        }

        return $res;
    }

    public function actionAddReply(ParameterBag $params)
    {
        $res = parent::actionAddReply($params);

        // SEO ATTACHMENT
        if($res instanceof \XF\Mvc\Reply\View)
        {
            $db = \XF::db();
            $seoAlt = $this->filter('seo_image_alt', 'array');
            $seoTitle = $this->filter('seo_image_title', 'array');
            $seoDesc = $this->filter('seo_description', 'array');

            if($seoAlt && \XF::options()->seo_image_attachments['internal'] && \XF::visitor()->canSetAltTitle())
            {
                foreach ($seoAlt as $key => $data)
                {
                    if(!isset($seoTitle[$key]))
                        continue;

                    $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_title=?, seo_alt=? WHERE attachment_id=?', [$seoTitle[$key], $data, $key]);
                }
            }

            if($seoDesc && \XF::visitor()->canSetAttachmentDesc())
            {
                foreach ($seoDesc as $key => $data)
                {
                    $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_description=? WHERE attachment_id=?', [$data, $key]);
                }
            }
        }

        return $res;
    }

    protected function setupThreadEdit(\XF\Entity\Thread $thread)
    {
        $editor = parent::setupThreadEdit($thread);

        $editor->setTitle(\XF::repository('XF:Thread')->changeThreadTitle($editor->getThread()->title));

        if(\XF::visitor()->canSetSeo())
        {
            $seo['ogimage_upload'] = '';
            $seo = $this->filter('seo', 'array-str');
            $thread = $editor->getThread();
            $node = $thread->Forum->Node;

            $deleteImage = $this->filter('delete_ogimage', 'bool');
            $ogImageExists = $this->filter('ogimage_exists', 'str');

            $newFileName = null;

            /** @var \xenMade\SEO\Service\OgImage $ogService */
            $ogService = $this->service('xenMade\SEO:OgImage', $node, $thread, 'thread');

            $upload = $this->request->getFile('ogimage_upload', false, false);

            if(!empty($thread->seo['ogimage_upload']))
                $seo['ogimage_upload'] = $thread->seo['ogimage_upload'];

            if ($upload)
            {
                $seo['ogimage_link'] = '';
                if (!$ogService->setImageFromUpload($upload))
                {
                    throw $this->exception($ogService->getError());
                }

                if (!$newFileName = $ogService->updateOgImage($ogImageExists))
                {
                    throw $this->exception($this->error(\XF::phrase('seo_new_ogimage_could_not_be_processed')));
                }
            }
            elseif ($deleteImage)
            {
                $ogService->deleteOgImage($ogImageExists);
                $seo['ogimage_upload'] = '';
            }

            if(!$seo['ogimage_link'])
            {
                if($newFileName !== null)
                {
                    $seo['ogimage_link'] = '';
                    $seo['ogimage_upload'] = $newFileName;
                }
            }
            else
            {
                if(!empty($thread->seo['ogimage_upload']))
                {
                    $ogService->deleteOgImage($thread->seo['ogimage_upload']);
                }

                $seo['ogimage_upload'] = '';
            }

            $thread['seo'] = $seo;

            if ($upload)
            {
                $ogService->moveOgImage($seo, $node);
            }
        }

        return $editor;
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }
}
if(false)
{
    class XFCP_Thread extends \XF\Pub\Controller\Thread {};
}