<?php

namespace xenMade\SEO\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\AbstractReply;
use XF\Mvc\Reply\View;

class Forum extends XFCP_Forum
{
    public function actionForum(ParameterBag $params)
    {
        $res = parent::actionForum($params);
        if ($res instanceof \XF\Mvc\Reply\View)
        {
            $forum = $res->getParam('forum');
            $page = $res->getParam('page');

            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndex($forum, null, null, $page);

            $res->setParam('meta_noindex', $metaNoIndex);
        }

        return $res;
    }

    protected function setupThreadCreate(\XF\Entity\Forum $forum)
    {
        $creator = parent::setupThreadCreate($forum);

        $creator->getThread()->title = \XF::repository('XF:Thread')->changeThreadTitle($creator->getThread()->title);

        $thread = $creator->getThread();

        if(\XF::visitor()->canSetSeo())
        {
            $seo = $this->filter('seo', 'array-str');
            $node = $thread->Forum->Node;

            $deleteImage = $this->filter('delete_ogimage', 'bool');
            $ogImageExists = $this->filter('ogimage_exists', 'str');

            $newFileName = null;

            /** @var \xenMade\SEO\Service\OgImage $ogService */
            $ogService = $this->service('xenMade\SEO:OgImage', $node, $thread, 'thread');

            $upload = $this->request->getFile('ogimage_upload', false, false);
            if ($upload)
            {
                if (!$ogService->setImageFromUpload($upload))
                {
                    throw $this->exception($ogService->getError());
                }

                if (!$newFileName = $ogService->updateOgImage($ogImageExists))
                {
                    throw $this->exception($this->error(\XF::phrase('seo_new_ogimage_could_not_be_processed')));
                }
            }
            elseif ($deleteImage)
            {
                $ogService->deleteOgImage($ogImageExists);
            }

            if($newFileName !== null)
            {
                $seo['ogimage_link'] = '';
                $seo['ogimage_upload'] = $newFileName;
            }

            $thread['seo'] = $seo;

            if ($upload)
            {
                $ogService->moveOgImage($seo, $node);
            }
        }
        elseif($forum->canSetMetaTitle() || $forum->canSetMetaDesc() || $forum->canSetMetaRobot())
        {
            $seo = $this->filter('seo', 'array-str');
            $thread['seo'] = $seo;
        }

        return $creator;
    }

    protected function finalizeThreadCreate(\XF\Service\Thread\Creator $creator)
    {
        $res = parent::finalizeThreadCreate($creator);

        $db = \XF::db();
        $seoAlt = $this->filter('seo_image_alt', 'array');
        $seoTitle = $this->filter('seo_image_title', 'array');

        $seoDesc = $this->filter('seo_description', 'array');

        if($seoAlt && \XF::options()->seo_image_attachments['internal'] && \XF::visitor()->canSetAltTitle())
        {
            foreach ($seoAlt as $key => $data)
            {
                if(!isset($seoTitle[$key]))
                    continue;

                $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_title=?, seo_alt=? WHERE attachment_id=?', [$seoTitle[$key], $data, $key]);
            }
        }

        if($seoDesc && \XF::visitor()->canSetAttachmentDesc())
        {
            foreach ($seoDesc as $key => $data)
            {
                $db->query('UPDATE xf_attachment_data LEFT JOIN xf_attachment USING(data_id) SET seo_description=? WHERE attachment_id=?', [$data, $key]);
            }
        }

        return $res;
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }
}
if(false)
{
    class XFCP_Forum extends \XF\Pub\Controller\Forum {};
}