<?php

namespace xenMade\SEO\XFRM\Admin\Controller;

use XF\Admin\Controller\AbstractController;
use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;

class Category extends XFCP_Category
{
    protected function categorySaveProcess(\XF\Entity\AbstractCategoryTree $category)
    {
        $res = parent::categorySaveProcess($category);

        $entityInput = $this->filter([
            'seo' => 'array',
        ]);

        $res->basicEntitySave($category, $entityInput);

        return $res;
    }
}
if(false)
{
    class XFCP_Category extends \XFRM\Admin\Controller\Category {};
}