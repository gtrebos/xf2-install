<?php

namespace xenMade\SEO\XFRM\Pub\Controller;

use XF\Mvc\ParameterBag;

class ResourceItem extends XFCP_ResourceItem
{
    public function actionView(ParameterBag $params)
    {
        $res = parent::actionView($params);

        // SEO
        if($res instanceof \XF\Mvc\Reply\View && !empty($res->getParam('resource')))
        {
            $resource = $res->getParam('resource');
            $category = $res->getParam('category');

            if(empty($resource) || empty($category))
                return $res;

            if(empty($resource->seo))
                $resource->seo = [];

            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndexForRM($resource, $category);

            $res->setParam('meta_noindex', $metaNoIndex);
            $res->setParam('_seoFlag', $_seoFlag);
        }

        return $res;
    }

    public function actionHistory(ParameterBag $params)
    {
        $res = parent::actionHistory($params);

        $res = $this->getSeoData($res);

        return $res;
    }

    public function actionUpdates(ParameterBag $params)
    {
        $res = parent::actionUpdates($params);

        $res = $this->getSeoData($res);

        return $res;
    }

    public function actionReviews(ParameterBag $params)
    {
        $res = parent::actionReviews($params);

        $res = $this->getSeoData($res);

        return $res;
    }


    public function actionSeo(ParameterBag $params)
    {
        $resource = $this->assertViewableResource($params->resource_id);

        if (!$resource->canEdit($error) || !\XF::visitor()->canSetSeo())
        {
            return $this->noPermission($error);
        }

        $category = $resource->Category;

        if ($this->isPost())
        {
            $resource['seo'] = $this->filter('seo', 'array-str');

            /** @var \XFRM\Service\ResourceItem\Edit $editor */
            $editor = $this->service('XFRM:ResourceItem\Edit', $resource);

            $editor->save();

            return $this->redirect($this->buildLink('resources', $resource));
        }
        else
        {
            $viewParams = [
                'resource' => $resource,
                'category' => $category,
            ];

            return $this->view('XF:ResourceItem\SEO', 'seo_xfrm_resource_seo_edit', $viewParams);
        }
    }

    protected function getSeoData($res)
    {
        // SEO
        if($res instanceof \XF\Mvc\Reply\View && !empty($res->getParam('resource')))
        {
            $resource = $res->getParam('resource');
            $category = $resource->Category;

            if(empty($resource) || empty($category))
                return $res;

            if(empty($resource->seo))
                $resource->seo = [];

            list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndexForRM($resource, $category);

            $res->setParam('meta_noindex', $metaNoIndex);
            $res->setParam('_seoFlag', $_seoFlag);
        }

        return $res;
    }

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return $this->repository('xenMade\SEO:NoIndex');
    }

}
if(false)
{
    class XFCP_ResourceItem extends \XFRM\Pub\Controller\ResourceItem {};
}