<?php

namespace xenMade\SEO\XFRM\Sitemap;

use XF\Sitemap\AbstractHandler;
use XF\Sitemap\Entry;

class ResourceItem extends XFCP_ResourceItem
{
	public function isIncluded($record)
	{
	    $category = $record->Category;

        list($metaNoIndex, $_seoFlag) = $this->getNoIndexRepo()->noIndexForRM($record, $category);

        if(strpos($metaNoIndex, 'noindex') !== false)
            return false;

	    return parent::isIncluded($record);
	}

    /**
     * @return \xenMade\SEO\Repository\NoIndex
     */
    protected function getNoIndexRepo()
    {
        return \XF::repository('xenMade\SEO:NoIndex');
    }
}
if(false)
{
    class XFCP_ResourceItem extends \XFRM\Sitemap\ResourceItem {}
}