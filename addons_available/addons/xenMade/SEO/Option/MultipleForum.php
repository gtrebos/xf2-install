<?php

namespace xenMade\SEO\Option;

class MultipleForum extends \XF\Option\AbstractOption
{
    public static function renderSelect(\XF\Entity\Option $option, array $htmlParams)
    {
        /** @var \XF\Repository\Node $nodeRepo */
        $nodeRepo = \XF::repository('XF:Node');

        $choices = $nodeRepo->getNodeOptionsData(true, 'Forum', 'option');
        $choices = array_map(function($v) {
            $v['label'] = \XF::escapeString($v['label']);
            return $v;
        }, $choices);

        $controlOptions = self::getControlOptions($option, $htmlParams);
        $controlOptions['multiple'] = true;

        $rowOptions = self::getRowOptions($option, $htmlParams);

        return self::getTemplater()->formSelectRow($controlOptions, $choices, $rowOptions);
    }	
}