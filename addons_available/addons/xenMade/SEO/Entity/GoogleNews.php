<?php

namespace xenMade\KWM\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

class GoogleNews extends Entity
{
    public static function getStructure(Structure $structure)
    {
        $structure->table      = 'xf_xenmade_seo_news';
        $structure->shortName  = 'xenMade\SEO:GoogleNews';
        $structure->primaryKey = 'thread_id';

        $structure->columns = [
            'thread_id' => ['type' => self::UINT],
            'post_date'  => ['type' => self::UINT, 'default' => 0]
        ];

        $structure->getters   = [];
        $structure->relations = [];

        return $structure;
    }

    protected function _preSave()
    {
    }

    protected function _postSave()
    {
    }

    protected function _preDelete()
    {
    }

    protected function _postDelete()
    {
    }
}
