<?php

namespace xenMade\SEO\Repository;

use XF\Mvc\Entity\Repository;

class NoIndex extends Repository
{
    CONST NOINDEX = 'noindex';
    CONST NOFOLLOW = 'nofollow';

    public function noIndex($forum, $thread, $posts, $page = null, $other = null)
    {
        $metaNoIndex = [];
        $_seoFlag = false;
        $prefix = [];


        if(!empty($forum->prefix_cache) && isset($thread->prefix_id))
        {
            $prefixes = $forum->getPrefixes();

            if(!empty($thread) && !empty($thread->prefix_id) && !empty($prefixes[$thread->prefix_id]))
                $prefix = $prefixes[$thread->prefix_id];
        }

        $options = \XF::options();

        $threadRobot = false;

        // Thread überwiegt IMMER!!!
        if(!empty($thread->seo['robot_index']) || !empty($thread->seo['robot_follow']))
        {
            $metaNoIndex['index'] = $thread->seo['robot_index'];
            $metaNoIndex['follow'] = $thread->seo['robot_follow'];

            $threadRobot = true;
        }
        else
        {
            if(!empty($thread) && (!empty($prefix->seo['robot_index']) || !empty($prefix->seo['robot_follow'])))
            {
                $metaNoIndex['index'] = $prefix->seo['robot_index'];
                $metaNoIndex['follow'] = $prefix->seo['robot_follow'];
            }
            elseif(!empty($thread) && (!empty($forum->seo['robot_index']) || !empty($forum->seo['robot_follow'])))
            {
                $metaNoIndex['index'] = $forum->seo['robot_index'];
                $metaNoIndex['follow'] = $forum->seo['robot_follow'];
            }
            elseif(empty($thread) && (!empty($other['robot_index']) || !empty($other['robot_follow'])))
            {
                $metaNoIndex['index'] = $other['robot_index'];
                $metaNoIndex['follow'] = $other['robot_follow'];
            }
            elseif(!empty($thread) && (!empty($options['seo_threadDefault']['robot_index']) || !empty($options['seo_threadDefault']['robot_follow'])))
            {
                $metaNoIndex['index'] = $options['seo_threadDefault']['robot_index'];
                $metaNoIndex['follow'] = $options['seo_threadDefault']['robot_follow'];
            }
            elseif(empty($thread) && !empty($forum->seo['forum']))
            {
                if(!empty($forum->seo['forum']['after_page']) && $forum->seo['forum']['after_page'] > 0 && $page >= $forum->seo['forum']['after_page'])
                {
                    $metaNoIndex['index'] = self::NOINDEX;

                    if(empty($forum->seo['forum']['follow']))
                    {
                        $metaNoIndex['follow'] = 'nofollow';
                    }
                    else
                    {
                        $metaNoIndex['follow'] = 'follow';
                    }
                }
            }
        }

        if(!empty($metaNoIndex))
        {
            //$metaNoIndex = implode(',', array_filter($metaNoIndex));
        }

        // Ok es gab wohl keine Frettchen Daten also nehmen wir Foren Daten - oder?! Nein wir Tanzen um einen Vulkan :)
        if(!empty($thread) && (!empty($forum->seo['extended']) || !empty($options->seo_threadDefault['extended'])) && !$threadRobot)
        {
            $robotStatus = $this->getRobotStatus($forum, $thread, $posts, $page);

            if($robotStatus)
            {
                $metaNoIndex['index'] = self::NOINDEX;
            }

            // Follow?
            if(!empty($metaNoIndex))
            {
                if(empty($forum->seo['follow']) && empty($options->seo_threadDefault['extended']['follow']))
                {
                    $metaNoIndex['follow'] = 'nofollow';
                }
                else
                {
                    $metaNoIndex['follow'] = 'follow';
                }

            }
        }

        $metaNoIndex = implode(',', array_filter($metaNoIndex));

        if($metaNoIndex)
        {
            if(strpos($metaNoIndex, 'no') !== false)
            {
                $_seoFlag = true;
            }
        }

        return [$metaNoIndex, $_seoFlag];
    }

    public function noIndexForRM($resource, $category)
    {
        $metaNoIndex = null;
        $_seoFlag = false;

        // Thread überwiegt IMMER!!!
        if(!empty($resource->seo['robot_index']) || !empty($resource->seo['robot_follow']))
        {
            $metaNoIndex['index'] = $resource->seo['robot_index'];
            $metaNoIndex['follow'] = $resource->seo['robot_follow'];
        }
        else
        {
            if(!empty($category->seo['robot_index']) || !empty($category->seo['robot_follow']))
            {
                $metaNoIndex['index'] = $category->seo['robot_index'];
                $metaNoIndex['follow'] = $category->seo['robot_follow'];
            }
        }

        if(!empty($metaNoIndex))
        {
            $metaNoIndex = implode(',', array_filter($metaNoIndex));
        }

        if($metaNoIndex !== null)
            $_seoFlag = true;

        return [$metaNoIndex, $_seoFlag];
    }

    protected function getRobotStatus($forum, $thread, $posts, $page)
    {
        /** @var \xenMade\SEO\XF\Repository\Thread $threadRepo */
        $threadRepo = $this->getThreadRepo();

        $options = \XF::options();

        //Prüfe Thread Age
        if(!empty($forum->seo['thread_age']))
        {
            if($thread->post_date <= \XF::$time - ($forum->seo['thread_age'] * 86400))
            {
                return true;
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['thread_age']))
        {
            if($thread->post_date <= \XF::$time - ($options->seo_threadDefault['extended']['thread_age'] * 86400))
            {
                return true;
            }
        }

        //Prüfe Thread Age
        if(!empty($forum->seo['last_post']))
        {
            if($thread->last_post_date <= \XF::$time - ($forum->seo['last_post'] * 86400))
            {
                return true;
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['last_post']))
        {
            if($thread->last_post_date <= \XF::$time - ($options->seo_threadDefault['extended']['last_post'] * 86400))
            {
                return true;
            }
        }

        //Prüfe Thread Age and Zero Post
        if(!empty($forum->seo['zero_post_days']) && $forum->seo['zero_post_days'] > 0)
        {
            if($thread->reply_count == 0 && $thread->post_date <= \XF::$time - ($forum->seo['zero_post_days'] * 86400))
            {
                return true;
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['zero_post_days']) && $options->seo_threadDefault['extended']['zero_post_days'] > 0)
        {
            if($thread->reply_count == 0 && $thread->post_date <= \XF::$time - ($options->seo_threadDefault['extended']['zero_post_days'] * 86400))
            {
                return true;
            }
        }

        //Zero Post only for -1 Post noIndex Pre Reply
        if(!empty($forum->seo['zero_post_days']) && $forum->seo['zero_post_days'] == -1 && $thread->reply_count == 0)
        {
            return true;
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['zero_post_days']) && $options->seo_threadDefault['extended']['zero_post_days'] == -1 && $thread->reply_count == 0)
        {
            return true;
        }



        //Prüfe Thread View Count
        if(!empty($forum->seo['thread_view']))
        {
            if($thread->view_count < $forum->seo['thread_view'])
            {
                return true;
            }
        }
        elseif(!empty($options->seo_threadDefault['extended']['thread_view']))
        {
            if($thread->view_count < $options->seo_threadDefault['extended']['thread_view'])
            {
                return true;
            }
        }

        //Prüfe Page
        if(!empty($forum->seo['after_page']) && $page !== null)
        {
            if($page >= $forum->seo['after_page'])
            {
                return true;
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['after_page']) && $page !== null)
        {
            if($page >= $options->seo_threadDefault['extended']['after_page'])
            {
                return true;
            }
        }

        // Prüfe Wörter im Thread!
        if(!empty($forum->seo['words']))
        {
            if($forum->seo['words']['mode'] == 'firstpage')
            {
                $words = $threadRepo->countWordsOnFirstPage($thread, $posts, $page);

                if($words < $forum->seo['words']['firstpage_words'])
                {
                    return true;
                }
            }
            elseif($forum->seo['words']['mode'] == 'allpages')
            {
                $words = $threadRepo->countWordsOnAllPages($thread);

                if($words < $forum->seo['words']['allpages_words'])
                {
                    return true;
                }
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['words']))
        {
            if($options->seo_threadDefault['extended']['words']['mode'] == 'firstpage')
            {
                $words = $threadRepo->countWordsOnFirstPage($thread, $posts, $page);

                if($words < $options->seo_threadDefault['extended']['words']['firstpage_words'])
                {
                    return true;
                }
            }
            elseif($options->seo_threadDefault['extended']['words']['mode'] == 'allpages')
            {
                $words = $threadRepo->countWordsOnAllPages($thread);

                if($words < $options->seo_threadDefault['extended']['words']['allpages_words'])
                {
                    return true;
                }
            }
        }

        //Prüfe Attachments
        if(!empty($forum->seo['no_attachments']))
        {
            if($forum->seo['no_attachments']['mode'] == 'firstpost')
            {
                if(!$threadRepo->countAttachmentsFirstPost($thread))
                {
                    return true;
                }

            }
            elseif($forum->seo['no_attachments']['mode'] == 'allposts')
            {
                if(!$threadRepo->countAttachmentsAllPost($thread))
                {
                    return true;
                }
            }
        }
        // NodeDefaultSettinigs
        elseif(!empty($options->seo_threadDefault['extended']['no_attachments']))
        {
            if($options->seo_threadDefault['extended']['no_attachments']['mode'] == 'firstpost')
            {
                if(!$threadRepo->countAttachmentsFirstPost($thread))
                {
                    return true;
                }

            }
            elseif($options->seo_threadDefault['extended']['no_attachments']['mode'] == 'allposts')
            {
                if(!$threadRepo->countAttachmentsAllPost($thread))
                {
                    return true;
                }
            }
        }

        return false;
    }


    protected function changeRobotData(array $robotData, $newValue)
    {
        $robotData = array_replace($robotData,
            array_fill_keys(
                array_keys($robotData, $value),
                $replacement
            )
        );
    }

    /**
     * @return \XF\Repository\Thread
     */
    protected function getThreadRepo()
    {
        return $this->repository('XF:Thread');
    }

    /**
     * @return \XF\Repository\Post
     */
    protected function getPostRepo()
    {
        return $this->repository('XF:Post');
    }
}