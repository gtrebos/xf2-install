<?php

namespace xenMade\SEO\Repository;

use XF\Mvc\Entity\Repository;

class Image extends Repository
{
    protected $_data = null;

    protected $_existingVariables = ['{boardtitle}', '{forumtitle}', '{threadtitle}', '{threadid}', '{threaddate}', '{threadtime}', '{postid}', '{userid}', '{username}', '{postdate}', '{posttime}', '{postcounter}', '{uploaddate}', '{filename}', '{filehash}', '{attachcount}', '{contenttype}', '{contentid}'];

    public function _manageData(\XF\Mvc\Reply\View $res)
    {
        if(!empty($res->getParam('thread')))
        {
            $thread = $res->getParam('thread');
        }
        else
        {
            return false;
        }

        if(empty($res->getParam('forum')))
        {
            if(!empty($thread->Forum))
                $forum = $thread->Forum;
        }
        elseif($res->getParam('forum'))
        {
            $forum = $res->getParam('forum');
        }

        if(empty($forum))
            return false;

        $this->_data['boardtitle'] = \XF::options()->boardTitle;
        $this->_data['forumtitle'] = $forum->title;

        $this->_data['threadtitle'] = $thread->title;
        $this->_data['threadid'] = $thread->thread_id;
        $this->_data['threaddate'] = \XF::language()->date($thread->post_date);
        $this->_data['threadtime'] = \XF::language()->time($thread->post_date);

        $posts = [];

        if(!empty($res->getParam('posts')))
        {
            $posts = $res->getParam('posts');
        }
        else if(!empty($res->getParam('post')))
        {
            $posts[] = $res->getParam('post');
        }

        foreach($posts as $post)
        {
            $this->_data['post'][$post->post_id] = [
                'postid' => $post->post_id,
                'userid' => $post->user_id,
                'username' => $post->username,
                'postdate' => \XF::language()->date($post->post_date),
                'posttime' => \XF::language()->time($post->post_date),
                'postcounter' => $post->position,
            ];

            if($post->attach_count && $post->Attachments instanceof \XF\Mvc\Entity\ArrayCollection)
            {
                foreach($post->Attachments as $attachment)
                {
                    $this->_data['attachData'][$attachment->data_id] = [
                        'uploaddate' => $attachment->Data->upload_date,
                        'filehash' => $attachment->Data->file_hash,
                        'attachcount' => $attachment->Data->attach_count,
                        'contenttype' => $attachment->content_type,
                        'contentid' => $attachment->content_id,
                    ];
                }
            }
        }

        return true;
    }

    public function getImageSeoData($attachment, $src = '')
    {
        $returnString = 'alt="%s" title="%s" ';
        $fileName = @basename($src);

        if($this->_data === null)
            return sprintf($returnString, $fileName, $fileName);

        $this->_data['filename'] = $fileName;

        $postId = @$attachment['content_id'];
        $dataId = @$attachment['data_id'];

        if(!empty($attachment['entity']) && $attachment['entity'] instanceof \XF\Entity\Post)
        {
            $postId = @$attachment['entity']->post_id;
            $dataId = 0;
        }

        $variables = $this->_getVariablesData($postId, $dataId);

        if(!empty($attachment->Data) && $attachment->Data instanceof \XF\Entity\AttachmentData)
        {
            $fileName = $attachment->Data->filename;

            // Internals Images
            if(!empty(\XF::options()->seo_image_attachments['internal']))
            {
                if(!empty($attachment->Data) && $attachment->Data instanceof \XF\Entity\AttachmentData)
                {
                    if(!$attachment->Data['seo_alt'])
                    {
                        $alt = str_replace(array_keys($variables), array_values($variables), \XF::options()->seo_image_attachments['internal']['alt']);
                    }
                    else
                    {
                        $alt = $attachment->Data['seo_alt'];
                    }

                    if(!$attachment->Data['seo_title'])
                    {
                        $title = str_replace(array_keys($variables), array_values($variables), \XF::options()->seo_image_attachments['internal']['title']);
                    }
                    else
                    {
                        $title = $attachment->Data['seo_title'];
                    }

                    // Clean Up alt/Title https://xenforo.com/community/threads/141984/page-9#post-1267756
                    $alt = \XF::escapeString($alt);
                    $title = \XF::escapeString($title);

                    return sprintf($returnString, $alt, $title);
                }
            }
        }


        // External Images
        if(!empty(\XF::options()->seo_image_attachments['external']))
        {
            if(!empty($attachment['entity']) && $attachment['entity'] instanceof \XF\Entity\Post)
            {
                $alt = str_replace(array_keys($variables), array_values($variables), \XF::options()->seo_image_attachments['external']['alt']);
                $title = str_replace(array_keys($variables), array_values($variables), \XF::options()->seo_image_attachments['external']['title']);

                // Clean Up alt/Title https://xenforo.com/community/threads/141984/page-9#post-1267756
                $alt = \XF::escapeString($alt);
                $title = \XF::escapeString($title);

                return sprintf($returnString, $alt, $title);
            }
        }

        return sprintf($returnString, $fileName, $fileName);
    }

    protected function _getVariablesData($postId, $attachmentId)
    {
        $data = [];

        foreach($this->_existingVariables as $variable)
        {
            $variable_ = str_replace('{', '', str_replace('}', '', $variable));

            if(isset($this->_data[$variable_]))
            {
                $data[$variable] = $this->_data[$variable_];
            }
            elseif(isset($this->_data['post'][$postId][$variable_]))
            {
                $data[$variable] = $this->_data['post'][$postId][$variable_];
            }
            elseif(isset($this->_data['attachData'][$attachmentId][$variable_]))
            {
                $data[$variable] = $this->_data['attachData'][$attachmentId][$variable_];
            }
        }

        return $data;
    }



    public function getOgImage($thread, $forum)
    {
        $ogImagePath = '';

        if(!empty($thread->seo['ogimage_link']) || !empty($thread->seo['ogimage_upload']))
        {
            if(!empty($thread->seo['ogimage_link']))
            {
                $ogImagePath = $thread->seo['ogimage_link'];
            }
            elseif(!empty($thread->seo['ogimage_upload']))
            {
                $ogImagePath = \XF::app()->applyExternalDataUrl(sprintf('seo_thread_images/%s', $thread->seo['ogimage_upload']), true);
            }
        }
        elseif(!empty($forum->seo['ogimage_link']) || !empty($forum->seo['ogimage_upload']))
        {
            if(!empty($forum->seo['ogimage_link']))
            {
                $ogImagePath = $forum->seo['ogimage_link'];
            }
            elseif(!empty($forum->seo['ogimage_upload']))
            {
                $ogImagePath = \XF::app()->applyExternalDataUrl(sprintf('seo_node_images/%s', $forum->seo['ogimage_upload']), true);
            }
        }
        elseif(!empty(\XF::options()->seo_threadDefault['ogimage_link']))
        {
            $ogImagePath = \XF::options()->seo_threadDefault['ogimage_link'];
        }

        if($ogImagePath)
        {
            return $ogImagePath;
        }

        $ogImageSettings = [];
        if(!empty(\XF::options()->seo_threadDefault['ogimage']))
        {
            $ogImageSettings = [
                'ogimage_attachment' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment']) ? 1 : 0),
                'ogimage_attachment_fallback' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment_fallback']) ? 1 : 0),
                'ogimage_attachment_avatar' => (!empty(\XF::options()->seo_threadDefault['ogimage_attachment_avatar']) ? 1 : 0),
            ];
        }

        if(!empty($forum->seo['ogimage']))
        {
            $ogImageSettings = [
                'ogimage_attachment' => (!empty($forum->seo['ogimage_attachment']) ? 1 : 0),
                'ogimage_attachment_fallback' => (!empty($forum->seo['ogimage_attachment_fallback']) ? 1 : 0),
                'ogimage_attachment_avatar' => (!empty($forum->seo['ogimage_attachment_avatar']) ? 1 : 0)
            ];
        }

        if(!empty($ogImageSettings['ogimage_attachment']))
        {
            $firstPost = $thread->FirstPost;
            $attachments = $firstPost->Attachments;

            foreach($attachments as $attachment)
            {
                if($attachment->has_thumbnail)
                {
                    $imageUrl = rtrim(\XF::app()->request()->getHostUrl(), '/') . '/' . ltrim($attachment->thumbnail_url, '/');

                    if($thread->canViewAttachments())
                    {
                        $imageUrl = \XF::app()->router()->buildLink('full:attachments', $attachment);
                    }

                    return $imageUrl;

                }
            }
        }

        if(!empty($ogImageSettings['ogimage_attachment_fallback']))
        {
            $firstPost = $thread->FirstPost;

            preg_match('#\[img\]([^\[\]\'"]+)\[\/img\]#is', $firstPost->message, $matches);

            if(!empty($matches[1]))
            {
                return $matches[1];
            }
        }

        if(!empty($ogImageSettings['ogimage_attachment_avatar']))
        {
            $firstPost = $thread->FirstPost;

            // Check iF avatar exists :)
            if(!empty($firstPost->User) && $firstPost->User instanceof \XF\Entity\User)
            {
                $userAvatar = '';
                $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('h');

                if(!\XF\Util\File::abstractedPathExists($avatarPath))
                {
                    $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('l');
                    if(!\XF\Util\File::abstractedPathExists($avatarPath))
                    {
                        $avatarPath = $firstPost->User->getAbstractedCustomAvatarPath('m');
                        if(\XF\Util\File::abstractedPathExists($avatarPath))
                        {
                            $userAvatar = $firstPost->User->getAvatarUrl('m', '', true);
                        }
                    }
                    else
                    {
                        $userAvatar = $firstPost->User->getAvatarUrl('l', '', true);
                    }
                }
                else
                {
                    $userAvatar = $firstPost->User->getAvatarUrl('h', '', true);
                }

                if($userAvatar)
                {
                    return $userAvatar;
                }
            }
        }

        return '';
    }
}