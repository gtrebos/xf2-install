<?php

namespace xenMade\SEO;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $sm = $this->schemaManager();

        $sm->alterTable('xf_attachment_data', function(Alter $table)
        {
            $table->addColumn('seo_title', 'tinytext')->nullable()->setDefault(null);
            $table->addColumn('seo_alt', 'tinytext')->nullable()->setDefault(null);
            $table->addColumn('seo_description', 'tinytext')->nullable()->setDefault(null);
        });

        $sm->alterTable('xf_thread', function(Alter $table)
        {
            $table->addColumn('seo', 'mediumblob')->nullable();
        });

        $sm->alterTable('xf_forum', function(Alter $table)
        {
            $table->addColumn('seo', 'mediumblob')->nullable();
        });

        $sm->alterTable('xf_category', function(Alter $table)
        {
            $table->addColumn('seo', 'mediumblob')->nullable();
        });

        $sm->alterTable('xf_thread_prefix', function(Alter $table)
        {
            $table->addColumn('seo', 'mediumblob')->nullable();
        });

        // XFRM
        if($sm->tableExists('xf_rm_category') && !$sm->columnExists('xf_rm_category', 'seo'))
        {
            $sm->alterTable('xf_rm_category', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }

        if($sm->tableExists('xf_rm_resource') && !$sm->columnExists('xf_rm_resource', 'seo'))
        {
            $sm->alterTable('xf_rm_resource', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }

        /*
        $this->schemaManager()->createTable('xf_xenmade_seo_news', function(\XF\Db\Schema\Create $table)
        {
            $table->addColumn('thread_id', 'int')->primaryKey();
            $table->addColumn('post_date', 'int', 10)->setDefault(0);

            $table->addUniqueKey('thread_id', 'thread_id');
            $table->addKey('post_date');
        });
        */
    }

    public function upgrade20010Step1()
    {
        $sm = $this->schemaManager();

        $sm->alterTable('xf_category', function(Alter $table)
        {
            $table->addColumn('seo', 'mediumblob')->nullable();
        });
    }

    public function upgrade20025Step1()
    {
        $sm = $this->schemaManager();

        // XFRM
        if($sm->tableExists('xf_rm_category') && !$sm->columnExists('xf_rm_category', 'seo'))
        {
            $sm->alterTable('xf_rm_category', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }

        if($sm->tableExists('xf_rm_resource') && !$sm->columnExists('xf_rm_resource', 'seo'))
        {
            $sm->alterTable('xf_rm_resource', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }
    }

    public function upgrade20040Step1()
    {
        $sm = $this->schemaManager();

        if(!$sm->columnExists('xf_attachment_data', 'seo_description'))
        {
            $sm->alterTable('xf_attachment_data', function(Alter $table)
            {
                $table->addColumn('seo_description', 'tinytext')->nullable()->setDefault(null);
            });
        }
    }

    public function upgrade20050Step1()
    {
        $sm = $this->schemaManager();

        if(!$sm->columnExists('xf_thread_prefix', 'seo'))
        {
            $sm->alterTable('xf_thread_prefix', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }
    }


    // Change SEO to NullTable
    public function upgrade20070Step1()
    {
        $sm = $this->schemaManager();

        $sm->alterTable('xf_thread', function(Alter $table)
        {
            $table->changeColumn('seo')->nullable();
        });

        $sm->alterTable('xf_forum', function(Alter $table)
        {
            $table->changeColumn('seo')->nullable();
        });

        $sm->alterTable('xf_category', function(Alter $table)
        {
            $table->changeColumn('seo')->nullable();
        });

        $sm->alterTable('xf_thread_prefix', function(Alter $table)
        {
            $table->changeColumn('seo')->nullable();
        });

        if($sm->tableExists('xf_rm_category') && $sm->columnExists('xf_rm_category', 'seo'))
        {
            $sm->alterTable('xf_rm_category', function(Alter $table)
            {
                $table->changeColumn('seo')->nullable();
            });
        }

        if($sm->tableExists('xf_rm_resource') && $sm->columnExists('xf_rm_resource', 'seo'))
        {
            $sm->alterTable('xf_rm_resource', function(Alter $table)
            {
                $table->changeColumn('seo')->nullable();
            });
        }
    }

    // Muss erneut da ich beim Install dies vergessen....
    public function upgrade20055Step1()
    {
        $sm = $this->schemaManager();

        if(!$sm->columnExists('xf_thread_prefix', 'seo'))
        {
            $sm->alterTable('xf_thread_prefix', function(Alter $table)
            {
                $table->addColumn('seo', 'mediumblob')->nullable();
            });
        }
    }

    public function uninstallStep1()
    {
        $sm = $this->schemaManager();

        $sm->alterTable('xf_attachment_data', function(Alter $table)
        {
            $table->dropColumns('seo_title');
            $table->dropColumns('seo_alt');
        });

        $sm->alterTable('xf_thread', function(Alter $table)
        {
            $table->dropColumns('seo');
        });

        $sm->alterTable('xf_forum', function(Alter $table)
        {
            $table->dropColumns('seo');
        });

        $sm->alterTable('xf_category', function(Alter $table)
        {
            $table->dropColumns('seo');
        });

        $sm->alterTable('xf_thread_prefix', function(Alter $table)
        {
            $table->dropColumns('seo');
        });

        // XF RM
        if($sm->tableExists('xf_rm_category'))
        {
            $sm->alterTable('xf_rm_category', function(Alter $table)
            {
                $table->dropColumns('seo');
            });
        }

        if($sm->tableExists('xf_rm_resource'))
        {
            $sm->alterTable('xf_rm_resource', function(Alter $table)
            {
                $table->dropColumns('seo');
            });
        }
    }
}