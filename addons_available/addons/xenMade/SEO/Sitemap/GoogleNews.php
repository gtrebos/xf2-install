<?php

namespace xenMade\SEO\Sitemap;

use XF\App;
use XF\Mvc\Entity\Finder;
use XF\Util\Xml;

class GoogleNews
{
    /** @var App */
    protected $app;

    protected $file;

    protected $tempFileName;

    protected $sitemapName = 'googlenews.xml';

    const MAX_FILE_ENTRIES = 1000;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function buildSitemap(\XF\Http\Response $response, \XF\Http\Request $request)
    {
        /** @var \XF\Entity\Thread $threads */
        $threads = $this->getThreads();

        $response->contentType('application/xml');

        // Create XML
        $document = $this->createXml();

        $xml_urlset = $document->createElement('urlset');
        $xml_urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $xml_urlset->setAttribute('xmlns:news', 'http://www.google.com/schemas/sitemap-news/0.9');
        $xml_urlset->setAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');



        $lang = \XF::language();

        $langCode = 'en';
        if($lang->language_code)
        {
            $langCode = substr($lang->language_code, 0, 2);
        }

        $imageRepo = $this->app->repository('xenMade\SEO:Image');

        foreach ($threads AS $thread)
        {
            $forum = $thread->Forum;
            $ogImagePath = '';

            $threadUrl = $this->app->router('public')->buildLink('canonical:threads', $thread);

            $urlElement = $document->createElement('url');
            $urlElement->appendChild(Xml::createDomElement($document, 'loc', $threadUrl));

            $newsElement = $document->createElement('news:news');

            $publicationElement = $document->createElement('news:publication');
            $publicationElement->appendChild(Xml::createDomElement($document, 'news:name', $this->app->options()->boardTitle));
            $publicationElement->appendChild(Xml::createDomElement($document, 'news:language', $langCode));

            $newsElement->appendChild($publicationElement);
            $newsElement->appendChild(Xml::createDomElement($document, 'news:genres', \XF\Util\Xml::createDomCdataSection($document, 'blog')));
            $newsElement->appendChild(Xml::createDomElement($document, 'news:publication_date', gmdate(\DateTime::W3C, $thread->post_date)));
            $newsElement->appendChild(Xml::createDomElement($document, 'news:title', \XF\Util\Xml::createDomCdataSection($document, $thread->title)));

            $urlElement->appendChild($newsElement);


            // Image
            $imgElement = $document->createElement('image:image');

            $ogImagePath = $imageRepo->getOgImage($thread, $forum);

            $imgElement->appendChild(Xml::createDomElement($document, 'image:loc', $ogImagePath));


            $urlElement->appendChild($imgElement);
            $xml_urlset->appendChild($urlElement);
        }

        $document->appendChild($xml_urlset);

        print $document->saveXML();
        return $response->sendHeaders();
    }

    /**
     * @return \DOMDocument
     */
    protected function createXml()
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->formatOutput = true;

        return $document;
    }


    protected function saveXML($containerName, \DOMNode $container)
    {
        $dataDir = $this->addOn->getDataDirectory();

        $newDoc = new \DOMDocument('1.0', 'utf-8');
        $newDoc->formatOutput = true;
        $newDoc->appendChild($newDoc->importNode($container, true));
        $xml = $newDoc->saveXML();

        file_put_contents($dataDir . DIRECTORY_SEPARATOR . "$containerName.xml", $xml);
    }

    protected function getThreads()
    {
        $visitor = \XF::visitor();

        $nodes = $this->app->options()->seo_googleNewsNodes;

        $threadFinder = $this->app->finder('XF:Thread');

        $threads = $threadFinder
            ->with(['Forum', 'Forum.Node', 'Forum.Node.Permissions|' . $visitor->permission_combination_id])

            ->where('discussion_state', 'visible')
            ->where('discussion_type', '<>', 'redirect')
            ->where('node_id', array_values($nodes))
            ->where('post_date', '>=', \XF::$time - 60*60*48) //48 Stunden Limit

            ->limit(self::MAX_FILE_ENTRIES)
            ->order('post_date')
            ->fetch();

        return $threads;
    }
}