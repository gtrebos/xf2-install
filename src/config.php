<?php

$host_write = $_ENV['XF_DB_HOST'] ?? '';
$port_write = $_ENV['XF_DB_PORT'] ?? '';
$username_write = $_ENV['XF_DB_USERNAME'] ?? '';
$password_write = $_ENV['XF_DB_PASSWORD'] ?? '';
$dbname_write = $_ENV['XF_DB_DBNAME'] ?? '';

$host_read = $_ENV['XF_DB_HOST_READ'] ?? $host_write;
$port_read = $_ENV['XF_DB_PORT_READ'] ?? $port_write;
$username_read = $_ENV['XF_DB_USERNAME_READ'] ?? $username_write;
$password_read = $_ENV['XF_DB_PASSWORD_READ'] ?? $password_write;
$dbname_read = $_ENV['XF_DB_DBNAME_READ'] ?? $dbname_write;

$config['db']['adapterClass'] = 'XF\Db\Mysqli\ReplicationAdapter';
$config['db']['write'] = [
    'host' => $host_write,
    'port' => $port_write,
    'username' => $username_write,
    'password' => $password_write,
    'dbname' => $dbname_write
];

$config['db']['read'] = [
    'host' => $host_read,
    'port' => $port_read,
    'username' => $username_read,
    'password' => $password_read,
    'dbname' => $dbname_read
];

$config['fullUnicode'] = true;

if(isset($_ENV['DOCKER_ENV']) && $_ENV['DOCKER_ENV'] == 'localhost'){
    $config['debug'] = true;
    $config['development']['enabled'] = true;
}
